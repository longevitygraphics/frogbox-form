<?php

require_once('TestBase.php');
require_once('VonigoUtil.php');

/**
 * Class WorkorderTest
 */
class WorkorderTest extends VonigoTest {

    private $franchise = 2;
    private $zip = 'V6G';

    /**
     * Tests workorder method
     */
    function testWorkorders() {
        $request = $this->co->workorders();
        $this->commonTests($request, 'data/workorders');
    }

    function testWorkorderDetails() {
        $request = $this->co->workorders();
        $wo = array_pop($request->WorkOrders);
        if (!empty($wo->objectID)) {
            $request = $this->co->workorders(array('objectID' => $wo->objectID));
            $this->commonTests($request, 'data/workorders');
        }
    }

    function testCreateWorkorder() {
        $util = new VonigoUtil();

        $client = $util->createClient($this->co);
        if (!empty ($client->Client->objectID)) {
            $clientID = $client->Client->objectID;

            $location = $util->createLocation($this->co, $clientID);
            $locationID = $location->Location->objectID;

            $contact = $util->createContact($this->co, $clientID);
            $contactID = $contact->Contact->objectID;

            $job = $util->createJob($this->co, $clientID);
            print_r($job);
            $jobID = $job->Job->jobID;

            list($lockRequest, $lockObject) = $util->createLock($this->co, $this->franchise, $this->zip);
            $routeOption = $util->getRouteOption($this->co, $lockObject->routeID);
            $deliveryDate = strtotime($lockObject->dayID) + ($lockObject->startTime * 60);

            $workorder = $util->createWorkorder(
                $this->co,
                $jobID,
                $clientID,
                $contactID,
                $lockRequest->Ids->lockID,
                $locationID,
                $routeOption,
                $deliveryDate
            );
            $this->commonTests($workorder, 'data/workorders - create workorder');
        }

        // delete client
        $params = array('method' => 4, 'objectID' => $clientID);
        $this->co->clients($params);

    }

    function testGetWorkorderbyClient() {

        $params = array(
            'method' => 0,
            'clientID' => 47435,
        );

        $request = $this->co->session(2);
        $request = $this->co->workorders($params);
        print_r($request);
        $wo = array_pop($request->WorkOrders);
        if (!empty($wo->objectID)) {
            $request = $this->co->jobs(array('objectID' => $wo->objectID));
            $this->commonTests($request, 'data/workorders');
        }
    }

}

?>
