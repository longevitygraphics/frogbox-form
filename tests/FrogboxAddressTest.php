<?php

require_once('TestBase.php');

/**
 * Class FrogboxAddressTest
 */
class FrogboxAddressTest extends VonigoTest {

    /**
     * Tests FrogboxAddress::countryByFranchise().
     */
    function testCountryByFranchise() {
        for ($a = 0; $a < 30; $a++) {
            $country = FrogboxAddress::countryByFranchise($a);
            if ($a == 16 || $a == 29 || $a == 18) {
                $this->assertEquals($country, 'United States');
            }
            else {
                $this->assertEquals($country, 'Canada');
            }
        }
    }

}

?>
