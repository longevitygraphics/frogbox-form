<?php

require_once('TestBase.php');
require_once('VonigoUtil.php');

/**
 * Class ClientsTest
 */
class ClientsTest extends VonigoTest {

    /**
     * Tests Clients methods
     */
    function testClients() {
        $request = $this->co->clients();
        $this->commonTests($request, 'data/clients');
    }

    function testGetClient() {
        $request = $this->co->clients();
        $client = array_pop($request->Clients);
        if (!empty($client->objectID)) {
            $request = $this->co->clients(array('method' => 1, 'objectID' => $client->objectID));
            $this->commonTests($request, 'data/clients');
        }
    }

    function testCreateClient() {
        $util = new vonigoUtil();
        $request = $util->createClient($this->co);
        print_r($request);
        $this->commonTests($request, 'data/clients');
    }

}

?>
