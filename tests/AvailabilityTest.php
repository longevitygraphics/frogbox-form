<?php

require_once('TestBase.php');
require_once('VonigoUtil.php');


/**
 * Class AvailabilityTest
 */
class AvailabilityTest extends VonigoTest {

    function setGroups(array $groups) {}

    function setRegisterMockObjectsFromTestArgumentsRecursively() {}

    private $zip = '98101';
    private $franchise = 16;


    function testGetLock() {
        // set franchise
        $settings = array(
            'username' => 'fbx.hq',
            'password' => 'testtest',
            'company'  => 'frogbox',
            'base_url' => 'https://frogbox.vonigo.com/api/v1',
        );
        $co = new Vonigo( $settings );

        $util = new VonigoUtil();
        list($request, $object) = $util->createLock($co, $this->franchise, $this->zip);
        print_r($request);


        $this->commonTests($request, 'data/availability - create lock');
        print_r($object);
    }

    function testAvailability() {

        $settings = array(
            'username' => 'fbx.hq',
            'password' => 'testtest',
            'company'  => 'frogbox',
            'base_url' => 'https://frogbox.vonigo.com/api/v1',
        );
        $co = new Vonigo( $settings );

        $time = strtotime( 'June 10 2018' );

        $co->session( $this->franchise );

        $co->setDebug(7);
        $request = $co->routes();


        $params  = array(
            'dateStart'     => $time,
            'dateEnd'       => $time + (86400),
            'serviceTypeID' => 8, // always 8
            'duration'      => 180,
            'zip'           => $this->zip,
        );
        $request = $co->availableTimes( $params );

        print_r($request);

    }

    public function getTests($i) {
        $tests = array(
            array(
                (object) array('dayID' => 20180531, 'routeID' => 1, 'startTime' => 120),
                (object) array('dayID' => 20180531, 'routeID' => 1, 'startTime' => 30),
                (object) array('dayID' => 20180531, 'routeID' => 1, 'startTime' => 60),
                (object) array('dayID' => 20180531, 'routeID' => 1, 'startTime' => 90),
                (object) array('dayID' => 20180531, 'routeID' => 1, 'startTime' => 150),
                (object) array('dayID' => 20180531, 'routeID' => 1, 'startTime' => 210),
                (object) array('dayID' => 20180531, 'routeID' => 1, 'startTime' => 240),
                (object) array('dayID' => 20180531, 'routeID' => 1, 'startTime' => 270),
            ),
            array(
                (object) array('dayID' => 20180531, 'routeID' => 1, 'startTime' => 180),
                (object) array('dayID' => 20180531, 'routeID' => 1, 'startTime' => 30),
                (object) array('dayID' => 20180531, 'routeID' => 1, 'startTime' => 90),
                (object) array('dayID' => 20180531, 'routeID' => 1, 'startTime' => 120),
                (object) array('dayID' => 20180531, 'routeID' => 1, 'startTime' => 150),
                (object) array('dayID' => 20180531, 'routeID' => 1, 'startTime' => 210),
                (object) array('dayID' => 20180531, 'routeID' => 1, 'startTime' => 240),
            ),
        );
        return $tests[$i];
    }

    public function testOptionSequences() {


        $list = new AvailabilityList($this->getTests[0]);
        $options = $list->getBookableOptions(120);
        $this->assertTrue(count($options) == 2);
        $options = $list->getBookableOptions(180);
        $this->assertEmpty($options);

        $list = new AvailabilityList($this->getTests[1]);
        $options = $list->getBookableOptions(120);
        $this->assertTrue(count($options) == 3);
        $options = $list->getBookableOptions(180);
        $this->assertTrue(count($options) == 1);

    }

    public function testFormatOptions() {
        $this->franchise = 2;

        $settings = array(
            'username' => 'fbx.hq',
            'password' => 'testtest',
            'company'  => 'frogbox',
            'base_url' => 'https://frogbox.vonigo.com/api/v1',
        );
        $co = new Vonigo( $settings );

        $time = strtotime( 'Jan 03 2018' );

        $co->session( $this->franchise );

        $routeRequest = $co->routes();
        $routes = array_map(function($object) {
            return $object->routeID;
        }, $routeRequest->Routes);

        $params  = array(
            'dateStart'     => $time,
            'dateEnd'       => $time + (86400),
            'serviceTypeID' => 8, // always 8
            'duration'      => 30,
            'zip'           => $this->zip,
        );
        $request = $co->availableTimes( $params );

        $list = new AvailabilityList(($request->Availability));
        $list->filterOptionsByDate('20180103');

        $bookable = $list->getBookableOptions(180);


        foreach ($bookable as $option) {
            $object = new AvailabilityOption($option);
            echo $object->formatAsOption(180) . PHP_EOL;
        }

    }

}

?>
