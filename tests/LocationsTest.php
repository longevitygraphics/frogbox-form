<?php

require_once('TestBase.php');
require_once('VonigoUtil.php');


/**
 * Class LocationTest
 */
class LocationTest extends VonigoTest {

    function testLocations() {
        $request = $this->co->locations(array('method' => 0));
        $this->commonTests($request, 'data/locations');
    }

    function testGetContact() {
        $request = $this->co->contacts(array('method' => 0));
        $contact = array_pop($request->Contacts);
        if (!empty($contact->objectID)) {
            $request = $this->co->contacts(array('method' => 1, 'objectID' => $contact->objectID));
            $this->commonTests($request, 'data/contacts');
        }
    }


    /**
     * Tests retrieval of a location object by ID (method=1).
     */
    function testGetLocation() {
        $request = $this->co->locations(array('method' => 0));
        $location = array_pop($request->Locations);
        if (!empty($location->objectID)) {
            $params  = array(
                'method'   => 1,
                'objectID' => $location->objectID, // test vancouver location
            );
            $request = $this->co->locations( $params );
            print_r($request);
            $this->commonTests( $request, 'data/locations' );
        }
    }

    /**
     * Tests the creation of a location record associated with a client (method=3).
     */
    function testCreateLocation() {
        $util = new VonigoUtil();

        $client = $util->createClient($this->co);
        if (!empty ($client->Client->objectID)) {
            $clientID = $client->Client->objectID;
            $request = $util->createLocation($this->co, $clientID);
            print_r($request);
            $this->commonTests($request, 'data/locations');
        }

        // delete client
        $params = array('method' => 4, 'objectID' => $clientID);
        $this->co->clients($params);

    }

}

?>
