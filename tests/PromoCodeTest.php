<?php

require_once('TestBase.php');

/**
 * Class LocationTest
 */
class PromoCodeTest extends VonigoTest {

    public function setGroups() {}

    public function setRegisterMockObjectsFromTestArgumentsRecursively() {}

    function testValidatePromo()
    {
        //resources/availability/?method=3
        $zRequest = $this->co->validateZipCode('V6G'); // a Vancouver postal code
        $zone = $zRequest->Ids->zoneID;

        // test valid code
        $request = $this->co->validatePromo('bazinga2015', 8, 1, $zone);
        $this->commonTests($request, 'resources/availability/?method=3');

        // test minimum threshold
        $request = $this->co->validatePromo('YYZ0820', 8, 1, $zone);
        print_r($request);

        // test invalid code for correct vonigo response only
        $request = $this->co->validatePromo('xxxxxxxx', 8, 1, $zone);
        $this->doVonigo($request, -600, 'resources/availability/?method=3');
        $request = $this->co->validatePromo('trevorMcCaw100', 8, 1, $zone);
        $this->commonTests($request, 'resources/availability/?method=3');

    }

}