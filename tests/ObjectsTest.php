<?php

require_once('TestBase.php');
require_once('VonigoUtil.php');

/**
 * Class ObjectsTest
 */
class Objects extends VonigoTest {

    function testObjects() {
        $request = $this->co->objects(); 
        $this->commonTests($request, 'system/objects');

        $request = $this->co->objects(12); // workorder object 
        $this->commonTests($request, 'system/objects');
        $this->assertTrue(!empty($request->Fields), 'workorder fields are missing');
        $this->assertTrue(!empty($request->Options), 'workorder options are missing');
    }

}

?>
