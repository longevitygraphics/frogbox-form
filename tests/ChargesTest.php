<?php

require_once('TestBase.php');
require_once('VonigoUtil.php');

/**
 * Class ChargesTest
 */
class ChargesTest extends VonigoTest {

    private $franchise = 15;
    private $zip = 'L4J';

    /**
     * Tests Charges method
     */
    function testChargeDetails() {
        $request = $this->co->workorders();
        $workorder = array_pop($request->WorkOrders);
        if (!empty($workorder->objectID)) {
            $request = $this->co->charges(array('workorderID' => $workorder->objectID, 'method' => 0));
            $this->commonTests($request, 'data/charges - get details');
        }
    }

	/**
     *
     */
    function testCreateCharge() {
$this->co->session($this->franchise);
        $util = new VonigoUtil();

        $client = $util->createClient($this->co);
        if (!empty ($client->Client->objectID)) {
            $clientID = $client->Client->objectID;

            $location = $util->createLocation($this->co, $clientID);
            $locationID = $location->Location->objectID;

            $contact = $util->createContact($this->co, $clientID);
            $contactID = $contact->Contact->objectID;

            $job = $util->createJob($this->co, $clientID);
            $jobID = $job->Job->jobID;

            list($lockRequest, $lockObject) = $util->createLock($this->co, $this->franchise, $this->zip);
            $routeOption = $util->getRouteOption($this->co, $lockObject->routeID);
            $deliveryDate = strtotime($lockObject->dayID) + ($lockObject->startTime * 60);

            $workorder = $util->createWorkorder(
                $this->co,
                $jobID,
                $clientID,
                $contactID,
                $lockRequest->Ids->lockID,
                $locationID,
                $routeOption,
                $deliveryDate
            );

            // get the free box rental priceItem for this zone
            $zvRequest = $this->co->validateZipCode($this->zip);
            foreach ($zvRequest->ServiceTypes as $serviceType) {
                if ($serviceType->typeID == 8) { // frogbox
                    $priceListID = $serviceType->priceID;
                    break;
                }
            }
            // get priceblocks for this pricelist
            $priceBlocks = $this->co->priceBlocks($priceListID);
            foreach ($priceBlocks->PriceBlocks as $priceBlock) {
                if (stripos($priceBlock->priceBlock, '5 weeks') !== false && Vonigo::isTrue($priceBlock->isActive)) {
                    $pBlock = $priceBlock;
                    break;
                }
            }
            // get items for this priceblock
            $priceItems = $this->co->priceItems($pBlock->priceListID, $pBlock->priceBlockID);
            foreach ($priceItems->PriceItems as $priceItem) {
                if (stripos($priceItem->priceItem, '2 bedroom min') !== false) {
                    $pItem = $priceItem;
                    break;
                }
            }


            if (!empty($pItem)) {
                $charges = array(
                    array(
                        'chargeID'    => 0, // always 0
                        'priceItemID' => $pItem->priceItemID,
                        'sequence'    => $pItem->sequence,
                        'Fields'      => array(
                            (object) array(
                                'fieldID'    => 9290,
                                'fieldValue' => $pItem->priceItem
                            ),
                            (object) array(
                                'fieldID'    => 9289,
                                'fieldValue' => 'Test order - box rental'
                            ),
                            (object) array(
                                'fieldID'    => 9288,
                                'fieldValue' => 1
                            ), // quantity
                            (object) array(
                                'fieldID'    => 9287,
                                'fieldValue' => $pItem->value,
                            ), // unit price
                        ),
                    ),
                );
                $this->co->setDebug(3);
                $charge = $util->createCharge( $this->co, $workorder->WorkOrder->objectID, $charges );
                $this->commonTests( $charge, 'data/charges - create charge' );
                $request = $this->co->charges( array(
                        'workorderID' => $workorder->WorkOrder->objectID,
                        'method'      => 0
                    ) );
                print_r( $request );
            }
            else {
                $this->assertTrue(false, 'could not find price item');
            }
            // now list the charges on the object
        }

    }

}

?>
