<?php

require_once('TestBase.php');
require_once('VonigoUtil.php');

/**
 * Class ContactsTest
 */
class ContactsTest extends VonigoTest {

    /**
     * Tests Contacts methods
     */
    function testContacts() {
        $request = $this->co->contacts(array('method' => 0));
        $this->commonTests($request, 'data/contacts');
    }

    function testGetContact() {
        $request = $this->co->contacts(array('method' => 0));
        $contact = array_pop($request->Contacts);
        if (!empty($contact->objectID)) {
            $request = $this->co->contacts(array('method' => 1, 'objectID' => $contact->objectID));
            $this->commonTests($request, 'data/contacts');
        }
    }

    function testCreateContact() {
        $util = new vonigoUtil();
        $client = $util->createClient($this->co);
        if (!empty ($client->Client->objectID)) {
            $clientID = $client->Client->objectID;
            $request = $util->createContact($this->co, $clientID);
            print_r($request);
            $this->commonTests($request, 'data/contacts');
        }
    }

}

?>
