<?php

namespace Frogbox\Tests;

use Frogbox\FF_Router;
use \PHPUnit_Framework_TestResult;

class FF_RouterTests extends \PHPUnit_Framework_TestCase {

    public function __construct() {
        ini_set('display_errors', 'on');
    }

    public function run(PHPUnit_Framework_TestResult $result = NULL)
    {
        $this->setPreserveGlobalState(false);
        return parent::run($result);
    }

	public function testNoBootstrap() {
		$this->assertTrue(true);
	}

/**
 * @preserveGlobalState disabled
 */
	public function testGetDisabledDates() {
		$zip = 'M4X';
		$result = FF_Router::getDisabledDates($zip);
$this->assertNotEmpty($result);
		print_r($result);
	}

	public function testGetPricelist() {
		$zip = 'M4X';
		$result = FF_Router::getPriceList($zip, 7);
		print_r($result);
	}

	public function testValidateZipJSON() {
		$zip = 'M4X';
		$result = FF_Router::validateZipJSON($zip);
		print_r($result);
	}

}
