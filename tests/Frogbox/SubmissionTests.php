<?php

namespace Frogbox\Test;

use Frogbox\Submission;
use \PHPUnit_Framework_TestCase;

require_once(__DIR__ . '/../TestBase.php');

//require_once(__DIR__ . '/../../vonigo-api/Vonigo.php');
//
//require_once(__DIR__ . '/../../util/util.inc');
//require_once(__DIR__ . '/../../frogbox-api.inc');
//
//define('BASE_PATH', find_wordpress_base_path()."/");
//define('WP_USE_THEMES', false);
//global $wp, $wp_query, $wp_the_query, $wp_rewrite, $wp_did_header, $wpdb;
//require(BASE_PATH . 'wp-load.php');
//require_wp_db();
//
//error_reporting(E_ALL ^ E_STRICT);

class SubmissionTests extends PHPUnit_Framework_TestCase
{

    public function testLookupRouteOption() {


        $reflectionClass = new \ReflectionClass(Submission::class);
        $reflectionMethod = $reflectionClass->getMethod('lookupRouteOption');
        $reflectionMethod->setAccessible(true);

        // the optionID for route 62 in vancouver (2) is 10197
        $mock = $this->loadMockSubmission(2, '62-900');
        $option = $reflectionMethod->invoke($mock, 'delivery');
        $this->assertEquals(10197, $option);

        // there is no optionID 100 in vancouver(2)
        $mock = $this->loadMockSubmission(2, '100-900');
        $option = $reflectionMethod->invoke($mock, 'delivery');
        $this->assertFalse($option);

        // there is no optionID 62 in toronto (15)
        $mock = $this->loadMockSubmission(15, '62-900');
        $option = $reflectionMethod->invoke($mock, 'delivery');
        $this->assertFalse($option);
    }

    private function loadMockSubmission($franchiseID, $routeTime) {
        $reflectionClass = new \ReflectionClass(Submission::class);
        $mock = $this->createMock(Submission::class);

        $reflectionProperty = $reflectionClass->getProperty('submission');
        $reflectionProperty->setAccessible(true);

        $submission = (object) ['post' =>
            [
                'delivery-time' => $routeTime,
                'delivery-city' => $franchiseID,
            ]
        ];

        $reflectionProperty->setValue($mock, $submission);

        return $mock;

    }

}