<?php

namespace Frogbox\Test;

use Frogbox\CAAMembership;
use \PHPUnit_Framework_TestCase;

class CAAMembershipTests extends PHPUnit_Framework_TestCase
{

    public function testCAAMembership() {

        $valid = 6202822082445008;
        $invalid = 12345;
        $invalid16 = 6202822082445009;

        $membership = new CAAMembership($valid);
        $this->assertTrue($membership->isValid());

        $membership = new CAAMembership($invalid);
        $this->assertFalse($membership->isValid());

        $membership = new CAAMembership($invalid16);
        $this->assertFalse($membership->isValid());

    }

}