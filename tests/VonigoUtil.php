<?php

define('SIX_MONTHS', 15552000);

class VonigoUtil {

	public function createClient($co) {
		$fields = array(
			array(
				'fieldID'    => 112, // phone
				'fieldValue' => '1234567890',
				'optionID'   => 0,
			),
			array(
				'fieldID'    => 121,
				'fieldValue' => '', // type
				'optionID'   => 59, // residential
			),
			array(
				'fieldID'    => 122,
				'fieldValue' => '', // status
				'optionID'   => 61, // active
			),
			array(
				'fieldID'    => 123,
				'fieldValue' => '', // stage
				'optionID'   => 64, //lead
			),
			array(
				'fieldID'    => 126, // name
				'fieldValue' => 'test, test', // other
				'optionID'   => 0,
			),
			array(
				'fieldID'    => 246, // m or f
				'fieldValue' => '',
				'optionID'   => 286, // female
			),
			array(
				'fieldID'    => 795, // marketing campaign
				'fieldValue' => '',
				'optionID'   => 10406, // Other
			),
		);
		$params = array( 'method' => 3 );

		return $co->clients( $params, $fields );
	}

	public function createContact($co, $clientID) {
		$fields = array(
			array(
				'fieldID'    => 128, // last name
				'fieldValue' => 'Test',
				'optionID'   => 0,
			),
			array(
				'fieldID'    => 125, // status
				'fieldValue' => '', //
				'optionID'   => 71, // active
			),
		);
		$params = array('method' => 3, 'clientID' => $clientID);
		$request = $co->contacts( $params, $fields );
		return $request;
	}

	public function createLocation($co, $clientID) {
		$fields = array(
			array(
				'fieldID' => 773,
				'fieldValue' => '123 Main street'
			), // street address
			array(
				'fieldID' => 776,
				'fieldValue' => 'Vancouver'), // city
			array(
				'fieldID' => 9312,
				'optionID' => 9886), // province code - british columbia
			array(
				'fieldID' => 775,
				'fieldValue' => 'V6G 1A1'), // postal code
			array(
				'fieldID' => 779,
				'optionID' => 9907), // country - canada
			array(
				'fieldID' => 9713,
				'fieldValue' => 'test location'), // description
			array(
				'fieldID' => 9714,
				'fieldValue' => 'test location'), // ttile
		);

		$params = array('method' => 3, 'clientID' => $clientID);
		return $co->locations($params, $fields);
	}

	public function createJob($co, $clientID) {
		$fields = array(
			array(
				'fieldID' => 978, // summary
				'fieldValue' => 'Test job.'
			),
			array(
				'fieldID' => 982, // service type
				'optionID' => 10006 // box rental
			),
		);
		$params = array('method' => 3, 'clientID' => $clientID);
		return $co->jobs($params, $fields);
	}

	public function createLock($co, $franchise, $zip) {
		$co->session($franchise);

		$request = $this->listAvailability($co, $zip);

		$object = array_pop($request->Availability);
		$datestamp = strtotime($object->dayID) + ($object->startTime * 60);

		// get zoneID from zip code
		$zipRequest = $co->validateZipCode($zip);
		$zoneID = $zipRequest->Ids->zoneID;

		$params = array(
			'serviceTypeID' => 8, // always 8 with frogbox
			'dateStart' => $datestamp,
			'startTime' => $object->startTime,
			'duration' => 180,
			'routeID' => $object->routeID,
			'zoneID' => $zoneID,
		);
		$request = $co->lockTimes($params);

		return array($request, $object);
	}

	/**
	 * @param $co - Vonigo connection object
	 * @param $deliveryDate -
	 */
	public function createWorkorder($co, $jobID, $clientID, $contactID, $lockID, $locationID, $routeOption, $deliveryDate) {

		$fields = array(
			array('fieldID' => 9949, 'optionID' => $routeOption), // route
			array('fieldID' => 200, 'fieldValue' => 'test workorder'), // summary
			array('fieldID' => 201, 'optionID' => 243), // label 243 = delivery; 248 pickup; 245 if/delivery; 247 if/pickup
			array('fieldID' => 187, 'fieldValue' => 10), // subtotal
			array('fieldID' => 181, 'optionID' => 161), // status = Open; 161 = booked today
			array('fieldID' => 184, 'fieldValue' => $locationID), // service location
			array('fieldID' => 185, 'fieldValue' => $deliveryDate), // scheduled on (unix timestamp)
			array('fieldID' => 186, 'fieldValue' => 180), // duration
			array('fieldID' => 216, 'fieldValue' => $clientID), // client
		);

		$params = array(
			'method' => 3,
			'jobID' => $jobID,
			'clientID' => $clientID,
			'contactID' => $contactID,
			'locationID' => $locationID,
			'lockID' => $lockID,
			'serviceTypeID' => 8, // always 8 for Frogbox
		);
		return $co->workorders($params, $fields);
	}

	public function createCharge($co, $workorderID, $charges) {
	    // charges params
		$params = array(
			'objectID' => $workorderID,
			'objectTypeID' => 12, // workorder
			'method' => 2,
			'Charges' => $charges,
		);
		return $co->charges($params);
	}

	public function getRouteOption($co, $routeID) {
		$routes = $co->routes();
		if (empty($routes->Routes)) {
			return false;
		}
		foreach ($routes->Routes as $route) {
			if ($route->routeID == $routeID) {
				return $route->optionID;
			}
		}
	}

	public function listAvailability($co, $zip) {
		$time = time() + SIX_MONTHS;

		$params = array(
			'dateStart' => $time,
			'dateEnd' => $time + 86400, // plus one day
			'serviceTypeID' => 8, // always 8
			'duration' => 180,
			'zip' => $zip,
		);
		return $co->availableTimes($params);
	}

}
