<?php

require_once('TestBase.php');

class InfusionsoftTest extends VonigoTest {

	public function testInfusionsoftConnection() {


		$contact = new Infusionsoft_Contact();
		$contact->addCustomField('_CAA');
		$contact->addCustomField('_JobID');

		$results = Infusionsoft_DataService::query($contact, array('ID' => 151536), 2);
		$this->assertEquals(get_class($results[0]), 'Infusionsoft_Contact');
		print_r($results);
	}
    
}