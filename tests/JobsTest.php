<?php

require_once('TestBase.php');
require_once('VonigoUtil.php');

/**
 * Class WorkorderTest
 */
class JobsTest extends VonigoTest {

    /**
     * Tests Jobs method
     */
    function testListJobs() {
        $request = $this->co->jobs();
        $this->commonTests($request, 'data/jobs');
    }

    function testListCancelledJobs() {
        $params = array('method' => -3);
        $request = $this->co->jobs($params);
print_r($request);
        $this->commonTests($request, 'data/jobs');
        $this->assertTrue(!empty($request->Jobs), 'no cancelled jobs were returned');
    }

    function testJobDetails() {
        $request = $this->co->jobs();
        $job = array_pop($request->Jobs);
        if (!empty($job->objectID)) {
            $request = $this->co->jobs(array('method' => 1, 'objectID' => $job->objectID));
            $this->commonTests($request, 'data/jobs');
        }
    }

    function testUpdateJob() {
        $scoreOption = 10121;
        $commentValue = 'This is a test. It is only a test.';

        $request = $this->co->jobs();
        $job = array_pop($request->Jobs);
        if (!empty($job->objectID)) {

            // update the job
            $fields = array(
                array(
                    'fieldID' => 1090, // NPS score
    				'fieldValue' => '',
	    			'optionID'   => $scoreOption,
                ),
                array(
                    'fieldID' => 1089, // NPS Main comments
                    'fieldValue' => $commentValue, // other
				    'optionID'   => 0,
                )

            );
            $request = $this->co->jobs(array('method' => 2, 'objectID' => $job->objectID), $fields);
            $this->commonTests($request, 'data/jobs');

            // get details of the updated job
            $request = $this->co->jobs(array('method' => 1, 'objectID' => $job->objectID));
            $fieldTest = 0;
            foreach ($request->Fields as $field) {
                // test the nps score field
                if ($field->fieldID == 1090) {
                    $this->assertEquals($scoreOption, $field->optionID);
                    $fieldTest++;
                }

                // test the nps comments field
                if ($field->fieldID == 1089) {
                    $this->assertEquals($scoreOption, $field->fieldValue);
                    $fieldTest++;
                }
            }
            // test that we tested the right number of fields
            $this->assertEquals($fieldTest, 2, 'did not update job');
        }
    }

    function testCreateJob() {
        $util = new vonigoUtil();
        $client = $util->createClient($this->co);
        if (!empty ($client->Client->objectID)) {
            $clientID = $client->Client->objectID;
            $job = $util->createJob($this->co, $clientID);
            $this->commonTests($job, 'data/jobs');
        }
    }

    function testCreateJobWithCAA() {
        $util = new vonigoUtil();
        $client = $util->createClient($this->co);
        if (!empty ($client->Client->objectID)) {
            $clientID = $client->Client->objectID;

            $fields = array(
                array(
                    'fieldID' => 978, // summary
                    'fieldValue' => 'Test job.'
                ),
                array(
                    'fieldID' => 982, // service type
                    'optionID' => 10006 // box rental
                ),
                array(
                    'fieldID' => 10118,
                    'fieldValue' => '6202826125911009',
                ),
            );
            $params = array('method' => 3, 'clientID' => $clientID);
            $this->co->setDebug(3);
            $job = $this->co->jobs($params, $fields);
            print_r($job);

            $this->commonTests($job, 'data/jobs');
        }
        else {
            $this->assertTrue(false, 'could not create client');
        }
    }

}

?>
