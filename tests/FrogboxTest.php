<?php

    require_once('TestBase.php');

  /**
   * Class FrogboxTest
   *
   * Test Vonigo API methods needed for Vonigo form for:
   *
   * 1. Valid HTTP response
   * 2. No Vonigo error
   * 3. Valid JSON data in response
   * 4. Expected data in response for each method
   */
  class FrogboxTest extends VonigoTest {

      public  function setGroups() {}
      public  function setRegisterMockObjectsFromTestArgumentsRecursively() {}

      private $clientID = 32088;
      private $priceListID = 387;
      private $priceBlockID = 627;
      private $deliveryLocation = 56429;
      private $pickupLocation = 56429;

      private $contactID = 86427;

      function testServiceCharge() {
        $zip = 98035;
        $co = $this->connect();
        $request = $co->validateZipCode($zip);
        $zone = $request->Ids->zoneID;
        $charge = _ff_service_charge($zone);
        $this->assertTrue($charge == 15);
      }

      function testRentalPeriods() {
          $weeks = FF_Router::getNearestRentalPeriod(6);
          $this->assertTrue($weeks == 1);

          $weeks = FF_Router::getNearestRentalPeriod(9);
          $this->assertTrue($weeks == 1);

          $weeks = FF_Router::getNearestRentalPeriod(13);
          $this->assertTrue($weeks == 2);

          $weeks = FF_Router::getNearestRentalPeriod(46);
          $this->assertTrue($weeks == 7);
      }

    function testAuth() {
      $co = $this->connect();
      $auth = $co->authenticate();
      $this->assertTrue($auth, 'could not authenticate');
    }

    function testSession() {
      //security/session
      $co = $this->connect();
      $request = $co->session(2);
      $this->commonTests($request, 'security/session');
      $this->assertTrue(is_string($request->securityToken));
            }

    function testFranchises() {
      //security/franchises
      $co = $this->connect();
//      $o = $co->getDeliveryChargeItem('V6G');

      $request = $co->franchises();
      $this->commonTests($request, 'security/franchises');
      $this->assertTrue(is_array($request->Franchises));
    }

//      function testCreateClient() {
//          $co = $this->connect();
//
//          // create a client
//          $params = array('method' => 3);
//          $clientData = array(
//              'contact-email' => 'gurpreet@longevitygraphics.com',
//              'contact-first' => 'simple',
//              'contact-last' => 'test',
//              'contact-primary-phone' => '5034427029',
//              'contact-alternate-phone' => '5034427029',
//          );
//          $fields = _ff_formData_to_vClient($clientData);
//          $request = $co->clients($params, $fields);
//          $this->commonTests($request, 'security/franchises');
//
//          $this->clientID = $request->Client->objectID;
//      }


    function testAvailableMonth() {
      $co = $this->connect();
      $co->session(30); // Calgary
      $time = strtotime('9/8/2015');
      $request = $co->availableTimes(array(
        'dateStart' => $time,
        'dateEnd' => $time + (86400 * 180),
        'serviceTypeID' => 8,
        'duration' => 180,
        'zip' => 'V6G',
      ));
      $this->commonTests($request, 'resources/availability/?method=0 - by month');
      $this->assertTrue(is_array($request->Availability));
    }

    function testAvailableTimes() {
      //resources/availability/?method=0
      $co = $this->connect();
      $co->session(30); // Calgary
      $time = strtotime('9/8/2015');
      $request = $co->availableTimes(array(
        'dateStart' => $time,
        'dateEnd' => $time + 86400,
        'serviceTypeID' => 8,
        'duration' => 180,
        'zip' => 'V6G',
      ));
      $this->commonTests($request, 'resources/availability/?method=0');
      $this->assertTrue(is_array($request->Availability));
    }

    function testValidateZipCode() {
      //resources/availability/?method=1
      $co = $this->connect();
      $request = $co->validateZipCode('V7G'); // vancouver
//      $request = $co->validateZipCode('T2B'); // calgary
      $this->commonTests($request, 'resources/availability/?method=1');
      $this->assertTrue(is_string($request->Ids->zoneID));
    }

    /**
     * Tests time locking. Request some times, lock one, then request again.
     */
    function testLockTimes() {
      // Request some times. 
      $co = $this->connect();

      $zR = $co->validateZipCode('V6G');
      $franchise = $zR->Ids->franchiseID;
      $zoneID = $zR->Ids->zoneID;

      $co->session($franchise); 
      $time = strtotime('09/01/2016');
      $params = array(
        'dateStart' => $time,
        'dateEnd' => $time + 86400,
        'serviceTypeID' => 8,
        'duration' => 120,
        'zip' => 'V6G',
      );	
      $request = $co->availableTimes($params);
      
      $this->commonTests($request, 'resources/availability/?method=0');
      $this->assertTrue(is_array($request->Availability));

      $searching = true;
      $count = 10;
      while ($searching) {
        $rand = array_rand($request->Availability);
        $routeID = $request->Availability[$rand]->routeID;
        $lockedHash = md5(json_encode($request->Availability[$rand]));
        $startTime = $request->Availability[$rand]->startTime;
      
        $timeslot = $request->Availability[$rand];
        if ($request->Availability[$rand]->dayID == '20151123') {
          $searching = false;
        }
        $count = $count -1;
        if (!$count) { $searching = false; }
      }

      // lock a time
      $lockParams = array(
        'serviceTypeID' => 8, // service type id is always 8 for frogbox
        'dateStart' => $time, //
        'startTime' => $startTime, 
        'duration' => 120, // 2 hrs
        'zoneID' => $zoneID, // A vancouver zone id
        'routeID' => $routeID, // route 97
      );
      $request = $co->lockTimes($lockParams);
      $this->commonTests($request, 'resources/availability/?method=2');
      $this->assertEquals(get_class($request->Ids), 'stdClass');

      $request = $co->availableTimes($params);
      $avail = array();
      foreach ($request->Availability as $obj) {
        if ($obj->dayID == '20151123') {
          $avail[] = $obj;
        }
      }
      $request->Availability = $avail;
      foreach ($request->Availability as $option) {
        if (md5(json_encode($option)) === $lockedHash) {
          $this->assertTrue(false, 'Locked timeslot ' . $routeID . '-' . $startTime . ' is still available');
        }
      }
    }


    function testPriceList() {
      //resources/pricelists
      $co = $this->connect();
      for ($f = 2; $f < 30; $f++) {
          $co->session( $f ); // set session first
          $request = $co->pricelists();
          $this->commonTests( $request, 'resources/pricelists' );
          $this->assertTrue( is_array( $request->PriceLists ) );
      }
    }


    function testPriceBlocks() {
      //resources/priceBlocks
      $co = $this->connect();
      $co->session(2); // seattle 16
//      $request = $co->priceBlocks($this->priceBlockID);
      $request = $co->priceBlocks(386);
      $this->commonTests($request, 'resources/priceBlocks');
      $this->assertTrue(is_array($request->PriceBlocks), 'array');
    }

    function testPriceItems() {
      //resources/priceItems
      $co = $this->connect();
      $co->session(2); // vancouver
      $request = $co->priceItems(386, 627); // pricelist, 1 week rental
      //      $request = $co->priceItems($this->priceListID, $this->priceBlockID); // pricelist, 1 week rental
      $this->commonTests($request, 'resources/priceItems');
      $this->assertTrue(is_array($request->PriceItems));
    }
    
    function testPriceListByZip() {
      $zip = 'l6h';
      $co = $this->connect();
      $request = $co->validateZipCode($zip);
      $franchise = $request->Ids->franchiseID;
      $co->session($franchise);
      foreach ($request->ServiceTypes as $serviceType) {
    	  if ($serviceType->typeID == 8) {
    		  $priceListID = $serviceType->priceID;
    				break;
    		}
      }
      $request = $co->priceBlocks($priceListID); 
      foreach ($request->PriceBlocks as $pblock) {
        if ($pblock->isActive == 1) {
          if ($pblock->priceBlock == 'Products') {
            $plRequest = $co->priceItems($pblock->priceListID, $pblock->priceBlockID); 
          }
        }
      }      
    }

    function testTaxes() {
      //resources/taxes
      $co = $this->connect();
      // switch to franchise
      $co->session(2); // vancouver
      $request = $co->taxes('V6G'); // vancouver
      $this->commonTests($request, 'resources/taxes'); 
      $this->assertTrue(is_array($request->Taxes));
    }

    function testClientLookup() {
      $co = $this->connect();
      $request = $co->session(2); // vancouver
      // client lookup request
      $params = array('searchPar' => 'gurpreet@longevitygraphics.com');
      $request = $co->clients($params);
      $this->commonTests($request, 'data/clients');
      // download full record
      $rRequest = $co->clients(array('method'=> 1, 'objectID' => $request->Clients[0]->objectID));
      $this->commonTests($rRequest, 'data/clients?ID=');
    }

    // test looking up a contact
    function testLookupContact() {
      $co = $this->connect();
      $request = $co->session(2); // vancouver

      // there should be results for this request
      $params = array(
        'method' => 0,
        'searchPar' => 'Barney Rubble',
        'clientID' => $this->clientID,
        'pageSize' => 100,
      );
      $request = $co->contacts(null, $params);
      $this->commonTests($request, 'data/contacts/?method=0');
      $this->assertTrue(is_array($request->Contacts), 'data/contacts/?method=0');

      // no results
      $params['searchPar'] = 'Snagglepuss'; // only change search parameter
      $request = $co->contacts(null, $params);
      $this->commonTests($request, 'data/contacts/?method=0');
      $this->assertNull($request->Contacts, 'data/contacts/?method=0');
    }

    // test creating a contact 
    function testCreateContact() {
      $co = $this->connect();
      $request = $co->session(2); // vancouver

      // create a contact
      $params = array(
        'method' => 3,
        'clientID' => $this->clientID,
      );
      $firstname = (object) array('fieldID' => 127, 'fieldValue' => 'Barney');
      $lastname = (object) array('fieldID' => 128, 'fieldValue' => 'Rubble');
        $status = (object) array('fieldID' => 125, 'optionID' => 71);

        $fields = array($firstname, $lastname, $status);
        $co->setDebug(3);
      $request = $co->contacts($params, $fields);
        $co->setDebug(false);
      $this->commonTests($request, 'data/contacts?method=3');

      // delete the contact
      if (!empty($request->Contact->objectID)) {
        $params = array('method' => 4, 'objectID' => $request->Contact->objectID);
        $request = $co->contacts(null, $params);
        $this->commonTests($request, 'data/contacts?method=4');
      }
    }


    function testServiceTypes() {
      $co = $this->connect();
      $request = $co->serviceTypes();
      $this->commonTests($request, 'data/servicetypes');
    }

    function testJobs() {
      $co = $this->connect();
      $request = $co->session(2); // vancouver
      $params = array(
        'method' => 3,
        'clientID' => $this->clientID,
      );
      $fields = array(
        (object) array('fieldID' => 978, 'fieldValue' => 'test job'), // summary
        (object) array('fieldID' => 982, 'optionID' => 10006), // service type = box rental
        (object) array('fieldID' => 984, 'optionID' => 9943), // status = open
        (object) array('fieldID' => 968, 'fieldValue' => 'zipcar10'), // promo code
      );
      $request = $co->jobs($params, $fields);
      $jobID = $request->Job->jobID;
      $this->commonTests($request, 'data/jobs');

      $deliveryTimestamp = strtotime('09/01/2016');
      $pickupTimestamp   = strtotime('09/08/2016');
      
      // get locks
      $params = array(
        'serviceTypeID' => 8, // service type id is always 8 for frogbox
        'dateStart' => $deliveryTimestamp, // sep 19 2015 
        'startTime' => 420, // 7 am
        'duration' => 180, // 3 hrs
        'zoneID' => 3, // vancouver
        'routeID' => 98, // route 98
      );
      $request = $co->lockTimes($params);

      $this->commonTests($request, 'resources/availability/?method=2');
      $this->assertEquals(get_class($request->Ids), 'stdClass');
      $deliveryLock = $request->Ids->lockID;
      
      $params = array(
        'serviceTypeID' => 8, // service type id is always 8 for frogbox
        'dateStart' => $pickupTimestamp, // sep 26 2015
        'startTime' => 840, // 2pm
        'duration' => 180, // 3 hrs
        'zoneID' => 3, // vancouver
        'routeID' => 98, // route 98
      );
      $request = $co->lockTimes($params);
      $this->commonTests($request, 'resources/availability/?method=2');
      $this->assertEquals(get_class($request->Ids), 'stdClass');
      $pickupLock = $request->Ids->lockID;

      // workorder request starts here
      $params = array(
        'method' => 3, 
        'jobID' => $jobID,
        'clientID' => $this->clientID,
        'contactID' => $this->contactID,
        'locationID' => $this->deliveryLocation, 
        'lockID' => $deliveryLock,
        'serviceTypeID' => 8, // always 8
      );

      $routeOption = $co->getRouteOption(2, 98);
      $deliveryDate = $deliveryTimestamp + (420 * 60);
      
      $fields = array(
        (object) array('fieldID' => 9949, 'optionID' => $routeOption), // route
        (object) array('fieldID' => 200, 'fieldValue' => 'new job via testing suite!'), // summary
        (object) array('fieldID' => 201, 'optionID' => 243), // label 243 = delivery; 248 pickup; 245 if/delivery; 247 if/pickup
        (object) array('fieldID' => 187, 'fieldValue' => 159), // subtotal
        (object) array('fieldID' => 181, 'optionID' => 161), // status 160 = Open; 161 = booked today
        (object) array('fieldID' => 184, 'fieldValue' => $this->deliveryLocation), // service location
        (object) array('fieldID' => 185, 'fieldValue' => $deliveryDate), // scheduled on 
        (object) array('fieldID' => 186, 'fieldValue' => 180), // duration
        (object) array('fieldID' => 216, 'fieldValue' => $this->clientID), // client
      );
      
      // build the charges
      $charges = array();

      // get the taxes for the delivery workorder
      $charges[] = array(
        'chargeID' => 0,
        'priceItemID' => 23850,
        'sequence' => 1,
        'Fields' => array(
  	      (object) array('fieldID' => 9290, 'fieldValue' => "1 BEDROOM Minimalist BUNDLE: 20 FROGBOXES + 5 SMALL FROGBOXES"),
	        (object) array('fieldID' => 9289, 'fieldValue' => 'Online order - box rental'),
	        (object) array('fieldID' => 9288, 'fieldValue' => 1), // quantity
	        (object) array('fieldID' => 9287, 'fieldValue' => 159), // unit price
	        (object) array('fieldID' => 9286, 'fieldValue' => 143.1), // subtotal
        	(object) array('fieldID' => 9283, 'fieldValue' => 160.28), // total
        ),
      );
      $charges[] = array(
        'chargeID' => 0,
        'priceItemID' => 24079,
        'taxID' => 2,
        'sequence' => 1,
        'Fields' => array(
          (object) array('fieldID' => 9290, 'fieldValue' => 'Charge For Job'),
          (object) array('fieldID' => 9290, 'fieldValue' => 'Description of the charge'),
          (object) array('fieldID' => 9290, 'fieldValue' => '1'),
          (object) array('fieldID' => 9290, 'fieldValue' => '12.34'),
        ),
      );
  //    $params['Charges'] = $charges;
      
      // delivery
      $request = $co->workorders($params, $fields);

      $this->commonTests($request, 'resources/workorders/?method=3');
      $woID = $request->WorkOrder->objectID;
      $deliveryWoID = $woID;

      // pickup
      $pickupDate = $pickupTimestamp + (840 * 60);

      $params['lockID'] = $pickupLock;
      $params['locationID'] = $this->pickupLocation;
      unset($params['Charges']);
      
      $fields = array(
        (object) array('fieldID' => 9949, 'optionID' => $routeOption), // route
        (object) array('fieldID' => 200, 'fieldValue' => 'new job via testing suite!'), // summary
        (object) array('fieldID' => 201, 'optionID' => 248), // label 243 = delivery; 248 pickup; 245 if/delivery; 247 if/pickup
        (object) array('fieldID' => 187, 'fieldValue' => 159), // subtotal
        (object) array('fieldID' => 181, 'optionID' => 161), // status 160 = Open; 161 = booked today
        (object) array('fieldID' => 184, 'fieldValue' => $this->pickupLocation), // service location
        (object) array('fieldID' => 185, 'fieldValue' => $pickupDate), // scheduled on 
        (object) array('fieldID' => 186, 'fieldValue' => 180), // duration
        (object) array('fieldID' => 216, 'fieldValue' => $this->clientID), // client
      );      
      
      $request = $co->workorders($params, $fields);
      $this->commonTests($request, 'resources/workorders/?method=3');

      // add charges after
      $params = array(
        'objectID' => $deliveryWoID,
        'objectTypeID' => 12, // workorder
        'method' => 2,
        'Charges' => $charges,
      );
      $chRequest = $co->charges($params);
    }



    function testRoutes() {
      $co = $this->connect();
      $request = $co->session(2); // vancouver
      $request = $co->routes();
      $this->commonTests($request, 'resources/routes');
    }
    
    function testObjects() {
      $co = $this->connect();
      $request = $co->session(2); // vancouver 
      // $params = array('field' => 795); 
      // $request = $co->objects(); 
      // all objects 
      // $request = $co->objects(null, $params); 
      // client object 
      // $request = $co->objects(7); // client object 
      // $request = $co->objects(8); // contact object
      // $request = $co->objects(10); // job object 
      // $request = $co->objects(12); // workorder object 
      // $request = $co->objects(20); // locations object 
      $request = $co->objects(57); // charges object

      $this->commonTests($request, 'system/objects');
    }

/* 
// offices functionality not implemented
    function testOffices() {
      $co = $this->connect();
      $request = $co->offices();
      $this->commonTests($request, 'security/offices');
    }
*/

    /**
     * Tests the assignment of a client to a second franchise. Necessary for
     * inter-franchise jobs. 
     */
    function testAssignClient() {
      $co = $this->connect();

      // set franchise to vancouver
      $request = $co->session(2); 
      
      // create a client
      $params = array('method' => 3);
      $clientData = array(
        'contact-email' => 'gurpreet@longevitygraphics.com',
        'contact-first' => 'simple',
        'contact-last' => 'test',
        'contact-primary-phone' => '5034427029',
        'contact-alternate-phone' => '5034427029',
      );
      $fields = _ff_formData_to_vClient($clientData);
      $request = $co->clients($params, $fields);
      $clientID = $request->Client->objectID;

      // verify that the client is available in the first franchise 
      $request = $co->clients(array('method' => 1, 'objectID' => $clientID));
      $this->assertEquals(get_class($request->Client), 'stdClass', 'client inaccessible in first franchise');

      // set franchise to torontoxxw (15) or okanagan (23)
      $request = $co->session(23); 

      // assign client to second franchise
      $params = array('method' => 7, 'objectID' => $clientID);
      $request = $co->clients($params);
      $this->commonTests($request, 'data/clients/?method=7 (assign client to franchise)');

      // test that we can retrieve the client in the second franchise
      $request = $co->clients(array('method' => 1, 'objectID' => $clientID));
      $this->assertEquals(get_class($request->Client), 'stdClass', 'client inaccessible in second franchise');
      
      // delete the client
      $params = array('method' => 4, 'objectID' => $clientID);
      $request = $co->clients($params);
      $this->commonTests($request, 'data/clients/?method=4 (delete client)');
    }

    function testClientLocations() {
      $co = $this->connect();
      $request = $co->session(2); // vancouver
      $request = $co->clients(array('method' => 1, 'objectID' => $this->clientID));
      $this->commonTests($request, 'data/clients');
      foreach ($request->Relations as $relation) {
        if ($relation->relationType === 'location') {
          $params = array(
            'method' => 1,
            'objectID' => $relation->objectID,
          );
          $lRequest = $co->locations($params);
          $this->commonTests($lRequest, 'data/locations');
          $this->assertEquals(get_class($lRequest->Location), 'stdClass');
        }
      }
    }   
  

/*
    function testEmailConfirmation() {  
      $submission->post['contact-email'] = 'gurpreet@longevitygraphics.com';
      $submission->post['contact-first'] = 'Calebo';
      $submission->post['contact-last'] = 'Testo';
      $submission->confirmation = '12345678910';
      require_once('../frogbox-form.submit-handler.inc');
      
      $response = _ff_email_confirmation($submission);
      
      $this->assertTrue($response);

    }
*/

  function testDeliveryCharges() {
    $zones = _ff_load_zones();

    $codes = array('v3x', 'v4c', 'V4C');
    echo PHP_EOL;
    foreach ($codes as $postCode) {
      echo _ff_service_charge( $postCode ) . PHP_EOL;
    }

  }

  function testLoadZones() {
    $zones = _ff_load_zones(false);
    print_r($zones);
    $co = new Frogbox();
    print_r(count($zones));
    $zip = $co->validateZipCode('T0A');
    print_r($zip);
  }


  function testValidatePriceList() {
    include_once(__DIR__ . '/../frogbox-form.submit-handler.inc');
    $submission = new stdClass;
    $submission->post = array(
        'delivery-city' => 15,
        'pickup-city' => 15,
        'bundle' => '1-bedroom',
        'FROGBOX-Moving-Box-quantity' => 0,
        'bundle-choice' => 42438,
        'Recycled-Packing-Paper-quantity' => 0,
        'Recyclable-Bubble-Wrap-quantity' => 0,
        'Recyclable-Bubble-Wrap---200-sq-ft-quantity' => 0,
        'Biodegradable-Mattress-Covers-quantity' => 0,
        'Biodegradable-Sofa-Cover-quantity' => 0,
        'Biodegradable-Chair-Cover-quantity' => 0,
        'Zip-Ties-quantity' => 0,
        'delivery-date' => '2017-03-11',
        'delivery-time' => '43-600',
        'pickup-date' => '2017-03-25',
        'pickup-time' => '43-960',
        'keep-for' => 14,
        'deliveryStreet' => '#1206 20 Bruyeres Mews',
        'deliveryCity' => 'Toronto',
        'deliveryProvince' => 'Ontario',
        'deliveryPostcode' => 'M5V 0G8',
        'deliveryCountry' => 'Canada',
        'deliveryDescription' => '',
        'pickupStreet' => '#2002 105 George Street',
        'pickupCity' => 'Toronto',
        'pickupProvince' => 'Ontario',
        'pickupPostcode' => 'M5A 0L4',
        'pickupCountry' => 'Canada',
        'pickupDescription' => '',
        'contact-first' => 'Nicole',
        'contact-last' => 'Gueldenpfennig',
        'contact-primary-phone' => '6479996135',
        'contact-email' => 'ngueld@gmail.com',
        'delivery-contact' => 'same as main contact',
        'pickup-contact' => 'same as main contact',
        'promo-code' => 'YYZ0820',
        'terms' => 1,
        'contact-primary-phone-reformatted' => '6479996135',
        'delivery-suburb' => 'M5V',
        'pickup-suburb' => 'M5A',
        'delivery-date-reformatted' => '20170311',
        'pickup-date-reformatted' => '20170325',
    );
    _ff_validate_pricelist($submission);
  }

  }

?>
