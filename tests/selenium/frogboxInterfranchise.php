<?php

require_once('vendor/autoload.php');
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverExpectedCondition;
use Facebook\WebDriver\WebDriverKeys;


$caps = array("browserName" => "chrome", "browserstack.debug" => "true", "build" => "frogbox-dev order test");

$order = new \Radiko\FrogboxInterFranchiseOrder();

$driver = RemoteWebDriver::create("https://calebtuckerraymo1:EyHqzpGBRHxBipebBdpw@hub-cloud.browserstack.com/wd/hub", $caps);

try {

    $driver->get($order->getUrl());

    /**
     * Submit the password form
     */
    $driver->wait()->until(
        function () use ($driver, $order) {
            $success = $driver->findElements(WebDriverBy::id('password_protected_pass'));
            return count($success);
        },
        'Page did not load.'
    );

// Password field
    $element = $driver->findElement(WebDriverBy::id('password_protected_pass'));
    $element->click();
    $element->sendKeys('sdnal604teW%');

// click submit
    $element = $driver->findElement(WebDriverBy::id('wp-submit'));
    $element->click();

    /**
     * Before we begin, wait for page to load
     */
    $driver->wait()->until(
        function () use ($driver, $order) {
            $success = $driver->findElements(WebDriverBy::cssSelector($order->getInitialElement()));
            return count($success);
        },
        'Page did not load.'
    );

    /**
     * Step 1 - choose delivery franchise, wait for it to load, then choose pickup franchise
     */

// choose the delivery franchise
    $select = $driver->findElement(WebDriverBy::id('delivery-city'));
    $selection = new \Facebook\WebDriver\WebDriverSelect($select);
    $selection->selectByVisibleText($order->getDeliveryFranchise());

// wait until the throbber is gone
    $driver->wait(30)->until(
        function () use ($driver) {
            $throbber = $driver->findElements(WebDriverBy::cssSelector('#delivery-city-throbber'));
            if (count($throbber) === 0) {
                return true;
            }
        }, 'Deliver priceslist never loaded.'
    );

// choose the pickup franchise
    $select = $driver->findElement(WebDriverBy::id('pickup-city'));
    $selection = new \Facebook\WebDriver\WebDriverSelect($select);
    $selection->selectByVisibleText($order->getPickupFranchise());

// wait until the throbber is gone
    $driver->wait(30)->until(
        function () use ($driver) {
            $throbber = $driver->findElements(WebDriverBy::cssSelector('#pickup-city-throbber'));
            if (count($throbber) === 0) {
                return true;
            }
        }, 'Pickup pricelist never loaded.'
    );

// advance to step 2
    $element = $driver->findElement(WebDriverBy::cssSelector('#step-1-next'));
    $element->click();

    /**
     * Step 2 - choose a bundle
     */

// click the first order button we find
    $element = $driver->findElement(WebDriverBy::cssSelector('input[name=bundle]'));
    $element->click();


// scroll down to the first visible radio button and click it.
    $group = $driver->findElements(WebDriverBy::cssSelector('input[name=bundle-choice]'));
    foreach ($group as $element) {
        if ($element->isDisplayed()) {
            $element->getLocationOnScreenOnceScrolledIntoView();
            $element->click();
            break;
        }
    }

    // click next
    $element = $driver->findElement(WebDriverBy::id('step-2-next'));
    $element->click();

    /**
     *  Step 3 - skip this
     */

    // wait for scrolling
    sleep(1);

    // click next
    $element = $driver->findElement(WebDriverBy::id('step-3-next'));
    $element->getLocationOnScreenOnceScrolledIntoView();
    $element->click();

    /**
     *  Step 4 -
     */

    // wait for scrolling
    sleep(1);

    // dropoff date/time
    $order->pickDate($driver, 'delivery', 4);

    // pickup date/time
    $order->pickDate($driver, 'pickup', 5);

    // dropooff address
    $order->fillAddress($driver, 'delivery');
    $order->fillAddress($driver, 'pickup');

    $order->clickNextStep($driver,4);

    /**
     * Step 5
     */

    $order->fillContact($driver);

    // terms of service
    $order->agreeToTerms($driver);

    // place your order
    $order->clickNextStep($driver,5);

    // wait up to 60 seconds to see if the title has changed
    $driver->wait(60, 1000)->until(
        WebDriverExpectedCondition::titleMatches('/Order Complete/')
    );

    // grab the confirmation number and print it out
    $element = $driver->findElement(WebDriverBy::id('order-number'));
    print $element->getText();

    // sleep a bit to capture the final state on the video
    sleep(5);
    $driver->quit();

}
catch (Exception $e) {
    error_log('exception: ' . $e->getMessage());
    $driver->quit();
    die;
}

?>

