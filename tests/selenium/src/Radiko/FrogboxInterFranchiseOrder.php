<?php

namespace Radiko;


class FrogboxInterFranchiseOrder extends FrogboxOrder {

    protected $deliveryAddress = array(
        'Street' => '123 Test Street',
        'City' => 'Edmonton',
        'Postcode' => 'T6G 0T9',
        'Description' => '',
    );

    protected $pickupAddress = array(
        'Street' => '123 Test Street',
        'City' => 'Calgary',
        'Postcode' => 'T2M 3W4',
        'Description' => '',
    );

    public function __construct() {

        parent::__construct(array(
                'deliveryFranchise' => '',
                'pickupFranchise' => '',
            )
        );

    }


}