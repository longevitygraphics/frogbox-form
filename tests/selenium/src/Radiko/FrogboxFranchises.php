<?php

namespace Radiko;

class FrogboxFranchises {

    private $franchises = array(
        'Vancouver' => 2,
    );

    public function getId($franchise) {
        return $this->franchises[$franchise];
    }

}