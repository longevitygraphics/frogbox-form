<?php

namespace Radiko;

use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverExpectedCondition;

class FrogboxOrder {

    protected $deliveryFranchise = 'Vancouver';
    protected $deliveryFranchiseId = null; // set in __construct()

    protected $pickupFranchise = 'Vancouver';
    protected $pickupFranchiseId = null; // set in __construct();

    protected $franchiseObject; // set in __construct ();

    protected $url = "http://dev.frogbox.com/order";

    protected $initialElement = '#frogbox-form';

    protected $pickupAddress = array(
        'Street' => '123 Test Street',
        'City' => 'Vancouver',
        'Postcode' => 'V6G 1A1',
        'Description' => '',
    );

    protected $deliveryAddress = array(
        'Street' => '123 Test Street',
        'City' => 'Vancouver',
        'Postcode' => 'V6G 1A1',
        'Description' => '',
    );

    protected $contact = array(
        'first' => 'Banana Jr.',
        'last' => '6000',
        'primary-phone' => '5034427029',
        'email' => 'gurpreet@longevitygraphics.com',
    );

    /**
     * FrogboxOrder constructor.
     *
     * @param array $params
     */
    public function __construct($params = array()) {

        $this->franchiseObject = new FrogboxFranchises();

        if (!empty($params['deliveryFranchise'])) {
            $this->deliveryFranchise = $params['deliveryFranchise'];
        }

        if (!empty($params['pickupFranchise'])) {
            $this->pickupFranchise = $params['pickupFranchise'];
        }

        // set franchise ids based on text
        $this->deliveryFranchiseId = $this->franchiseObject->getId($this->deliveryFranchise);
        $this->pickupFranchiseId = $this->franchiseObject->getId($this->pickupFranchise);

    }

    /**
     * __call magic method
     *
     * @param $name
     * @param null $arguments
     * @return mixed
     * @throws \Exception
     */
    public function __call($name, $arguments = null) {
        // generically handle get functions
        if (strpos($name, 'get') === 0) {
            $property = lcfirst(substr($name, 3));
            if (isset($this->{$property})) {
                return $this->{$property};
            }
            else {
                throw new \Exception(sprintf('No property %s on %s', $property, __CLASS__));
            }
        }
        else {
            throw new \Exception(sprintf('No function %s on %s.', $name, __CLASS__));
        }
    }

    public function pickDate(RemoteWebDriver $driver, $which, $months = 4) {
        // click date field
        $element = $driver->findElement(WebDriverBy::id($which . '-date'));
        $element->click();

        // wait for calendar to show
        $driver->wait(5)->until(
            WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('.datepicker .next'))
        );

        // browse forward a number of months equal to $months
        $element = $driver->findElement(WebDriverBy::cssSelector('.datepicker .next'));
        for ($i = 0; $i < $months; $i++) {
            $element->click();
        }

        // click any day that is not disabled
        $elements = $driver->findElements(WebDriverBy::cssSelector('.day:not(.disabled):not(.old)'));
        foreach ($elements as $element) {
            if ($element->isDisplayed()) {
                $element->click();
                break;
            }
        }

        // now wait to make sure the select has rendered
        $driver->wait(30)->until(
            WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('#' . $which .'-time option'))
        );

        // click the select element
        $element = $driver->findElement(WebDriverBy::id($which . '-time'));
        $element->click();

        // click the first option
        $element = $driver->findElement(WebDriverBy::cssSelector('#' . $which . '-time option:nth-child(2)'));
        $element->click();
    }

    public function fillAddress(RemoteWebDriver $driver, $which) {

        // figure out which address property to use
        if ($which === 'delivery') {
            $address = $this->deliveryAddress;
        }
        else {
            $address = $this->pickupAddress;
        }

        // for each peroperty, scroll the element into view and fill it
        foreach ($address as $key => $value) {
            $elementID = $which . $key;
            $element = $driver->findElement(WebDriverBy::id($elementID));
            $element->getLocationOnScreenOnceScrolledIntoView();
            $element->click();
            $element->sendKeys($value);
        }

    }

    public function fillContact(RemoteWebDriver $driver) {

        $contact = $this->contact;

        // for each peroperty, scroll the element into view and fill it
        foreach ($contact as $key => $value) {
            $elementID = 'contact-' . $key;
            $element = $driver->findElement(WebDriverBy::id($elementID));
            $element->getLocationOnScreenOnceScrolledIntoView();
            $element->click();
            $element->sendKeys($value);
        }

    }

    public function clickNextStep($driver, $step) {
        $elementID = 'step-' . $step . '-next';
        $element = $driver->findElement(WebDriverBy::id($elementID));
        $element->getLocationOnScreenOnceScrolledIntoView();
        $element->click();
        sleep(1);
    }


    public function agreeToTerms($driver) {
        $elementID = 'terms';
        $element = $driver->findElement(WebDriverBy::id($elementID));
        $element->getLocationOnScreenOnceScrolledIntoView();
        $element->click();
    }

}
