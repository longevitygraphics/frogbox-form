<?php

namespace Radiko;

class FrogboxCaaOrder extends FrogboxOrder {

    protected $url = "http://dev.frogbox.com/caa";

    protected $caaMembership = '6202826684577001';

    protected $initialElement = '#membership_number';

    /**
     * @return string
     */
    public function getCaaMembership()
    {
        return $this->caaMembership;
    }

}
