<?php
/**
 * Created by PhpStorm.
 * User: caleb
 * Date: 1/27/16
 * Time: 9:43 PM
 */

require_once('TestBase.php');

/**
 * Class RoutesTest
 */
class RoutesTest extends VonigoTest {

    private $franchise = 2;
    private $route = 79;

    function testListRoutes() {
        $request = $this->co->routes();
        $this->commonTests($request, 'routes');
    }

    function testRouteOptions() {
        $option = $this->co->getRouteOption($this->franchise, $this->route, false);
        $this->assertTrue(!empty($option), 'could not get route option');
    }

}
