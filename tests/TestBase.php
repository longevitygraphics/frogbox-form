<?php

require_once(__DIR__ . '/../vonigo-api/Vonigo.php');
require_once('VonigoTest.php');

require_once(__DIR__ . '/../util/util.inc');
require_once(__DIR__ . '/../frogbox-api.inc');
require_once(__DIR__ . '/../frogbox-form.submit-handler.inc');



define('BASE_PATH', find_wordpress_base_path()."/");
define('WP_USE_THEMES', false);
global $wp, $wp_query, $wp_the_query, $wp_rewrite, $wp_did_header, $wpdb;
require(BASE_PATH . 'wp-load.php');
require_wp_db();

error_reporting(E_ALL ^ E_STRICT);


?>
