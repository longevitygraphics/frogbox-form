<?php

require_once('TestBase.php');
require_once('VonigoUtil.php');

/**
 * Class LeadsTest
 */
class LeadsTest extends VonigoTest {

    /**
     * Tests Clients methods
     */
    function testListLeads() {
        $request = $this->co->leads(array('method' => 0));
        $this->commonTests($request, 'data/leads');
    }

    function testViewLead() {
        $this->co->setDebug(3);

        $request = $this->co->leads();
        if (!empty($request->Clients)) {
            $client = array_pop($request->Clients);
            if (!empty($client->objectID)) {
                $request = $this->co->leads(array('method' => 1, 'objectID' => $client->objectID));
                $this->commonTests($request, 'data/leads');
            }
        }
        else {
            $this->assertTrue('false', 'could not read lead records');
        }
    }

}

?>
