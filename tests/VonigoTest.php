<?php

abstract class VonigoTest extends PHPUnit_Framework_TestCase {

    function __construct() {
//        $this->co = $this->connect();
    }

    public function connect() {
        $co = new Frogbox();
        $request = $co->authenticate();
        if (!$request) {
            return false;
        }
        return $co;
    }

    public function commonTests($request, $method) {
        $expected = 0;
        $this->doHttpCode($request, $method);
        $this->doVonigo($request, $expected, $method);
        $this->doJson($request, $method);
    }

    public function doHttpCode($request, $method) {
        if (!empty($request->httpCode)) {
          $this->assertEmpty($request->httpCode, 'HTTP issue with method ' . $method);
        }
        else {
          $this->assertTrue(true);
        }
    }

    public function doVonigo($request, $expected = 0, $method) {
        $this->assertEquals($request->errNo, $expected, 'Vonigo error in method ' . $method);
    }

    public function doJson($request, $method) {
        $this->assertNotNull(json_decode(json_encode($request)), 'JSON error in method ' . $method);
    }

}

?>
