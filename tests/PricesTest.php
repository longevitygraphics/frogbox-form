<?php
/**
 * Created by PhpStorm.
 * User: caleb
 * Date: 1/27/16
 * Time: 9:43 PM
 */

require_once('TestBase.php');

/**
 * Class LocationTest
 */
class PricesTest extends VonigoTest {

    private $zip = 'V6G';
    private $franchise = 15;

    function testSpecific() {
        $this->co->session(20);
        $request = $this->co->priceLists();
        $request = $this->co->priceBlocks(437);
        print_r($request);
    }

    function testPriceLists() {
        $this->co->session($this->franchise);
        $request = $this->co->priceLists();
        print_r($request);
        $this->commonTests($request, 'pricelists');
    }

    function testPriceBlocks() {
        $this->co->session(15);
        $request = $this->co->priceLists();
        $priceList = array_pop($request->PriceLists);
//        $request = $this->co->priceBlocks($priceList->priceListID);
        $request = $this->co->priceBlocks(443);
        print_r($request);
        $this->commonTests($request, 'priceblocks');
    }

    function testPriceItems() {
        $request = $this->co->priceLists();
        $priceList = array_pop($request->PriceLists);
        $request = $this->co->priceBlocks($priceList->priceListID);
        foreach ($request->PriceBlocks as $priceBlock) {
            $request = $this->co->priceItems( $priceBlock->priceListID, $priceBlock->priceBlockID );
            $this->commonTests($request, 'priceblocks');
            print_r( $request );
        }
    }

    function testFreeBoxes() {
        $session = $this->co->session($this->franchise);
        if ($session->errNo != 0) {
            delete_transient('ff_security_token');
            $this->co->authenticate();
            $session = $this->co->session( $this->franchise );
            if ( $session->errNo != 0 ) {
                $this->assertTrue( FALSE, 'could not authenticate' );
                return;
            }
        }

        // get the free box rental priceItem for this zone
        $priceListID = 0;
        $zvRequest = $this->co->validateZipCode($this->zip);
        foreach ($zvRequest->ServiceTypes as $serviceType) {
            if ($serviceType->typeID == 8) { // frogbox
                $priceListID = $serviceType->priceID;
                break;
            }
        }

        $pItem = (object) array( 'priceItem' => FALSE );
        if ($priceListID) {
            // get priceblocks for this pricelist
            $priceBlocks = $this->co->priceBlocks( $priceListID );
            foreach ( $priceBlocks->PriceBlocks as $priceBlock ) {
                if ( stripos( $priceBlock->priceBlock, 'products' ) !== FALSE && Vonigo::isTrue( $priceBlock->isActive ) ) {
                    $pBlock = $priceBlock;

                    // get items for this priceblock
                    $priceItems = $this->co->priceItems( $pBlock->priceListID, $pBlock->priceBlockID );
print_r($priceItems);
                    if ( ! empty( $priceItems->PriceItems ) ) {
                        foreach ( $priceItems->PriceItems as $priceItem ) {
                            if ( stripos( $priceItem->priceItem, '5 free' ) !== FALSE ) {
                                $pItem = $priceItem;
                                break;
                            }
                        }
                    }
                }
            }
        }
        $this->assertTrue(stripos( $pItem->priceItem, '5 free boxes' ) !== FALSE);
    }

    function testPriceItemMatch() {
        require_once(__DIR__ . '/../frogbox-form.submit-handler.inc');
        $zip = 'M4X';
        $priceBlockMatch = 'Products';
        $priceItemMatch = '5 free boxes';
        $info = _ff_priceItem_match($zip, $priceBlockMatch, $priceItemMatch);
        $this->assertTrue(is_string($info->priceItem));
    }

    function testGetPromoItem() {
        $promo = 'FrogSquad2016';
        $zip = 'M4X';
        $info = _ff_get_promo_item($zip, $promo, false);
        $this->assertTrue(is_string($info->priceItem));
    }

    function testMovePack() {
        $promoItem = _ff_movePackMatch('move pack 1');
        $this->assertTrue(is_string($promoItem->priceItem));
        print_r($promoItem);
    }

}
