<?php
/**
 * Plugin Name: Frogbox Form
 * Description: A Wordpress plugin to add a booking form for Frogbox.
 * Version: 1.3.37 - bugfix: dont email confirmation on failed submission
 */

require_once(__DIR__ . '/vendor/autoload.php');

require WP_PLUGIN_DIR.'/frogbox-form/vendor/stripe/stripe-php/init.php';



 if ( ! function_exists('lg_write_log')) {
     function lg_write_log ( $log )  {
        if ( is_array( $log ) || is_object( $log ) ) {
           error_log( print_r( $log, true ) );
        } else {
           error_log( $log );
        }
     }
  }

// define the version, but only in production
$dotenv = new \Dotenv\Dotenv(__DIR__);
$dotenv->load();

$environment = getenv('FROGBOX_ENVIRONMENT');
if (!empty($environment) && ($environment == 'local' || $environment == 'development')) {
    define('FF_VERSION', md5(time()));
}
else {
    define('FF_VERSION', '1.3.37');
}

define('FF_TRANSIENT_EXPIRE', 604800);
define('FF_RENTAL_GRACE', 2);
define('VONIGO_LOCK_EXPIRE', 1500);

require_once('vonigo-api/Vonigo.php');
require_once('frogbox-form.shortcode.inc');
require_once('frogbox-api.inc');

add_action('plugins_loaded','_ff_add_routing');

/**
 *  Adds routing to site
 */
function _ff_add_routing() {
    new Frogbox\FF_Router();
}

function _ff_quote_entities($input) {
    return str_replace(array('"', "'", ',', '*'), array('&quot;', '&apos;', '&comma;', '&ast;'), $input);
}

/**
 * Normalizes price items names
 */
function _ff_normalizePriceItemName($name) {
  $name = preg_replace('/(<br\/>)/i', '', $name);
  $name = preg_replace('/(\(?most popular\)?)/i', '', $name);
  $name = preg_replace('/(\d+ ?weeks? rental)/i', '', $name);
  $name = preg_replace('/(\d+ ?weeks?)/i', '', $name);
  $name = preg_replace('/(Bdr)/', 'Bedroom', $name);
  $name = preg_replace('/(\d+? - )/i', '', $name);
  $name = preg_replace('/(\(\))/i', '', $name);
  $name = preg_replace('/(box rental)/i', '', $name);
  $name = preg_replace('/( - )/i', '', $name);

  $name = preg_replace('/(Wrap50)/i', 'Wrap - 50', $name);
  $name = preg_replace('/(Wrap200)/i', 'Wrap - 200', $name);

  return $name;
}

/**
 * Start the output buffer. This allows the frogbox form to gently fail over to
 * the default Vonigo form if there is something wrong with the API.
 */
add_action('init', '_ff_output_buffer');
function _ff_output_buffer() {
    ob_start();
}

/**
 * Accept 'ffsh' as query
 */
add_filter('query_vars', '_ff_parameter_queryvars' );
function _ff_parameter_queryvars($qvars) {
    $qvars[] = ' caa';
    $qvars[] = ' membership_number';
    $qvars[] = ' promo';
    return $qvars;
}


/**
 * Defines a class to manage Frogbox Form settings.
 */
class frogboxFormSettingsPage {
  /**
   * Holds the values to be used in the fields callbacks
   */
  private $options;

  /**
   * Start up
   */
  public function __construct() {
    add_action('admin_menu', array($this, 'add_plugin_page'));
    add_action('admin_init', array($this, 'page_init'));
  }

  /**
   * Add options page
   */
  public function add_plugin_page() {
    // This page will be under "Settings"
    add_options_page(
        'Frogbox Form',
        'Frogbox Form Settings',
        'manage_options',
        'frogbox-form-settings-admin',
        array( $this, 'create_admin_page' )
    );
  }

  /**
   * Options page callback
   */
  public function create_admin_page() {
    // Set class property
    $this->options = get_option('frogbox_form_settings');
    ?>
    <div class="wrap">
      <h2>My Settings</h2>
      <form method="post" action="options.php">
        <?php
        // This prints out all hidden setting fields
        settings_fields('frogbox_form');
        do_settings_sections('frogbox-form-settings-admin');
        submit_button();
        ?>
      </form>
    </div>
  <?php
  }

  /**
   * Register and add settings
   */
  public function page_init() {
    register_setting(
        'frogbox_form', // Option group
        'frogbox_form_settings', // Option name
        array( $this, 'sanitize' ) // Sanitize
    );

    add_settings_section(
        'frogbox_vonigo', // ID
        'Vonigo Settings', // Title
        array( $this, 'print_section_info' ), // Callback
        'frogbox-form-settings-admin' // Page
    );

    add_settings_field(
        'username', // ID
        'Vonigo username', // Title
        array( $this, 'vusername_callback' ), // Callback
        'frogbox-form-settings-admin', // Page
        'frogbox_vonigo' // Section
    );

    add_settings_field(
        'password',
        'Vonigo password',
        array( $this, 'vpassword_callback' ),
        'frogbox-form-settings-admin',
        'frogbox_vonigo'
    );
    add_settings_field(
        'company',
        'Vonigo company',
        array( $this, 'vcompany_callback' ),
        'frogbox-form-settings-admin',
        'frogbox_vonigo'
    );
    add_settings_field(
        'base_url',
        'Vonigo base url',
        array( $this, 'vbase_url_callback' ),
        'frogbox-form-settings-admin',
        'frogbox_vonigo'
    );

    //stripe settings
      add_settings_section(
		  'frogbox_stripe', // ID
		  'Stripe Settings', // Title
		  array( $this, 'print_section_info' ), // Callback
		  'frogbox-form-settings-admin' // Page
	  );

	  add_settings_field(
		  'vancouver_public_key', // ID
		  'Vancouver: Stripe Publishable key', // Title
		  array( $this, 'vancouver_public_key_callback' ), // Callback
		  'frogbox-form-settings-admin', // Page
		  'frogbox_stripe' // Section
	  );

	  add_settings_field(
		  'vancouver_secret_key', // ID
		  'Vancouver: Stripe Secret key', // Title
		  array( $this, 'vancouver_secret_key_callback' ), // Callback
		  'frogbox-form-settings-admin', // Page
		  'frogbox_stripe' // Section
	  );

	  add_settings_field(
		  'seattle_public_key', // ID
		  'Seattle: Stripe Publishable key', // Title
		  array( $this, 'seattle_public_key_callback' ), // Callback
		  'frogbox-form-settings-admin', // Page
		  'frogbox_stripe' // Section
	  );

	  add_settings_field(
		  'seattle_secret_key', // ID
		  'Seattle: Stripe Secret key', // Title
		  array( $this, 'seattle_secret_key_callback' ), // Callback
		  'frogbox-form-settings-admin', // Page
		  'frogbox_stripe' // Section
	  );
  }

	public function vancouver_public_key_callback() {
		printf(
			'<input size=40 type="text" id="vancouver_public_key" name="frogbox_form_settings[vancouver_public_key]" value="%s" />',
			isset( $this->options['vancouver_public_key'] ) ? esc_attr( $this->options['vancouver_public_key']) : ''
		);
	}
	public function vancouver_secret_key_callback() {
		printf(
			'<input size=40 type="password" id="vancouver_secret_key" name="frogbox_form_settings[vancouver_secret_key]" value="%s" />',
			isset( $this->options['vancouver_secret_key'] ) ? esc_attr( $this->options['vancouver_secret_key']) : ''
		);
	}
	public function seattle_public_key_callback() {
		printf(
			'<input size=40 type="text" id="seattle_public_key" name="frogbox_form_settings[seattle_public_key]" value="%s" />',
			isset( $this->options['seattle_public_key'] ) ? esc_attr( $this->options['seattle_public_key']) : ''
		);
	}
	public function seattle_secret_key_callback() {
		printf(
			'<input size=40 type="password" id="seattle_secret_key" name="frogbox_form_settings[seattle_secret_key]" value="%s" />',
			isset( $this->options['seattle_secret_key'] ) ? esc_attr( $this->options['seattle_secret_key']) : ''
		);
	}

  /**
   * Sanitize each setting field as needed
   *
   * @param array $input Contains all settings fields as array keys
   */
  public function sanitize( $input ) {
    $new_input = array();
    foreach ($input as $key => $value) {
      if (in_array($key, array('username', 'password', 'company', 'base_url', 'vancouver_public_key', 'vancouver_secret_key', 'seattle_public_key', 'seattle_secret_key'))) {
        $new_input[$key] = sanitize_text_field($input[$key]);
      }
    }
    return $new_input;
  }

  /**
   * Print the Section text
   */
  public function print_section_info() {
    print 'Enter your settings below:';
  }

  /**
   * Get the settings option array and print one of its values
   */
  public function vusername_callback() {
    printf(
        '<input size=40 type="text" id="vonigo_username" name="frogbox_form_settings[username]" value="%s" />',
        isset( $this->options['username'] ) ? esc_attr( $this->options['username']) : ''
    );
  }

  /**
   * Get the settings option array and print one of its values
   */
  public function vpassword_callback() {
    printf(
        '<input size=40 type="password" id="vonigo_password" name="frogbox_form_settings[password]" value="%s" />',
        isset( $this->options['password'] ) ? esc_attr( $this->options['password']) : ''
    );
  }

  /**
   * Get the settings option array and print one of its values
   */
  public function vcompany_callback() {
    printf(
        '<input size=40 type="text" id="vonigo_company" name="frogbox_form_settings[company]" value="%s" />',
        isset( $this->options['company'] ) ? esc_attr( $this->options['company']) : ''
    );
  }

  /**
   * Get the settings option array and print one of its values
   */
  public function vbase_url_callback()
  {
    printf(
        '<input size=40 type="text" id="vonigo_base_url" name="frogbox_form_settings[base_url]" value="%s" />',
        isset( $this->options['base_url'] ) ? esc_attr( $this->options['base_url']) : ''
    );
  }
}

/**
 * Include the settings page if the user is and administrator.
 */
if (is_admin()) {
  $my_settings_page = new frogboxFormSettingsPage();
}

/**
 * Sorts a PHP array of objects by two fields
 * from comments at http://php.net/array_multisort
 * h/t http://stackoverflow.com/questions/4582649/php-sort-array-by-two-field-values
 */
function _ff_array_orderby() {
  $args = func_get_args();
  $data = array_shift($args);
  foreach ($args as $n => $field) {
    if (is_string($field)) {
      $tmp = array();
      foreach ($data as $key => $row) {
        $tmp[$key] = $row->{$field}; // modified to use object property
      }
      $args[$n] = $tmp;
    }
  }
  $args[] = &$data;
  call_user_func_array('array_multisort', $args);
  return array_pop($args);
}

/**
 * Sets a lock and marks nearby times as unavailable
 */
function _ff_set_vonigo_lock($lockID, $date, $routeTime, $franchiseID) {
  $key = _ff_make_vonigo_lock_key($date, $routeTime);
  set_transient($key, $lockID, VONIGO_LOCK_EXPIRE);
}

/**
 * Gets a lock
 */
function _ff_get_vonigo_lock($date, $routeTime) {
  $key = _ff_make_vonigo_lock_key($date, $routeTime);
  return get_transient($key);
}

/**
 * Returns a lock key
 */
function _ff_make_vonigo_lock_key($date, $routeTime) {
  return '_ff_lock_' . $date . '_' . $routeTime;
}
