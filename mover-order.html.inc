
<form id="frogbox-form" action="/m-order-complete" method="POST" novalidate>

    <div id="accordion">

        <div id="step-2-heading" class="step-heading">
            <h2>1 - Number of Boxes</h2>
        </div>
        <div class="content">
            <div id="step-2" class="step"></div>
        </div>

        <div id="step-3-heading" class="step-heading">
            <h2>2 - Packing Supplies and Options</h2>
        </div>
        <div class="content">
            <div id="step-3" class="step"></div>
        </div>

        <div id="step-4-heading" class="step-heading">
            <h2>3 - Delivery and Pickup</h2>
        </div>
        <div class="content">
            <div id="step-4" class="step"></div>
        </div>

        <div id="step-5-heading" class="step-heading">
            <h2>4 - Customer Information</h2>
        </div>
        <div class="content">
            <div id="step-5" class="step"></div>
        </div>

    </div>

</form>