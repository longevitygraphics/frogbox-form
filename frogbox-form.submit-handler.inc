<?php


// check that priceitems are part of the right priceblocks
function _ff_validate_pricelist($submission) {

    // 1. figure out the price list zip
    if ($submission->post['delivery-city'] != $submission->post['pickup-city']) {
        $pricelistZip = 'IF' . $submission->post['delivery-city'];
    }
    elseif($submission->post['includeMovePack'] == '1') {
        $pricelistZip = $submission->post['PricelistCode'];
    }
    else {
        $pricelistZip = strtoupper($submission->post['delivery-suburb']);
        $size = ($submission->post['deliveryCountry'] == 'Canada') ? 3 : 5;
        $pricelistZip = substr($pricelistZip, 0, $size);
    }

    // calculate the days between delivery and pickup; don't relly on keep-for value
    $delivery = strtotime($submission->post['delivery-date-reformatted']);
    $pickup = strtotime($submission->post['pickup-date-reformatted']);
    $pricelistDays = ($pickup - $delivery) / 3600 / 24;

    // 2. get an uncached priceblock for this order
    $priceList = Frogbox\FF_Router::getPriceList($pricelistZip, $pricelistDays, false, false);

    if (empty($priceList['priceBlock']->priceBlockID)) {
        return (object) array(
            'element' => 'step-1',
            'errMsg' => 'Oops! some pricing was incorrect for your cart. Please review the updated pricing.',
            'reason' => 'no priceblock: line ' . __LINE__,
            'real_pricelist' => $priceList,
        );
    }
    else {
        // 3. examine the data in the cart
        $cartItems = json_decode( stripslashes( $submission->post['frogboxCart'] ) );

        if (trim((string) $cartItems->priceBlock->priceBlockID) == (string) trim($priceList['priceBlock']->priceBlockID)) {
            foreach ( array_merge( $cartItems->bundles, $cartItems->supplies ) as $item ) {
                if ( $item->source == 'bundles' ) {
                    if ( $item->priceBlockID != $priceList['priceBlock']->priceBlockID ) {
                        return (object) array(
                            'element' => 'step-1',
                            'errMsg'  => 'Oops! some pricing was incorrect for your cart. Please review the updated pricing.',
                            'reason' => 'bundle does not match priceblock: line ' . __LINE__,
                            'real_pricelist' => $priceList,
                        );
                    }
                }
            }
        }
        else {
            return (object) array(
                'element' => 'step-1',
                'errMsg'  => 'Oops! some pricing was incorrect for your cart. Please review the updated pricing.',
                'reason' => 'priceblock does not match: line ' . __LINE__,
                'zip' => $pricelistZip,
                'days' => $pricelistDays,
                'real_priceblock' => $priceList['priceBlock'],
                'cart_priceblock' => $cartItems->priceBlock
            );
        }

        return true;
    }
}

/**
 * Locks a service time through vonigo;
 *
 * @param strip $zip - zip code for locking
 * @param string $date - the date
 * @param string $routeTime - a code containing the route id and the the start time, separated by -
 *
 * @return mixed - a lock id if it is valid, an error object if it is not
 */
function _ff_lock_time($zip, $date, $routeTime) {
    list($route, $time) = explode('-', $routeTime);
    // validate route - must be integer
    if (intval($route) === 0) {
        $rtError = new stdClass;
        $rtError->errMsg = 'Invalid route-time parameter: ' + htmlspecialchars($routeTime);
        return json_decode(Frogbox\FF_Router::routeError('Invalid route.', false, $rtError, false, __LINE__));
    }

    // validate time ; integer divisible by 30 and between 0 and 1440
    $time = intval($time);
    if ($time % 30 !== 0 || $time < 0 || $time > 1440) {
        $timeError = new stdClass;
        $timeError->errMsg = 'Invalid time in set time method: ' + htmlspecialchars($time);
        return json_decode(Frogbox\FF_Router::routeError('Invalid time.', false, $timeError, false, __LINE__));
    }

    $datestamp = Frogbox\FF_Router::validateDate(str_replace('-', '', $date));

    if (!is_int($datestamp)) {
        return $datestamp;
    }

    try {
        $co = new frogbox();
    }
    catch (\Frogbox\LockException $e) {
        $lockError = new stdClass;
        $lockError->errMsg = 'Unable to get lock ' . __LINE__;
        return json_decode(Frogbox\FF_Router::routeError('Unable to verify pickup times.', true, $lockError, false, __LINE__));
    }

    $co->releaseLock();

    // get the zone id from the zip code
    $zipRequest = $co->validateZipCode($zip);
    if ($zipRequest->errNo != 0 || empty($zipRequest->Ids->zoneID)) {
        $co->releaseLock();
        return json_decode(Frogbox\FF_Router::routeError('Unable to process Zip/Postal Code. Please check your delivery location and enter the promo code again.', true, $zipRequest, true, __LINE__));
    }
    $franchise = $zipRequest->Ids->franchiseID;
    $zoneID = $zipRequest->Ids->zoneID;

    // get the duration
    $duration = $co->getWindow($franchise);

    // set the franchise
    $fRequest = $co->session($franchise);
    if ($fRequest->errNo != 0) {
        $errObj = (object) array('errMsg' => 'Invalid value for delivery city: ' . $franchise, 'element' => 'delivery-city', 'request' => $fRequest);
        $co->releaseLock();
        $co->clearSecurityToken();
        return json_decode(Frogbox\FF_Router::routeError('Could not lock time. ', true, $errObj, true, __LINE__));
    }

    // make lock request
    $params = array(
        'serviceTypeID' => FROGBOX_SERVICE_TYPE, // always 8 with frogbox
        'dateStart' => $datestamp,
        'startTime' => $time,
        'duration' => $duration,
        'routeID' => $route,
        'zoneID' => $zoneID,
    );
    $lRequest = $co->lockTimes($params);

    if ($lRequest->errNo != 0) {
        $lRequest->zip = $zip;
        $lRequest->date = $date;
        $lRequest->routeTime = $routeTime;
        return json_decode(Frogbox\FF_Router::routeError('Date and time selection is no longer available.', true, $lRequest, false, __LINE__, true));
    }
    return $lRequest->Ids->lockID;
}

/**
 * Displays the order confirmation page.
 */
function _ff_confirmation_page($theme) {
    header('Content-type: text/html');
    include_once('confirmation.html.inc');
}

function _ff_extractMovePack($details) {
    $return = array();
    foreach ($details as $item) {
        if (stripos($item->title, 'move pack') !== false) {
            $items = explode("<br>", $item->dimensions);
            foreach ($items as $i) {
                $trimmed = trim($i);
                $parts = explode(' ', $trimmed, 2);
                $return[$parts[1]] = $parts[0];
            }
        }
    }
    return $return;
}

/**
 * Log failed submission for later.
 *
 * Deletes objects that were created so far in the process.
 */
function _ff_failed_submission($submission, $rollback = null, $error = array()) {

    $message = print_r($error, true) . PHP_EOL . print_r($submission, true);

// mail Phil, and Gurpreet

    $mailResult = wp_mail('phil@frogbox.com,Mike.Mackenzie@frogbox.com,gurpreet@longevitygraphics.com', 'failed api submission', $message);

// store locally
    $key = '_ff_failure_' . time();
    set_transient($key, $message, 0);

    $submission->valid = false;
    $submission->errors = array($error);
    return $submission;
}

/**
 * Formats a time, in minutes as hours:minutes with period, ie 7:30am
 */
function _ff_formatTime($time) {
    $hours = intval($time / 60);
    $minutes = sprintf("%'.02d", $time % 60);
    $period = 'am';
    if ($hours >= 12) {
        $period = 'pm';
        if ($hours > 12) {
            $hours = $hours -12;
        }
    }
    return $hours . ':' . $minutes  . $period;
}

/**
 * Formats a priceitem, suitable for display in HTML.
 */
function _ff_formatPriceItem($item, $quantity) {
    $markup = '<div class="product">';
    $markup .= '<span class="product-name">' . $item . '</span>';
    $markup .= '<span class="product-quantity">' . $quantity . '</span></div>';
    return $markup;
}

/**
 *  Formats a priceitem, suitable for rendering as JSON for google Analytics
 */
function _ff_formatPriceItemGA($item) {
    $return = new stdClass;
    $return->id = $item->key;
    $return->name = $item->name;
    $return->category = '';
    $return->brand = 'frogbox';
    $return->variant = '';
    $return->price = money_format('%.2n', $item->value);
    $return->quantity = $item->quantity;
    return $return;
}

/**
 * Returns a list of Franchise tags used in Infusionsoft keyed by name.
 */
function _ff_is_franchise_name_tags() {

    $tags = array(
        'Boise' => 128,
        'Calgary' => 130,
        'Durham Frogbox' => 132,
        'Edmonton' => 134,
        'Fraser Valley' => 136,
        'Halifax' => 138,
        'KW-Hamilton-Oakville' => 140,
        'Lethbridge' => 142,
        'London' => 144,
        'Okanagan' => 146,
        'Ottawa' => 148,
        'Regina' => 150,
        'Saskatoon' => 152,
        "St. John's" => 154,
        'Toronto' => 156,
        'Vancouver' => 158,
        'Victoria' => 160,
        'Puget Sound' => 162,
        'Winnipeg' => 164,
        'Twin Cities' => 166,
    );

    return $tags;
}

/**
 * Returns a list of Franchise tags used in Infusionsoft keyed by Franchise ID.
 */
function _ff_is_franchise_id_tags() {
    $tags = array(
        29 => 128, // 'Boise',
        30 => 130, // 'Calgary',
        22 => 132, // 'Durham Frogbox',
        27 => 134, // 'Edmonton',
        17 => 136, // 'Fraser Valley',
        19 => 138, // 'Halifax',
        21 => 140, // 'KW-Hamilton-Oakville',
        32 => 142, // 'Lethbridge',
        20 => 144, // 'London',
        23 => 146, // 'Okanagan',
        24 => 148, // 'Ottawa',
        28 => 150, // 'Regina',
        33 => 152, // 'Saskatoon',
        16 => 162, // 'Seattle',
        38 => 154, // 'St. John\'s',
        15 => 156, // 'Toronto',
        18 => 166, // 'Twin Cities',
        2 => 158,  // 'Vancouver',
        26 => 160, // 'Victoria',
        25 => 164, // 'Winnipeg'
    );
    return $tags;
}

/**
 * Returns a franchise name, given a franchise ID.
 */
function _ff_is_franchise_name($franchise) {
    $tags = _ff_is_franchise_id_tags();
    $names = array_flip(_ff_is_franchise_name_tags());
    return $names[$tags[$franchise]];
}

/**
 * Gets info about a promo code from the spreadsheet.
 *
 * @param $zip string
 * @param $promo string
 * @param $use_cache boolean
 */
function _ff_get_promo_item($zip, $promo, $use_cache = true) {
    $items = array();
    $key = '_ff_promo_item_' . $zip . '_' . $promo;

    if ($use_cache) {
        $items = get_transient($key);
    }

    if (empty($items)) {
        $spreadsheet_url = 'https://docs.google.com/spreadsheets/d/1WBaUStw5wWErj-7MPUEvn5bDI_4xxP_4i883e8Sgs4o/pub?gid=0&single=true&output=csv';
        if(!ini_set('default_socket_timeout', 15)) {
            error_log('Cannot set timeout.');
            return;
        }

        if (($handle = fopen($spreadsheet_url, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                if (strtolower($data[0]) == strtolower($promo)) {
                    $items[] = _ff_priceItem_match($zip, $data[1], $data[2]);
                }
            }
            fclose($handle);
        }
        else {
            error_log("Can't read promo code CSV.");
            return;
        }
        set_transient($key, $items, 604800); // 1 week
    }

    return $items;
}

/**
 * Gets a 'move pack' item for the mover pages
 */
function _ff_movePackMatch($match, $priceListID, $franchise) {
    $key = '_ff_movePack_' . $match . '_' . $priceListID . '_' . $franchise;
    $item = get_transient($key);
    if (empty($item)) {
        $item  = _ff_priceItem_fuzzy_match(null, 'Products', $match, $priceListID, $franchise);
    }
    if (!empty($item) && !empty($item->priceItem)) {
        set_transient($key, $item, 604800); // 1 week
    }
    return $item;
}


/**
 * Gets info about a priceItem based on the zip code and its title
 *
 * @param $zip string
 * @param $priceBlock string
 * @param $priceItem string
 */
function _ff_priceItem_match($zip, $priceBlockMatch, $priceItemMatch, $priceListID = 0, $franchise = null, $retry=true) {

    $pItem = (object) array( 'priceItem' => FALSE );

    // authenticate through vonigo API
    try {
        $co = new frogbox();
    }
    catch (\Frogbox\LockException $e) {
        $lockError = new stdClass;
        $lockError->errMsg = 'Unable to get lock ' . __LINE__;
        return json_decode(Frogbox\FF_Router::routeError('There was an error processing your order.', true, $lockError, false, __LINE__));
    }

    // get the free box rental priceItem for this zone
    if (!$priceListID) {
        $zvRequest = $co->validateZipCode($zip);
        if ($zvRequest->errNo != 0) {
            $error = (object)array('error' => 'invalid zip code');
            $co->releaseLock();
            return $error;
        }
        $franchise = $zvRequest->Ids->franchiseID;
        foreach ($zvRequest->ServiceTypes as $serviceType) {
            if ($serviceType->typeID == 8) { // frogbox
                $priceListID = $serviceType->priceID;
                break;
            }
        }
    }

    if ($priceListID) {
        $fRequest = $co->session($franchise);
        if ($fRequest->errNo != 0) {
            $co->releaseLock();
            $co->clearSecurityToken();
            if ($retry) {
                return _ff_priceItem_match($zip, $priceBlockMatch, $priceItemMatch, $priceListID, $franchise, false);
            }
            $errObj = (object) array('errMsg' => 'Couldn not get priceblocks: ' . $franchise, 'element' => 'delivery-city', 'request' => $fRequest);
            return json_decode(Frogbox\FF_Router::routeError('Could get products. ', true, $errObj, true, __LINE__));
        }

        $pItem->priceListID = $priceListID;
        // get priceblocks for this pricelist
        $priceBlocks = $co->priceBlocks( $priceListID );

        if (!empty($priceBlocks->PriceBlocks)) {
            foreach ( $priceBlocks->PriceBlocks as $priceBlock ) {
                if ( stripos( $priceBlock->priceBlock, $priceBlockMatch ) !== FALSE && Vonigo::isTrue( $priceBlock->isActive ) ) {
                    $pBlock = $priceBlock;
                    $pItem->priceBlock = $priceBlock->priceBlockID;

                    // get items for this priceblock
                    $priceItems = $co->allPriceItems( $pBlock->priceListID, $pBlock->priceBlockID );
                    if ( ! empty( $priceItems->PriceItems ) ) {
                        foreach ( $priceItems->PriceItems as $priceItem ) {
                            if ( stripos( $priceItem->priceItem, $priceItemMatch ) !== FALSE ) {
                                $pItem = $priceItem;
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
    $co->releaseLock();

    return $pItem;
}


/**
 * Gets info about a priceItem based on the zip code and its title
 *
 * @param $zip string
 * @param $priceBlock string
 * @param $priceItem string
 */
function _ff_priceItem_fuzzy_match($zip, $priceBlockMatch, $priceItemMatch, $priceListID = 0, $franchise = null) {

    $pItem = (object) array( 'priceItem' => FALSE );

    // authenticate through vonigo API
    try {
        $co = new frogbox();
    }
    catch (\Frogbox\LockException $e) {
        $lockError = new stdClass;
        $lockError->errMsg = 'Unable to get lock ' . __LINE__;
        return json_decode(Frogbox\FF_Router::routeError('There was an error processing your order.', true, $lockError, false, __LINE__));
    }

    // get the free box rental priceItem for this zone
    if (!$priceListID) {
        $zvRequest = $co->validateZipCode($zip);
        if ($zvRequest->errNo != 0) {
            $error = (object)array('error' => 'invalid zip code');
            $co->releaseLock();
            return $error;
        }
        $franchise = $zvRequest->Ids->franchiseID;
        foreach ($zvRequest->ServiceTypes as $serviceType) {
            if ($serviceType->typeID == FROGBOX_SERVICE_TYPE) { // frogbox
                $priceListID = $serviceType->priceID;
                break;
            }
        }
    }

    $priceItemParts = explode(' ', $priceItemMatch);

    if ($priceListID) {
        $fRequest = $co->session($franchise);
        if ($fRequest->errNo != 0) {
            $errObj = (object) array('errMsg' => 'Could not get priceblocks: ' . $franchise, 'element' => 'delivery-city', 'request' => $fRequest);
            $co->releaseLock();
            $co->clearSecurityToken();
            return json_decode(Frogbox\FF_Router::routeError('Could get products. ', true, $errObj, true, __LINE__));
        }

        $pItem->priceListID = $priceListID;
        // get priceblocks for this pricelist
        $priceBlocks = $co->priceBlocks($priceListID);

        if (!empty($priceBlocks->PriceBlocks)) {
            foreach ($priceBlocks->PriceBlocks as $priceBlock) {
                if (stripos($priceBlock->priceBlock, $priceBlockMatch) !== FALSE && Vonigo::isTrue($priceBlock->isActive)) {
                    $pBlock = $priceBlock;
                    $pItem->priceBlock = $priceBlock->priceBlockID;

                    // get items for this priceblock
                    $priceItems = $co->allPriceItems($pBlock->priceListID, $pBlock->priceBlockID);
                    if (!empty($priceItems->PriceItems)) {
                        foreach ($priceItems->PriceItems as $priceItem) {
                            $matchCount = 0;
                            foreach ($priceItemParts as $part) {
                                if (stripos($priceItem->priceItem, $part) !== FALSE) {
                                    $matchCount++;
                                }
                                if ($matchCount == count($priceItemParts)) {
                                    $pItem = $priceItem;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    $co->releaseLock();

    return $pItem;
}
