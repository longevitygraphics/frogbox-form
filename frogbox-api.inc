<?php

// Maxmind GeoIP2 geolocation service
require 'vendor/autoload.php';
use GeoIp2\WebService\Client;
define('GEO2IP_ID', 102529);
define('GEO2IP_KEY', 'OT1qcV684oRM');

class frogboxAutoloader {
    public static function loader ($class) {
        {
            $filename = $class . '.php';
            $file = __DIR__ . '/src/' . $filename;
            if (!file_exists($file)) {
                return false;
            }
            include $file;
        }
    }
}
spl_autoload_register('frogboxAutoloader::loader');

/**
 *
 * Child class of Vonigo connection for Frogbox-specific functionality on 
 * wordpress.
 */
class frogbox extends vonigo {

    private $hash = '';
    private $lock = '';
    public $stripe_locations = ['2'];

    /**
     * Retrieves credentials from the wordpress database and uses them to
     * connect to Vonigo.
     */
    public function __construct() {

        $this->lock = new \Frogbox\Lock();
        $this->hash = $this->lock->lock();
        $pool = new \Frogbox\CredentialsPool();
        $settings = $pool->getCredentials($this->hash);

        parent::__construct($settings);
    }

    public function releaseLock() {
        if (!empty($this->lock)) {
            $this->lock->unlock();
        }
        $this->lock = null;
    }

    public function getZoneID($zip) {
        $info = $this->validateZipCode($zip);
        if (!empty($info->Ids->zoneID)) {
            return $info->Ids->zoneID;
        }
        return false;
    }

    public function getFranchiseByZip($zip) {
        $info = $this->validateZipCode($zip);
        if (!empty($info->Ids->franchiseID)) {
            return $info->Ids->franchiseID;
        }
    }

    public function clearSecurityToken() {
        $this->setSecurityToken(null);
    }

    /**
     * Override priceItems method
     */
    public function allPriceItems($priceListID=NULL, $priceBlockID=NULL, $pageNo=NULL, $pageSize=NULL) {
        $params = array(
            'method' => 0,
            'pageSize' => 1000,
        );
        if (!empty($priceListID)) {
            $params['priceListID'] = $priceListID;
        }
        if (!empty($priceBlockID)) {
            $params['priceBlockID'] = $priceBlockID;
        }
        if (!empty($pageNo)) {
            $params['pageNo'] = $pageNo;
        }
        if (!empty($pageSize)) {
            $params['pageSize'] = $pageSize;
        }
        return $this->resources('priceItems', $params);
    }

    private function securityTokenKey() {
        return sprintf('_ff_security_token_%s', $this->hash);
    }

    /**
     * Gets the security token from the WordPress database.
     */
    protected function getSecurityToken() {
        // get the setting
        global $wpdb;
        $query = sprintf("SELECT option_value, %d, '%s' FROM {$wpdb->prefix}options WHERE {$wpdb->prefix}options.option_name='%s';",  getmypid(), microtime(), $this->securityTokenKey() );
        $results =  $wpdb->get_results($query, OBJECT);
        if (empty($results)) {
            return false;
        }

        $object = unserialize($results[0]->option_value);

        $now = time();
        // if we don't have a proper object, return
        if (!empty($object->expire) && $object->expire > $now + 1) {
            // if it exist and won't expire in the next second, return it
            return $object->token;
        }
        return false;
    }

    /**
     * Sets the Vonigo security token and saves it to the WordPress database.
     *
     * @param int $token - the Vonigo security token
     */
    protected function setSecurityToken($token) {
        global $wpdb;
        $expire = (empty($token)) ? 1 : time() + 300;
        $encoded =  serialize((object) array('token' => $token, 'expire' => $expire));
//        $query = sprintf("UPDATE {$wpdb->prefix}options set option_value='%s' where option_name='%s'", $encoded, $this->securityTokenKey());

        $query = sprintf("INSERT INTO {$wpdb->prefix}options values (null, '%s', '%s', 'no') ON DUPLICATE KEY UPDATE option_value='%s'", $this->securityTokenKey(), $encoded, $encoded);

        $result = $wpdb->get_results($query, OBJECT);
        return true;
    }
 
    /**
     * Handles an error from a Vonigo request
     *
     * @param array $data the Vonigo result
     * @param bool $die whether or not to quit
     */
    protected function error($data, $die = FALSE) {
        error_log('Vonigo error: ' . json_encode($data));
        if ($die) {
            $this->backupRedirect();
        }
    }

    /**
     * Redirects the user to the Vonigo backup form
     */
    private function backupRedirect() {
        wp_redirect('http://ordering.frogbox.com/external/', 302);
        die;
    }

    /**
     * Returns an array of franchise names keyed by franchise id.
     */
    public function listFranchises() {
        $franchises = array();
        $franchiseResult = $this->franchises();
        if (!empty($franchiseResult->Franchises)) {
            foreach ($franchiseResult->Franchises as $f) {
                $franchises[$f->franchiseID] = $f->name;
            }
        }
        // rearrange : 16, 15, 2, the rest
        $first = array('16' => $franchises[16], '15' => $franchises['15'], '2' => $franchises[2], '-' => '-');
//        unset($franchises[16], $franchises[15], $franchises[2]);
        return $first + $franchises;
    }
    
    /**
     * Gets the standard appointment window for a given franchise. Defaults to 
     * 2 hours.
     *
     * @param $franchiseID int - the franchise ID of the franchise
     * @return int - the standard appointment window for the franchise, in minutes
     */
    public static function getWindow($franchiseID) {
        // duration must be visible by 30; 
        $window = 120;
        // Vancouver (2), Toronto(15), Puget Sound (16) and Kitchener-Waterloo (21) get 3-hour windows
        if (in_array($franchiseID, array(2, 15, 16, 21))) {
          $window = 180;
        }
        return $window;
    }
    
    /**
     * Converts route ID to an optionID;
     * 
     * @param $franchiseID int - the franchise ID of the franchise
     * @param $routeID int - the route ID to look up
     *
     * @return int - the optionID of the route
     */
    public function getRouteOption($franchiseID, $routeID, $use_cache = true) {
        $key = '_ff_routes_' . $franchiseID;

        $routes = null;
        if ($use_cache) {
            $routes = get_transient($key);
        }

        if (empty($routes->Routes)) {
            $routes = $this->routes();
            if ($routes->errNo != 0) {
                set_transient($key, $routes, 604800); // 1 week
            }
        }

        if (empty($routes->Routes)) {
            return false;
        }

        foreach ($routes->Routes as $route) {
            if ($route->routeID == $routeID) {
                return $route->optionID;
            }
        }

        // if we have got this far and haven't found the route id, try again
        if ($use_cache) {
            return $this->getRouteOption($franchiseID, $routeID, false);
        }

        return false;
    }

    /**
     * Gets an objectID from the relations of a particular type of object.
     */
    public static function getRelationID($object, $objectType) {
      foreach ($object->Relations as $relation) {
        if ($relation->objectTypeID == $objectType) {
          return $relation->objectID;
        }
      }
      return false;
    }

    /**
     * requests a response from the wp cache or else from vonigo
     */
    private function cacheWrapper($key, $method, $param, $use_cache = true) {
        $object = null;

        if ($use_cache) {
            $object = get_transient($key);
        }

        if (empty($object)) {
            $object = call_user_func(array('vonigo', $method), $param);
            if ($object->errNo == 0) {
                set_transient( $key, $object, 604800 ); // 1 week
            }
        }
        return $object;
    }


    /**
     * Retrieves zip code stuff from cache or Vonigo.
     */
    public function validateZipCode($zip) {
        $key = '_ff_validateZipCode_' . $zip;
        return $this->cacheWrapper($key, 'validateZipCode', $zip);
    }

}

function _ff_formData_to_vClient($clientData) {

  $how_hear_options = array (
    'Online Search' => 10221,
    'Fall Campaign1' => 10222,
    'You saw a FROGBOX truck' => 10223,
    'Craigslist/kijiji or other online classified' => 10224,
    'Someone told you about us' => 10225,
    'Used FROGBOXES at work' => 10226,
    'Repeat customer' => 10227,
    'EPIC show' => 10228,
    'Other' => 10229,
    'At eco fair or Home Show' => 10230,
    'Mover referred you' => 10231,
    "Saw us on Dragons' Den" => 10232,
    'Facebook/Twitter/Other Social Media' => 10233,
    'Realtor' => 10234,
    'Hardware Store' => 10235,
    'Property Manager' => 10236,
    'Rent-a-son25' => 10237,
    'YYCBEYOND25' => 10609,
  );
 
  // how did you hear
  if (empty($clientData['how-did-you-hear'])) {
    $how_hear = $how_hear_options['Other'];
  }
  else {
    $how_hear = $how_hear_options[$clientData['how-did-you-hear']];
  }

  // normalize phone number
  $clientData['contact-primary-phone'] = str_replace(array(' ', '(', ')', '-'), '', $clientData['contact-primary-phone']);
  $clientData['contact-alternate-phone'] = str_replace(array(' ', '(', ')', '-'), '', $clientData['contact-alternate-phone']);

  $notes = '';

  // normalize extensions
  if (!empty($clientData['contact-primary-ext'])) {
    if (preg_match('/(\d{1,4})/', $clientData['contact-primary-ext'], $matches) 
      &! preg_match('/\d{5}/', $clientData['contact-primary-ext'])) {
      $clientData['contact-primary-phone'] .= 'X' . $matches[1];
    }
    else {
      $notes .= 'Primary phone extension: ' . $clientData['contact-primary-ext'];
    }
  }
  
  if (!empty($clientData['contact-alternate-ext'])) {
    if (preg_match('/(\d{1,4})/', $clientData['contact-alternate-ext'], $matches)
        &! preg_match('/\d{5}/', $clientData['contact-alternate-ext'])) {
      $clientData['contact-alternate-phone'] .= 'X' . $matches[1];
    }
    else {
      $notes .= 'Alternate phone extension: ' . $clientData['contact-alternate-ext'];
    }
  }

  $fields = array(
    array(
      'fieldID' => 126, 
      'fieldValue' => $clientData['contact-last'] . ', ' . $clientData['contact-first'],
      'optionID' => 0,
    ),
    array(
      'fieldID' => 130,
      'fieldValue' => $clientData['contact-first'] . ' ' . $clientData['contact-last'],
      'optionID' => 0,
    ),
    array(
      'fieldID' => 206,
      'fieldValue' => $clientData['contact-primary-phone'],

      'optionID' => 0,
    ),
    array(
      'fieldID' => 112,
      'fieldValue' => $clientData['contact-alternate-phone'],
      'optionID' => 0,
    ),
    array(
      'fieldID' => 239,
      'fieldValue' => $clientData['contact-email'],
      'optionID' => 0,
    ),
    array(
      'fieldID' => 121, // client type
      'optionID' => 59, // residential
    ),
    array(
      'fieldID' => 795, // how did you hear
      'optionID' => $how_hear, // defaults to other
    ),
    array(
      'fieldID' => 108, // notes
      'fieldValue' => $notes,
    ),
  );

  return $fields;
}


/**
 * Generate a key for a priceitem.
 */
function _ff_priceItemKey($priceList, $priceBlock, $object) {
  $period = '';
  if (preg_match('/(\d*)\s?week/i', $priceBlock, $matches)) {
    $period = $matches[1];    
  }
  
  $priceBlock = strtolower($priceBlock);
  if (strpos($priceBlock, 'week') > -1) {
    $priceBlock = 'rentals';
    if (!empty($period)) {
      $priceBlock .= '-' . $period;
    }
  }
  
  return implode(':', array($priceList, $priceBlock, $object->sequence));
}


/**
 * Get a franchise based on IP address
 */
function _ff_geolocate($co, $use_cache = true) {

  $ip_address = $_SERVER['REMOTE_ADDR'];
//    $ip_address = '174.6.113.84'; // Phil's IP for testing
  $key = 'frogbox_geolocate_' . $ip_address;
  $return = null;
  
  if ($ip_address == '127.0.0.1') {
    return $return;
  }
  
  if ($use_cache) { 
    $return = get_transient($key);
  }

  if (!empty($return)) {
    return json_encode($return);
  }
  $return = new stdClass;


  try {
    $client = new Client(GEO2IP_ID, GEO2IP_KEY);
    $record = $client->city($ip_address);
  }
  catch (Exception $e) {
    error_log('Could not geolocate: ' . $e->getMessage());
    return json_encode($return);
  }
  $return->place = $record->city->name;
  $return->postCode = $record->postal->code;

  // no need to lock vonigo or authenticate because we are inside a lock already
  // get the franchise id from the zip code;
  if (!empty($return->postCode)) {
    $zipRequest = $co->validateZipCode($return->postCode);
    if ($zipRequest->errNo != 0) {
      error_log('ff_geolocate: Vonigo error: ' . print_r($zipRequest, true));
                 return false;
    }
    $return->franchise = $zipRequest->Ids->franchiseID;
  }

    if (empty($return->franchise)) {
    $return->postCode = null;
    $return->place = null; 
  }

  // save transient
  set_transient($key, $return, FF_TRANSIENT_EXPIRE);

  return json_encode($return);
}

/**
 * Load zone object from wordpress cache or file
 */
function _ff_load_zones($use_cache = true) {
  $key = '_ff_zone_zips';
  $data = array();
  
  if ($use_cache) {
    $data = get_transient($key);
  }
   
  if (empty($data)) {
      $spreadsheet_url = 'https://docs.google.com/spreadsheets/d/1nb0nVYeEkUkzGYwhDGv3WUn_YKRJfmsiuFM632pweJU/pub?output=csv';
      if ( ! ini_set( 'default_socket_timeout', 15 ) ) {
          error_log( 'Cannot set timeout.' );
          return;
      }

      if ( ( $handle = fopen( $spreadsheet_url, "r" ) ) !== FALSE ) {
          while ( ( $line = fgetcsv( $handle, 1000, "," ) ) !== FALSE ) {
              // data is already uppercase, but force it just in case that changes one day
              $zone          = trim(strtoupper( $line[7] ));
              if (!empty($zone)) {
                  $data[ $zone ] = $line;
              }
          }
          fclose( $handle );
      } else {
          error_log( "Can't read zip zones CSV." );
          return;
      }
      set_transient( $key, $data, 86400 ); // 1 day
  }
  return $data;
}

/**
 * Returns a delivery charge for a post code
 */
function _ff_service_charge($zone) {
  $zones = _ff_load_zones();
  if (!empty($zones[$zone][5])) {
    return $zones[$zone][5];
  }
  return 0;
}

/**
 * Returns an interfranchise delivery charge for a post code
 */
function _ff_if_service_charge($postCode) {
  $postCode = strtoupper($postCode);
  $zones = _ff_load_zones();
  if (!empty($zones[strtoupper($postCode)][6])) {
    return $zones[$postCode][6];
  }
  return 0;
}

?>
