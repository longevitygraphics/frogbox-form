<?php
/**
 * A lock system to interact with WordPress.
 *
 * Based on cron.helper.php by Abhinav Singh
 *
 * http://abhinavsingh.com/how-to-use-locks-in-php-cron-jobs-to-avoid-cron-overlaps/
 */

class lockHelper {

  private static $pid;

  function __construct() {}
  function __clone() {}

  private static function isrunning() {
    $pids = explode(PHP_EOL, `ps -e | awk '{print $1}'`);
    if (in_array(self::$pid, $pids)) {
      return TRUE;
    }
    return FALSE;
  }
  
  private static function lockname($name) {
    return 'lh_' . $name;
  }

  public static function unlock($name) {
    $lock_option = self::lockname($name);
    update_option($lock_option, null);
    return true;
  }

  public static function lock($name) {    
    $lock_option = self::lockname($name);
    $lock_value = get_option($lock_option);

    if(!empty($lock_value)) {
      // Is running?
      self::$pid = $lock_value;
      if(self::isrunning()) {
        error_log("==" . self::$pid . "== Can't get lock...");
        return false;
      }
      else {
        error_log("==" . self::$pid . "== Previous job died abruptly...");
      }
    }

    self::$pid = getmypid();
    update_option($lock_option, self::$pid);
    $lock_value = get_option($lock_option);
    register_shutdown_function(array('lockHelper', 'unlock'), $name);

    return self::$pid;
  }

}

?>