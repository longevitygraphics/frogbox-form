
<form id="frogbox-form" action="/order-complete" method="POST" novalidate>

    <div id="accordion">

        <div id="step-1-heading" class="step-heading">
            <h2>1 - Location</h2>
        </div>

        <div class="content">
            <div id="step-1" class="step">

            </div>
        </div>


        <div id="step-2-heading" class="step-heading">
            <h2>2 - Number of Boxes</h2>
        </div>

        <div class="content">
            <div id="step-2" class="step">

            </div>
        </div>

        <div id="step-3-heading" class="step-heading">
            <h2>3 - Packing Supplies and Options</h2>
        </div>

        <div class="content">
            <div id="step-3" class="step">

            </div>
        </div>

        <div id="step-4-heading" class="step-heading">
            <h2>4 - Delivery and Pickup</h2>
        </div>

        <div class="content">
            <div id="step-4" class="step">

            </div>
        </div>

        <div id="step-5-heading" class="step-heading">
            <h2>5 - Customer Information</h2>
        </div>

        <div class="content">
            <div id="step-5" class="step">

            </div>
        </div>
    </div>

</form>