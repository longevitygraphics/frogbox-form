<?php
/*
  Template Name: Stripe Template
*/

//CA TEST CARD 4000001240000000

require 'stripe-php/init.php';

if ($_POST) {
  print_r($_POST);
  // Set your secret key: remember to change this to your live secret key in production
// See your keys here: https://dashboard.stripe.com/account/apikeys
  \Stripe\Stripe::setApiKey('sk_test_Ngxpj6aWewWe7fHSqFkj3rXl00RRKdsTfQ');

// Token is created using Stripe Checkout or Elements!
// Get the payment token ID submitted by the form:
  $token = $_POST['stripeToken'];
  $charge = \Stripe\Charge::create([
    'amount' => 999,
    'currency' => 'cad',
    'description' => 'Example charge',
    'source' => $token,
  ]);
  print_r($charge);
  die;
}

?>
<?php get_header(); ?>

<div class="container-fluid container-inside-text">

    <div class="row">
        <div class="col-sm-12 page-header"><h1>Pricing and Order</h1></div>
    </div>

    <div class="row">

        <div class="col-xs-12 col-sm-9">

            <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" id="payment-form">
                <div class="form-row">
                    <label for="card-element">
                        Credit or debit card
                    </label>
                    <div id="card-element">
                        <!-- A Stripe Element will be inserted here. -->
                    </div>

                    <!-- Used to display Element errors. -->
                    <div id="card-errors" role="alert"></div>
                </div>

                <button>Submit Payment</button>
            </form>
            <script src="https://js.stripe.com/v3/"></script>


            <script>
              // Set your publishable key: remember to change this to your live publishable key in production
              // See your keys here: https://dashboard.stripe.com/account/apikeys
              var stripe = Stripe('pk_test_2FhPOaQaT17kAawQdaZDUQlo00I1OjdvOz');
              var elements = stripe.elements();


              // Custom styling can be passed to options when creating an Element.
              var style = {
                base: {
                  // Add your base input styles here. For example:
                  fontSize: '16px',
                  color: '#32325d',
                },
              };

              // Create an instance of the card Element.
              var card = elements.create('card', {style: style, hidePostalCode: true});

              // Add an instance of the card Element into the `card-element` <div>.
              card.mount('#card-element');


              // Create a token or display an error when the form is submitted.
              var form = document.getElementById('payment-form');
              form.addEventListener('submit', function (event) {
                event.preventDefault();

                var other_data = {
                  name: 'Gurpreet Dhanju',
                  address_line1: '7 Burbidge St #103',
                  address_city: 'Coquitlam',
                  address_state: 'BC',
                  address_zip: 'V3K 7B2',
                  address_country: 'CA'
                }

                stripe.createToken(card, other_data).then(function (result) {
                  if (result.error) {
                    // Inform the customer that there was an error.
                    var errorElement = document.getElementById('card-errors');
                    errorElement.textContent = result.error.message;
                  } else {
                    // Send the token to your server.
                    stripeTokenHandler(result.token);
                  }
                });
              });


              function stripeTokenHandler(token) {
                // Insert the token ID into the form so it gets submitted to the server
                var form = document.getElementById('payment-form');
                var hiddenInput = document.createElement('input');
                hiddenInput.setAttribute('type', 'hidden');
                hiddenInput.setAttribute('name', 'stripeToken');
                hiddenInput.setAttribute('value', token.id);
                form.appendChild(hiddenInput);

                // Submit the form
                form.submit();
              }

            </script>


        </div>

      <?php get_sidebar("pricing"); ?>

    </div>

  <?php get_footer(); ?>


