<?php

/**
 * Validates a CAA membership number
 */
function _ff_validate_caa($input) {
	$input = str_replace(' ', '', $input);

	if (substr($input, 0, 3) != '620' || strlen($input) != 16) {
		return false;
	}
	$check_digit = intval(substr($input, -1));
	$data = str_split(substr($input, 0, 15));
	$x = 1;
	$checkstring = '';
	foreach ($data as $digit) {
		if ($x) {
			$digit = (int) $digit * 2;
		}
		$checkstring .= (string) $digit;
		$x = ($x * -1) + 1; // toggle 0 and 1
	}
	$sum = array_sum(str_split($checkstring));
	$result = ($sum + $check_digit) % 10;
	if ($result === 0) {
		return $input;
	}
	else {
		return false;
	}
}
