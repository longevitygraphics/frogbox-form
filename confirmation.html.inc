<?php error_log('confirmation page loaded for order#' . $theme['confirmation'] ); ?>

<div id="order-complete">

    <p>Thank you for your order!</p>

    <p>Your order number is <span id="order-number">#<?php echo $theme['confirmation']; ?></span>

    <p>You will receive an email to <?php echo $theme['contact-email']; ?> confirming your order within 1 hour. If you do not receive this please check your spam filter and call us if it doesn't arrive.</p>

    <p>If you have any questions or need to make a change to your order please contact us or call 1-877-Frogbox.</p>

    <p>The Frogbox Team</p>

    <hr>

    <div id="order-summary">

        <div class="row">
            <div id="your-order" class="col-xs-12 col-sm-6">
                <h2>Your Order</h2>
                <div id="your-price-price">
                    <?php echo $theme['charges']->markup; ?>
                </div>
                <div id="mover-recommendations">
                    <?php echo $theme['mover-recommendations']; ?>
                </div>
            </div>

            <div id="your-price" class="col-xs-12 col-sm-6">
                <h2>Your Price</h2>
                <?php echo $theme['charges']->price; ?>
            </div>
        </div>

        <hr>

        <div class="row">
            <div id="delivery-details" class="address-details col-xs-12 col-sm-6">
                <h2>Delivery Details</h2>
                <div class="appointment-detail">
                    Delivery Date: <span class="appointment-value"><?php echo $theme['deliveryDate']; ?></span>
                </div>
                <div class="appointment-detail">
                    Delivery Time: <span class="appointment-value"><?php echo $theme['deliveryWindow']; ?></span>
                </div>
                <div class="appointment-address">
                    Delivery Address: <span class="address-value"><?php echo $theme['deliveryAddress']; ?></span>
                </div>
            </div>

            <div id="pickup-details" class="address-details col-xs-12 col-sm-6">
                <h2>Pick-up Details</h2>
                <div class="appointment-detail">
                    Pick-up Date: <span class="appointment-value"><?php echo $theme['pickupDate']; ?></span>
                </div>
                <div class="appointment-detail">
                    Pick-up Time: <span class="appointment-value"><?php echo $theme['pickupWindow']; ?></span>
                </div>
                <div class="appointment-address">
                    Pick-up Address: <span class="address-value"><?php echo $theme['pickupAddress']; ?></span>
                </div>
            </div>
        </div>

    </div>

    <hr>

    <?php if(!in_array($theme['delivery-city'], ['2', '16'])) : ?>

        <div class="row">
            <div id="order-payment" class="col-xs-12">
                <h2>Order Payment</h2>
                <p>We will collect payment at the same time we deliver your Frogboxes.</p>
            </div>

            <!--<div id="share-your-move" class="col-xs-12 col-sm-6">
                <h2>Share Your Move With Your Friends</h2>
                <div class="social-media-icons">
                    <span class="social-media-icon twitter"></span>
                    <span class="social-media-icon facebook"></span>
                </div>
            </div>-->
        </div>

        <hr>
    <?php endif; ?>

    <div id="thanks-again">
        <h2>Thank you again for your order!</h2>
        <p>Please <a href="/contact/">contact us</a> if you have any questions or concerns.</p>
        <p>The Frogbox Team</p>
    </div>

</div><!-- #order-complete -->

<script>
    $(document).ready(function() {
        $('#order-summary').hide();
    });
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-4293405-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-4293405-1');
</script>

<!-- Start Conversion Tracking 2018-10-20 - francis@leadeight.com -->
<script>
// GTM EEC Tracking
window.dataLayer = window.dataLayer || [];
window.dataLayer.push({
  event: 'eec.purchase',
  ecommerce: {
    currencyCode: 'CAD',
    purchase: {
      actionField: {
        id: '<?php echo $theme['confirmation']; ?>',
		affiliation	: 'Website',
        revenue: '<?php echo $theme['charges']->subtotal; ?>',
		tax: '<?php echo $theme['charges']->taxes; ?>'
      },
      products: [{
        id: '0',
        price: '<?php echo $theme['charges']->subtotal; ?>',
        quantity: 1
      }]
    }
  }
});
</script>
<script>
    // Facebook Conversion Tracking
    fbq('track', 'Purchase', {
        value: <?php echo $theme['charges']->subtotal; ?>,
        currency: 'CAD'
    });
</script>
<!-- End Conversion Tracking 2018-10-20 - francis@leadeight.com -->