<?php

add_shortcode('frogbox-form', '_ff_frogbox_form');
add_shortcode('frogbox-promo-form', '_ff_frogbox_promo_form');

/**
 * Builds the Frogbox Form to connect to the Vonigo API
 */
function _ff_frogbox_form($atts = array(), $content = null) {
    $submission = null; // set the submission variable to use in confirmation page later

    // if a caa membership # has been passed, validate it again before passing it to the form
    if (! empty($_GET['caa'])) {
        require_once(WP_PLUGIN_DIR . '/frogbox-form/frogbox-caa.inc');
        if (_ff_validate_caa($_GET['caa'])) {
            $submission['promo-code'] = htmlspecialchars('caa');
            $submission['caaID'] = htmlspecialchars($_GET['caa']);
        }
    }

    /* Add the promo code form querystirng if it exists */
    if (!empty($_GET['promo']) && empty($submission)) {
        $submission = array();
        $submission['promo-code'] = htmlspecialchars($_GET['promo']);
    }

    // First check that we are on the right page; redirect to ordering page if there
    // is no POST data unless user is logged in; validate if there is post data, etc

    if (empty($_POST['_ff_submit']) && is_page('order-complete')) {
        if (is_user_logged_in()) {
            include_once(plugin_dir_path(__FILE__) . '/frogbox-form.submit-handler.inc');
            _ff_confirmation_page($submission);
        }
        else {
            wp_redirect(site_url() . '/order');
        }
        return;
    }

    $pageData = array('title' => get_the_title(), 'url' => get_permalink());

    if (!empty($atts['confirmation'])) {
        $pageData['confirmation'] = $atts['confirmation'];
    }
    if (!empty($atts['affiliate'])) {
        $pageData['affiliate'] = $atts['affiliate'];
    }

    if (!empty($_POST['_ff_submit']) && $_POST['_ff_submit'] === 'submit') {
        include_once(plugin_dir_path(__FILE__). '/frogbox-form.submit-handler.inc');
        $submission = new \Frogbox\Submission();
        $validated = $submission->validate();
        if (!empty($validated->valid)) {
            $pageData['valid'] = true;
            $submission->emailConfirmation();
            _ff_confirmation_page($submission->theme());
            return;
        }
        else {
            $pageData['valid'] = false;
            // load invalid submission as js object and continue
            wp_register_script('ff_submission',
                plugins_url('frogbox-form') . '/js/submission.js',
                array('ff_form'),
                FF_VERSION,
                true); // script changes url
            // frogboxcart property needs to be json decoded before being passed back to the form
            $data = $_POST;
            if (!empty($data['pageTitle'])) {
                $pageData['title'] = $data['pageTitle'];
            }
            if (!empty($data['pageUrl'])) {
                $pageData['url'] = $data['pageUrl'];
            }
            $data['frogboxCart'] = json_decode(stripslashes($data['frogboxCart']));
            wp_localize_script('ff_submission', 'Submission', $data);
            wp_localize_script('ff_submission', 'Errors', $validated->errors);
            wp_enqueue_script('ff_submission');
        }
    }

    // First make sure the library is installed
    $path = plugin_dir_path(__FILE__);
    try {
        $file = 'vonigo-api/Vonigo.php';
        if (!file_exists($path . $file)) {
            throw new Exception ('file does not exist: ' . $path . $file);
        }
        else {
            require_once($path . $file);
            require_once($path . 'frogbox-api.inc');
        }
    }
    catch (Exception $e) {
        error_log('Caught exception: ' . $e->getMessage());
        _ff_backup_form();
    }

    // include javascript files
    wp_enqueue_script('ff_jquery.validate.v1.14.1',
        plugins_url('frogbox-form') . '/js/jquery.validate.min.js', null, null, true);
    wp_enqueue_script('ff_jquery.validate_additional_methods.v1.14.1',
        plugins_url('frogbox-form') . '/js/additional-methods.min.js', null, null, true);


    // ip geolocation for standard form
    if (empty($atts['form'])) {
        $ipKey = '_frogbox_franchise_by_ip' . $_SERVER['REMOTE_ADDR'];
        $franchise = get_transient($ipKey);

        if (false && empty($franchise)) {
            $url = sprintf('http://ip-api.com/json/%s', $_SERVER['REMOTE_ADDR']);
            $result = file_get_contents($url);
            $data = json_decode($result);
            $zip = $data->zip;
            // see if the zip code is in seattle
            try {
                $co = new \frogbox();
                $franchise = $co->getFranchiseByZip($zip);
                $co->releaseLock();
                set_transient($ipKey, $franchise, 86400 * 30);

            } catch (\Frogbox\LockException $e) {
                error_log('could not not get lock at ' . __LINE__);
            }
        }

        // use calculator form for seattle & bc:
        // vancouver (2), seattle (16), delta-surrey (17), okanagan (23) or victoria (26)
        if (!empty($franchise) && in_array($franchise, array(2, 16, 17, 23, 26))) {
            $atts['form'] = 'calculator';
        }
        // show the caclulator form to all of BC, which has a postal code starting with V.
        else if (!empty($zip) && strtolower(substr($zip, 0, 1)) == 'v') {
            $atts['form'] = 'calculator';
        }

    }

    // record toronto sessions with inspectlet
    if (!empty($franchise) && $franchise == 15) {
        wp_enqueue_script('inspectlet', plugins_url('frogbox-form') . '/js/inspectlet.js');
    }

    if (!empty($atts['form']) && $atts['form'] == 'mover-order') {

        $validFranchises = array();
        if (!empty($atts['validfranchises'])) {
            $validFranchises = explode(',', $atts['validfranchises']);
        }

        $pricelistCode = 'MOVER';
        if (!empty($atts['pricelistcode'])) {
            $pricelistCode = 'MOVER-' . $atts['pricelistcode'] . '-' . $validFranchises[0];
        }

        $zipPlaceholder = 'e.g. M4X 2A3';
        if (!empty($atts['zipplaceholder'])) {
            $zipPlaceholder = $atts['zipplaceholder'];
        }

        $zipLabelText = 'Your <span class="hoverText" title="Toronto - Mississauga - Brampton - Markham - Vaughan - Richmond Hill - Milton - Newmarket">Greater Toronto Area</span> Postal Code';
        if (!empty($content)) {
            $zipLabelText = $content;
        }

        $trackingCode = '';
        if (!empty($atts['trackingcode'])) {
            $trackingCode = $atts['trackingcode'];
        }

        $extraBoxes = 1;
        if (!empty($atts['extraboxes']) && strtolower($atts['extraboxes']) == 'false') {
            $extraBoxes = 0;
        }

        // mover order form
        $pricelist = Frogbox\FF_Router::getPriceList($pricelistCode, 7);
        wp_register_script('ff_form',
            plugins_url('frogbox-form') . '/js/mover-order-min.js',
            array('jquery'),
            FF_VERSION,
            true); // script changes url

        $moverOptions = array(
            'extraBoxes' => $extraBoxes,
            'PricelistCode' => $pricelistCode,
            'trackingCode' => $trackingCode,
            'zipLabelText' => $zipLabelText,
        );

        wp_localize_script('ff_form', 'moverOptions', $moverOptions);
        wp_localize_script('ff_form', 'Pricelist', $pricelist);
        wp_localize_script('ff_form', 'validFranchises', $validFranchises);
        wp_localize_script('ff_form', 'zipPlaceholder', $zipPlaceholder);

        $includeForm = 'mover-order.html.inc';
    }
    elseif (!empty($atts['form']) && $atts['form'] == 'commercial-order') {
        // commercial order form
        wp_register_script('ff_form',
            plugins_url('frogbox-form') . '/js/commercial-order-min.js',
            array('jquery'),
            FF_VERSION,
            true);
        $includeForm = 'form.html.inc';

    }
    elseif (!empty($atts['form']) && $atts['form'] == 'calculator') {
        // calculator form
        wp_enqueue_script( 'slick_js',
            '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js',
            array('jquery'), '', true );
        wp_register_script('ff_form',
            plugins_url('frogbox-form') . '/js/form-calculator-min.js',
            array('jquery'),
            FF_VERSION,
            true);
        $includeForm = 'form.html.inc';

    }
    else {
        // otherwise enqueue the regular orm
        wp_register_script('ff_form',
            plugins_url('frogbox-form') . '/js/form-min.js',
            array('jquery'),
            FF_VERSION,
            true);
        $includeForm = 'form.html.inc';
    }
    if (is_object($validated)) {
        $failed = get_object_vars($validated);
    }
    wp_localize_script('ff_form', 'Submission', $failed);

    $minimumCharge = 95;
    if (isset($atts['minimumcharge'])) { // isset() instead of !empty() because value may be 0
        $minimumCharge = (int) $atts['minimumcharge'];
    }
    wp_localize_script('ff_form', 'minimumCharge', array('minimumCharge' => $minimumCharge));
    wp_localize_script('ff_form', 'pageData', $pageData);
    wp_enqueue_script('ff_form');

    // include css
    wp_enqueue_style('ff_css', plugins_url('frogbox-form') . '/css/style.css');

    header('Content-type: text/html');
    ob_start();
    include_once($path . '/' . $includeForm);
    return ob_get_clean();
}

/**
 * Redirects to default vonigo form.
 */
function _ff_backup_form() {
    echo '<strong>Something is broken. See error log.</strong>';
}

/**
 * Builds markup for HTML select options based on an associative array, with
 * the array's keys used as each option's value attribute and the array's values
 * used as each option's inner text.
 */
function _ff_associative_to_options($array, $initial = '') {
    $markup = '';
    if (!empty($initial)) {
        $markup .= '<option value="">' . $initial . '</option>' . PHP_EOL;
    }
    foreach ($array as $key => $value) {
        $markup .= '<option value="' . $key . '">' . $value . '</option>' . PHP_EOL;
    }
    return $markup;
}

/**
 * Maps vonigo location object fields to a vonigoAddress object; assumes wordpress
 */
class frogboxAddress extends vonigoAddress {

    public $calculated;
    public $objectID;

    /**
     * Maps vonigo location object to this object's properties
     */
    public function __construct($object = null) {
        $fields = vonigo::mapFieldsArray($object);
        $this->street = $fields[773]->fieldValue;
        $this->city = $fields[776]->fieldValue;
        if (!empty($fields[9312])) {
            $this->province = $fields[9312]->fieldValue;
        }
        if (!empty($fields[9311])) {
            $this->state = $fields[9311]->fieldValue;
        }
        $this->postcode = $fields[775]->fieldValue;
        $this->country = $fields[779]->fieldValue;
        $this->description = $fields[9713]->fieldValue;
    }

    public static $map = array(
        '773' => 'Street',
        '776' => 'City',
        '775' => 'Post code',
        '779' => 'Country',
        '9713' => 'Description',
        '9311' => 'State',
        '9312' => 'Province',
    );

    /**
     * Maps address properties back to fields array;
     */
    public function asFields() {
        $fields = array();

        if (!empty($this->street)) {
            $fields[] = array('fieldID' => 773, 'fieldValue' => $this->street);
            $fields[] = array('fieldID' => 9714, 'fieldValue' => 'Location'); // title
        }
        if (!empty($this->city)) {
            $fields[] = array('fieldID' => 776, 'fieldValue' => $this->city);
        }
        if (!empty($this->province)) {
            $fields[] = array('fieldID' => 9312, 'optionID' => $this->provinceCode());
        }
        if (!empty($this->state)) {
            $fields[] = array('fieldID' => 9311, 'optionID' => $this->stateCode());
        }
        if (!empty($this->postcode)) {
            $fields[] = array('fieldID' => 775, 'fieldValue' => $this->postcode);
        }
        if (!empty($this->country)) {
            $fields[] = array('fieldID' => 779, 'optionID' => $this->countryCode());
        }
        if (!empty($this->description)) {
            $fields[] = array('fieldID' => 9713, 'fieldValue' => $this->description);
        }
        else {
            $fields[] = array('fieldID' => 9713, 'fieldValue' => 'no directions');
        }
        return $fields;
    }

    public static function countryByFranchise($franchiseID) {
        $us = array(29, 16, 18);
        $canada = array(30, 22, 27, 17, 19, 21, 32, 20, 23, 24, 28, 33, 38, 15, 2, 26, 25);
        if (in_array($franchiseID, $us)) {
            return 'United States';
        }
        else {
            return 'Canada';
        }
    }

}

/**
 * Returns an error message in HTML
 */
function _ff_makeErrorCode($element, $message) {
    $error = '';
    $error .= 'errorElement = ' . _ff_formatError($element, $message);
    $error .= "if ($('label[for=\"" . $element . "\"]').length) {" . PHP_EOL;
    $error .= "  $('label[for=\"" . $element . "\"]').append(errorElement);" . PHP_EOL;
    $error .= "} " . PHP_EOL . "else { " . PHP_EOL . "  $(errorElement).insertAfter('#"
        . $element . "');" . PHP_EOL . "}" . PHP_EOL;
    return $error;
}

/**
 * Returns the javascript part of an error mesasge.
 */
function _ff_formatError($element, $errorMessage) {
    $errorID = $element . '-error';
    $placeholderPlace = strpos($errorMessage, '%s');
    $placeholder = '';
    if ($placeholderPlace > -1) {
        $placeholder .= "'+ jQuery('label[for=\"" . $element . "\"]').html().slice(0, -1) + '";
        $errorMessage = str_replace('%s', $placeholder, $errorMessage);
    }

    return "'" . '<div class="error" id="' . $errorID . '">' . $errorMessage . "</div>';" . PHP_EOL;
}

/**
 * Builds a small promo code form to add to partner pages.
 */
function _ff_frogbox_promo_form($attributes) {
    // default button text
    $form_attributes = shortcode_atts(array(
        'button' => 'Submit',
        'placeholder' => 'Enter your promo code',
    ), $attributes);

    $form = '<div class="col-sm-6 col-sm-offset-3">';
    $form .= '<form action="/order" method="get">';
    $form .= '<div class="form-group row">';
    $form .= '<div class="col-md-4"><label class="control-label" for="promo" >Promo Code</label></div>';
    $form .= '<div class="col-md-8"><input class="form-control" id="promo" type="text" name="promo" placeholder="' . $form_attributes['placeholder'] . '"></div>';
    $form .= '</div>';
    $form .= '<button class="btn btn-default" type="submit">' . $form_attributes['button'] . '</button>';
    $form .= '</form>';
    $form .= '</div>';
    return $form;
}

?>
