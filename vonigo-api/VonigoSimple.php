<?php

class VonigoSimple extends vonigo implements VonigoInterface {

    /**
     * @param $objectID = objectID of a record to be deactivated
     * @param method = method to call to deactivate a record
     */
    private function activateRecord($objectID, $method) {
        $params = array(
            'method' => 6,
            'objectID' => $objectID,
        );
        return call_user_func(array($this, $method), $params);
    }

    /**
     * @param $objectID = objectID of a contact2 record
     **/
    public function activateClient($objectID) {
        return $this->activateRecord($objectID, 'clients');
    }

    /**
     * @param $objectID = objectID of a contact2 record
     **/
    public function activateContact2($objectID) {
        return $this->activateRecord($objectID, 'contacts2');
    }

    /**
     * @param $objectID = objectID of a record to be deactivated
     * @param method = method to call to deactivate a record
     */
    private function deactivateRecord($objectID, $method) {
        $params = array(
            'method' => 5,
            'objectID' => $objectID,
        );
        return call_user_func(array($this, $method), $params);
    }

    /**
     * @param $objectID = objectID of a contact2 record
     **/
    public function deactivateContact2($objectID) {
        return $this->deactivateRecord($objectID, 'contacts2');
    }

    /**
     * @param $objectID = objectID of a client record
     **/
    public function deactivateClient($objectID) {
        return $this->deactivateRecord($objectID, 'clients');
    }

    /**
     * @param $workorderID = objectID of a workorder record
     */
    public function getWorkorderCharges($workorderID) {
        $params = array(
            'method' => 0,
            'workOrderID' => $workorderID,
        );
        return $this->charges($params);
    }

    public function getCharge($chargeID) {
        return $this->getRecordbyID('charges', $chargeID);
    }

    /**
     * @param $clientID
     */
    public function getClient($clientID) {
        return $this->getRecordbyID('clients', $clientID);
    }

    /**
     * @param $contactID
     * @return mixed
     */
    public function getContact($contactID) {
        return $this->getRecordbyID('contacts', $contactID);
    }

    /**
     * @param $contactID
     * @return mixed
     */
    public function getContact2($contactID) {
        return $this->getRecordbyID('contacts2', $contactID);
    }

    /**
     * @param $objectID
     */
    public function getJob($objectID) {
        return $this->getRecordbyID('jobs', $objectID);
    }

    /**
     * @param $locationID
     */
    public function getLocation($locationID) {
        return $this->getRecordbyID('locations', $locationID);
    }

    /**
     * @param $quoteID
     */
    public function getQuote($quoteID) {
        return $this->getRecordbyID('quotes', $quoteID);
    }

    /**
     * @param $method
     * @param $objectID
     * @return mixed
     */
    private function getRecordByID($method, $objectID) {
        $params = array('method' => 1, 'objectID' => $objectID);
        return call_user_func(array($this, $method), $params);
    }

    /**
     * Returns an associative array of service types for this instance.
     */
    public function getServiceTypes() {
        $serviceTypes = $this->serviceTypes();
        $return = array();
        foreach ($serviceTypes->ServiceTypes as $serviceType) {
            if ($this->isTrue($serviceType->isActive)) {
                $return[$serviceType->serviceTypeID] = $serviceType->serviceType;
            }
        }
        return $return;
    }

    /**
     * @param $workOrderID
     */
    public function getWorkOrder($workOrderID) {
        return $this->getRecordByID('workorders', $workOrderID);
    }


}
