<?php
/**
 * @file checkProduct.php
 *
 * Check a product ID to see what vonigo says about it.
 *
 * use: checkProduct.php priceItemID 23130
 *      checkProdcut.php priceItem '1 bedroom minimalist'
 */

$key = $argv[1];
$value = $argv[2];

require_once('../vonigo-api/vonigo_api.class');
require_once('../frogbox-api.inc');

// get products for 99801, 99802

  $prodSettings = array(
      'base_url' => 'https://frogbox.vonigo.com/api/v1',
      'username' => 'website.order',
      'company' => 'Frogbox',
      'password' => 'website.order',
  );

  $co = new Frogbox($prodSettings);
  $co->authenticate();
  $co->serviceTypeID = 8;

  $zips = array(99801, 99802);

  $items = array();

  foreach ($zips as $zip) {
    $zvRequest = $co->validateZipCode($zip);
print_r($zvRequest);
    foreach ($zvRequest->ServiceTypes as $st) {
      if ($st->typeID == $co->serviceTypeID) {
        $pricelist = $st->priceID;
        break;
      }
    }  
    $franchise = $zvRequest->Ids->franchiseID;          
    $sRequest = $co->session($franchise);

    $pbRequest = $co->priceBlocks($pricelist);
    foreach ($pbRequest->PriceBlocks as $pb) {
if ($pb->priceListID == 385 && $pb->priceBlockID == 627) {
  $co->setDebug(1);
}
      $piRequest = $co->priceItems($pb->priceListID, $pb->priceBlockID);
$co->setDebug(0);
      if (count($piRequest->PriceItems)) {
echo PHP_EOL . count($piRequest->PriceItems) .PHP_EOL;
        foreach ($piRequest->PriceItems as $pi) {
          if (strpos(strtolower($pi->{$key}), $value) > -1) {
echo $pb->priceListID;
echo $pb->priceBlockID;
            print_r($pi);
          }
        }
      }
    }
  }

?>
