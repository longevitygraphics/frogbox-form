<?php
/**
 * @file suburbs2json.php
 *
 * Converts spreadsheet of suburbs to json data.
 *
 * Usage: php suburbs2json.php > ../js/suburbs.jsonp
 */
$flag = '';
if (!empty($argv[1])) {
  $flag = $argv[1];
}

$file = '../data/20_zone_zips.csv';

$franchises = array_flip(array(
29 => 'Boise',
30 => 'Calgary',
22 => 'Durham Region',
27 => 'Edmonton',
19 => 'Halifax',
21 => 'Kitchener-Waterloo',
32 => 'Lethbridge',
20 => 'London',
23 => 'Okanagan',
24 => 'Ottawa',
16 => 'Seattle',
28 => 'Regina',
33 => 'Saskatoon',
17 => 'Surrey-Delta',
38 => "St. John's",
15 => 'Toronto',
18 => 'Minneapolis',
2 => 'Vancouver',
26 => 'Victoria',
25 => 'Winnipeg',
));

$franchises['Hamilton- Oakville'] = 21;
$franchises['Mississaugua'] = 15;
$franchises['Mississauga'] = 15;
$franchises['Abbotsford-Langley'] = 17;

$suburbs = array();

$fh = fopen($file, 'r');
while ($line = fgetcsv($fh)) {
  if (!empty($line[0])) {
    if ($line[0] != 'Franchise') {
      $key = $franchises[$line[0]];
      $suburbs[$key][$line[1]] = $line[3];
    }
  }
}

$data = array();

foreach ($suburbs as $key => $options) {
  $markup = '<option value="">(select Area)</option>';
  foreach ($options as $burb => $zip) {
    $markup .= '<option value="' . $zip . '">' . $burb . '</option>' . PHP_EOL;
  }
  $data[$key] = $markup;
}

if ($flag == '-dev') {
  print_r($data);
}
else {
  echo 'function getSuburbs() { return ' . json_encode($data) . '};';
}

?>
