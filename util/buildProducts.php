<?php
/**
 * @file buildProducts.php
 *
 * Imports products metadata to wordpress.
 *
 * Use -dry flag to output products to screen instead of updating db
 */
  require_once('util.inc');
  require_once('../vonigo-api/Vonigo.php');
  require_once('../frogbox-api.inc');

  define( 'BASE_PATH', find_wordpress_base_path()."/" );
  define('WP_USE_THEMES', false);
  global $wp, $wp_query, $wp_the_query, $wp_rewrite, $wp_did_header;
  require(BASE_PATH . 'wp-load.php');

  $valid_flags = array('-dry', '-v', '-p');
  $command = $argv;
  $flag_commands = array_slice($argv, 1);
  $flags = array();

  foreach ($flag_commands as $flag) {
    if (!empty($flag) && !in_array($flag, $valid_flags)) {
      echo 'Valid flags: ' . PHP_EOL;
      print_r($valid_flags);
      exit;
    }
    elseif (in_array($flag, $valid_flags)) {
      $flags[$flag] = true;
    }
  }

  $co = new frogbox();
  $co->authenticate();
  $co->serviceTypeID = 8;

  $items = array();

  $franchises = $co->franchises();

  foreach ($franchises->Franchises as $fran) {
    $franchise = $fran->franchiseID;
    if ($franchise !=2) { continue; }
    $co->session($franchise);
    $plRequest = $co->pricelists();

    foreach ($plRequest->PriceLists as $pl) {
      $short = substr($pl->priceList, 0, 3);
      if (in_array($short, array('105'))) {
        if ($flags['-p']) {
          echo $fran->name . ' : ' . $pl->priceList . PHP_EOL;
          continue;
        }
        $more = getItems($pl->priceListID, $pl->priceList, $co);
        foreach ($more as $key => $values) {
          if (is_array($items[$key])) {
            $items[$key] = array_merge($items[$key], $values);
          }
          else {
            $items[$key] = $values;
          }
        }
      }
    }
  }

  // combine similar items
  if (!empty($items['Wardrobe']) && !empty($items['FROGBOX Wardrobe'])) {
    $items['Wardrobe'] = $items['Wardrobe'] + $items['FROGBOX Wardrobe'];
    unset($items['FROGBOX Wardrobe']);
  }

  if ($flags['-dry']) {
    ksort($items);
    foreach ($items as $key => $values) {
      $values = array_unique($values);
      echo $key . ' => ' . sizeof($values) . PHP_EOL;
      if ($flags['-v']) {
        sort($values);
        print_r($values);
      }
    }
    exit;
  }

  $field_key='field_555b9601eb2d6'; // field id for repeater field

  foreach ($items as $title => $ids) {
    $ids = array_unique($ids);
    sort($ids);
    $author_id = 1;
    $slug = str_replace(' ', '-', $title);

    // If the page doesn't already exist, then create it
    $post = get_page_by_title($title, NULL, 'product');
print_r($post);
    if (is_null($post)) {
      // Set the post ID so that we know the post was created successfully
      $post_id = wp_insert_post(
        array(
          'comment_status'  => 'closed',
          'ping_status'   => 'closed',
          'post_author'   => $author_id,
          'post_name'   => $slug,
          'post_title'    => $title,
          'post_status'   => 'publish',
          'post_type'   => 'product'
        )
      );
      $values = array();
      foreach ($ids as $id) {
        $values[] = array('product_ids' => $id); // field name for sub-field
      }
      update_field($field_key, $values, $post->ID);
    } 
    else {
      // if the product exists, simply update the product ids
      $values = array();
      foreach ($ids as $id) {
        $values[] = array('product_id' => $id); // field name for sub-field
      }
      update_field($field_key, $values, $post->ID);
    }
  }

function getItems($pricelist, $priceListName, $co) {

  $items = array();
  $pbRequest = $co->priceBlocks($pricelist);
  foreach ($pbRequest->PriceBlocks as $pb) {
    if ($pb->isActive == 1) {
      $piRequest = $co->priceItems($pb->priceListID, $pb->priceBlockID);
      if (count($piRequest->PriceItems)) {
        foreach ($piRequest->PriceItems as $pi) {
          if ($pi->isActive == 1) {
            $name = _ff_normalizePriceItemName($pi->priceItem);
            // hash up block name, sequence property and value property - maybe don't even need value}
            $key = _ff_priceItemKey($priceListName, $pb->priceBlock, $pi);
            $items[$name][] = $key;
          }
        }
      }
    }
  }
  return $items;
}

?>
