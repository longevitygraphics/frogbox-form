<?php

require_once(__DIR__ . '/util.inc');
require_once(__DIR__ . '/../vonigo-api/Vonigo.php');
require_once(__DIR__ . '/../frogbox-api.inc');

define('BASE_PATH', find_wordpress_base_path()."/");
define('WP_USE_THEMES', false);

global $wp, $wp_query, $wp_the_query, $wp_rewrite, $wp_did_header;
require(BASE_PATH . 'wp-load.php');

// load zip zones document and extract unique zip codes
$zones = _ff_load_zones();

$firstLine = array_shift($zones);

// generate pricelists for 1-4 weeks and cache
foreach ($zones as $zone) {
    $zip = $zone[3];
    $a = FF_Router::validateZip($zip);
    $result = FF_Router::getPriceList($zip, 7);
    $disabled = FF_Router::getDisabledDates($zip);
}

?>
