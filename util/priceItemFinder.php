<?php

require_once('util.inc');

define( 'BASE_PATH', find_wordpress_base_path()."/" );
define('WP_USE_THEMES', false);
global $wp, $wp_query, $wp_the_query, $wp_rewrite, $wp_did_header;
require(BASE_PATH . 'wp-load.php');

require_once('../vonigo-api/Vonigo.php');
require_once('../frogbox-api.inc');
require_once('../frogbox-form.php');

$data = json_decode(stripslashes(file_get_contents('cart.json')));
$keepFor = 28;


$data->supplies = removeEmpties($data->supplies);
$data->bundles = removeEmpties($data->bundles);

//print_r($data);
// get the pricelist for

$priceList = \FF_Router::getPriceList($data->zip, $keepFor, false, false);

$bad = 0;
if (empty($priceList['priceBlock']->priceBlockID)) {
    $bad = 1;
    $errObj = (object) array('element' => 'step-1', 'errMsg' => 'Oops! some pricing was incorrect for your cart. Please review the updated pricing.');
}
else {
    // 3. examine the data in the cart
    $cartItems = $data;

    if ( $cartItems->priceBlock->priceBlockID == $priceList['priceBlock']->priceBlockID ) {
        foreach ( array_merge( $cartItems->bundles, $cartItems->supplies ) as $item ) {
            if ( $item->source == 'bundles' ) {
                if ( $item->priceBlockID != $priceList['priceBlock']->priceBlockID ) {
$bad =2;
$errObj = (object) array(
                        'element' => 'step-1',
                        'errMsg'  => 'Oops! some pricing was incorrect for your cart. Please review the updated pricing..'
                    );
                }
            }
        }
    }
    else {
$bad =3;        $errObj = (object) array(
            'element' => 'step-1',
            'errMsg'  => 'Oops! some pricing was incorrect for your cart. Please review the updated pricing...'
        );
    }
}
print_r($bad);



die;



print_r($pricelist); die;
$transormed = transformPricelist($pricelist);

foreach ($data->supplies as $item) {
    echo $item->priceItemID . ': ' . $item->priceItem . (empty($transformed->supplies[$item->priceItemID]) ? '' : ' not') . ' found' . PHP_EOL;
}

foreach ($data->bundles as $item) {
    echo $item->priceItemID . ': ' . $item->priceItem . (empty($transformed->bundles[$item->priceItemID]) ? '' : ' not') . ' found' . PHP_EOL;
}

if (empty($transformed->supplies[$data->deliveryChargeItem->priceItemID])) {
    echo 'delivery charge item not found' . PHP_EOL;
}
else {
    print_r($transformed->supplies[$data->deliveryChargeItem->priceItemID]);
}
print_r($data->deliveryChargeItem);

die;

////////////////////////

function removeEmpties($list) {
    foreach ($list as $i => $item) {
        if ($item->quantity == 0) {
            unset($list[$i]);
        }
    }
    return $list;
}

function transformPricelist($pricelist) {
    $pricelist['bundles'] = transformPriceBlock($pricelist['bundles']);
    $pricelist['supplies'] = transformPriceBlock($pricelist['supplies']);
    return $pricelist;
}

function transformPriceBlock($priceBlock) {
    $return = array();
    foreach ($priceBlock as $item) {
        $return[$item->priceItemID] = $item;
    }
    return $return;
}