<?php

  require_once('util.inc');
  require_once('../vonigo-api/Vonigo.php');
  require_once('../frogbox-api.inc');
  require_once('../vendor/autoload.php');

  define( 'BASE_PATH', find_wordpress_base_path()."/" );
  define('WP_USE_THEMES', false);
  global $wp, $wp_query, $wp_the_query, $wp_rewrite, $wp_did_header;
  require(BASE_PATH . 'wp-load.php');

  $co = new frogbox;
  $co->authenticate();

  if (!empty($_GET['priceListID'])) {
    $priceListID = htmlspecialchars( strtoupper( $_GET['priceListID'] ) );
  }

  if (!empty($_GET['franchise'])) {
    $franchise = htmlspecialchars( $_GET['franchise'] );
  }

  if (!empty($_GET['priceListName'])) {
      $priceListName = htmlspecialchars( $_GET['priceListName'] );
      echo substr($priceListName, 0, 5);
      if (substr($priceListName, 0, 5) == 'MOVER') {
          $parts = explode('-', $priceListName);
          $priceListID = Frogbox\FF_Router::getPriceListIDByName($parts[1], $parts[2]);
          $franchise = $parts[2];
      }
  }



  if (empty($priceListID)) {
    $zip = htmlspecialchars(strtoupper($_GET['zip']));
    if (!Frogbox\FF_Router::validateZip($zip)) {
     echo $zip . ' is not a valid zip code';
     die;
    }

    echo '<p>Zip: ' . $zip . '</p>';

    // validate the zip/postal code with vonigo
    $zvRequest = $co->validateZipCode($zip);
    $pRequest = $co->priceLists();
    if (empty($zvRequest->Ids->zoneID)) {
     echo 'error getting zone';
    } 
    else echo '<p>Zone ID: ' . $zvRequest->Ids->zoneID . '</p>';

print_r($zvRequest);
     		  	
    foreach ($zvRequest->ServiceTypes as $serviceType) {
      if ($serviceType->typeID == 8) {
        $priceListID = $serviceType->priceID;
        echo 'pricelist: ' . $priceListID . "<br>";
      }
    }
    $franchise = $zvRequest->Ids->franchiseID;
  }

  // set the franchise
  echo '<p>Franchise: ' . $franchise . '</p>';
  $sRequest = $co->session($franchise);

  if (empty($priceListID)) {
    echo 'error getting pricelist id';
    echo pre_print($zvRequest);
  }
  else {
    $found = false;
    $pRequest = $co->priceLists();
    echo 'pricelist info: ';
    foreach ($pRequest->PriceLists as $pl) {
      if ($pl->priceListID == $priceListID) { 
        echo '<pre>'; print_r($pl); echo '</pre>';
        $found = true;
      }
    }
    if (!$found) {
      echo 'looking for ' . $priceListID;
      echo '<pre>';
      print_r($pRequest);
      echo '</pre>';
    }
  }
            
 if ($sRequest->errNo != 0) {
    echo 'error: ' . pre_print($sRequest) . ' ' . __LINE__;
    die;
  }

// retrieve priceblocks from Vonigo
$pbRequest = $co->priceBlocks($priceListID);
if ($pbRequest->errNo != 0 || empty($pbRequest->PriceBlocks)) {
  echo 'error:' . pre_print($pbRequest). ' ' . __LINE__;
  die;
}

// identify the priceblock with the shortest rental time that is still
// greater than or equal to the rental period
$blocks = array();
$productBlock = 0;

$blocks = array();

foreach ($pbRequest->PriceBlocks as $pBlock) {
  if ($pBlock->isActive) {
    $blocks[] = $pBlock;
  }
}

echo '<table cellspacing="4px"><tr>';
echo '<th>Name</th>';
foreach ($blocks as $pBlock) {
  echo '<th>' . $pBlock->priceBlock . '</th>';
}
echo '</tr><tr>';
echo '<td>Price list ID</td>';
foreach ($blocks as $pBlock) {
  echo '<td>' . $pBlock->priceListID . '</td>';
}
echo '</tr><tr>';
echo '<td>Price block ID</td>';
foreach ($blocks as $pBlock) {
echo '<td>' . $pBlock->priceBlockID . '</td>';
}

$items = array();

$maxSequence = 0;
$example = false;
foreach ($blocks as $block) {
  if ($example) {
    $co->setDebug(7);
  }
if ($block->priceBlockID == 880) { $co->setDebug(3); }
  $piRequest = $co->allPriceItems($block->priceListID, $block->priceBlockID);
if ($block->priceBlockID == 880) { $co->setDebug(0); }
  if ($example) {
    $co->setDebug(0);
    $example = false;
  }
  if (!empty($piRequest->PriceItems)) {
    foreach ($piRequest->PriceItems as $pItem) {
      $items[$block->priceBlockID][$pItem->sequence] = $pItem;
      if ($pItem->sequence > $maxSequence) {
        $maxSequence = $pItem->sequence;
      }
    }
  }
}

//error_log(print_r($items, true));

for ($i = 1; $i <= $maxSequence; $i++) {
  echo '</tr><tr>';
  echo '<td>' . $i . '</td>';
  foreach ($blocks as $block) {
    if (!empty($items[$block->priceBlockID][$i]->priceItem)) {
      echo '<td>' . $items[ $block->priceBlockID ][ $i ]->priceItem . '<br>' . $items[ $block->priceBlockID ][ $i ]->value
           . '<br>' . $items[ $block->priceBlockID ][ $i ]->priceItemID . '</td>';

    }
    else {
      echo '<td></td>';
    }
  }
}

echo '</tr></table>';

function pre_print($object) {
  return '<pre>' . print_r($object, true) . '</pre>';
}

?>

