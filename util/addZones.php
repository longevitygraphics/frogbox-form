<?php

require_once('util.inc');
require_once('../vonigo-api/Vonigo.php');
require_once('../frogbox-api.inc');

define( 'BASE_PATH', find_wordpress_base_path()."/" );
define('WP_USE_THEMES', false);
global $wp, $wp_query, $wp_the_query, $wp_rewrite, $wp_did_header;
require(BASE_PATH . 'wp-load.php');

// open the file, read it line by line
$data = file('../data/20_zone_zips.csv');
$output = fopen('../data/zone_data.csv', 'w');

$co = new Frogbox();

// look up the zone for each zip
foreach ($data as $line) {
	$info = explode(',', $line);
	array_pop($info);
	$zip = $info[3];
	$zipInfo = $co->validateZipCode($zip);
	if ($zipInfo->errNo == 0) {
		$info[] = $zipInfo->Ids->zoneID;
		$info[] = $zipInfo->Ids->zone;
	}
	$info[] = PHP_EOL;
	$newline = implode(',', $info);
	fwrite($output, $newline);

}

fclose($output);


// write out the new file
