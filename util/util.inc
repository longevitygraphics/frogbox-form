<?php
  function find_wordpress_base_path() {
    $dir = dirname(__FILE__);
    do {
      //it is possible to check for other files here
      if (file_exists($dir . "/wp-config.php")) {
        return $dir;
      }
    } while( $dir = realpath("$dir/..") );
    return null;
  }
?>
