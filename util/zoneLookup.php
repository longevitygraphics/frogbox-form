<?php

require_once('util.inc');
require_once('../vonigo-api/Vonigo.php');
require_once('../frogbox-api.inc');

define( 'BASE_PATH', find_wordpress_base_path()."/" );
define('WP_USE_THEMES', false);
global $wp, $wp_query, $wp_the_query, $wp_rewrite, $wp_did_header;
require(BASE_PATH . 'wp-load.php');

$zip = $argv[1];

$co = new Frogbox();
$zipInfo = $co->validateZipCode($zip);
print_r($zipInfo);
