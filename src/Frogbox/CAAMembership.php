<?php

namespace Frogbox;


class CAAMembership
{

    private $membershipId;

    private $valid = false;

    public function __construct($membershipId) {
        $this->membershipId = $membershipId;
        $this->valid = $this->validate();
    }

    public function isValid() {
        return $this->valid;
    }

    private function validate() {
        $membershipNumber = str_replace(' ', '', $this->membershipId);

        if (substr($membershipNumber, 0, 3) != '620' || strlen($membershipNumber) != 16) {
            return false;
        }
        $check_digit = intval(substr($membershipNumber, -1));
        $data = str_split(substr($membershipNumber, 0, 15));
        $x = 1;
        $checkstring = '';
        foreach ($data as $digit) {
            if ($x) {
                $digit = (int) $digit * 2;
            }
            $checkstring .= (string) $digit;
            $x = ($x * -1) + 1; // toggle 0 and 1
        }
        $sum = array_sum(str_split($checkstring));
        $result = ($sum + $check_digit) % 10;
        if ($result === 0) {
            return true;
        }
        else {
            return false;
        }
    }

}