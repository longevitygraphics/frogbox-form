<?php

namespace Frogbox;

class Lock {

    private $hash = '';

	public function lock($n = 45) {
        // if we couldn't get a lock, give up
        if ($n == 0) {
            throw new LockException();
        }

	    $pool = new CredentialsPool();
	    $hashes = $pool->getHashes();
	    shuffle($hashes);

	    $locks = array();

	    foreach ($hashes as $hash) {
            $result = $this->readDBLock($hash);
            if (empty($result[0]->option_value)) {
                $this->hash = $hash;
                break;
            }
            $locks[$hash] = $result[0]->option_value;
        }

        $this->breakExpiredLocks($locks);

        if (!empty($this->hash)) {
			// if there is no lock, make one and return true
			$result = $this->createDBLock($this->hash);

			// if the query is successful, $result will be an empty array;
			// it it wasn't successful it means a lock has been created by
			// another process
			if (is_wp_error($result)) {
			    $this->hash = '';
			    if ($n > 0) {
			        sleep(1);
                    return $this->lock($n - 1);
                }
			    error_log('lock exception for ' . $this->hash . ' ' . microtime());
				throw new LockException();
			}

            error_log('locking ' . $this->hash . ' ' . print_r($_SERVER['REQUEST_URI'] . ' ' . microtime(), true));
            return $this->hash;
		}

	    sleep(1);
        return $this->lock($n - 1);

	}

	private function breakExpiredLocks($locks) {
	    $now = time();
	    foreach ($locks as $hash => $time) {
	        if ($now > ($time + 45)) {
	            $this->deleteDBLock($hash);
            }
        }
    }

	private function createDBLock($hash) {
		global $wpdb;
		$query = sprintf("INSERT INTO {$wpdb->prefix}options values(null, '_ff_lock_%s', %s, 'no');", $hash, time());
		return $wpdb->get_results($query, OBJECT);
	}

    private function readDBLock($hash) {
		global $wpdb;
		$query = sprintf("SELECT *, %d, '%s' FROM {$wpdb->prefix}options WHERE {$wpdb->prefix}options.option_name='_ff_lock_%s';", getmypid(), microtime(), $hash );
		return $wpdb->get_results($query, OBJECT);
	}

	private static function deleteDBLock($hash) {
		global $wpdb;
		$query = sprintf("DELETE FROM {$wpdb->prefix}options WHERE {$wpdb->prefix}options.option_name='_ff_lock_%s';", $hash);
		$wpdb->get_results($query, OBJECT);
	}

	public function unlock() {
//		session_write_close();
        error_log('unlocking ' . $this->hash . ' ' . print_r($_SERVER['REQUEST_URI'] . ' ' . microtime(), true));
		self::deleteDBLock($this->hash);
	}

	public static function staticUnlock() {
//		session_write_close();
		self::deleteDBLock(self::hash);
	}

}