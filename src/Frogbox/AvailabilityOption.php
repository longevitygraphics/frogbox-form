<?php

namespace Frogbox;

class AvailabilityOption
{

    /**
     * The option's date, in the format YYYYMMDD.
     *
     * @var
     */
    private $dayID;

    /**
     * The option's route ID.
     *
     * @var
     */
    private $routeID;

    /**
     * The option's start time, a multiple of 30.
     *
     * @var
     */
    private $startTime;

    /**
     * AvailabilityOption constructor.
     * @param $object
     */
    public function __construct($object) {

        $this->dayID = $object->dayID;

        $this->routeID = $object->routeID;

        $this->startTime = $object->startTime;

    }

    /**
     * Formats the object as a stdClass object.
     *
     * @param $duration
     * @return object
     */
    public function asStdClass($duration) {

        $label = $this->makeLabel($duration);

        return (object) array(
            'routeID' => $this->routeID,
            'startTime' => $this->startTime,
            'dayID' => $this->dayID,
            'label' => $label,
        );
    }

    /**
     * Formats the object as an HTML option.
     *
     * @param $duration
     * @return string
     */
    public function formatAsOption($duration) {
        $label = $this->makeLabel($duration);
        return sprintf('<option time="%s" value="%s-%s">%s</option>', $this->startTime, $this->routeID, $this->startTime, $label);
    }

    /**
     * Makes a label to be used with an html option element.
     *
     * @param $duration
     * @return string
     */
    private function makeLabel($duration) {
        $optionTime = strtotime($this->dayID) + (60 * $this->startTime);
        $start = date('g:ia', $optionTime);
        $end = date('g:ia', $optionTime + (60 * $duration));
        return sprintf('%s to %s', $start, $end);
    }

    /**
     * @return int
     */
    public function getDayID()
    {
        return $this->dayID;
    }

    /**
     * @return int
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

}