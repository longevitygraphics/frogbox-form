<?php

namespace Frogbox;

class Promo
{

    const MEASURE_PERCENT = 'percent';

    private $discount = 0;

    private $measure;

    private $request;

    private $threshold = 0;

    private $valid = false;

    public function __construct($request)
    {
        $this->request = $request;

        if (empty($request->Ids)) {
            return;
        }

        $this->discount = $request->Ids->promoDiscount;

        $this->measure = $request->Ids->promoMeasure;

        if (is_numeric($request->Ids->promoAmountIfMoreThen)) {
            $this->threshold = $request->Ids->promoAmountIfMoreThen;
        }

        $this->valid = true;
    }

    public function getDiscount($subtotal)
    {
        $discount = $this->discount;

        if ($subtotal < $this->threshold) {
            $discount = 0;
        }

        return $discount;
    }

    public function getDiscountAmount($subtotal) {
        $discount = $this->getDiscount($subtotal);
        $discountAmount = $discount;

        if ($this->measure === self::MEASURE_PERCENT) {
            $discountAmount = $subtotal * $discount / 100;
        }

        return $discountAmount;
    }

    public function getChargeProperty()
    {
        $type = 'discountAmount';

        if ($this->measure === self::MEASURE_PERCENT) {
            $type = 'discountPercent';
        }

        return $type;
    }

    public function isValid() {
        return $this->valid;
    }

}