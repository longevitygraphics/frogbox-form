<?php

namespace Frogbox;

define('FROGBOX_SERVICE_TYPE', 8);
define('FROGBOX_CLIENT_TYPE', 1);


class FF_Router
{
    const ERROR_SESSION_EXPIRED = -421;
    const ERROR_SESSION_DOES_NOT_EXIST = -422;
    const ERROR_ZONE_VALUE_NOT_SUPPLIED = -7611;
    const ERROR_PROMO_DOES_NOT_EXIST = -7601;
    const ERROR_PROMO_IS_EXPIRED= -7606;

    public function __construct()
    {
        add_action('wp_router_generate_routes', array(
            get_class(),
            'addRoutes'
        ));
    }

    public static function addRoutes(\WP_Router $router)
    {
        $router->add_route('ff-pricelist', array(
            'path' => '^ff/pricelist/(.*?)/(.*?)$',
            'query_vars' => array(
                'zip' => 1,
                'days' => 2,
            ),
            'page_callback' => function ($zip, $days) {
                error_log('getPriceList:' . $zip . ':' . $days);
                echo FF_Router::getPriceList($zip, $days);
            },
            'page_arguments' => array('zip', 'days'),
            'access_callback' => TRUE,
            'template' => false,
        ));
        $router->add_route('ff-all-available', array(
            'path' => '^ff/availability/(.*?)$',
            'query_vars' => array(
                'zip' => 1,
            ),
            'page_callback' => function ($zip) {
                error_log('getAllTimes:' . $zip);
                echo FF_Router::getAllTimes($zip, true);
            },
            'page_arguments' => array('zip'),
            'access_callback' => TRUE,
            'template' => false,
        ));
        $router->add_route('ff-times', array(
            'path' => '^ff/times/(.*?)/(.*?)$',
            'query_vars' => array(
                'zip' => 1,
                'date' => 2,
            ),
            'page_callback' => function ($zip, $date) {
                error_log('getTimes:' . $zip . ':' . $date);
                echo FF_Router::getTimes($zip, $date);
            },
            'page_arguments' => array('zip', 'date'),
            'access_callback' => TRUE,
            'template' => false,
        ));
        $router->add_route('ff-promo', array(
            'path' => '^ff/promo/(.*?)/(.*?)$',
            'query_vars' => array(
                'zip' => 1,
                'promo' => 2,
            ),
            'page_callback' => function ($zip, $promo) {
                error_log('checkPromo:' . $zip . ':' . $promo);
                if (gettype($zip) == 'string' && gettype($promo) == 'string') {
                    $result = FF_Router::checkPromo($zip, $promo);
                    error_log(var_export($result, true));
                    echo $result;
                }
                else {
                    echo self::routeError('Sorry, this promo code is invalid.', true, null, true, __LINE__);
                }
            },
            'page_arguments' => array('zip', 'promo'),
            'access_callback' => TRUE,
            'template' => false,
        ));
        $router->add_route('ff-dates', array(
            'path' => '^ff/dates/(.*?)$',
            'query_vars' => array(
                'zip' => 1,
            ),
            'page_callback' => function ($zip) {
                error_log('getDisabledDates:' . $zip);
                echo FF_Router::getDisabledDates($zip);
            },
            'page_arguments' => array('zip'),
            'access_callback' => TRUE,
            'template' => false,
        ));
        $router->add_route('ff-service-charge', array(
            'path' => '^ff/serviceCharge/(.*?)$',
            'query_vars' => array(
                'zip' => 1,
            ),
            'page_callback' => function ($zip) {
                error_log('getServiceCharge:' . $zip);
                echo FF_Router::getServiceCharge($zip);
            },
            'page_arguments' => array('zip'),
            'access_callback' => TRUE,
            'template' => false,
        ));
        $router->add_route('zip', array(
            'path' => '^ff/zip/(.*?)$',
            'query_vars' => array(
                'zip' => 1,
            ),
            'page_callback' => function ($zip) {
                error_log('validateZipJSON:' . $zip);
                echo FF_Router::validateZipJSON($zip);
            },
            'page_arguments' => array('zip'),
            'access_callback' => TRUE,
            'template' => FALSE,
        ));

	    $router->add_route('ff-payment', array(
		    'path' => '^ff/payment/(.*?)/(.*?)/(.*?)$',
		    'query_vars' => array(
			    'token' => 1,
			    'amount' => 2,
			    'location' => 3
		    ),
		    'page_callback' => function ($token,$amount, $location) {
			    error_log('Payment:' . $token . ': ' . $amount . ': '. $location);
			    echo FF_Router::createCharge($token, $amount, $location);
		    },
		    'page_arguments' => array('token', 'amount','location'),
		    'access_callback' => TRUE,
		    'template' => FALSE,
	    ));
    }

    public static function createCharge($token,$amount, $location){
        $stripeSecretKey = self::getStripeSecretKey($location);
		// Set your secret key: remember to change this to your live secret key in production
		// See your keys here: https://dashboard.stripe.com/account/apikeys
	    \Stripe\Stripe::setApiKey($stripeSecretKey);
	     try{
			 $charge = \Stripe\Charge::create([
			     'amount' => floatval($amount) * 100, //in cents
			     'currency' => self::getStripeCurrency($location),
			     'description' => 'Frogbox',
			     'source' => $token,
			     'capture' => false
		     ]);
			 return json_encode(array('chargeId' => $charge->id));
		 }catch (\Exception $e){
		     return self::routeError('Payment failed. Please try again.', false, null, false, __LINE__);
			//echo json_encode($e->getMessage());
		 }
	}

	public static function getStripeCurrency($location){
		if($location == '2'){
			return 'cad';
		}
		if($location == '16'){
			return 'usd';
		}
	}

	public static function getStripePublicKey($location){
		if($location == '2'){
			return get_option("frogbox_form_settings")['vancouver_public_key'];
		}
		if($location == '16'){
			return get_option("frogbox_form_settings")['seattle_public_key'];
		}
	}

	public static function getStripeSecretKey($location){
		if($location == '2'){
			return get_option("frogbox_form_settings")['vancouver_secret_key'];
		}
		if($location == '16'){
			return get_option("frogbox_form_settings")['seattle_secret_key'];
		}
	}

    /**
     * Makes a key we can use to cache responses from Vonigo's validate zip
     * method.
     *
     * @param $zip string
     * @return string
     */
    public static function makeValidateZipCodeKey($zip)
    {
        return '_frogbox_zip' . $zip;
    }

    public static function getServiceCharge($zip, $retry = true) {
        header('Content-type: application/json');
        header('Cache-Control: max-age=43200'); // okay to cache 12 hours

        // if the first character is numeric, shorten to 5 characters else 3
        $length = is_numeric($zip[0]) ? 5 : 3;
        $zip = substr($zip, 0, $length);

        if (!self::validateZip($zip)) {
            return json_encode(0);
        }

        // authenticate
        try {
            $co = new \frogbox();
        }
        catch (LockException $e) {
            if ($retry) {
                return self::getServiceCharge($zip, false);
            }
            $lockError = new \stdClass;
            $lockError->errMsg = 'Unable to get lock in ' . __FUNCTION__;
            return self::routeError('Unable to get service charge.', false, $lockError, false, __LINE__);
        }

        $zoneID = $co->getZoneID($zip);
        $co->releaseLock();
        // get the delivery charge item from the spreadsheet
        if (!empty($zoneID)) {
            return (int) _ff_service_charge($zoneID);
        }

        $mail = $zoneID === false ? false : true;
        return self::routeError('No Zone ID for ' . $zip, false, null, false, __LINE__, $mail);
    }

    /**
     * Validates a zip code against Vonigo.
     *
     * @param $zip
     * @param bool $use_cache
     *
     * @return mixed|boolean
     */
    public static function validateZipJSON($zip, $use_cache = true, $retry = true)
    {
        header('Content-type: application/json');
        header('Cache-Control: max-age=43200'); // okay to cache 12 hours

        // if the first character is numeric, shorten to 5 characters else 3
        $length = is_numeric($zip[0]) ? 5 : 3;
        $zip = substr($zip, 0, $length);

        if (!self::validateZip($zip)) {
            return json_encode(0);
        }

        $response = array();
        $key = self::makeValidateZipCodeKey($zip);
        if ($use_cache) {
            $response = get_transient($key);
        }

        if (empty($response->Ids->franchiseID)) {

            try {
                $co = new \frogbox();
            }
            catch (LockException $e) {
                $lockError = new \stdClass;
                $lockError->errMsg = 'Unable to get lock in ' . __FUNCTION__;
                if ($retry) {
                    return self::validateZipJSON($zip, $use_cache, false);
                }
                return self::routeError('Unable to disable dates.', false, $lockError, false, __LINE__);
            }

            $zvRequest = $co->validateZipCode($zip);
            if (empty($zvRequest->Ids->franchiseID)) {
                $response = (object)array('franchiseID' => FALSE);
            } else {
                $response = $zvRequest;
            }
            $co->releaseLock();

            set_transient($key, $response, 86400); // keep for 1 day

        }
        //cast response as int
        if (!empty($response->Ids)) {
            return json_encode((int)$response->Ids->franchiseID);
        }
        else {
            return 0;
        }
    }


    /**
     * Validates a zip/postal code.
     *
     * Zip/postal code must be either exactly three characters and match
     * Canadian post code format (A1A) or be exactly 5 numeric characters to
     * match US Zip Code format.
     */
    public static function validateZip($zip)
    {
        /* Validate $zip parameter.
         *
         * Require zip parameter to be either exactly three characters and match
         * Canadian post code format or be exactly 5 numeric characters to match US
         * zip code format.
         */
        $zip = trim(strtoupper($zip));
        if (strlen($zip) == 3 && preg_match('/^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1}/', $zip)) { // valid Canadian post code (1st part)
            return true;
        } elseif (strlen($zip) == 5 && is_numeric($zip)) { // valid US zip code
            return true;
        }
        return false;
    }

    /**
     * Returns a list of disabled dates for a given franchise.
     */
    public static function getDisabledDates($zip, $retry = true) {
        header('Content-type: application/json');
        header('Cache-Control: max-age=43200'); // okay to cache 12 hours

        // validate zip code
        if (!(self::validateZip($zip))) {
            $zipError = new \stdClass;
            $zipError->errMsg = 'Invalid zip code: ' . htmlspecialchars($zip);
            return self::routeError('Unknown Zip/Postal Code. Please check your delivery location.', true, $zipError, false, __LINE__);
        }

        $dates = array();
        $key = '_ff_getDates_' . $zip;
        $dates = get_transient($key);

        if (empty($dates)) {

            // authenticate
            try {
                $co = new \frogbox();
            }
            catch (LockException $e) {
                $lockError = new \stdClass;
                $lockError->errMsg = 'Unable to get lock in ' . __FUNCTION__;
                if ($retry) {
                    return self::getDisabledDates($zip, false);
                }
                return self::routeError('Unable to disable dates.', false, $lockError, false, __LINE__);
            }

            $zipRequest = $co->validateZipCode($zip);
            if ($zipRequest->errNo != 0 || empty($zipRequest->Ids->zoneID)) {
                $co->releaseLock();
                if ($retry && self::sessionError($zipRequest)) {
                    $co->clearSecurityToken();
                    return self::getDisabledDates($zip, false);
                }

                // don't mail if it was a valid zip code
                if (self::validateZip($zip)) {
                    return self::routeError('Unable to disable dates.', false, $zipRequest, true, __LINE__, false);
                } else {
                    return self::routeError('Unable to disable dates.', false, $zipRequest, true, __LINE__ . ' ' . __FUNCTION__ . ' invalid zip ' . $zip);
                }
            }
            $franchise = $zipRequest->Ids->franchiseID;

            $fRequest = $co->session($franchise);
            if ($fRequest->errNo != 0) {
                $co->releaseLock();
                $co->clearSecurityToken();
                if ($retry) {
                    return self::getDisabledDates($zip, false);
                }
                return self::routeError('Unable to disable dates.', true, $fRequest, true, __LINE__ . ' trying to set franchise=' . $franchise);
            }

            // build an array of available dates
            $start = time();
            for ($i = 0; $i < 360; $i++) {
                $time = $start + ($i * 86400);
                $dates[date('Ymd', $time)] = true;
            }

            // get the dates that are available
            for ($i = 0; $i < 12; $i++) {
                $params = array(
                    'dateStart' => $start + ($i * 2592000),
                    'dateEnd' => $start + (($i + 1) * 2592000),
                    'duration' => 30,
                    'zip' => $zip,
                    'serviceTypeID' => 8, // always 8
                );
                $dRequest = $co->availableTimes($params);

                if ($dRequest->errNo != 0) {
                    if ($retry && self::sessionError($dRequest)) {
                        $co->releaseLock();
                        $co->clearSecurityToken();
                        return self::getDisabledDates($zip, false);
                    }
                    $dRequest->Availability = array();
                }

                $availability = new AvailabilityList($dRequest->Availability);
                $enabled = $availability->getEnabledDays();

                foreach ($enabled as $dayID) {
                    if (!empty($dates[$dayID])) {
                        unset($dates[$dayID]);
                    }
                }

            }

            // add today as unavailable
            $dates[date('Ymd', time())] = true;
            $newdates = array();

            foreach ($dates as $date => $bool) {
                $ndate = substr($date, 0, 4) . '-' . substr($date, 4, 2) . '-' . substr($date, 6, 2);
                $newdates[] = $ndate;
            }
            $dates = $newdates;
            set_transient($key, $dates, 3600); // 1 hour
            $co->releaseLock();
        }

        return json_encode($dates);
    }

    /**
     * Retrieves promo code information from Vonigo; cache until midnight only.
     *
     * @param string $zip - the zip code
     * @param string $promo - the promo code
     * @param bool $use_cache - whether or not to cache
     */
    public function checkPromo($zip, $promo, $use_cache = true, $retry = true)
    {
        header('Content-type: application/json');
        header('Cache-Control: max-age=3600'); // okay to cache 1 hour

        // validate zip code
        if (!(self::validateZip($zip))) {
            $zipError = new \stdClass;
            $zipError->errMsg = 'Invalid zip code: ' . htmlspecialchars($zip);
            return self::routeError('Unknown Zip/Postal Code. Please check your delivery location and enter the promo code again.', true, $zipError, false, __LINE__);
        }

        // wp transients API doesn't accept keys longer than 45 characters; limit
        // promo code to 31
        if (strlen($promo) > 31) {
            return self::routeError('Sorry, this promo code is invalid.', true, null, false, __LINE__, false);
        }

        $key = 'frogobx_promo_' . $zip . '_' . $promo;
        $response = null;
        if ($use_cache) {
            $response = get_transient($key);
        }

        if (!empty($response)) {
            return json_encode($response);
        }

        try {
            $co = new \frogbox();
        }
        catch (LockException $e) {
            $lockError = new \stdClass;
            $lockError->errMsg = 'Unable to get lock in ' . __FUNCTION__;
            if ($retry) {
                return self::checkPromo($zip, $promo, $use_cache, false);
            }
            return self::routeError('Unable to disable dates.', false, $lockError, false, __LINE__);
        }

        $zoneID = $co->getZoneID($zip);

        $promoRequest = $co->validatePromo($promo, FROGBOX_SERVICE_TYPE, FROGBOX_CLIENT_TYPE, $zoneID);
        $co->releaseLock();

        if ($promoRequest->errNo != 0) {
            if ($retry && self::sessionError($promoRequest)) {
                $co->clearSecurityToken();
                return self::checkPromo($zip, $promo, $use_cache, false);
            }
            if ($retry && self::zoneValueNotSuppliedError($promoRequest)) {
                $co->clearSecurityToken();
                return self::checkPromo($zip, $promo, $use_cache, false);
            }
            // we don't want to know about expired and non-existent promos
            if (self::promoDoesNotExist($promoRequest) || self::promoIsExpired($promoRequest)) {
                return self::routeError('Sorry, this promo code is invalid.', true, null, true, __LINE__, false);
            }
            if ($promoRequest->errNo <= -640 && $promoRequest->errNo >= -646) {
                return self::routeError('Sorry, this promo code is invalid.', true, null, true, __LINE__, false);
            }
            return self::routeError($promoRequest->errMsg, true, $promoRequest, true, __LINE__);
        }

        // process a valid response
        if (!empty($promoRequest->Ids)) {
            $response = $promoRequest->Ids;
        } else {
            return self::routeError('Sorry, this promo code is invalid.', true, null, true, __LINE__);
        }

        // cache until midnight
        $midnightTomorrow = strtotime('midnight tomorrow') + 28800;
        $expire = $midnightTomorrow - $_SERVER['REQUEST_TIME'];
        if ($response) {
            set_transient($key, $response, $expire);
        }

        return json_encode($response);
    }

    private static function errorKeys($request) {
        if ($request->errNo != -600) {
            return array($request->errNo);
        }

        $keys = array();
        foreach ($request->Errors as $error) {
            $keys[] = $error->errNo;
        }

        return $keys;
    }

    private static function zoneValueNotSuppliedError($request)
    {
        $keys = self::errorKeys($request);
        return in_array(self::ERROR_ZONE_VALUE_NOT_SUPPLIED, $keys);
    }


    private static function promoDoesNotExist($request) {
        $keys = self::errorKeys($request);
        return in_array(self::ERROR_PROMO_DOES_NOT_EXIST, $keys);
    }

    private static function promoIsExpired($request) {
        $keys = self::errorKeys($request);
        return in_array(self::ERROR_PROMO_IS_EXPIRED, $keys);
    }

    /**
     * Performas a series of tasks when we receive an error from Vonigo.
     *
     * @param $error string - the error message
     * @param $display bool - whether or not to display the error to the user
     * @param $object stdClass - the Vonigo response object
     * @param $unlock bool - whether or not to release the frogbox API lock
     */
    public static function routeError($error, $display = true, $object = null, $unlock = true, $line = null, $mail = true)
    {
        $message = '';
        $errorObj = new \stdClass;
        $errorObj->error = $error;
        $errorObj->displayError = $display;

        if (!empty($object)) {
            $message = $object->errMsg . ' ' . print_r($object, TRUE);
        }

        if (!empty($line)) {
            $errorObj->line = $line;
            $message .= ' at ' . $line;
        }

        if (!empty($message) && $mail) {
            $message .= PHP_EOL . $_SERVER['REQUEST_URI'];
            error_log('Vonigo error: ' . $message);
            wp_mail('gurpreet@longevitygraphics.com', 'route error from frogbox', $message);
        }

        return json_encode($errorObj);
    }

    /**
     * Validates a date as YYYYMMDD
     */
    public static function validateDate($date)
    {
        date_default_timezone_set('UTC');
        $nDate = intval($date);
        $time = strtotime($nDate);
        if ($nDate === 0 || $time === 0) {
            return self::routeError('Invalid date.', true, null, false, __LINE__ . ' (' . $date . ')');
        }
        elseif (date('Ymd', $time) !== $date) {
            return self::routeError('Invalid date.', true, null, false, __LINE__ . ' (' . $date . ')');
        }
        return $time;
    }


    /**
     * Gets all times for a zip/postal code
     */
    public function getAllTimes($zip, $retry = true) {
        header('Content-type: application/json');
        header('Cache-Control: no-cache, must-revalidate'); // do not cache
        header('Expires: Mon, 11 May 1981 12:00:00 GMT'); // Date in the past
        header(sprintf('Last-Modified: %s GMT', gmdate('D, d M Y H:i:s')));

        if (!(self::validateZip($zip))) {
            $zipError = new \stdClass;
            $zipError->errMsg = 'Invalid zip code: ' . htmlspecialchars($zip);
            $zipError->server = $_SERVER;
            return self::routeError('Unknown Zip/Postal Code. Please check your location. ', true, $zipError, false, __LINE__ . ' invalid zip ' . $zip, $zip);
        }

        try {
            $co = new \frogbox();
        }
        catch (LockException $e) {
            $lockError = new \stdClass;
            $lockError->errMsg = 'Unable to get lock in ' . __FUNCTION__;
            if ($retry) {
                return self::getAllTiems($zip, false);
            }
            return self::routeError('Unable to disable dates.', false, $lockError, false, __LINE__);
        }

        // validate the zip/postal code with vonigo
        $zvRequest = $co->validateZipCode($zip);
        if ($zvRequest->errNo == -421 && $retry) {
            $co->clearSecurityToken();
            $co->releaseLock();
            return $this->getAllTimes($zip, false);
        }
        if (empty($zvRequest->Ids->franchiseID)) {
            $co->releaseLock();
            if (self::validateZip($zip)) {
                // don't mail if it was a valid zip
                return self::routeError('Sorry, we are unable to check for available times. Please try choosing your date again. ', true, $zvRequest, true, __LINE__, false);
            } else {
                return self::routeError('Sorry, we are unable to check for available times. Please try choosing your date again. ', true, $zvRequest, true, __LINE__ . ' ' . __FUNCTION__ . ' invalid zip code ' . $zip);
            }
        }
        $franchise = $zvRequest->Ids->franchiseID;
        // set the franchise

        $fRequest = $co->session($franchise);
        if ($fRequest->errNo != 0) {
            $co->releaseLock();
            $co->clearSecurityToken();
            if ($retry) {
                return self::getAllTimes($zip, false);
            }
            return self::routeError('Sorry, we are unable to check for available times. Please try choosing your date again. ', true, $fRequest, true, __LINE__ . ' franchise: ' . $franchise);
        }

        $time = strtotime('today');
        $params = array(
            'dateStart' => $time,
            'dateEnd' => $time + (90 * 86400), // plus 3 months
            'serviceTypeID' => 8, // always 8
            'duration' => 30,
            'zip' => $zip,
        );
        $pRequest = $co->availableTimes($params);

        // build an array of the options
        $options = array();

        if (!empty($pRequest->Availability)) {
            $availability = new AvailabilityList($pRequest->Availability);

            // remove options that are locked
            $duration = $co->getWindow($franchise);
            $stored = self::getStoredLocks();
            $availability->removeOptionsForDuration($stored, $duration);

            $bookableOptions = $availability->getBookableOptions($duration);

            if (!empty($bookableOptions)) {
                foreach ($bookableOptions as $option) {
                    $object = new AvailabilityOption($option);
                    if (empty($options[$object->getDayID()][$object->getStartTime()])) {
                        $options[$object->getDayID()][$object->getStartTime()] = $object->asStdClass($duration);
                    }
                }
            }
        }
        $co->releaseLock();

        // build html select
        $return = new \stdClass;
        $return->options = $options;

        return json_encode($return, JSON_PRETTY_PRINT);
    }


    private static function getStoredLocks() {
        global $wpdb;
        $return = array();

        $query = sprintf("SELECT SUBSTR(option_name,21) AS lockobject FROM %s WHERE option_name LIKE '_transient__ff_lock%%'",
            $wpdb->options);

        $results= $wpdb->get_results($query, OBJECT);
        if (empty($results)) {
            return $return;
        }

        foreach ($results as $result) {
            $dayID = substr($result->lockobject, 0, 8);
            list($routeID, $startTime) = explode('-', substr($result->lockobject, 9));
            $return[] = (object) array('dayID' => $dayID, 'routeID' => $routeID, 'startTime' => $startTime);
        }

        return $return;
    }

    /**
     * Retrieves available times from Vonigo; not a cacheable request
     *
     * @param int $franchise - the franchise id for this date, to identify time zone
     * @param string $date - a string representing a date YYYYMMDD
     */
    public function getTimes($zip, $date, $retry = true)
    {
        header('Content-type: application/json');
        header('Cache-Control: no-cache, must-revalidate'); // do not cache
        header('Expires: Mon, 11 May 1981 12:00:00 GMT'); // Date in the past
        header(sprintf('Last-Modified: %s GMT', gmdate('D, d M Y H:i:s')));

        if (!(self::validateZip($zip))) {
            $zipError = new \stdClass;
            $zipError->errMsg = 'Invalid zip code: ' .  htmlspecialchars($zip);
            return self::routeError('Unknown Zip/Postal Code. Please check your location. ', true, $zipError, false, __LINE__ . ' invalid zip ' . $zip);
        }

        // Validate $date parameter as YYYYMMDD; date must be in the future
        $time = self::validateDate($date);
        if (!is_int($time)) {
            return $time;
        }

        try {
            $co = new \frogbox();
        }
        catch (LockException $e) {
            $lockError = new \stdClass;
            $lockError->errMsg = 'Unable to get lock in ' . __FUNCTION__;
            if ($retry) {
                return self::getTimes($zip, $date, false);
            }
            return self::routeError('Unable to disable dates.', false, $lockError, false, __LINE__);
        }

        // validate the zip/postal code with vonigo
        $zvRequest = $co->validateZipCode($zip);
        if (empty($zvRequest->Ids->franchiseID)) {
            $co->releaseLock();
            if (self::validateZip($zip)) {
                // don't mail if it was a valid zip
                return self::routeError('Sorry, we are unable to check for available times. Please try choosing your date again. ', true, $zvRequest, true, __LINE__, false);
            } else {
                return self::routeError('Sorry, we are unable to check for available times. Please try choosing your date again. ', true, $zvRequest, true, __LINE__ . ' ' . __FUNCTION__ . ' invalid zip code ' . $zip);
            }
        }
        $franchise = $zvRequest->Ids->franchiseID;
        // set the franchise

        $fRequest = $co->session($franchise);
        if ($fRequest->errNo != 0) {
            $co->releaseLock();
            $co->clearSecurityToken();
            if ($retry) {
                return self::getTimes($zip, $date, false);
            }
            return self::routeError('Sorry, we are unable to check for available times. Please try choosing your date again. ', true, $fRequest, true, __LINE__ . ' franchise: ' . $franchise . ' date: ' . $date);
        }

        // get resource availability for date + 24 hours
        $params = array(
            'dateStart' => $time,
            'dateEnd' => $time + 86400, // plus one day
            'serviceTypeID' => 8, // always 8
            'duration' => 30,
            'zip' => $zip,
        );
        $pRequest = $co->availableTimes($params);

        // build an array of the options
        $options = array();
        $availability = new AvailabilityList($pRequest->Availability);
        $availability->filterOptionsByDate($date);

        // remove options that are locked
        $duration = $co->getWindow($franchise);
        $stored = self::getStoredLocks();
        $availability->removeOptionsForDuration($stored, $duration);

        $bookableOptions = $availability->getBookableOptions( $duration );
        if (!empty($bookableOptions)) {
            foreach ($bookableOptions as $option) {
                $object = new AvailabilityOption($option);
                if (empty($options[$object->getStartTime()])) {
                    $options[$object->getStartTime()] = $object->formatAsOption($duration);
                }
            }
        }
        ksort($options);

        $co->releaseLock();

        // build html select
        $output = '<option value="">(select Time)</option>' . implode('', array_values($options));
        $return = new \stdClass;
        $return->options = $output;

        if (defined(JSON_UNESCAPED_SLASHES)) { // PHP 5.4+
            return json_encode($return, JSON_UNESCAPED_SLASHES);
        } else {
            return str_replace('\\/', '/', json_encode($return)); // PHP 5.3
        }
    }

    /**
     * Looks up a PriceListID by pricelist name, matching the beginning of the string.
     *
     * @param $name
     * @param $franchise
     * @return bool
     */
    public static function getPriceListIDByName($name, $franchise, $retry = true)
    {
        try {
            $co = new \frogbox();
        }
        catch (LockException $e) {
            $lockError = new \stdClass;
            $lockError->errMsg = 'Unable to get lock in ' . __FUNCTION__;
            if ($retry) {
                return self::getPriceListIDByName($name, $franchise, false);
            }
            return self::routeError('Unable to find pricelist name.', false, $lockError, false, __LINE__);
        }

        $fRequest = $co->session($franchise);
        if (!empty($fRequest->errNo)) {
            $co->releaseLock();
            $co->clearSecurityToken();
            if ($retry) {
                return self::getPriceListIDByName($name, $franchise, false);
            }
            return self::routeError('Unable to find pricelist name.', false, $fRequest, false, __LINE__);
        }
        $priceLists = $co->pricelists();
        $co->releaseLock();

        foreach ($priceLists->PriceLists as $pl) {
            if (strpos($pl->priceList, $name) === 0) {
                return $pl->priceListID;
            }
        }
        return self::routeError('Unable to find pricelist name.', false, $priceLists, false, __LINE__);
    }

    public static function getNearestRentalPeriod($value) {
        $weeks = intval($value / 7);
        if ($weeks <= 0) {
            return 1;
        }
        $mod = $value % 7;
        return ($mod <= FF_RENTAL_GRACE) ? $weeks : $weeks +1;
    }

    public static function getWeeksFromTitle($title) {
        if (preg_match('/(\d*)\s?week/i', $title, $matches)) {
            return $matches[1];
        }
    }


    /**
     * Retrieves pricelists from the WordPress cache or Vonigo.
     *
     * @param string $zip the zip or postal code to validate
     * @param int $days the number of days items will be rented; defaults to 7
     * @param bool $use_cache whether or not to use cached data; defaults to TRUE.
     * @param book $recurse - whether or not to recurse this function (only happens if days is 7)
     */
    public static function getPriceList($zip, $days = 7, $use_cache = true, $recurse = true, $retry = true) {
        header('Content-type: application/json');
        header('Cache-Control: max-age=3600'); // okay to cache 1 hour

        $priceListID = null;

        if (substr($zip, 0, 5) == 'MOVER') {
            $parts = explode('-', $zip);
            $priceListID = self::getPriceListIDByName($parts[1], $parts[2]);
            $franchise = $parts[2];
            if (!is_numeric($priceListID)) { // an error if it is not numeric
                return $priceListID;
            }

        }
        elseif ($zip == 'commercial') {
            return self::getCommercialPriceList();
        }

        $interfranchise = false;
        if (substr($zip, 0, 2) === 'IF' || substr($zip, 0, 2) === 'if') {
            $interfranchise = true;
            $franchise = substr($zip, 2);
        }

        // validate zip code
        if (!$priceListID & !$interfranchise & !(self::validateZip($zip))) {
            $zipError = new \stdClass;
            $zipError->errMsg = 'Invalid zip code: ' . htmlspecialchars($zip);
            return self::routeError('Unknown Zip/Postal Code. Please check your delivery location. ', true, $zipError, false, __LINE__ . ' invalid zip ' . $zip, false);
        }

        /* Validate $days parameter.
         *
         * Make sure $days is an integer that is at greater than 0.
         */
        $days = intval($days);
        if (!($days > 0)) {
            return self::routeError('Rental period must be at least 1 day.', true, null, false, __LINE__, false);
        }

        $weeks = self::getNearestRentalPeriod($days);
        $days = $weeks * 7;

        // if we are getting the 7-day pricelist, also get the 14, 21 and 28 day lists
        if (($zip == 'commercial' || $zip == 'mover') && $days == 7 && $recurse) {
            $return = array(
                self::getPriceList($zip, 7, $use_cache, false),
                self::getPriceList($zip, 14, $use_cache, false),
                self::getPriceList($zip, 21, $use_cache, false),
                self::getPriceList($zip, 28, $use_cache, false),
                self::getPriceList($zip, 35, $use_cache, false),
                self::getPriceList($zip, 42, $use_cache, false),
                self::getPriceList($zip, 49, $use_cache, false),
                self::getPriceList($zip, 56, $use_cache, false),
                self::getPriceList($zip, 63, $use_cache, false),
                self::getPriceList($zip, 70, $use_cache, false),
                self::getPriceList($zip, 77, $use_cache, false),
                self::getPriceList($zip, 84, $use_cache, false),
                self::getPriceList($zip, 91, $use_cache, false),
                self::getPriceList($zip, 98, $use_cache, false),
                self::getPriceList($zip, 105, $use_cache, false),
                self::getPriceList($zip, 112, $use_cache, false),
                self::getPriceList($zip, 119, $use_cache, false),
                self::getPriceList($zip, 126, $use_cache, false),
                self::getPriceList($zip, 133, $use_cache, false),
                self::getPriceList($zip, 140, $use_cache, false),
                self::getPriceList($zip, 147, $use_cache, false),
                self::getPriceList($zip, 154, $use_cache, false),
                self::getPriceList($zip, 161, $use_cache, false),
                self::getPriceList($zip, 168, $use_cache, false)
            );
            foreach ($return as $list) {
                if (is_string($list)) {
                    $list = json_decode($list);
                    if (!empty($list->error)) {
                        return json_encode($list);
                    }
                }
            }
            return json_encode($return);
        } elseif ($days == 7 && $recurse) {
            $return = array(
                self::getPriceList($zip, 7, $use_cache, false),
                self::getPriceList($zip, 14, $use_cache, false),
                self::getPriceList($zip, 21, $use_cache, false),
                self::getPriceList($zip, 28, $use_cache, false)
            );
            foreach ($return as $list) {
                if (is_string($list)) {
                    $list = json_decode($list);
                    if (!empty($list->error)) {
                        return json_encode($list);
                    }
                }
            }
            return json_encode($return);
        }

        try {
            $co = new \frogbox();
        }
        catch (LockException $e) {
            $lockError = new \stdClass;
            $lockError->errMsg = 'Unable to get lock in ' . __FUNCTION__;
            if ($retry) {
                return self::getPriceList($zip, $days, $use_cache, $recurse, false);
            }
            return self::routeError('Unable to disable dates.', false, $lockError, false, __LINE__);
        }

        $zvRequest = $co->validateZipCode($zip);
        if (!empty($zvRequest->Ids->zoneID)) {
            $zoneID = $zvRequest->Ids->zoneID;
        }
        $co->releaseLock();

        if (!empty($zoneID)) {
            $key = sprintf('frogbox_pricelist_zone_%s_%s', $zoneID, $days);
        }
        else {
            $key = sprintf('frogbox_pricelist_%s_%s', $zip, $days);
        }

        $data = array();

        if ($use_cache) {
            $data = get_transient($key);
        }

        if (empty($data)) { // cache is empty or expired or $use_cache is false

            if ($interfranchise && !empty($franchise)) {
                $priceListID = self::getPriceListIDByName('103', $franchise);
                if (is_object(json_decode($priceListID))) {
                     $object = json_decode($priceListID);
                     if (!empty($object->error)) {
                         return $priceListID;
                     }
                }
            }

            try {
                $co = new \frogbox();
            }
            catch (LockException $e) {
                $lockError = new \stdClass;
                $lockError->errMsg = 'Unable to get lock in ' . __FUNCTION__;
                if ($retry) {
                    return self::getPriceList($zip, $days, $use_cache, $recurse, false);
                }
                return self::routeError('Unable to disable dates.', false, $lockError, false, __LINE__);
            }

            $error_message = 'Sorry, we are unable to load products at this time. Please try choosing another area or date. ';

            $co->serviceTypeID = FROGBOX_SERVICE_TYPE;

            if (empty($priceListID)) {
                // validate the zip/postal code with vonigo
                $zvRequest = $co->validateZipCode($zip);
                if (empty($zvRequest->Ids->zoneID) || empty($zvRequest->Ids->franchiseID)) {
                    if (self::validateZip($zip)) {
                        $co->releaseLock();
                        // don't mail if we have a validly formatted zip code
                        return self::routeError($error_message, TRUE, $zvRequest, TRUE, __LINE__, false);
                    } else {
                        $co->releaseLock();
                        return self::routeError($error_message, TRUE, $zvRequest, TRUE, __LINE__ . ' ' . __FUNCTION__ . ' invalid zip code ' . $zip);
                    }
                }
                $zoneID = $zvRequest->Ids->zoneID;
                $franchise = $zvRequest->Ids->franchiseID;
                foreach ($zvRequest->ServiceTypes as $serviceType) {
                    if ($serviceType->typeID == $co->serviceTypeID) {
                        $priceListID = $serviceType->priceID;
                        break;
                    }
                }
            }

            if (empty($priceListID)) {
                $errObj = (object) array(
                    'params' => func_get_args(),
                    'param_names' => array('zip', 'days', 'use_cache', 'recurse'),
                    'zvRequest' => !empty($zvRequest) ? $zvRequest : null,
                );
                $co->releaseLock();
                return self::routeError($error_message, true, $errObj, true, __LINE__);
            }

            // find out the pricelist level and name
            $sRequest = $co->session($franchise);
            if ($sRequest->errNo != 0) {
                $co->releaseLock();
                $co->clearSecurityToken();
                if ($retry) {
                    return self::getPriceList($zip, $days, $use_cache, $recurse, false);
                }
                return self::routeError($error_message, true, $sRequest, true, __LINE__ . ' setting franchise ' . $franchise);
            }
            $plRequest = $co->pricelists();
            if ($plRequest->errNo != 0) {
                $co->releaseLock();
                if ($retry && self::sessionError($plRequest)) {
                    $co->clearSecurityToken();
                    return self::getPriceList($zip, $days, $use_cache, $recurse, false);
                }
                return self::routeError($error_message, true, $plRequest, true, __LINE__);
            }
            $priceListLevelValue = 0; // default
            foreach ($plRequest->PriceLists as $pl) {
                if ($pl->priceListID == $priceListID) {
                    $priceListName = $pl->priceList;
                    $priceListLevelValue = $pl->priceListLevelValue;
                    break;
                }
            }

            if (empty($priceListName)) {
                $co->releaseLock();
                if ($retry) {
                    return self::getPriceList($zip, $days, $use_cache, $recurse, false);
                }
                return self::routeError($error_message, true, $plRequest, true, __LINE__ . '; could not find pricelist name for pricelistID' . $priceListID . ' (' . $franchise . ')');
            }

            // retrieve priceblocks from Vonigo
            $sRequest = $co->session($franchise);
            if ($sRequest->errNo != 0) {
                $co->releaseLock();
                $co->clearSecurityToken();
                if ($retry) {
                    return self::getPriceList($zip, $days, $use_cache, $recurse, false);
                }
                return self::routeError($error_message, true, $sRequest, true, __LINE__);
            }

            $pbRequest = $co->priceBlocks($priceListID);
            if ($pbRequest->errNo != 0 || empty($pbRequest->PriceBlocks)) {
                $co->releaseLock();
                if ($retry && self::sessionError($pbRequest)) {
                    $co->clearSecurityToken();
                    return self::getPriceList($zip, $days, $use_cache, $recurse, false);
                }
                return self::routeError($error_message, true, $pbRequest, true, __LINE__ . ' could not find priceblocks for pricelist ' . $priceListID . ' in franchise ' . $franchise);
            }

            // identify the priceblock with the shortest rental time that is still
            // greater than or equal to the rental period
            $blocks = array();
            $productBlock = 0;

            foreach ($pbRequest->PriceBlocks as $pBlock) {
                if (!\frogbox::isTrue($pBlock->isActive)) {
                    continue;
                }

                $blockWeeks = self::getWeeksFromTitle($pBlock->priceBlock);

                if ($blockWeeks == $weeks) {
                    $blocks[$weeks] = $pBlock;
                }
                elseif (stripos($pBlock->priceBlock, 'products') > -1) {
                    $productBlock = $pBlock;
                }

            }
            // if the blocks array is empty it means the rental period is too long to get a price for
            if (sizeof($blocks) == 0) {
                $co->releaseLock();
                return self::routeError('Please <a href="/contact">contact us</a> for a quote for a ' . $days . '-day rental', true, null, true, '', false);
            }

            ksort($blocks);
            $block = array_shift(array_values($blocks));

            // get the items for the products block and the price blocks and separate into
            // bundles, single boxes and products

            // load the wp items
            global $wpdb;
            $query = "SELECT wpm2.meta_value, wpm.post_id
                        FROM $wpdb->postmeta AS wpm
                        JOIN $wpdb->postmeta AS wpm2
                          ON concat('_',wpm2.meta_key)=wpm.meta_key
                         AND wpm.post_id=wpm2.post_id
                         AND wpm.meta_id != wpm2.meta_id
                      WHERE wpm.meta_value='field_556e00982129f'";

            $productReference = array();
            $products = $wpdb->get_results($query, OBJECT);
            if (count($products)) {
                foreach ($products as $product) {
                    $keyElements = explode(':', $product->meta_value);
                    $product->meta_value = implode(':', array_slice($keyElements, 0, 3));
                    $productReference[$product->meta_value] = $product->post_id;
                }
            }

            $items = array('bundles' => array(), 'supplies' => array(), 'priceListLevelValue' => $priceListLevelValue);
            foreach (array($block, $productBlock) as $pb) {
                if (empty($pb)) {
                    continue;
                }
                $sRequest = $co->session($franchise);
                if ($sRequest->errNo != 0) {
                    $co->releaseLock();
                    $co->clearSecurityToken();
                    if ($retry) {
                        return self::getPriceList($zip, $days, $use_cache, $recurse, false);
                    }
                    return self::routeError($error_message, true, $sRequest, true, __LINE__);
                }
                $piRequest = $co->priceItems($pb->priceListID, $pb->priceBlockID);

                if ($piRequest->errNo != 0) {
                    $co->releaseLock();
                    if ($retry && self::sessionError($piRequest)) {
                        $co->clearSecurityToken();
                        return self::getPriceList($zip, $days, $use_cache, $recurse, false);
                    }
                    return self::routeError($error_message, true, $piRequest, true, __LINE__);
                }

                if (!sizeof($piRequest->PriceItems)) {
                    $co->releaseLock();
                    if ($retry) {
                        return self::getPriceList($zip, $days, $use_cache, $recurse, false);
                    }
                    return self::routeError($error_message, true, $piRequest, true, __LINE__ . '; no priceitems for pricelistID ' . $pb->priceListID . ' and priceblockID ' . $pb->priceBlockID . '(' . $franchise . ')');
                }

                foreach ($piRequest->PriceItems as $pItem) {
                    if ($pItem->isActive == '1') {
                        // get product info from the database
                        $piKey = _ff_priceItemKey($priceListName, $pb->priceBlock, $pItem);
                        if (!empty($productReference[$piKey])) {
                            $productID = $productReference[$piKey];
                            $post = get_post($productID);
                            $custom = get_post_custom($productID);

                            // set the key to the wordpress post id
                            $pItem->key = $productID;
                            $pItem->name = _ff_quote_entities($post->post_title);
                            if (!empty($custom['description_option'])) {
                                $pItem->description = _ff_quote_entities(array_pop($custom['description_option']));
                            } else {
                                $pItem->description = "No description entered.";
                            }

                            $pItem->image = '/wp-content/uploads/2015/07/widget_frog_bg.png';
                            $pItem->imageLarge = '/wp-content/uploads/2015/07/widget_frog_bg.png';

                            if (has_post_thumbnail($productID)) {
                                $imageInfo = wp_get_attachment_image_src(get_post_thumbnail_id($productID), 'medium');
                                if (!empty($imageInfo[0])) {
                                    $pItem->image = $imageInfo[0];
                                }
                                $largeImageInfo = wp_get_attachment_image_src(get_post_thumbnail_id($productID), 'large');
                                if (!empty($largeImageInfo[0])) {
                                    $pItem->imageLarge = $largeImageInfo[0];
                                }
                            }

                            // set the group based and othe custom fields
                            if (!empty($custom['group_option'])) {
                                $group = str_replace(' ', '-', strtolower(array_pop($custom['group_option'])));
                                if (!empty($group)) {
                                    $pItem->group = $group;
                                }
                            }

                            // if the 'product elements' custom field is set, build
                            // description from that instead; bundles only
                            if (!empty($custom['product_elements']) && (empty($pItem->group) || stripos($pItem->group, 'bundle') > 0)) {
                                $description_details = array();
                                $bundle_items = array();
                                if (!empty($custom['product_elements'][0])) {
                                    $rows = $custom['product_elements'][0];
                                    for ($r = 0; $r < $rows; $r++) {
                                        $product_details = (object)array('title' => '', 'dimensions' => '', 'description' => '');
                                        /**
                                         * For each row,
                                         * product_elements_0_item_title  // h4
                                         * product_elements_0_item_dimensions // p
                                         * product_elements_0_item_description  // br
                                         */
                                        $itemCount = 0;
                                        $itemName = '';
                                        $product_details->dimensions = _ff_quote_entities($custom['product_elements_' . $r . '_dimensions'][0]);

                                        if (empty($pItem->group) || strpos($pItem->group, 'bundle') == -1) {
                                            if (!empty($custom['product_elements_' . $r . '_description'])) {
                                                $product_details->description = _ff_quote_entities($custom['product_elements_' . $r . '_description'][0]);
                                            }
                                        }

                                        if (!empty($custom['product_elements_' . $r . '_item_title'][0])) {
                                            $product_details->title = _ff_quote_entities($custom['product_elements_' . $r . '_item_title'][0]);
                                            list($itemCount, $itemName) = explode(' ', $custom['product_elements_' . $r . '_item_title'][0], 2);
                                        }

                                        if (!empty($itemName) && !empty($itemCount)) {
                                            $bundle_items[$itemName] = $itemCount;
                                        }

                                        $description_details[] = $product_details;
                                    }
                                }
                                if (!empty($description_details)) {
                                    $pItem->description_details = $description_details;
                                    $pItem->description = '';
                                }
                                if (!empty($bundle_items)) {
                                    $pItem->bundle_items = $bundle_items;
                                }
                            }

                            /**
                             *  other custom fields just need to be added to the object
                             *  if they are present.
                             */
                            $custom_fields = array(
                                'title_display_option',
                                'delivery_pickup',
                                'value_message',
                                'maximum_quantity',
                            );
                            foreach ($custom_fields as $cf) {
                                if (!empty($custom[$cf])) {
                                    $pItem->{$cf} = $custom[$cf][0];
                                }
                            }
                        } else {
                            $pItem->name = _ff_normalizePriceItemName($pItem->priceItem);
                            $pItem->description = 'No database match for ' . $piKey . '.';
                            $pItem->image = '';
                            $pItem->imageLarge = '';
                            $pItem->key = $pItem->priceItemID;

                            if (stripos($pItem->priceItem, 'bundle') > -1) {
                                $pItem->group = 'bundle';
                            }
                            $pItem->maximum_quantity = 10;
                        }

                        // set a default quantity
                        $pItem->quantity = 0;

                        // default maximum quantity is 10
                        if (empty($pItem->maximum_quantity)) {
                            $pItem->maximum_quantity = 10;
                        }

                        // ignore the delivery charge item
                        if (strtolower(substr($pItem->priceItem, 0, 15)) != 'delivery charge') {
                            // set the type/step based on whether the word 'bundle' is in the group
                            if (!empty($pItem->group) && stripos($pItem->group, 'bundle') > -1) {
                                $items['bundles'][$pItem->key] = $pItem;
                            } else {
                                // set the 'source' property based on which pricelist the item came from so we can sort on it later
                                if ($pItem->priceBlockID == $productBlock->priceBlockID) {
                                    $pItem->source = 'products';
                                    $pItem->sequence += 100; // make sure items-for-purchase (this list) sort after items-for-rent ('bundles' list)
                                } else {
                                    $pItem->source = 'bundles';
                                }
                                $items['supplies'][$pItem->key] = $pItem;
                            }
                        } else {
                            $items['deliveryChargeItem'] = $pItem; // delivery charge item
                        }
                    }
                }
            }

            // sort $items['bundles'] and $items['supplies'] by group then price
            $items['bundles'] = _ff_array_orderby($items['bundles'], 'group', SORT_ASC, 'sequence', SORT_ASC);
            $items['supplies'] = _ff_array_orderby($items['supplies'], 'source', SORT_ASC, 'sequence', SORT_ASC);
            $items['zip'] = $zip;

            // also get the taxes while we're at it
            $sRequest = $co->session($franchise);
            if ($sRequest->errNo != 0) {
                $co->releaseLock();
                $co->clearSecurityToken();
                if ($retry) {
                    return self::getPriceList($zip, $days, $use_cache, $recurse, false);
                }
                return self::routeError($error_message, true, $sRequest, true, __LINE__);
            }
            $tRequest = $co->taxes($zip);
            if ($tRequest->errNo != 0) {
                $co->releaseLock();
                if ($retry && self::sessionError($tRequest)) {
                    $co->clearSecurityToken();
                    return self::getPriceList($zip, $days, $use_cache, $recurse, false);
                }
                return self::routeError($error_message, true, $tRequest, true, __LINE__);
            }
            $taxPercent = 0;
            foreach ($tRequest->Taxes as $tax) {
                if ((int)$tax->isActive == 1 && (int)$tax->taxPercent > 0) {
                    $taxPercent += $tax->taxPercent;
                }
            }
            $items['taxPercent'] = $taxPercent;

            // release the lock at this point
            $co->releaseLock();

            // update the cache if we have some data
            if (!empty($items['bundles']) || !empty($items['supplies'])) {
                $items['priceBlock'] = $block;
                $items['priceList'] = $priceListID;
                $items['taxPercent'] = $taxPercent;
                $data = $items;
                set_transient($key, $data, FF_TRANSIENT_EXPIRE);
            }
        }

        // if the recurse parameter was true, we need to add an array wrapper around the data
        if ($recurse && !empty($data)) {
            $data = json_encode(array($data));
        }

        // return an error if we didn't get any data
        if (empty($data)) {
            $error = (object)array('error' => 'could not retrieve pricelists', 'displayError' => true);
            if ($recurse) {
                $data = json_encode($error);
            } else {
                return $error;
            }
        }
        return $data;
    }

    /**
     * Checks to see if an error from Vonigo has to do with a session expiring or not existing.
     *
     * @param $request
     * @return bool
     */
    public static function sessionError($request) {
        if ($request->errNo == self::ERROR_SESSION_EXPIRED || $request->errNo == self::ERROR_SESSION_DOES_NOT_EXIST) {
            return true;
        }
        return false;
    }
}
