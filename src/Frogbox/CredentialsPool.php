<?php

namespace Frogbox;

class CredentialsPool
{
    private $credentials;

    public function __construct() {

        $credentials =  $this->getDefaultCredentials();
        $this->credentials[$this->makeHash($credentials)] = $credentials;

//        // if the hour is not 1am, add additional credentials
//        if (date('G') != '1') {
//            $credentials =
//                array(
//                    'username' => 'fbx.hq',
//                    'password' => 'testtest',
//                    'company' => 'Frogbox',
//                    'base_url' => $this->getBaseUrl(),
//                );
//            $this->credentials[$this->makeHash($credentials)] = $credentials;
//        }

    }

    private function makeHash($credentials) {
        return md5(json_encode($credentials));
    }

    private function getBaseUrl() {
        $settings = $settings = get_option('frogbox_form_settings');
        return $settings['base_url'];
    }

    private function getDefaultCredentials() {
        return get_option('frogbox_form_settings');
    }

    public function getCredentials($hash) {
        if (empty($hash) || empty($this->credentials[$hash])) {
            throw new \Exception('Invalid hash in credentials pool.');
        }
        return $this->credentials[$hash];
    }

    public function getHashes() {
        return array_keys($this->credentials);
    }
}