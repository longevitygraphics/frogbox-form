<?php

namespace Frogbox;

use \Infusionsoft_Contact;
use \Infusionsoft_ContactService;
use \Infusionsoft_ContactServiceBase;
use \Infusionsoft_EmailService;
use Stripe\Exception\ApiErrorException;


class Submission
{
    /** @var  Promo $promo */
    private $promo;

    private $submission;

    private $subtotal = 0;

    private $tip = 0;

    private $valid = true;

    public function __construct() {
        $submission = new \stdClass;
        $submission->post = $_POST;
        $this->submission = $submission;
    }

    public function isValid() {
        return $this->valid;
    }

    public function validate() {

        $submission = $this->submission;

        $errors = array();

        // get the rest of the submission ready by stripping out bad data
        foreach ($submission->post as $key => $element) {
            // strip funny characters from input, except 'frogboxCart' element
            if ($key != 'frogboxCart' && is_string($element)) {
                $submission->post[$key] = htmlspecialchars($element);
            }
            // ensure quantity fields are valid, positive integers
            if (substr($key, -8) == 'quantity') {
                $submission->post[$key] = abs(intval($submission->post[$key]));
            }
        }

        // validate required fields
        $required = array(
            'delivery-city', 'pickup-city', // step 1
            'bundle', // step 2
            // no required fields in step 3
            'delivery-date', 'delivery-time', 'pickup-date', 'pickup-time', // step 4 time info
            'deliveryStreet', 'deliveryCity', 'deliveryProvince', 'deliveryPostcode', // step 4 delivery address fields
            'pickupStreet', 'pickupCity', 'pickupProvince', 'pickupPostcode', // step 4 pickup address fields
            'contact-first', 'contact-last', 'contact-primary-phone', 'contact-email', // step 5 primary contact fields
        );

        // if different contacts are selected, require values for first and last names
        if ($submission->post['delivery-contact'] == 'different delivery contact') {
            $required[] = 'delivery-contact-first';
            $required[] = 'delivery-contact-last';
        }
        if ($submission->post['pickup-contact'] == 'different pickup contact') {
            $required[] = 'pickup-contact-first';
            $required[] = 'pickup-contact-last';
        }

        foreach ($required as $field) {
            if (empty($submission->post[$field])) {
                $errMsg = '%s is required. ';
                if ($field == 'bundle') {
                    $errMsg = 'Please choose a bundle size. ';
                    $field = 'step-2-content p';
                }
                $errors[] = (object) array('element' => $field, 'errMsg' => $errMsg);
            }
        }

        // additional, conditional required fields:
        // either bundle-choice needs to have a value OR one of the items in group-custom-bundle needs to have a quantity > 0
        if ($submission->post['bundle'] == 'custom') {
            $cartItems = json_decode(stripslashes($submission->post['frogboxCart']));
            $totalCustom = 0;
            // find the items in the custom bundle group
            foreach ($cartItems->bundles as $product) {
                if ($product->group == 'custom-bundle') {
                    $totalCustom += $product->quantity;
                }
            }
            if ($totalCustom == 0) {
                $errors[] = (object) array('element' => 'bundle-custom', 'errMsg' => 'Please choose some items for your custom order. ');
            }
        }
        else {
            if (empty($submission->post['bundle-choice'])) {
                $errors[] = (object) array('element' => 'bundle-' . $submission->post['bundle'], 'errMsg' => 'Please choose a bundle. ');
            }
        }

        // phone numbers have to have 10 digits
        if (!empty($submission->post['contact-primary-phone'])) {
            $submission->post['contact-primary-phone-reformatted'] =
                filter_var(str_replace(array('+', '-'), '', $submission->post['contact-primary-phone']), FILTER_SANITIZE_NUMBER_INT);
            if (strlen($submission->post['contact-primary-phone-reformatted']) != 10) {
                $errors[] = (object) array('element' => 'contact-primary-phone', 'errMsg' => 'Please enter a valid phone number. ');
            }
        }
        if (!empty($submission->post['contact-alternate-phone'])) {
            $submission->post['contact-alternate-phone-reformatted'] =
                filter_var(str_replace(array('+', '-'), '', $submission->post['contact-alternate-phone']), FILTER_SANITIZE_NUMBER_INT);
            if (strlen($submission->post['contact-alternate-phone-reformatted']) != 10) {
                $errors[] = (object) array('element' => 'contact-alternate-phone', 'errMsg' => 'Please enter a valid phone number. ');
            }
        }
        // email address format
        if (!empty($submission->post['contact-email'])) {
            $email_sanitized = filter_var($submission->post['contact-email'], FILTER_SANITIZE_EMAIL);
            if ($email_sanitized != $submission->post['contact-email'] || filter_var($submission->post['contact-email'], FILTER_VALIDATE_EMAIL) === false) {
                $errors[] = (object) array('element' => 'contact-email', 'errMsg' => 'Please enter a valid email address. ');
            }
        }

        // return invalid submission if the errors array has any data, part 1, pre-zip-code validation
        if (sizeof($errors) > 0) {
            $this->valid = false;
            $submission->errors = $errors;
            return $submission;
        }

        // set delivery and pickup suburub values
        $length = is_numeric($submission->post['deliveryPostcode']) ? 5 : 3;
        $submission->post['delivery-suburb'] = substr($submission->post['deliveryPostcode'], 0, $length);
        if (strlen($submission->post['delivery-suburb']) < $length) {
            $errors[] = (object) array('element' => 'delivery-suburb', 'errMsg' => sprintf('Invalid postal code for delivery %s', $submission->post['delivery-suburb']));
        }

        // pickup suburb
        $length = is_numeric($submission->post['pickupPostcode']) ? 5 : 3;
        $submission->post['pickup-suburb'] = substr($submission->post['pickupPostcode'], 0, $length);
        if (strlen($submission->post['pickup-suburb']) < $length) {
            $errors[] = (object) array('element' => 'pickup-suburb', 'errMsg' => sprintf('Invalid postal code for pickup %s', $submission->post['pickup-suburb']));
        }

        // validate zip codes; normalize first
        $submission->post['deliveryPostcode-reformatted'] = preg_replace('/[^a-zA-Z0-9]+/', '', $submission->post['deliveryPostcode']);
        $submission->post['pickupPostcode-reformatted'] = preg_replace('/[^a-zA-Z0-9]+/', '', $submission->post['pickupPostcode']);

        try {
            $co = new \frogbox();
        }
        catch (LockException $e) {
            $lockError = new \stdClass;
            $lockError->errMsg = 'Unable to get lock ' . __LINE__;
            return json_decode(FF_Router::routeError('There was an error processing your order.', true, $lockError, false, __LINE__));
        }

        if ($submission->post['deliveryCountry'] != 'United States' && $submission->post['deliveryCountry'] != 'Canada') {
            $submission->post['deliveryCountry'] = \frogboxAddress::countryByFranchise($submission->post['delivery-city']);
        }
        if ($submission->post['pickupCountry'] != 'United States' && $submission->post['pickupCountry'] != 'Canada') {
            $submission->post['pickupCountry'] = \frogboxAddress::countryByFranchise($submission->post['pickup-city']);
        }
        if ($submission->post['deliveryCountry'] == 'United States') {
            $zipTest = substr($submission->post['deliveryPostcode-reformatted'], 0, 5);
            if (is_numeric($zipTest)) {
                $zRequest = $co->validateZipCode($zipTest);
                if ($zRequest->errNo != 0) {
                    $errors[] = (object) array('element' => 'deliveryPostcode', 'errMsg' => 'Please enter a valid zip code. ');
                }
                else {
                    if ($zRequest->Ids->franchiseID == $submission->post['delivery-city']) {
                        $submission->post['deliveryPostcode-reformatted'] = $zRequest->Ids->zip;
                    }
                    else {
                        $errors[] = (object) array('element' => 'deliveryPostcode', 'errMsg' => 'Please enter a valid zip code. ');
                    }
                }
            }
            else {
                $errors[] = (object) array('element' => 'deliveryPostcode', 'errMsg' => 'Please enter a valid zip code. ');
            }
        }
        elseif ($submission->post['deliveryCountry'] == 'Canada') {
            $zipTest = substr($submission->post['deliveryPostcode-reformatted'], 0, 6);
            if (preg_match('/^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1}\d{1}[A-Z]{1}\d{1}$/i', $zipTest)) {
                $zRequest = $co->validateZipCode($zipTest);
                if ($zRequest->errNo != 0) {
                    $errors[] = (object) array('element' => 'deliveryPostcode', 'errMsg' => 'Please enter a valid postal code. ');
                }
                else {
                    if ($zRequest->Ids->franchiseID == $submission->post['delivery-city']) {
                        $submission->post['deliveryPostcode-reformatted'] = $zRequest->Ids->zip;
                    }
                    else {
                        $errors[] = (object) array('element' => 'deliveryPostcode', 'errMsg' => 'Invalid postal code. Please make sure your location in Step 1 matches your address here. ');
                    }
                }
            }
            else {
                $errors[] = (object) array('element' => 'deliveryPostcode', 'errMsg' => 'Please enter a valid postal code. ');
            }
        }
        if ($submission->post['pickupCountry'] == 'United States') {
            $zipTest = substr($submission->post['pickupPostcode-reformatted'], 0, 5);
            if (is_numeric($zipTest)) {
                $zRequest = $co->validateZipCode($zipTest);
                if ($zRequest->errNo != 0) {
                    $errors[] = (object) array('element' => 'pickupPostcode', 'errMsg' => 'Please enter a valid postal code. ');
                }
                else {
                    if ($zRequest->Ids->franchiseID == $submission->post['pickup-city']) {
                        $submission->post['pickupPostcode-reformatted'] = $zRequest->Ids->zip;
                    }
                    else {
                        $errors[] = (object) array('element' => 'pickupPostcode', 'errMsg' => 'Please enter a valid zip code. ');
                    }
                }
            }
            else {
                $errors[] = (object) array('element' => 'pickupPostcode', 'errMsg' => 'Please enter a valid zip code. ');
            }
        }
        elseif ($submission->post['pickupCountry'] == 'Canada') {
            $zipTest = substr($submission->post['pickupPostcode-reformatted'], 0, 6);
            if (preg_match('/^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1}\d{1}[A-Z]{1}\d{1}$/i', $zipTest)) {
                $zRequest = $co->validateZipCode(substr($submission->post['pickupPostcode-reformatted'], 0, 6));
                if ($zRequest->errNo != 0) {
                    $errors[] = (object) array('element' => 'pickupPostcode', 'errMsg' => 'Please enter a valid postal code. ');
                }
                else {
                    if ($zRequest->Ids->franchiseID == $submission->post['pickup-city']) {
                        $submission->post['pickupPostcode-reformatted'] = $zRequest->Ids->zip;
                    }
                    else {
                        $errors[] = (object) array('element' => 'pickupPostcode', 'errMsg' => 'Invalid postal code. Please make sure your location in Step 1 matches your address here.');
                    }
                }
            }
            else {
                $errors[] = (object) array('element' => 'pickupPostcode', 'errMsg' => 'Please enter a valid postal code. ');
            }
        }

        // dates can't be so far apart that we can't select a pricelist

        // release lock
        $co->releaseLock();
        unset($co);

        // return invalid submission if the errors array has any data, part 2, post zip code validation
        if (sizeof($errors) > 0) {
            $this->valid = false;
            $submission->errors = $errors;
            return $submission;
        }

        // make sure we can get the route options for the pickup and delivery routes
        $this->submission->deliveryRouteOption = $this->lookupRouteOption('delivery');
        $this->submission->pickupRouteOption = $this->lookupRouteOption('pickup');;

        // normalize dates
        $submission->post['delivery-date-reformatted'] = str_replace('-', '', $submission->post['delivery-date']);
        $submission->post['pickup-date-reformatted'] = str_replace('-', '', $submission->post['pickup-date']);

        // check that the delivery lock is still valid
        if (!empty($submission->post['deliveryLock']) && is_integer($submission->post['deliveryLock']) && $submission->post['deliveryLock'] > 1) {
            $lockTest = _ff_get_vonigo_lock($submission->post['delivery-date-reformatted'], $submission->post['delivery-time']);
            if ($lockTest != $submission->post['deliveryLock']) {
                unset($submission->post['deliveryLock']);
            }
        }

        if (!empty($submission->post['deliveryLock']) && is_integer($submission->post['deliveryLock']) && $submission->post['deliveryLock'] > 1) {
            $submission->deliveryLock = $submission->post['deliveryLock'];
        }
        else {
            $submission->deliveryLock = _ff_lock_time($submission->post['delivery-suburb'],
                $submission->post['delivery-date-reformatted'],
                $submission->post['delivery-time']);
            if (!is_numeric($submission->deliveryLock)) {
                $errors[] = (object) array('element' => 'delivery-time', 'errMsg' => $submission->deliveryLock->error);
                unset($submission->deliveryLock);
            }
            elseif (intval($submission->deliveryLock) > 1) {
                _ff_set_vonigo_lock($submission->deliveryLock, $submission->post['delivery-date-reformatted'], $submission->post['delivery-time'], $submission->post['delivery-city']);
            }
        }

        // check that the pickup lock is still valid
        if (!empty($submission->post['pickupLock']) && is_integer($submission->post['pickupLock']) && $submission->post['pickupLock'] > 1) {
            $lockTest = _ff_get_vonigo_lock($submission->post['pickup-date-reformatted'], $submission->post['pickup-time']);
            if ($lockTest != $submission->post['pickupLock']) {
                unset($submission->post['pickupLock']);
            }
        }
        if (!empty($submission->post['pickupLock']) && is_integer($submission->post['pickupLock']) && $submission->post['pickupLock'] > 1) {
            $submission->pickupLock = $submission->post['pickupLock'];
        }
        else {
            $submission->pickupLock = _ff_lock_time($submission->post['pickup-suburb'],
                $submission->post['pickup-date-reformatted'],
                $submission->post['pickup-time']);
            if (!is_numeric($submission->pickupLock)) {
                $errors[] = (object) array('element' => 'pickup-time', 'errMsg' => $submission->pickupLock->error);
                unset($submission->pickupLock);
            }
            elseif (intval($submission->pickupLock) > 1) {
                _ff_set_vonigo_lock($submission->pickupLock, $submission->post['pickup-date-reformatted'], $submission->post['pickup-time'], $submission->post['pickup-city']);
            }
        }

        // return invalid submission if the errors array has any data, part 3, post time locking
        if ( sizeof( $errors ) > 0 ) {
            $this->valid = false;
            $submission->errors = $errors;

            return $submission;
        }

        $validPriceBlock = _ff_validate_pricelist($submission);
        if ($validPriceBlock !== true) {
            $errObj = $validPriceBlock;
        }

        if (!empty($errObj)) {
            // return invalid submission if the errors array has any data, part 4, after validating priceblock on items
            $this->valid = false;
            $submission->errors[] = $errObj;
            _ff_failed_submission($submission, null, $errObj);
            return $submission;
        }

        $this->submission = $submission;
        $submission  = $this->submit();
        return $submission;
    }

    private function lookupRouteOption($which, $retry = true) {
        $timeProperty = sprintf('%s-time', $which);
        $cityProperty = sprintf('%s-city', $which);

        try {
            $co = new \frogbox();
        }
        catch (LockException $e) {
            if ($retry) {
                return $this->lookupRouteOption($which, false);
            }
            $lockError = new \stdClass;
            $lockError->errMsg = 'Unable to get lock ' . __LINE__;
            return json_decode(FF_Router::routeError('There was an error processing your order.', true, $lockError, false, __LINE__));
        }

        $submission = $this->submission;

        $request = $co->session($submission->post[$cityProperty]);
        if ($request->errNo != 0 && FF_Router::sessionError($request)) {
            $co->releaseLock();
            $co->clearSecurityToken();
            if ($retry) {
                return $this->lookupRouteOption($which, false);
            }
            $errors[] = (object) array('element' => $timeProperty, 'errMsg' => 'Sorry, this time is no longer available. ');
            wp_mail('gurpreet@longevitygraphics.com', sprintf('failed to get %s route option', $which),
                'franchise: ' . $submission->post[$cityProperty] . '; ' . print_r($submission->post, true)
                . ' line: ' . __LINE__
            );
        }

        $info = explode( '-', $submission->post[$timeProperty] );
        $routeID = $info[0];

        $routeOption = $co->getRouteOption($submission->post[$cityProperty], $routeID);
        $co->releaseLock();

        if (empty($routeOption)) {
            $errors[] = (object) array('element' => $timeProperty, 'errMsg' => 'Sorry, this time is no longer available. ');
            wp_mail('gurpreet@longevitygraphics.com', sprintf('failed to get %s route option', $which),
                'franchise: ' . $submission->post[$cityProperty] .
                '; route: ' . $routeID.
                '; ' . print_r($submission->post, true)
                . ' line: ' . __LINE__
            );
        }

        return $routeOption;
    }

    public function submit() {

        $submission = $this->submission;
        $submission->valid = $this->valid;


        try {
            $co = new \frogbox();
        }
        catch (LockException $e) {
            $lockError = new \stdClass;
            $lockError->errMsg = 'Unable to get lock ' . __LINE__;
            return json_decode(FF_Router::routeError('There was an error processing your order.', true, $lockError, false, __LINE__));
        }

        // set the franchise
        $fRequest = $co->session($submission->post['delivery-city']);
        if ($fRequest->errNo != 0) {
            $errObj = (object) array('errMsg' => 'Invalid value for delivery city', 'element' => 'delivery-city');
            $errObj->params = $submission->post['delivery-city'];
            $co->releaseLock();
            $co->clearSecurityToken();
            return _ff_failed_submission($submission, null, $errObj);
        }

        // Always create a new customer in Vonigo
        $params = array(
            'method' => 3,
        );
        $fields = _ff_formData_to_vClient($submission->post);

        $fRequest = $co->session($submission->post['delivery-city']);
        if ($fRequest->errNo != 0) {
            $errObj = (object) array('errMsg' => 'Invalid value for delivery city', 'element' => 'delivery-city');
            $errObj->params = $submission->post['delivery-city'];
            $co->releaseLock();
            return _ff_failed_submission($submission, null, $errObj);
        }
        $ccRequest = $co->clients($params, $fields);

        if ($ccRequest->errNo != 0) {
            $errObj = (object) array('errObj' => $ccRequest, 'element' => 'step-5-heading', 'line' => __LINE__);
            $errObj->params = $params;
            $errObj->fields = $fields;
            $co->releaseLock();
            return _ff_failed_submission($submission, null, $errObj);
        }

        $clientID = $ccRequest->Client->objectID;

        // build an object for the delivery loaction
        $deliveryLocation = new \frogboxAddress();
        $deliveryLocation->street = $submission->post['deliveryStreet'];
        $deliveryLocation->city = $submission->post['deliveryCity'];
        if($submission->post['deliveryCountry'] === 'Canada'){
	        $deliveryLocation->province = $submission->post['deliveryProvince'];
        }else{
	        $deliveryLocation->state = $submission->post['deliveryProvince'];
        }
        $deliveryLocation->postcode = $submission->post['deliveryPostcode-reformatted'];
        $deliveryLocation->country = $submission->post['deliveryCountry'];
        $deliveryLocation->description = $submission->post['deliveryDescription'];
        $deliveryLocation->calculated = $deliveryLocation->hash();

        // build an object for the pickup location
        $pickupLocation = new \frogboxAddress();
        $pickupLocation->street = $submission->post['pickupStreet'];
        $pickupLocation->city = $submission->post['pickupCity'];
        $pickupLocation->province = $submission->post['pickupProvince'];
        $pickupLocation->postcode = $submission->post['pickupPostcode-reformatted'];
        $pickupLocation->country = $submission->post['pickupCountry'];
        $pickupLocation->description = $submission->post['pickupDescription'];
        $pickupLocation->calculated = $pickupLocation->hash();

        // create the delivery location
        $fRequest = $co->session($submission->post['delivery-city']);
        if ($fRequest->errNo != 0) {
            $errObj = (object) array('errMsg' => 'Invalid value for delivery city', 'element' => 'delivery-city');
            $errObj->params = $submission->post['delivery-city'];
            $co->releaseLock();
            $co->clearSecurityToken();
            return _ff_failed_submission($submission, null, $errObj);
        }

        $params = array(
            'method' => 3,
            'clientID' => $clientID,
        );
        $dlRequest = $co->locations($params, $deliveryLocation->asFields());
        if ($dlRequest->errNo != 0) {
            $errObj = (object) array('errObj' => $dlRequest, 'element' => 'delivery-address');
            $errObj->params = $params;
            $errObj->fields = $deliveryLocation->asFields();
            $co->releaseLock();
            return _ff_failed_submission($submission, null, $errObj);
        }
        $deliveryLocation->objectID = $dlRequest->Location->objectID;

        if ($pickupLocation->calculated == $deliveryLocation->calculated) {
            $pickupLocation->objectID = $deliveryLocation->objectID;
        }

        // create the pickup location if we have to
        if (empty($pickupLocation->objectID)) {
            $fRequest = $co->session($submission->post['delivery-city']);
            if ($fRequest->errNo != 0) {
                $errObj = (object) array('errMsg' => 'Invalid value for delivery city', 'element' => 'delivery-city');
                $errObj->params = $submission->post['delivery-city'];
                $co->releaseLock();
                $co->clearSecurityToken();
                return _ff_failed_submission($submission, null, $errObj);
            }

            $params = array(
                'method' => 3,
                'clientID' => $clientID,
            );
            $puRequest = $co->locations($params, $pickupLocation->asFields());
            if ($puRequest->errNo != 0) {
                $errObj = (object) array('errObj' => $puRequest, 'element' => 'pickup-address');
                $errObj->params = $params;
                $errObj->fields = $pickupLocation->asFields();
                $co->releaseLock();
                return _ff_failed_submission($submission, null, $errObj);
            }
            $pickupLocation->objectID = $puRequest->Location->objectID;
        }

        // build contacts array
        $contacts = array();
        $firstname = (object) array('fieldID' => 127, 'fieldValue' => $submission->post['contact-first']);
        $lastname = (object) array('fieldID' => 128, 'fieldValue' => $submission->post['contact-last']);
        $contacts['primary'] = array($firstname, $lastname);
        if ($submission->post['delivery-contact'] === 'different delivery contact') {
            $firstname = (object) array('fieldID' => 127, 'fieldValue' => $submission->post['delivery-contact-first']);
            $lastname = (object) array('fieldID' => 128, 'fieldValue' => $submission->post['delivery-contact-last']);
            $contacts['delivery'] = array($firstname, $lastname);
        }
        if ($submission->post['pickup-contact'] === 'different pickup contact') {
            $firstname = (object) array('fieldID' => 127, 'fieldValue' => $submission->post['pickup-contact-first']);
            $lastname = (object) array('fieldID' => 128, 'fieldValue' => $submission->post['pickup-contact-last']);
            $contacts['pickup'] = array($firstname, $lastname);
        }

        // for each contact, check to see if it exists, if not, add it
        foreach ($contacts as $key => $fields) {
            // create the contact
            $params = array(
                'method' => 3,
                'clientID' => $clientID,
            );
            $fRequest = $co->session($submission->post['delivery-city']);
            if ($fRequest->errNo != 0) {
                $errObj = (object) array('errMsg' => 'Invalid value for delivery city', 'element' => 'delivery-city');
                $errObj->params = $submission->post['delivery-city'];
                $co->releaseLock();
                $co->clearSecurityToken();
                return _ff_failed_submission($submission, null, $errObj);
            }

            $ccRequest = $co->contacts($params, $fields);
            if ($ccRequest->errNo != 0) {
                $errObj = (object) array('errObj' => $ccRequest, 'element' => 'contact-details');
                $errObj->params = $params;
                $errObj->fields = $fields;
                $co->releaseLock();
                return _ff_failed_submission($submission, null, $errObj);
            }
            $contactID = $ccRequest->Contact->objectID;
            $contacts[$key] = $contactID;
        }

        if (empty($contacts['delivery'])) {
            $contacts['delivery'] = $contacts['primary'];
        }
        if (empty($contacts['pickup'])) {
            $contacts['pickup'] = $contacts['primary'];
        }

        // create the job
        $params = array(
            'method' => 3,
            'clientID' => $clientID,
        );

        $fields = array(
            (object) array('fieldID' => 978, 'fieldValue' => 'Online booking from new form.'), // summary
            (object) array('fieldID' => 982, 'optionID' => 10006), // service type = box rental
        );

        // decode cart
        $cartItems = json_decode(stripslashes($submission->post['frogboxCart']));

        // find out if the promo code is valid
        if (!empty($submission->post['promo-code'])) {
            $promo = $submission->post['promo-code'];

            $zipRequest = $co->validateZipCode( $submission->post['delivery-suburb'] );
            if ( $zipRequest->errNo != 0 || empty( $zipRequest->Ids->zoneID ) ) {
                $co->releaseLock();
                // fail if we can't validate the zip code at this point
                return _ff_failed_submission( $submission, NULL, (object) array(
                    'params' => $submission->post['delivery-suburb'],
                    'errMsg' => 'invalid zip code in promo section'
                ) );
            }

            // service type id = 8; clienttypeid = 1
            $this->promo = new Promo($co->validatePromo( $promo, 8, 1, $zipRequest->Ids->zoneID ));

            // add promo discount
            if ($this->promo->isValid()) {
                $fields[] = (object) array(
                    'fieldID' => 968,
                    'fieldValue' => $promo
                ); // promo code

                // see if we need to add any items to the cart
                $promoItems = _ff_get_promo_item($submission->post['delivery-suburb'], $promo);
                if (!empty($promoItems)) {
                    foreach ($promoItems as $promoItem) {
                        $promoItem->quantity = 1;
                        $promoItem->value = 0;
                        $promoItem->name = $promo . ': ' . $promoItem->priceItem;
                        $cartItems->supplies[] = $promoItem;
                    }
                }
            }

        }

        // include a move pack if that is selected
        if ($submission->post['includeMovePack'] == '1') {
            // figure out which bundle was selected
            $parts = explode('-', $submission->post['PricelistCode']);
            $priceListName = $parts[1];
            $franchise = $parts[2];
            $priceListID = FF_Router::getPriceListIDByName($priceListName, $franchise);

            foreach ($cartItems->bundles as $bundle) {
                if ($bundle->quantity > 0) {
                    $promoItem = _ff_movePackMatch('move pack ' . substr($bundle->name, 0, 1), $priceListID, $franchise);
                    if ($promoItem && empty($promoItem->error)) {
                        $promoItem->quantity = 1;
                        $promoItem->name = $promoItem->priceItem;
                        $cartItems->supplies[] = $promoItem;
                    } else {
                        error_log('error adding move pack: ' . print_r($promoItem, true));
                    }
                }
            }
        }

        // re-encode the cart in case we've added a promo item or move pack
        $submission->post['frogboxCart'] = json_encode($cartItems);

        $fRequest = $co->session($submission->post['delivery-city']);
        if ($fRequest->errNo != 0) {
            $errObj = (object) array('params' => $submission->post['delivery-city'], 'errMsg' => 'Invalid value for delivery city', 'element' => 'delivery-city');
            $co->releaseLock();
            $co->clearSecurityToken();
            return _ff_failed_submission($submission, null, $errObj);
        }
        $jRequest = $co->jobs($params, $fields);

        if ($jRequest->errNo != 0) {
            $errObj = (object) array('errObj' => $jRequest, 'element' => 'contact-details');
            $errObj->params = $params;
            $errObj->fields = $fields;
            $co->releaseLock();
            return _ff_failed_submission($submission, null, $errObj);
        }
        $this->jobID = $jRequest->Job->jobID;
        $jobParams = $params;
        $jobFields = $fields;

        // build the charges
        $charges = array();

        foreach (array($cartItems->bundles, $cartItems->supplies) as $pList) {
            foreach ($pList as $pItem) {
                if ($pItem->quantity > 0 && !empty($pItem->priceItemID)) {
                    $pItem->subtotal = $pItem->quantity * $pItem->value;
                    $pItem->tax = $pItem->subTotal * $cartItems->taxPercent / 100;
                    $this->subtotal += $pItem->subtotal;
                    $quantity = $pItem->quantity;

                    $charges[] = array(
                        'chargeID' => 0,
                        'priceItemID' => $pItem->priceItemID,
                        'sequence' => $pItem->sequence,
                        'Fields' => array(
                            (object) array('fieldID' => 9290, 'fieldValue' => $pItem->name),
                            (object) array('fieldID' => 9289, 'fieldValue' => 'Online order - box rental'),
                            (object) array('fieldID' => 9288, 'fieldValue' => $quantity), // quantity
                            (object) array('fieldID' => 9287, 'fieldValue' => $pItem->value), // unit price
                        ),
                    );
                }
                else if ($pItem->quantity > 0) {
                    // send a route error but don't stop
                    FF_Router::routeError('bad priceitem', false, $pItem, false, $line = __LINE__);
                }
            }
        }

        if ($cartItems->deliveryCharge > 0 && !empty($cartItems->deliveryChargeItem)) {
            $charges[] = array(
                'chargeID' => 0,
                'priceItemID' => $cartItems->deliveryChargeItem->priceItemID,
                'sequence' => $cartItems->deliveryChargeItem->sequence,
                'Fields' => array(
                    (object) array('fieldID' => 9290, 'fieldValue' => 'Delivery charge'),
                    (object) array('fieldID' => 9289, 'fieldValue' => 'Online order - box rental'),
                    (object) array('fieldID' => 9288, 'fieldValue' => 1), // quantity
                    (object) array('fieldID' => 9287, 'fieldValue' => $cartItems->deliveryCharge), // unit price
                ),
            );
        }
        if ($cartItems->pickupCharge > 0 && !empty($cartItems->deliveryChargeItem)) {
            $charges[] = array(
                'chargeID' => 0,
                'priceItemID' => $cartItems->deliveryChargeItem->priceItemID,
                'sequence' => $cartItems->deliveryChargeItem->sequence,
                'Fields' => array(
                    (object) array('fieldID' => 9290, 'fieldValue' => 'Pickup charge'),
                    (object) array('fieldID' => 9289, 'fieldValue' => 'Online order - box rental'),
                    (object) array('fieldID' => 9288, 'fieldValue' => 1), // quantity
                    (object) array('fieldID' => 9287, 'fieldValue' => $cartItems->pickupCharge), // unit price

                ),
            );
        }

        $params = array(
            'method' => 3,
            'jobID' => $this->getJobID(),
            'clientID' => $clientID,
            'contactID' => $contacts['delivery'],
            'locationID' => $deliveryLocation->objectID,
            'lockID' => $submission->deliveryLock,
            'serviceTypeID' => 8, // always 8
        );

        if ($submission->post['delivery-city'] == $submission->post['pickup-city']) {
            $interfranchise = false;
            $deliveryOptionID = 243;
            $pickupOptionID = 248;
        }
        else {
            $interfranchise = true;
            $deliveryOptionID = 245;
            $pickupOptionID = 247;
        }
        $parts = explode('-', $submission->post['delivery-time']);
        $deliveryTime = $parts[1];
        date_default_timezone_set('UTC');
        $deliveryDate = strtotime($submission->post['delivery-date']) + ($deliveryTime * 60);

        $fields = array(
            (object) array('fieldID' => 9949, 'optionID' => $submission->deliveryRouteOption), // route
            (object) array('fieldID' => 200, 'fieldValue' => 'new job!'), // summary
            (object) array('fieldID' => 201, 'optionID' => $deliveryOptionID), // label 243 = delivery; 248 pickup; 245 if/delivery; 247 if/pickup
            (object) array('fieldID' => 187, 'fieldValue' => $this->subtotal), // subtotal
            (object) array('fieldID' => 181, 'optionID' => 161), // status = Open; 161 = booked today
//            (object) array('fieldID' => 184, 'fieldValue' => $deliveryLocation->objectID), // service location
            (object) array('fieldID' => 185, 'fieldValue' => $deliveryDate), // scheduled on
            (object) array('fieldID' => 186, 'fieldValue' => $co->getWindow($submission->post['delivery-city'])), // duration
            (object) array('fieldID' => 216, 'fieldValue' => $clientID), // client
        );

        $fRequest = $co->session($submission->post['delivery-city']);

        if ($fRequest->errNo != 0) {
            $errObj = (object) array('params' => $submission->post['delivery-city'], 'errMsg' => 'Invalid value for delivery city', 'element' => 'delivery-city');
            $co->releaseLock();
            $co->clearSecurityToken();
            return _ff_failed_submission($submission, null, $errObj);
        }
        $dwRequest = $co->workorders($params, $fields);
        if ($dwRequest->errNo != 0 || !$this->getJobID() || empty($dwRequest->WorkOrder->objectID)) {
            $dwRequest->params = $params;
            $dwRequest->fields = $fields;
            _ff_failed_submission($submission, null, $dwRequest);
        }



        // charges params
        $chParams = array(
            'objectID' => $dwRequest->WorkOrder->objectID,
            'objectTypeID' => 12, // workorder
            'method' => 2,
            'Charges' => $charges,
        );

	    //calculate tip and add to delivery charges
	    if($submission->post['tip']){
		    //also consider the discount. Tip should be calculated after promo discount
	    	$discount = 0;
		    if (!empty($this->promo) && $this->promo->isValid()) {
			    $discount = $this->promo->getDiscountAmount($cartItems->chargeTotal);
		    }
		    $discountedAmount = $cartItems->chargeTotal - $discount;
	    	$this->tip = $submission->post['tip'] * $discountedAmount / 100;
		    $chParams['tips'] = $this->tip;
	    }

        if (!empty($this->promo) && $this->promo->isValid()) {
            $chParams[$this->promo->getChargeProperty()] = $this->promo->getDiscount($this->subtotal);
        }

        // make sure we are switched to delivery city
        $fRequest = $co->session($submission->post['delivery-city']);
        if ($fRequest->errNo != 0) {
            $errObj = (object) array('errMsg' => 'Invalid value for delivery city', 'element' => 'delivery-city');
            $errObj->params = $submission->post['delivery-city'];
            $co->releaseLock();
            $co->clearSecurityToken();
            return _ff_failed_submission($submission, null, $errObj);
        }

        $chRequest = $co->charges($chParams);
        if ($chRequest->errNo != 0) {
            $chRequest->params = $chParams;
            $co->releaseLock();
            return _ff_failed_submission($submission, null, $chRequest);
        }

        // if it is an interfranchise workorder:
        //   1. assign the client to the second franchise
        //   2. create new job
        //   3. assign pickup workorder

        $pJobID = $this->getJobID();

        if ($interfranchise) {
            // switch franchise
            $fRequest = $co->session($submission->post['pickup-city']);

            if ($fRequest->errNo != 0) {
                $errObj = (object) array('errMsg' => 'Unable to set session using pickup city', 'element' => 'pickup-city');
                $errObj->params = $submission->post['pickup-city'];
                $errObj->request = $fRequest;
                $co->releaseLock();
                $co->clearSecurityToken();
                return _ff_failed_submission($submission, null, $errObj);
            }

            // assign client to franchisee
            $params = array(
                'method' => 7,
                'objectID' => $clientID,
            );
            $acRequest = $co->clients($params);
            if ($acRequest->errNo != 0) {
                $errObj = (object) array('errMsg' => 'Invalid value for client ID', 'element' => 'pickup-city');
                $errObj->params = $params;
                $errObj->request = $acRequest;
                return _ff_failed_submission($submission, null, $errObj);
            }

            // create pickup job
            $pjRequest = $co->jobs($jobParams, $jobFields);
            if ($pjRequest->errNo != 0) {
                $errObj = (object) array('errMsg' => 'Error creating pickup job', 'element' => 'pickup-city');
                $errObj->params = $jobParams;
                $errObj->fields = $jobFields;
                $errObj->request = $pjRequest;
                $co->releaseLock();
                return _ff_failed_submission($submission, null, $errObj);
            }
            $pJobID = $pjRequest->Job->jobID;
        }

        // build the pickup workorder object and submit
        // http://frogboxtraining.vonigo.com/api/v1/data/workorders/?securityToken=66446&method=3
        $params = array(
            'method' => 3,
            'jobID' => $pJobID,
            'clientID' => $clientID,
            'contactID' => $contacts['pickup'],
            'locationID' => $pickupLocation->objectID,
            'lockID' => $submission->pickupLock,
            'serviceTypeID' => 8, // always 8
        );

        $parts = explode('-', $submission->post['pickup-time']);
        $pickupTime = $parts[1];
        $pickupDate = strtotime($submission->post['pickup-date']) + ($pickupTime * 60);

        $fields = array(
            (object) array('fieldID' => 9949, 'optionID' => $submission->pickupRouteOption), // route
            (object) array('fieldID' => 200, 'fieldValue' => 'new job!'), // summary
            (object) array('fieldID' => 201, 'optionID' => $pickupOptionID), // label 243 = delivery; 248 pickup; 245 if/delivery; 247 if/pickup
            (object) array('fieldID' => 181, 'optionID' => 161), // status = Open; 161 = booked today
//            (object) array('fieldID' => 184, 'fieldValue' => $pickupLocation->objectID), // service location
            (object) array('fieldID' => 185, 'fieldValue' => $pickupDate), // scheduled on
            (object) array('fieldID' => 186, 'fieldValue' => $co->getWindow($submission->post['pickup-city'])), // duration
            (object) array('fieldID' => 216, 'fieldValue' => $clientID), // client
        );

        $pwRequest = $co->workorders($params, $fields);
        if ($pwRequest->errNo != 0) {
            $pwRequest->params = $params;
            $pwRequest->fields = $fields;
            $co->releaseLock();
            return _ff_failed_submission($submission, null, $pwRequest);
        }

        // for interfranchise jobs, update the inventory
        $jobID = $this->getJobID();
        if ($interfranchise && !empty($pJobID) && !empty($jobID)) {
            $params = array(
                'method' => 5,
                'objectID' => $this->getJobID(),
                'jobIFID' => $pJobID,
            );
            $inventoryRequest = $co->jobs($params);
            if ($inventoryRequest->errNo != 0) {
                $inventoryRequest->params = $params;
                $co->releaseLock();
                return _ff_failed_submission($submission, null, $inventoryRequest);
            }
        }
        $co->releaseLock();
        
	    //do the following only for Vancouver and Seattle
	    if(in_array($submission->post['delivery-city'], $co->stripe_locations)){
		    $stripeSecretKey = FF_Router::getStripeSecretKey($submission->post['delivery-city']);
		    $stripe_currency = FF_Router::getStripeCurrency($submission->post['delivery-city']);
		    // Set your secret key: remember to change this to your live secret key in production
		    // See your keys here: https://dashboard.stripe.com/account/apikeys
		    \Stripe\Stripe::setApiKey($stripeSecretKey);

		    $is_corporate = (isset($submission->post['is_corporate']) and ($submission->post['is_corporate'] === "1"));
		    if($is_corporate){
				//create customer account and invoice for corporate customers
				$stripe_customer_info = array(
					'name' => $submission->post['contact-first'] .' '. $submission->post['contact-last'],
					'phone' => $submission->post['contact-primary-phone'],
					'email' => $submission->post['contact-email'],
					'address' => array(
						'line1' => $deliveryLocation->street,
						'city' => $deliveryLocation->city,
						'country' => $deliveryLocation->country,
						'postal_code' => $deliveryLocation->postcode,
						'state' => $deliveryLocation->province ? $deliveryLocation->province : $deliveryLocation->state
					),
				);

				try{

					//check if customer already exists
					$customer_list = \Stripe\Customer::all(['limit' => 1, 'email' => $submission->post['contact-email']]);
					if(isset($customer_list->data) and !empty($customer_list->data) and $customer_list->data[0]->id){
						$stripe_customer_id = $customer_list->data[0]->id;
					}else{
						$stripe_customer =\Stripe\Customer::create($stripe_customer_info);
						$stripe_customer_id = $stripe_customer->id;
					}

					//create invoice items
					\Stripe\InvoiceItem::create([
						'customer' => $stripe_customer_id,
						'amount' => $cartItems->grandTotal*100,
						'currency' => $stripe_currency,
						'description' => 'Frogbox website order',
					]);

					//create an invoice
					\Stripe\Invoice::create([
						'customer' => $stripe_customer_id,
						'auto_advance' => false,
						'collection_method' => 'send_invoice',
						'days_until_due' => 30
					]);

				}
				catch (ApiErrorException $e){
					$lockError = new \stdClass;
					$lockError->errMsg = 'Customer creation failed. ' . __LINE__;
					//log error but dont stop
					return json_decode(FF_Router::routeError('Customer creation failed.', true, $lockError, true, __LINE__));
				}



		    }else{
			    //process the stripe payment for non corporate customers
			    if($submission->post['chargeId']){
				    //capture the charge
				    try{
					    $charge = \Stripe\Charge::retrieve($submission->post['chargeId']);
					    $charge->capture();
				    }catch (ApiErrorException $e){
					    $lockError = new \stdClass;
					    $lockError->errMsg = 'Payment charge failed. ' . __LINE__;
					    return json_decode(FF_Router::routeError('There was an error processing your order.', true, $lockError, false, __LINE__));
				    }

				    //update the Stripe charge description with job id
				    try{
					    \Stripe\Charge::update(
						    $submission->post['chargeId'],
						    ['description'=> 'Job ID: '. $jobID]
					    );
				    }catch (ApiErrorException $e){
					    return _ff_failed_submission($submission, null, $e->getError()->message);
				    }

				    //add "PREPAID" to Directions field(9713) of Location of DELIVERY Work Order
				    $params = array(
					    'method' => 2,
					    'clientID' => $clientID,
					    'objectID' => $deliveryLocation->objectID
				    );
				    $deliveryLocation->description = "PREPAID. " . $deliveryLocation->description;
				    $dlRequest = $co->locations($params, $deliveryLocation->asFields());
				    if ($dlRequest->errNo != 0) {
					    $errObj = (object) array('errObj' => $dlRequest, 'element' => 'delivery-address');
					    $errObj->params = $params;
					    $errObj->fields = $deliveryLocation->asFields();
					    $co->releaseLock();
					    return _ff_failed_submission($submission, null, $errObj);
				    }
			    }else{
				    $lockError = new \stdClass;
				    $lockError->errMsg = 'Payment charge failed. ' . __LINE__;
				    return json_decode(FF_Router::routeError('There was an error processing your order.', true, $lockError, false, __LINE__));
			    }
		    }

	    }




        $this->submission = $submission;

        return $submission;
    }

    /**
     * Send email confirmation to customer via InfusionSoft.
     */
    public function emailConfirmation() {

        $submission = $this->submission;
        $theme = $this->theme();

        $result = false;

        // The Infusionsoft SDK loves to throw exceptions. The calls need to be
        // wrapped in a try/catch block.
        try {

            // add custom fields to infusionsoft contact object
            Infusionsoft_Contact::addCustomField('_JobID');
            Infusionsoft_Contact::addCustomField('_Franchise');
            Infusionsoft_Contact::addCustomField('_NextDeliveryDate0');
            Infusionsoft_Contact::addCustomField('_NextPickUpDate');
            Infusionsoft_Contact::addCustomField('_DeliveryWindow');
            Infusionsoft_Contact::addCustomField('_PickUpWindow');
            Infusionsoft_Contact::addCustomField('_AdditionalOrderInfo');
            Infusionsoft_Contact::addCustomField('_CAA');
            //Infusionsoft_Contact::addCustomField('_MoverRecommendation');

            Infusionsoft_Contact::addCustomField('_Subtotal');
            Infusionsoft_Contact::addCustomField('_Discount');
            Infusionsoft_Contact::addCustomField('_Taxes');
            Infusionsoft_Contact::addCustomField('_Total');

            // add new contact - not 'addWithDupCheck' - duplicate email addresses are
            // required for clients with multiple orders
            $data = array(
                'FirstName' => $submission->post['contact-first'],
                'LastName' => $submission->post['contact-last'],
                'Email' => $submission->post['contact-email'],
            );
            $contactID = intval(Infusionsoft_ContactServiceBase::add($data));
            if ($contactID < 2) {
                $msg = 'Could not create Infusionsoft record for client.';
                return _ff_failed_submission($submission, null, $msg);
            }

            $chargesCount = sizeof($theme['charges']->charges);

            // initialize charges
            for ($chargeNo = 1; $chargeNo <= $chargesCount; $chargeNo++) {
                if ($chargeNo == 1) {
                    $chargeKey = '_DeliveryCharges1';
                }
                elseif ($chargeNo == 17) {
                    $chargeKey = '_Charges170';
                }
                else {
                    $chargeKey = '_Charges' . $chargeNo;
                }
                Infusionsoft_Contact::addCustomField($chargeKey);
            }

            // initialize promo field if it is present
            if (!empty($theme['charges']->promo)) {
                Infusionsoft_Contact::addCustomField('_Promocode');
            }

            $contact = new InfusionSoft_Contact($contactID);

            // opt in the user
            $optin = Infusionsoft_EmailService::optIn($submission->post['contact-email'], 'Customer agreed to Terms of Service');

            // gather data and update IS fields
            $contact->_JobID = $this->getJobID();
            $contact->_Franchise = _ff_is_franchise_name($submission->post['delivery-city']);
            $contact->_NextDeliveryDate0 = $submission->post['delivery-date'];
            $contact->_DeliveryWindow = $theme['deliveryWindow'];
            $contact->_NextPickUpDate = $submission->post['pickup-date'];
            $contact->_PickUpWindow = $theme['pickupWindow'];

            // map delivery address to address 1
            $contact->StreetAddress1 = $submission->post['deliveryStreet'];
            $contact->City = $submission->post['deliveryCity'];
            $contact->State = $submission->post['deliveryProvince'];
            $contact->PostalCode = $submission->post['deliveryPostcode'];

            // map pickup address to address 2
            $contact->Address2Street1 = $submission->post['pickupStreet'];
            $contact->City2 = $submission->post['pickupCity'];
            $contact->State2 = $submission->post['pickupProvince'];
            $contact->PostalCode2 = $submission->post['pickupPostcode'];

            // charges and pricing info
            $chargeNo = 1;
            foreach ($theme['charges']->charges as $charge) {
                // skip move pack in output
                if (stripos($charge, 'move pack') !== false) {
                    continue;
                }
                if ($chargeNo == 1) {
                    $contact->_DeliveryCharges1 = $charge;
                }
                elseif ($chargeNo == 17) {
                    $contact->_Charges170 = $charge;
                }
                else {
                    $chargeKey = '_Charges' . $chargeNo;
                    $contact->{$chargeKey} = $charge;
                }
                $chargeNo++;
            }
            $contact->_Subtotal = $theme['charges']->subtotal;
            $contact->_Discount = $theme['charges']->discount;
            $contact->_Taxes = $theme['charges']->taxes;
            $contact->_Total = $theme['charges']->total;

            // promo code if it is present
            if (!empty($theme['charges']->promo)) {
                $contact->_Promocode = $theme['charges']->promo;
            }

            // caa number if present
            if (!empty($submission->post['caaID'])) {
                $contact->_CAA = $submission->post['caaID'];
            }

            // phone
            $contact->Phone1 = $submission->post['contact-primary-phone'];
            if (!empty($submission->post['contact-primary-ext'])) {
                $contact->Phone1Ext = $submission->post['contact-primary-ext'];
            }
            if (!empty($submission->post['contact-alternate-phone'])) {
                $contact->Phone2 = $submission->post['contact-alternate-phone'];
            }
            if (!empty($submission->post['contact-alternate-ext'])) {
                $contact->Phone2Ext = $submission->post['contact-alternate-ext'];
            }

            /*
              $contact->_AdditionalOrderInfo = '';
            */

            $result = $contact->save();
            if ($result != $contactID) {
                $msg = 'Could not update custom fields for Infusionsoft client ' . $contactID . '.';
                _ff_failed_submission($submission, null, $msg);
            }

            // tag with franchise FIRST
            $tags = _ff_is_franchise_id_tags();
            $franchiseTagResult = Infusionsoft_ContactService::addToGroup($contactID, $tags[$submission->post['delivery-city']]);
            if ($franchiseTagResult != 1) {
                $fTags = _ff_is_franchise_id_tags();
                $msg = 'Could not tag submission for client ' . $contactID . ' with "'
                    . $fTags[$submission->post['delivery-city']] . '" in Infusionsoft.';
                _ff_failed_submission($submission, null, $msg);
            }

            // tag with 'open job' SECOND
            $openJobTagResult = Infusionsoft_ContactService::addToGroup($contactID, 118); // "open job"
            if ($openJobTagResult != 1) {
                $msg = 'Could not tag submission for client ' . $contactID . ' with "Open Job" in Infusionsoft.';
                _ff_failed_submission($submission, null, $msg);
            }

            // tag with 'website conversion' THIRD
            $openJobTagResult = Infusionsoft_ContactService::addToGroup($contactID, 230); // "website conversion"
            if ($openJobTagResult != 1) {
                $msg = 'Could not tag submission for client ' . $contactID . ' with "Website Conversion" in Infusionsoft.';
                _ff_failed_submission($submission, null, $msg);
            }

            // tag with 'Mover recommendation' if applicable
            if ($submission->post['mover-recommendations']) {
                $moverRecTagResult = Infusionsoft_ContactService::addToGroup($contactID, 214); // "mover rec"
                if ($moverRecTagResult != 1) {
                    $msg = 'Could not tag submission for client ' . $contactID . ' with "Mover Rec" in Infusionsoft.';
                    _ff_failed_submission($submission, null, $msg);
                }
            }

            // if there is an 'isTag' field, use that tag as well\
            if (!empty($submission->post['isTag'])) {
                $moverRecTagResult = Infusionsoft_ContactService::addToGroup($contactID, $submission->post['isTag']);
                if ($moverRecTagResult != 1) {
                    $msg = 'Could not tag submission for client ' . $contactID . ' with tag "' . $submission->post['isTag'] . '" in Infusionsoft.';
                    _ff_failed_submission($submission, null, $msg);
                }
            }

        }
        catch (\Exception $e) {
            _ff_failed_submission($submission, null, $e->getMessage() . ' at ' . __LINE__);
        }

        return $result;
    }

    private function getJobID() {
        return $this->jobID;
    }

    public function theme() {

        $submission = $this->submission;

        $deliveryWindow = \frogbox::getWindow($submission->post['delivery-city']);
        $pickupWindow = \frogbox::getWindow($submission->post['pickup-city']);

        $theme = array(
            'confirmation' => $this->getJobID(),
            'delivery-city' => $submission->post['delivery-city'],
            'contact-email' => $submission->post['contact-email'],
            'deliveryDate' => $submission->post['delivery-date'],
            'pickupDate' => $submission->post['pickup-date'],
            'deliveryWindow' => $this->formatAppointmentTime($submission->post['delivery-time'], $deliveryWindow),
            'pickupWindow' => $this->formatAppointmentTime($submission->post['pickup-time'], $pickupWindow),
            'deliveryAddress' => implode('<br>', array(
                    $submission->post['deliveryStreet'],
                    $submission->post['deliveryCity'],
                    $submission->post['deliveryProvince'],
                    $submission->post['deliveryPostcode']
            )),
            'pickupAddress' => implode('<br>', array(
                $submission->post['pickupStreet'],
                $submission->post['pickupCity'],
                $submission->post['pickupProvince'],
                $submission->post['pickupPostcode']
            )),
            'charges' => $this->themeCharges(),
            'mover-recommendations' => ($submission->post['mover-recommendations']) ? 'Please send me recommendations on credible movers.' : '',

        );

        return $theme;
    }

    private function themeCharges() {
        $submission = $this->submission;

        /**
         * Gets the charges list ready for display as HTML.
         */
        $cart = json_decode($submission->post['frogboxCart']); // stripslashes has been called before re-encoding
        $subtotal = 0;
        $markup = '';
        $charges = array();
        $gaProducts = array();
        $gaPurchase = new \stdClass;

        foreach (array($cart->bundles, $cart->supplies) as $list) {
            foreach ($list as $item) {
                // skip 'move pack'
                if (stripos($item->name, 'move pack') !== false) {
                    continue;
                }
                if ($item->quantity > 0) {
                    $subtotal += $item->value * $item->quantity;
                    $gaProducts[] = _ff_formatPriceItemGA($item);
                    if (empty($item->bundle_items)) {
                        $markup .= _ff_formatPriceItem($item->name, $item->quantity);
                        $charges[] = $item->name . ' X ' . $item->quantity;
                    }
                    else {
                        $markup .= '<div class="bundle-details">' . $item->name . '<ul>';
                        foreach ($item->bundle_items as $bundleItem => $bundleQuantity) {
                            // skip move pack
                            if (stripos($bundleItem, 'pack') === false) {
                                $markup .= '<li>' . _ff_formatPriceItem($bundleItem, $bundleQuantity) . '</li>';
                                $charges[] = $bundleItem . ' X ' . $bundleQuantity;
                            }
                            else {
                                $details = _ff_extractMovePack($item->description_details);
                                foreach ($details as $dItem => $dQuantity) {
                                    $markup .= '<li>' . _ff_formatPriceItem($dItem, $dQuantity) . '</li>';
                                    $charges[] = $dItem . ' X ' . $dQuantity;
                                }
                            }
                        }
                        $markup .= '</ul></div>';
                    }
                }
            }
        }

        if ($cart->deliveryCharge > 0) {
            $charges[] = 'Delivery Charge: ' . '$' . $cart->deliveryCharge;
            $markup .= _ff_formatPriceItem('Delivery Charge', '$' . number_format((float) $cart->deliveryCharge, 2, '.', ''));
            $subtotal += $cart->deliveryCharge;
        }

        if ($cart->pickupCharge > 0) {
            $charges[] = 'Pickup Charge: ' . '$' . $cart->pickupCharge;
            $markup .= _ff_formatPriceItem('Pickup Charge', '$' . number_format((float) $cart->pickupCharge, 2, '.', ''));
            $subtotal += $cart->pickupCharge;
        }

        $discount = 0;
        if (!empty($this->promo) && $this->promo->isValid()) {
            $discount = $this->promo->getDiscountAmount($subtotal);
        }

        $taxable = $subtotal - $discount;
        $taxes = $taxable * $cart->taxPercent / 100;
        $total = $taxable + $taxes;

        //add tip to total
	    $total += $this->tip;

        $gaPurchase->id = $submission->confirmation;
        $gaPurchase->affiliation = '';
        $gaPurchase->revenue = money_format('%.2n', $total);
        $gaPurchase->tax = money_format('%.2n', $taxes);
        $gaPurchase->shipping = '';
        $gaPurchase->coupon = $cart->promo->promoCode;

        $price = '<div class="order-subtotal"><span class="pricing-description">Subtotal:</span><span class="pricing-price">';
        $price .= money_format('%.2n', $subtotal) . '</span></div>';
        if ($cart->promo != null && $this->promo->getDiscountAmount($subtotal) > 0) {
            $price .= '<div class="order-discount"><span class="pricing-description promo">';
            $price .= $cart->promo->promoCode . '</span><span class="pricing-price">-';
            $price .= money_format('%.2n', $discount) . '</span></div>';
        }
        $price .= '<div class="order-tax"><span class="pricing-description tax">Taxes:</span><span class="pricing-price">';
        $price .= money_format('%.2n', $taxes) . '</span></div>';
        if($this->tip){
	        $price .= '<div class="order-tax"><span class="pricing-description tip">Tip:</span><span class="pricing-price">';
	        $price .= money_format('%.2n', $this->tip) . '</span></div>';
        }
	    $price .= '<div class="order-total"><span class="pricing-description">Total:</span><span class="pricing-price">';
        $price .= money_format('%.2n', $total) . '</span></div>';

        // add the product data to google analytics
        wp_localize_script('frogbox-custom-ga', 'fb_ga_vars', array(
                'products' => json_encode($gaProducts),
                'action' => json_encode($gaPurchase),
            )
        );

        $return = (object) array(
            'markup' => $markup,
            'price' => $price,
            'promo' => $cart->promo->promoCode,
            'charges' => $charges,
            'subtotal' => money_format('%.2n', $subtotal),
            'discount' => money_format('%.2n', $discount),
            'taxes' => money_format('%.2n', $taxes),
            'total' => money_format('%.2n', $total),
        );
        return $return;

    }

    /**
     * Formats an appointment time, ie 10:30am-12:30pm based on a routeTime and
     * duration.
     */
    public function formatAppointmentTime($routeTime, $duration = 120) {
        $parts = explode('-', $routeTime);
        return _ff_formatTime($parts[1]) . ' to ' . _ff_formatTime($parts[1] + $duration);
    }

}
