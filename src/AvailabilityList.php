<?php

class AvailabilityList
{
    /**
     * An array of stdClass objects.
     *
     * @var array
     */
    private $objects = array();

    /**
     * AvailabilityList constructor.
     * @param $objects
     */
    public function __construct($objects) {
        $this->objects = $objects;
    }

    /**
     * Sets the $objects property to the subset of objects that match the given date.
     *
     * @param $date
     */
    public function filterOptionsByDate($date) {
        $groupedOptions = $this->groupObjectArrayBy( $this->objects, 'dayID');
        $this->objects = $groupedOptions[$date];
    }

    /**
     * Returns all objects as an array.
     *
     * @return array
     */
    public function getAllOptions() {
        return $this->objects;
    }

    /**
     * Returns the list of objects that are bookable for a given size of time slot.
     *
     * @param $duration
     * @return array
     */
    public function getBookableOptions($duration) {
        $bookable = array();
        $groupedByDate = $this->groupByDate();

        foreach ($groupedByDate as $dayID => $options) {
            $groupedByRoute = $this->groupObjectArrayBy($options, 'routeID');

            foreach ($groupedByRoute as $routeID => $choices) {
                $bookable = array_merge($bookable, $this->findOptionSequences($choices, $duration));
            }
        }
        return $bookable;
    }

    /**
     * Returns the list of unique dates included in the object list.
     *
     * @return array
     */
    public function getEnabledDays() {
        return array_filter(array_unique($this->extractProperty('dayID')));
    }

    /**
     * Returns an array containing a specific property for each object in the $objects property.
     *
     * @param $property
     * @return array
     */
    private function extractProperty($property) {
        return array_map(function ($object) use ($property) {
            if (isset($object->{$property})) {
                return $object->{$property};
            }
            return null;
        }, $this->objects);
    }

    /**
     * Returns a subset of a given array that are bookable, given a specific size of time slot or duration.
     *
     * @param $choices
     * @param $duration
     * @return array
     */
    private function findOptionSequences($choices, $duration) {
        $sequences = array();

        $targetCount = $duration / 30;

        $keyed = $this->keyObjectArrayBy( $choices, 'startTime' );
        ksort($keyed);
        $keys = array();
        foreach (array_keys($keyed) as $key) {
            $keys[] = $key/30;
        }

        for ($i = 0; $i < count($keys) - $targetCount + 1; $i++) {
            $slice = array_slice($keys, $i, $targetCount);
            if ($this->isNumericSequence($slice)) {
                $sequences[] = $keyed[$keys[$i] * 30];
            }
        }

        return $sequences;
    }

    /**
     * Returns an array of arrays, grouping objects by dayID.
     * @return array
     */
    private function groupByDate() {
        return $this->groupObjectArrayBy($this->objects, 'dayID');
    }

    /**
     * Returns an array of arrays, grouped by a given property.
     *
     * @param $array
     * @param $key
     * @return array
     */
    private  function groupObjectArrayBy($array, $key) {
        $return = array();
        if (empty($array)) {
            return $return;
        }
        foreach ($array as $object) {
            if (!empty($object->{$key})) {
                if (empty($return[$object->{$key}])) {
                    $return[$object->{$key}] = array();
                }
                $return[$object->{$key}][] = $object;
            }
        }
        return $return;
    }

    /**
     * Returns an array of the objects keyed by a specific property.
     *
     * @param $array
     * @param $key
     * @return array
     */
    private  function keyObjectArrayBy($array, $key) {
        $return = array();
        if (empty($array)) {
            return $return;
        }
        foreach ($array as $object) {
            if (!empty($object->{$key})) {
                $return[$object->{$key}] = $object;
            }
        }
        return $return;
    }

    /**
     * Returns true if the array is a numeric sequence of any length (ie [1,2,3,4] but not [1,2,3,4,6]).
     * @param $slice
     * @return bool
     */
    private function isNumericSequence($slice) {
        sort($slice);
        $lastIndex = count($slice) - 1;
        return ($slice[$lastIndex]) == $lastIndex + $slice[0];
    }

}