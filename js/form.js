// @codekit-prepend "ff_functions.js"
// @codekit-prepend "components/step1.js"
// @codekit-prepend "components/step2.js"
// @codekit-prepend "components/step3.js"
// @codekit-prepend "components/step4.js"
// @codekit-prepend "components/step5.js"
// @codekit-prepend "components/validate.js"
// @codekit-prepend "components/accordion.js"

// define these variables in the global scope
frogboxCart = null;
disabledDates = {'delivery': [], 'pickup': []};
pendingRequests = {};
// Set your publishable key: remember to change this to your live publishable key in production
// See your keys here: https://dashboard.stripe.com/account/apikeys

//frogbox locations for which the the Stripe is active
//Vancouver and Seattle
stripeLocations = ['V6G', '98101'];
stripeLocationIds = ["2"];

$(document).ready(function () {

    var steps = new Array(Step1, Step2, Step3, Step4, Step5);

    /**
     * Load the code for our steps and render it.
     *
     * Markup should be rendered before any event listeners are created in .load().
     */
    for (var i = 0; i < steps.length; ++i) {
        $('#step-' + (i+1)).append(steps[i].render());
        steps[i].load();
     }

    Validator.validate();

    /**
     * Look for a submission object. If it exists, set form fields based on submission.
     */
    if (window.Submission) {
        // set the page title if there was an error
        document.title = pageData.title;
        $('.page-header h1').html(pageData.title);

        frogboxCart = Submission.frogboxCart;
        delete Submission.frogboxCart;
        for (var property in Submission) {
            if (Submission.hasOwnProperty(property)) {
                if (property != 'terms' && property.substr(property.length -8) != 'quantity') {
                    $("*[name=" + property + "]").not('*[type="radio"]').val(Submission[property]);
                }
            }
        }

        // initialize availability
        ff_functions.initializeAvailability();

        // load pricelists by calling .change() method on .city elements
        $('.city').change();

        // load dates by triggering changeDate event on .booking-date elements
        $('.booking-date').trigger('changeDate');

        // validate promo if it exists by calling .change() method
        if ($('#promo-code').val().length) {
            $('#promo-code').change();
        }

        // if caaID is set, add a hidden form element for it
        if (Submission.caaID) {
            var caaElement = '<input type="hidden" name="caaID" value="' + Submission.caaID+ '">'
            $('#frogbox-form').append(caaElement);
        }

        // choose the bundle in step 2 and call the change event
        $('#bundle-' + Submission.bundle).prop('checked', true).change();
    }

    /**
     * Display errors, if any.
     */
    if (window.Errors) {
        // display the errors
        for (var n = 0; n < window.Errors.length; n++) {
            var newElement = '<p class="error">' + window.Errors[n].errMsg + '</p>';
            $('#frogbox-form').prepend(newElement);
            if ($('#' + window.Errors[n].element).length) {
                $(newElement).insertBefore('#' + window.Errors[n].element);
            }
        }
    }

    /**
     * Accordion runs after validation
     */
    Accordion.accordion();

    /**
     * Open the accordion to where the first error is, if any.
     */
    if (window.Errors) {
        var first = Errors[0];
        var openStep = $('#' + Errors[0].element).closest('.step-content').attr('id')[5] - 1;
        if (openStep > 3) { // select new dates in step 4
            openStep = 3;
        }
        $('#accordion').accordion('option', 'active', ( openStep ));
        Accordion.scrollToStep(openStep);
    }

});