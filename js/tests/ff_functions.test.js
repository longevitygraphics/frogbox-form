const ff_functions = require('../ff_functions');

test('calculates the difference between two dates correctly', () => {
  expect(ff_functions.dateDifference('2018-08-28', '2018-09-20')).toBe(23);
});
