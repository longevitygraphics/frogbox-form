/* closure for functions so we can access them from other scripts */
var ff_functions=function(){
// formats the keep for info to be pretty and consistent
function C(e){e||(e=parseInt(jQuery("#keep-for").val(),10));var r=parseInt(e/7,10),i="";0<r&&(i=r+" week"),1<r&&(i+="s");var t=Math.round(e%7),o="";0<t&&(o=t+" day"),1<t&&(o+="s");var n="",a;return 0<r&&0<t&&(n=", "),i+n+o}
// gets a property for an item for a pricelist
function i(e,r,i){for(var t=0;t<2;++t){var o=new Array("bundles","supplies")[t];if(i[o]&&Array.isArray(i[o]))for(var n=0;n<i[o].length;++n)if(r===i[o][n].key)return i[o][n][e]}return 0}
// gets a quantity from the data model for a specific item
function n(e){return i("quantity",e,window.frogboxCart)}
// gets the value of an item in a given pricelist
function c(e,r){return i("value",e,r)}
// calculates a total for a pricelist based on what is in the frogboxCart
function s(e){var r=window.frogboxCart;if(!e)return 0;var i=0;for(var t in{bundles:"",supplies:""})if(void 0!==r[t]&&r[t].length&&0<r[t].length)for(var o=0;o<r[t].length;++o)0<r[t][o].quantity&&(i+=r[t][o].quantity*c(r[t][o].key,e));var n=0,a;return null!=e.taxPercent&&(n=i*e.taxPercent/100),Number(i+n).toFixed(2)}function P(e,r){var i;return'<div class="product"><span class="product-name">'+e+'</span><span class="product-quantity">'+r+"</span></div>"}function u(e){window.pendingRequests[e]=1}function p(e){window.pendingRequests[e]=0}function f(e){return 1===window.pendingRequests[e]}function t(t){var e="/ff/serviceCharge/"+t;
// if the request is pending, don't request it a second time
if(!f(e)){if(void 0===window.serviceCharges)window.serviceCharges={};else if(void 0!==window.serviceCharges[t])return window.serviceCharges[t];u(e),$.ajax({dataType:"json",url:e,async:!0,complete:function(){p(e),m()},success:function(e,r,i){window.serviceCharges[t]=0<e?e:0},error:function(e,r,i){console.log("error",e,r,i)}})}}
// calculate delivery charege
function S(){var e;return r(ff_functions.getDeliverySuburb())}
// calculate pickup charge
function D(){var e;return r(ff_functions.getPickupSuburb())}
// calculate service charge
function r(e){return void 0===window.serviceCharges&&(window.serviceCharges={}),void 0===window.serviceCharges[e]?(window.setTimeout(function(){t(e)},Math.floor(1e3*Math.random())+1),0):+window.serviceCharges[e];var r}
// update delivery, pickup and taxes
function _(){null==window.frogboxCart&&(window.frogboxCart={}),
// read delivery charge and taxes from delivery priceslist
null!=ce.delivery&&null!=ce.delivery&&(window.frogboxCart.taxPercent=ce.delivery.taxPercent)}
// process a pricelist - called from $.ajax:success or statically
function v(e,r){"IF"===r?G("interfranchise",e):r?G("delivery",e):!1===r&&G("pickup",e),null!=X("interfranchise")?o(X("interfranchise"),r):null!=X("delivery")&&null!=X("pickup")&&(Z()?o(X("delivery"),r):o(X("pickup"),r)),w()}
// returns true if an order is interfranchise
function l(){var e,r;return $("#delivery-city").val()!==$("#pickup-city").val()}
// updateModel takes a new model object and overwrites the old one
function o(e,r){
// if frogboxCart is empty, set its value to the incoming data
if(null==window.frogboxCart)return window.frogboxCart=e,void(window.frogboxCart.groupsInit=0);
// otherwise, go through each item and update the quantity
for(var i=0;i<2;++i)for(var t=new Array("bundles","supplies")[i],o=0;o<e[t].length;++o)e[t][o].name&&(e[t][o].quantity=n(e[t][o].key));window.frogboxCart=e,m()}
// extract the move pack details from the dimension fields
function T(e){for(var r=0;r<e.length;++r)if("Move Pack"===e[r].title){var i=e[r].dimensions.split("<br>");return e[r].dimensions.split("<br>")}}function j(e){
// check for valid promo code
if($("#promoThresholdNotMet").remove(),null==window.frogboxCart.promo)return 0;
// check to see that we meet the threshold
if(!isNaN(window.frogboxCart.promo.promoAmountIfMoreThen)&&e<window.frogboxCart.promo.promoAmountIfMoreThen){var r,i='<span class="error" id="promoThresholdNotMet">'+(" You must purchase at least $"+window.frogboxCart.promo.promoAmountIfMoreThen+" of goods and services to use this promo code. ")+"</span>";return $("#promo-valid").length?$(i).insertAfter("#promo-valid"):$(i).insertAfter("#promo-code"),0}return"percent"==window.frogboxCart.promo.promoMeasure?Math.ceil(e*window.frogboxCart.promo.promoDiscount)/100:window.frogboxCart.promo.promoDiscount}
// generate a summary of items in the frogbox cart
function m(){
// if there's a keep-for error, copy it to the sidebar and return
if(_(),jQuery("#keep-for-error").length){var e=jQuery("#keep-for-error").clone();return jQuery("#your-price-price").html(e),void jQuery("#your-order").addClass("group-hidden")}if(null!=window.frogboxCart){
// override frogboxCart.minimumCharge with window.minimumCharge
window.frogboxCart.minimumCharge=window.minimumCharge.minimumCharge;var r=0,i="",t=0,o=0;for(var n in jQuery("#your-order").removeClass("group-hidden"),{bundles:"",supplies:""})if(null!=window.frogboxCart[n]&&0<window.frogboxCart[n].length)for(var a=0;a<window.frogboxCart[n].length;++a)if(0<window.frogboxCart[n][a].quantity)if(r+=window.frogboxCart[n][a].quantity*window.frogboxCart[n][a].value,null!=window.frogboxCart[n][a].bundle_items){for(var c in window.frogboxCart[n][a].bundle_items)if(0<c.length&&"Pack"!=c)i+=P(c,window.frogboxCart[n][a].bundle_items[c]);else if("Pack"==c&&window.frogboxCart[n][a].description_details)for(var s=T(window.frogboxCart[n][a].description_details),l=0;l<s.length;++l){var d=s[l].trim().split(/ (.+)/,2);i+=P(d[1],d[0])}}else i+=P(window.frogboxCart[n][a].name,window.frogboxCart[n][a].quantity);
// if there is a subtotal, calculate delivery and pickup charges
if(r){var u=S(),p=D(),f=r+u+p;if(
// make sure we clear delivery charge message before creating it
jQuery("#minimum-charge-wrapper").remove(),f<window.frogboxCart.minimumCharge){var v=window.frogboxCart.minimumCharge-f,m="<div id='minimum-charge-wrapper'><input type='checkbox' name='minimum-charge' id='minimum-charge' required><label for='minimum-charge'>I understand that I will be charged a delivery fee to meet the $"+window.minimumCharge.minimumCharge+" minimum charge.</label><a href='#step-3-header'>Add more products instead</a></div>";
// add required checkbox to step 5 if this a local list that usually doesn't have a delivery charge
jQuery(m).insertBefore("#payment-wrapper"),jQuery("#minimum-charge-wrapper a").click(function(){$("#accordion").accordion("option","active",2),Accordion.scrollToStep(3)}),u<v&&(u+=v)}0<u&&(r+=u,i+=P("Delivery Charge","$"+u.toFixed(2))),frogboxCart.deliveryCharge=u,0<p&&(r+=p,i+=P("Pickup Charge","$"+p.toFixed(2))),frogboxCart.pickupCharge=p}var b=j(r),y=r-b;(t=Math.ceil(y*window.frogboxCart.taxPercent)/100)||(t=0),o=t+y;var h='<div class="order-subtotal"><span class="pricing-description">Subtotal:</span><span class="pricing-price">$'+r.toFixed(2)+"</span></div>";0<(b=parseInt(b))&&(h+='<div class="order-discount"><span class="pricing-description promo">'+window.frogboxCart.promo.promoCode+'</span><span class="pricing-price">-$'+b.toFixed(2)+"</span></div>"),h+='<div class="order-tax"><span class="pricing-description">Tax:</span><span class="pricing-price">$'+t.toFixed(2)+"</span></div>",h+='<div class="order-total"><span class="pricing-description">Total:</span><span class="pricing-price">$'+o.toFixed(2)+"</span></div>",jQuery("#your-price-price").html(h),jQuery("#your-price-products").html(i),jQuery("#mobile-order-price").html("$"+o.toFixed(2)),jQuery("#your-order-extra").html(""),
// append (change) if your-order is set
0<jQuery("#your-order").length&&(jQuery("#your-price-products").append('<div class="change-link">(<a href="#" id="change-order">change</a>)</div>'),jQuery("#change-order").click(function(){jQuery("#accordion").accordion({active:1});// accordion sections start numbering at 0; switch to #2
}));
// append delivery & pickup locations and (change)
var g=ff_functions.getDeliverySuburb(),w=ff_functions.getPickupSuburb();g&&w&&(jQuery("#your-order-extra").append('<h4>Delivery:</h4><div class="sidebar-detail">'+jQuery("#delivery-city option:selected").text()+'</div><h4>Pick-up:</h4><div class="sidebar-detail">'+jQuery("#pickup-city option:selected").text()+'</div><div class="change-link">(<a href="#" id="change-locations">change</a>)</div>'),jQuery("#change-locations").click(function(){jQuery("#accordion").accordion({active:0});// accordion sections start numbering at 0; switch to #1
})),jQuery("#delivery-date").val()&&jQuery("#pickup-date").val()&&jQuery("#your-order-extra").append('<h4>Keep for:</h4><div class="sidebar-detail">'+C()+"</div>");var k=q("delivery"),x=q("pickup");k&&x&&(jQuery("#your-order-extra").append('<h4>Delivery Address:</h4><div class="sidebar-detail">'+k+'</div><h4>Pick-up Address:</h4><div class="sidebar-details">'+x+'</div><div class="change-link">(<a href="#" id="change-address">change</a>)</div>'),jQuery("#change-address").click(function(){jQuery("#accordion").accordion({active:3});// accordion sections start numbering at 0; switch to #4
})),Q()}}
// lookup province based on first letter of post code
function d(e){var r;if(e)return{A:"Newfoundland and Labrador",B:"Nova Scotia",C:"Prince Edward Island",E:"New Brunswick",G:"Quebec",H:"Quebec",J:"Quebec",K:"Ontario",L:"Ontario",M:"Ontario",N:"Ontario",P:"Ontario",R:"Manitoba",S:"Saskatchewan",T:"Alberta",V:"British Columbia",X:"Northwest Territories|Nunavut",Y:"Yukon Territory"}[e.substring(0,1).toUpperCase()]}
// lookup state based on first two numbers in zip code
function b(e){lookup=e.substring(0,5);var r,i={83:"Idaho",55:"Minnesota",56:"Minnesota",98:"Washington",99:"Washington"}[lookup.substring(0,2)];return"Washington"===i&&99500<+lookup&&(i=void 0),
// it is an error if the code is less than 5  letters
(void 0===e.length||e.length<5)&&(i=void 0),i}
// update a pricelist
// url - ajax url to get pricelist from
// throbberID - ID of throbber to remove after ajax is complete
// errorID - div to write errors in
// errorParent - the 'for' attribute of the label element to place the error in
// delivery - boolean, is this the pricelist for delivery or not
// update - boolean - update the model
function y(a,e,c,s,l,d,r){if($("#"+c).remove(),null!=X(a))return d&&v(X(a),l),r&&r(),window.setTimeout(function(){
// don't remove the throbber immediately - give items time to render
$(e).remove()},1e3),!0;
// if the request is pending, don't request it a second time
f(a)?window.setTimeout(function(){y(a,e,c,s,l,d,r)},1e3):(u(a),$.ajax({dataType:"json",url:a,async:!0,success:function(e,r,i){if(!e||null!=e.error&&e.displayError){$("#"+c).remove();var t='<div class="error" id="'+c+'">'+e.error+"</div>";$('label[for="'+s+'"]').length?$(t).appendTo('label[for="'+s+'"]'):$(t).appendTo("#"+s),m()}else
// expect an array of pricelists
for(var o="",n=0;n<e.length;++n)
// if we are getting 4 priceslists, keep track of them all
4==e.length&&(a=(o=a.substring(0,a.lastIndexOf("/")+1))+7*(n+1)),0<n&&(d=!1),G(a,e[n]),d&&v(e[n],l)},complete:function(){p(a),r&&r(),window.setTimeout(function(){
// don't remove the throbber immediately - give items time to render
$(e).remove()},1e3)},error:function(e,r,i){console.log("json parse error",e,r,i)}}))}function a(){if(null==window.frogboxCart.zip)return[];l()?zip=window.frogboxCart.zip:zip=ff_functions.getDeliverySuburb();for(var e="/ff/pricelist/"+zip+"/",r=[],i=7;i<=28;i+=7){priceList=e+i;var t,o,n,a=+s(ce[priceList])+ +S()+ +D();r.push({price:a.toFixed(2),weeks:C(i)})}return r}
// updates the 'your current order' section
function Q(){for(var e=a(),r="",i=0;i<e.length;i++)r+="<li>$"+e[i].price+" for "+e[i].weeks+"</li>";jQuery("#current-order-price-by-duration").html(r)}
// returns a string with an address formatted as HTML
function q(e){var r=jQuery("#"+e+"Street").val(),i=jQuery("#"+e+"City").val(),t=jQuery("#"+e+"Province").val(),o=jQuery("#"+e+"Postcode").val(),n=jQuery("#"+e+"Description").val(),a="";return r?(a+=r,i?(a+="<br>"+i,t&&(a+=", "+t),o?(a+="<br>"+o,n&&(a+="<br>"+n),a):""):""):""}
// format a single item
function h(e,r){r||(r="product");
// format the columns
var i='<div class="'+r+'">';// close product
return null!=e.wrapper&&(i+=e.wrapper),i+='<div class="inner-border">',i+='<div class="col1">',null!=e.col1&&(i+=e.col1),i+='</div><div class="col2">',null!=e.col2&&(i+=e.col2),i+='</div><div class="col3">',null!=e.col3&&(i+=e.col3),i+='</div><div class="col4">',null!=e.col4&&(i+=e.col4),i+="</div></div>",// close col4 and inner-border
null!=e.wrapperClose&&(i+=e.wrapperClose),i+="</div>"}
// updates the data model with a new quantity for a specific item
function g(e,r){for(var i=0;i<2;++i){var t=Array("bundles","supplies")[i];if(Array.isArray(window.frogboxCart[t]))for(var o=0;o<window.frogboxCart[t].length;++o)if(e==window.frogboxCart[t][o].key)return window.frogboxCart[t][o].quantity=r,!0}return!1}function w(){null==window.frogboxCart||jQuery.isEmptyObject(window.frogboxCart)||(jQuery("#bundles-products-wrapper").html(Step2.formatItems(window.frogboxCart.bundles,!0)),Step2.processBundles(),jQuery("#packing-supplies-wrapper").html(Step3.formatItems(window.frogboxCart.supplies,!1)),
// adding function for hide and showing group items
jQuery("#packing-supplies-wrapper .group .product .inner-border").hide(),jQuery(".show-options").click(function(){if(jQuery(this).data("target").length){var e="."+jQuery(this).data("target")+" .product .inner-border";
//jQuery('#packing-supplies-wrapper .group .product').not(target).hide();
jQuery(e).length&&jQuery(e).toggle()}}),
// bind new selects with change event
jQuery(".product select").change(function(){g(jQuery(this).attr("key"),jQuery(this).val()),m()}),jQuery("input:radio[name='bundle-choice']").change(function(){jQuery("input:radio[name='bundle-choice']").closest(".inner-border").removeClass("active"),jQuery("input:radio[name='bundle-choice']").each(function(){g(jQuery(this).attr("key"),0)}),jQuery(this).is(":checked")&&(jQuery(this).closest(".inner-border").addClass("active"),g(jQuery(this).attr("key"),1),m())}),Step1.updateItemsDone())}function k(e){
// if the first five digits are numeric, use that
var r=e.trim().substring(0,5);return isNaN(r)?
// use the first three if we have at least three
3<=e.trim().length&&e.trim().substring(0,3).toUpperCase():r}
// validates a suburb/zip and retrieves the franchise for a zip from Vonigo
function x(o,n){var e="/ff/zip/"+(n=k(n));
// if the request is pending, don't request it a second time
f(e)?window.setTimeout(function(){x(o,n)},1e3):(u(e),$.ajax({dataType:"json",url:e,complete:p(e),success:function(e,r,i){var t;F(o,n,e,O(o))},error:function(){F(o,n,0),console.log("json parse error",jqXHR,textStatus,errorThrown)}}))}
// changes mm/dd/yy to yyyymmdd
function I(e){return e.substring(0,4)+e.substring(5,7)+e.substring(8)}function N(e,r,i){var t=e.substring(0,e.length-5),o=$("#"+t+"-time option:selected").attr("time"),n;
// check if we've got a time selected
// if we had a time selected, try to select it again
if(void 0===window.chosenTime&&(window.chosenTime={"pickup-time":"","delivery-time":""}),o?window.chosenTime[e]=o:void 0!==window.chosenTime[e]&&(o=window.chosenTime[e]),void 0===window.formSubmitting&&(window.formSubmitting={"pickup-time":!1,"delivery-time":!1,"delivery-date":!1,"pickup-date":!1}),
// put the new data in
$("#"+t+"-time").html(r),o)if(0<$("#"+t+'-time option[time="'+o+'"').length)$("#"+t+'-time option[time="'+o+'"').attr("selected","selected").change();else if(!window.formSubmitting[e]){var a="Your "+t+" time is no longer available. Please choose a new one.";ff_functions.makeAlert(a,!1)}
// empty set handling
!i&&$("#"+t+"-time option").length<=1&&$('<div class="error" id="'+e+'-error">No times are available for this date. Please choose another date.</div>').appendTo('label[for="'+e+'"]')}function A(e){N(e,[],!0)}
// checks that that a zip code is filled out
function O(e){var r;return!!$("#"+("pickup"==e?"deliveryPostcode":"pickupPostcode")).val().length}
// callback for ajax requests
function F(r,e,i,t){
// re-validate promo codes if the delivery suburb changed
"delivery"==r&&$("#promo-code").change();
// if valid, maybe make an update
var o=r+"Postcode";if(0<i){var n;
// update the service times if the city has changed
if(
// remove any errors
$('label[for="'+o+'"] .error').remove(),i!=z(r)){
// note that the franchise has changed
ff_functions[r+"FranchiseHasChanged"]=!0;
// update availability
var a,c=$("#"+r+"-date").val().replace(/-/g,"");ff_functions.initializeAvailability(),ff_functions.getAvailability(r,e,function(){var e=r+"-date";window.availability[r]&&window.availability[r][c]?N(e,window.availability[r][c]):
// reload time options if it isn't in the availability array
H(e,c)})}
// always update the option with newcity to use the new zip, select it but don't trigger
$("#"+r+"-city option[value='"+i+"']").first().attr("zip",e).attr("selected","selected")}
// if invalid, show an error message
else{var s;$("<div class='error'>Currently we don't service this address but please <a href='/contact'>Contact Us</a> if you have any questions.</div>").appendTo('label[for="'+o+'"]')}
// update the pricelist if both postcodes are set
if(t){
// trigger .change() on the franchise to update pricelists
$("#"+r+"-city option[value='"+i+"']").first().attr("zip",e).attr("selected","selected").change();
// trigger the change on the other franchise; use a delay to try to prevent a franchise conflict
var l="pickup"==r?"delivery":"pickup",d=z(l),u=Step1.getSuburb(l),p=window.setTimeout(function(){$("#"+l+"-city option[value='"+d+"']").attr("zip",u).attr("selected","selected").change()},1500),f;
// show a message if at least one of the franchises has changed
if(ff_functions.deliveryFranchiseHasChanged||ff_functions.pickupFranchiseHasChanged)E("<p>Oh no! Looks like your postal/zips don't match the location we thought you were in. No problem! Now you will need to re-enter some info because your location has changed.</p>",!0),$("#bundles-products-wrapper .row").remove(),$("#bundles-products-wrapper").append('<div class="throbber-loader" id="step2-throbber">Loading...</div>'),ff_functions.deliveryFranchiseHasChanged=!1,ff_functions.pickupFranchiseHasChanged=!1}}function E(e,r){if(0<$(".alert").length){var i;$('<div class="alert-body">'+e+"</div>").insertAfter(".alert-body")}else{var t='<div class="alert-overlay"><div class="alert"><div class="alert-body">'+e+'</div><div class="alert-ok">Ok</div></div></div>';$("body").append(t),$(".alert-ok").click(function(){M(),$(".bundle-choice").length<1&&r&&(jQuery("#accordion").accordion({active:1}),// accordion sections start numbering at 0; switch to #2
// scroll to bundles
$("html, body").animate({scrollTop:$("#bundles-products-wrapper").offset().top-40},500))})}$(".alert").focus()}function M(){$(".alert-overlay").remove()}function z(e){return!!jQuery("#"+e+"-city").val()&&jQuery("#"+e+"-city").val()}function e(e,r){null!=r&&0<r&&jQuery("#"+e+"-city").val(r)}
// add a throbber to a div after a form element
// @param string elementID - the element id of the element to add the throbber after
// - do not prefix with '#'.
function L(e){var r=e+"-throbber",i;$("#"+r).length||$('<div class="throbber-loader" id="'+r+'">Loading...</div>').insertAfter("#"+e);return"#"+r}
// gets availability for times that haven't already loaded
function H(o,e){if(void 0===window.formSubmitting&&(window.formSubmitting={"pickup-time":!1,"delivery-time":!1,"delivery-date":!1,"pickup-date":!1}),ff_functions.validDate(e)){var r=o.substring(0,o.length-5);$("#"+o+"-error").remove();var i=ff_functions.addThrobber(r+"-time");window.formSubmitting[o]=!0,A(o);var t,n="/ff/times/"+("delivery"==r?ff_functions.getDeliverySuburb():ff_functions.getPickupSuburb())+"/"+I(e);
// if the request is pending, don't request it a second time
if(f(n))return void window.setTimeout(function(){H(o,e)},1e3);u(n),$.ajax({dataType:"json",url:n,success:function(e,r,i){if(void 0!==e&&null!=e.error&&e.displayError){var t='<div class="error" id="'+o+'-error">'+e.error+"</div>";$(t).appendTo('label[for="'+o+'"]')}else void 0!==e.options&&0<e.options.length&&(window.formSubmitting[o]=!1,N(o,e.options))},complete:function(e,r){p(n),window.formSubmitting[o]=!1,$(i).remove()},error:function(e,r,i){console.log("error",e,r,i)}})}}function K(o,e,n){if(e){
// if the request is pending, do not request it a second time
if(url="/ff/availability/"+e,f(url))return void window.setTimeout(function(){K(o,e,n)},1e3);var r={delivery:$("#delivery-time").attr("time"),pickup:$("#pickup-time").attr("time")};u(url),$.ajax({dataType:"json",url:url,complete:p(url),async:!0,success:function(e,r,i){var t=e.options;"both"===o?(window.availability.delivery=t,window.availability.pickup=t):window.availability[o]=t,n&&n()},error:function(e,r,i){console.log("error",e,r,i)}})}}
// initialize availability - franchises should have been chosen at this point
function B(){window.availability||(window.availability={delivery:{},pickup:{}}),deliverySuburb=ff_functions.getDeliverySuburb(),pickupSuburb=ff_functions.getPickupSuburb(),deliverySuburb===pickupSuburb?K("both",ff_functions.getDeliverySuburb()):(K("delivery",ff_functions.getDeliverySuburb()),K("pickup",ff_functions.getPickupSuburb()))}function R(e){return 16==e||18==e||29==e?"United States":"Canada"}
// determine if an element belongs to the 'delivery' or 'pickup' group based on its ID.
function V(e){return"delivery"==$(e).attr("id").slice(0,8)?"delivery":"pickup"}function Y(o,e,r){
// if the request is pending, don't request it a second time
if(!f(n)){
// update calendar and grey out dates for that franchise
var i=R(e),t;if("Canada"===i?t=d(r):"United States"===i&&(t=b(r)),t){var n="/ff/dates/"+r,a=o+"-city";u(n),$.ajax({dataType:"json",url:n,complete:p(n),success:function(e,r,i){if(null!==e.error&&e.displayError){var t='<div class="error" id="'+a+'-error">'+e.error+"</div>";$(t).appendTo('label[for="'+a+'"]')}else e.error||(disabledDates[o]=e,ff_functions.initializeDatepicker(o))},error:function(e,r,i){console.log("error",e,r,i)}})}}}function U(){var e=a();return'<div class="datepickerKeyWrapper"><span class="1-week-pricing datePickerKey">'+(e[0].price?e[0].price:"1 wk")+'</span><span class="2-week-pricing datePickerKey">'+(e[1].price?e[1].price:"2 wks")+'</span><span class="3-week-pricing datePickerKey">'+(e[2].price?e[2].price:"3 wks")+'</span><span class="4-week-pricing datePickerKey">'+(e[3].price?e[3].price:"4 wks")+"</span></div>"}
// intialize or re-initialize a datepicker
function W(t){var e=new Date,r="#"+t+"-date",o;if($(r).datepicker("remove"),e.setDate(e.getDate()+1),"pickup"===t){var i=$("#delivery-date").val();ee(i)&!ee($("#pickup-date").val())?(e=new Date(i)).setDate(e.getDate()+8):e.setDate(e.getDate()+6)}$(r).datepicker({format:"yyyy-mm-dd",startDate:"+1d",todayBtn:!1,autoclose:!0,todayHighlight:!1,defaultViewDate:{year:e.getYear(),month:e.getMonth(),day:e.getDate()},beforeShowDay:function(e){var r=null;"pickup"===t&&(r=J(jQuery("#delivery-date").val(),e));var i=jQuery.datepicker.formatDate("yy-mm-dd",e);return o=r?0<r&&r<=9?"1-week-pricing":r<=16?"2-week-pricing":r<=23?"3-week-pricing":r<=30?"4-week-pricing":"":"",-1===disabledDates[t].indexOf(i)?{enabled:!0,classes:o}:{enabled:!1,classes:o}}})}
// calculate the difference between two dates
function J(e,r){return dateOne=new Date(e),dateTwo=new Date(r),!(dateTwo<dateOne)&&Math.round((dateTwo-dateOne)/864e5)}
// compare the pricelist to the one in the cart and use the one with the
// higher priceListLevelValue property
function Z(){return X("delivery").priceListLevelValue>=X("pickup").priceListLevelValue}function X(e){return ff_functions.allPriceLists()[e]}function G(e,r){r&&!r.error&&(ce[e]=r)}
// validate a date based on format
function ee(e){var r=/\d{4}\-\d{2}\-\d{2}/;return!!e.match(r)}function re(){if(ff_functions.validDate($("#delivery-date").val())&&ff_functions.validDate($("#pickup-date").val())){var e=$("#keep-for").val(),r=ff_functions.dateDifference($("#delivery-date").val(),$("#pickup-date").val());r!=e&&0<r&&(
// update DOM and user info
$("#keep-for").val(r),$("#keep-for-display").text(ff_functions.formatKeepFor()),
// if the number of weeks has changed, update pricing
ie(e)!=ie(r)&&te())}}function ie(e){var r=e%7,i=parseInt(e/7);return 2<r&&(i+=1),i}function te(){
// update pricelists
var e=Step1.getPriceListCode("delivery");if(e){var r="/ff/pricelist/"+e+"/"+$("#keep-for").val();throbberID=ff_functions.addThrobber("keep-for");var i="keep-for-error",t="keep-for-wrapper"}var o=Step1.getPriceListCode("pickup");
// if interfranchise, update that one
if(o&&(ff_functions.updatePricelist(r,throbberID,i,t,!0,!0),r="/ff/pricelist/"+o+"/"+$("#keep-for").val(),throbberID=ff_functions.addThrobber("keep-for"),i="keep-for-error",ff_functions.updatePricelist(r,throbberID,i,t,!1,!0)),ff_functions.orderIsInterfranchise()){var n=ff_functions.getDeliveryFranchise();if(n){var a="/ff/pricelist/IF"+n+"/"+$("#keep-for").val();throbberID=ff_functions.addThrobber("keep-for"),ff_functions.updatePricelist(a,throbberID,i,t,"IF",!0)}}}function oe(e){return e.replace(/[^A-Za-z0-9 ]/g,"").replace(/ /g,"-")}
// updates price listings
var ne=!1,ae=!1,ce={delivery:null,pickup:null};return{addThrobber:function(e){return L(e)},allPriceLists:function(){return ce},clearPending:function(e){p(e)},dateDifference:function(e,r){return J(e,r)},formatKeepFor:function(){return C()},formatItem:function(e,r){return h(e,r)},generateSummary:function(){m()},getAvailability:function(e,r,i){return K(e,r,i)},getDeliveryFranchise:function(){return z("delivery")},getDeliverySuburb:function(){return Step1.getSuburb("delivery")},getPickupFranchise:function(){return z("pickup")},getPickupSuburb:function(){return Step1.getSuburb("pickup")},getPriceList:function(e){return X[e]},getQuantity:function(e){return n(e)},getSingleAvailability:function(e,r){return H(e,r)},getWhich:function(e){return V(e)},initializeAvailability:function(){return B()},initializeDatepicker:function(e){return W(e)},isPending:function(e){return f(e)},loadDisabledDates:function(e,r,i){return Y(e,r,i)},lookupProvince:function(e){return d(e)},lookupState:function(e){return b(e)},makeAlert:function(e,r){return E(e,r)},orderIsInterfranchise:function(){return l()},processPricelist:function(e,r){v(e,r)},sanitizeItemName:function(e){return oe(e)},setDeliverySuburb:function(e){return x("delivery",e)},setPending:function(e){u(e)},setPickupSuburb:function(e){return x("pickup",e)},setPriceList:function(e,r){return G(e,r)},setQuantity:function(e,r){return g(e,r)},setTimeOptions:function(e,r){return N(e,r)},shortenPostCode:function(e){return k(e)},updateKeepFor:function(){return re()},updatePricelist:function(e,r,i,t,o,n,a){return y(e,r,i,t,o,n,a)},unSetPriceList:function(e){ce[e]=null},updateItems:function(){w()},validDate:function(e){return ee(e)},whichCountry:function(e){return R(e)}}}();"undefined"!=typeof module&&void 0!==module.exports?module.exports=ff_functions:window.ff_functions=ff_functions;var Step1=function(){
// determine if an element belongs to the 'delivery' or 'pickup' group based on its ID.
function e(e){return"delivery"==$(e).attr("id").slice(0,8)?"delivery":"pickup"}
// sets defaults for location state/province, zip/postcode and country based on franchise
function a(e,r){var i=o(r);
// set the country
$("#"+e+"Country").val(i);
// set defaults for state/province and zipcode/postcode based on country, without losing original value
var t=$("#"+e+"-suburb").val();"Canada"==i?($("#"+e+"Province").html(n()).val(ff_functions.lookupProvince(t)),$("#"+e+"-province-label").html("Province:"),$("#"+e+"-postcode-label").html("Postal Code:")):($("#"+e+"Province").html(c()).val(ff_functions.lookupState(t)),$("#"+e+"-province-label").html("State:"),$("#"+e+"-postcode-label").html("Zip Code:"))}
// determine if a franchise is in the US or Canada
function o(e){return 16==e||18==e||29==e?"United States":"Canada"}
// select list of Canadian provinces - matches what Vonigo needs to validate against
function n(){var e;return r(["","Alberta","British Columbia","Manitoba","New Brunswick","Newfoundland and Labrador","Nova Scotia","Ontario","Prince Edward Island","Quebec","Saskatchewan","Northwest Territories","Nunavut","Yukon"])}
// select list of US states - matches what Vonigo needs to validate against
function c(){var e;return r(["","Idaho","Minnesota","Washington","","Alabama","Alaska","Arizona","Arkansas","California","Colorado","Connecticut","Delaware","District Of Columbia","Florida","Georgia","Hawaii","Illinois","Indiana","Iowa","Kansas","Kentucky","Louisiana","Maine","Maryland","Massachusetts","Michigan","Mississippi","Missouri","Montana","Nebraska","Nevada","New Hampshire","New Jersey","New Mexico","New York","North Carolina","North Dakota","Ohio","Oklahoma","Oregon","Pennsylvania","Rhode Island","South Carolina","South Dakota","Tennessee","Texas","Utah","Vermont","Virginia","West Virginia","Wisconsin","Wyoming"])}
// build a state/provinces options list
function r(e){for(var r="",i=0;i<e.length;++i)r+='<option value="'+e[i]+'">'+e[i]+"</option>";return r}function s(e,r,t,o){var i="/ff/zip/"+e;
// if the request is pending, don't request it a second time
ff_functions.isPending(i)?window.setTimeout(function(){s(e,r,t,o)},1e3):(ff_functions.setPending(i),$.ajax({dataType:"json",url:i,success:function(e,r,i){l(e)&&t?t(e):o&&o(e)},error:function(){console.log("error",jqXHR,textStatus,errorThrown)},complete:function(){ff_functions.clearPending(i),$(r).remove()}}))}function l(e){return-1<P.indexOf(e)}function d(){return $("#keep-for").val()}function u(e){$("#delivery-city").val(e),a("delivery",e),y(e);
// this is the part where we update the pricelist
var r=!1;7===d()&&(r=!0);var i="delivery-city",t=i+"-error",o="delivery-city-throbber",n="/ff/pricelist/"+b("delivery")+"/"+d();ff_functions.updatePricelist(n,o,t,i,!0,r,function(){ff_functions.processPricelist(ff_functions.getPriceList(n),!0)}),
// this is the part where we get the delivery day options
ff_functions.loadDisabledDates("delivery",e,m("delivery"))}function p(e){$("#pickup-city").val(e),a("pickup",e);
// update the pickup pricelist
var r="/ff/pricelist/"+b("pickup")+"/"+d();ff_functions.updatePricelist(r,"fakeThrob","fakeError","fakeErr",!1,!0),
// reiinitialize the datepicker
ff_functions.loadDisabledDates("pickup",e,m("pickup"))}function i(){t(),
// validate the zip code
$(".city").change(function(){var r=$(this).attr("id"),i=r+"-error";$("#"+i).remove();var t=!1;"delivery-suburb"===r&&(t=!0);var o=$(this).val(),e,n;s(ff_functions.shortenPostCode(o),ff_functions.addThrobber($(this).attr("id")),function(e){// success callback
$("#"+i).remove(),t?($("#deliveryPostcode").val(o).removeClass("error"),u(e)):($("#pickupPostcode").val(o).removeClass("error"),p(e))},function(){// failure callback
var e='<div class="error" id="'+i+'">These bundles aren\'t available in your area. You can still <a href="order">place a regular order!</a></div>';$('label[for="'+r+'"]').length?$(e).appendTo('label[for="'+r+'"]'):$(e).appendTo("#"+r),
// set the active step to step 1 (at index 0)
$("#accordion").accordion("option","active",0),Accordion.scrollToStep(0)})})}
/**
     * Loads initial bundles - the 7, 14, 21 and 28 day pricelist for mover
     */function t(){
// the pricelist should be added from wp_localize_script
if(Pricelist){var e=JSON.parse(Pricelist),r="/ff/pricelist/"+window.moverOptions.PricelistCode+"/7";
// if there was an error, try loading it over ajax
if(e.error)ff_functions.updatePricelist(r,"fakeThrob","fakeError","fakeErr",!0,!0,function(){// callback - set the pickup pricelist
ff_functions.updatePricelist(r,"fakeThrob","fakeError","fakeErr",!1,!0)});else
// expect an array of pricelists
for(var i=0;i<e.length;++i){var t;r=r.substring(0,r.lastIndexOf("/")+1)+7*(i+1),ff_functions.setPriceList(r,e[i]),
// only upate if it is the first list we are processing
0==i&&(ff_functions.processPricelist(e[i],!0),ff_functions.processPricelist(e[i],!1))}}
// in any case, bind the bundle selection process to the bundles
f()}function f(){
// when the user selects a bundle type, show that bundle type and hide the others
$(".bundle-choice").change(function(){var e;$("#bundleError").remove(),v($("input:radio[name='bundle-choice']:checked").val()),ff_functions.updateItems(),// resets markup
f(),$(document).resize()})}
// setBundleSize removes any quantities from the model on non-selected
// bundles, effectively deselecting items in other bundles
function v(e){if($("#bundle").val(e),null!=window.frogboxCart&&window.frogboxCart.bundles&&0<window.frogboxCart.bundles.length)for(var r=0;r<window.frogboxCart.bundles.length;++r)window.frogboxCart.bundles[r].priceItemID!=e?window.frogboxCart.bundles[r].quantity=0:window.frogboxCart.bundles[r].quantity=1}function m(e){return jQuery("#"+e+"Postcode").val()?ff_functions.shortenPostCode(jQuery("#"+e+"Postcode").val()):!!jQuery("#"+e+"-suburb").val()&&ff_functions.shortenPostCode(jQuery("#"+e+"-suburb").val())}function b(e){return window.moverOptions.PricelistCode}function y(e){var r=window.moverOptions.PricelistCode.split("-"),i;h(r[0]+"-"+r[1]+"-"+e)}function h(e){window.moverOptions.PricelistCode=e,$("#PricelistCode").val(e)}function g(){var e={required:!0,noRelatedError:!0,messages:{required:"Please enter a postal code.",noRelatedError:""}};$("#delivery-suburb").rules("add",e),$("#pickup-suburb").rules("add",e),$("#deliveryPostcode").rules("add",e),$("#pickupPostcode").rules("add",e)}function w(e){P=e.map(Number)}function k(){f()}var x="step-1-content",C='<p>Enter the postal code where you\'d like us to deliver the Frogboxes and the postal code where you\'d like us to pick them up when you are done with your move.</p><div class="row"><div class="location col-sm-6"><h4>Delivery Location:</h4><div id="delivery-suburb-wrapper"><label for="delivery-suburb" class="suburb-label"></label><div class="input-wrapper"><input id="delivery-suburb" class="city" name="delivery-suburb" type="textfield"></div></div></div><div class="location col-sm-6"><h4>Pick-up Location:</h4><div id="pickup-suburb-wrapper"><label for="pickup-suburb" class="suburb-label"></label><div class="input-wrapper"><input id="pickup-suburb" class="city" name="pickup-suburb" type="textfield"></div></div></div><input id="delivery-city" type="hidden" value="" name="delivery-city"><input id="pickup-city" type="hidden" value="" name="pickup-city"><input id="includeMovePack" type="hidden" value="1" name="includeMovePack"><input id="isTag" type="hidden" value="274" name="isTag"><input id="PricelistCode" type="hidden" value="" name="PricelistCode"></div>',P=Array();return{getPriceListCode:function(e){return b(e)},getSuburb:function(e){return m(e)},getValidFranchises:function(){return P},load:function(){return i()},render:function(){var e='<div id="'+x+'" class="step-content">';return e+=C},setValidFranchises:function(e){return w(e)},updateItemsDone:function(){return k()},validate:function(){return g()},validFranchise:function(e){return l(e)},validateZip:function(e,r,i,t){return s(e,r,i,t)}}}(),Step2=function(){
// formats a list of items
function i(e,r){if(null!=e){var i,t=Array(),o={},n;if(r)e=e.slice(0,4);for(var i=0;i<e.length;++i){var a=e[i],c=ff_functions.sanitizeItemName(a.name),s={},l=!1;
// set up the item so we can format it generically
if(null!=a.group&&-1<a.group.indexOf("bundle")&&"custom-bundle"!=a.group){
// the layout for regular bundles
a.group="bundles",l=!0;var d="",u;if(ff_functions.getQuantity(a.key)&&(d="CHECKED"),s.wrapper='<label for="'+c+'"><h4>'+a.name+" </h4>",s.col1='<input type="radio" name="bundle-choice" value="'+a.priceItemID+'" key="'+a.key+'" id="'+c+'"'+d+' class="bundle-choice">',null!=a.description_details){s.col2="";for(var p=0;p<a.description_details.length;++p)s.col2+='<div class="product-details"><h4>'+a.description_details[p].title+"</h4>",s.col2+="<p>"+a.description_details[p].dimensions,a.description_details[p].description&&(s.col2+="<br>"+a.description_details[p].description),s.col2+="</p></div>"}u=0==a.value?"Free":"$"+parseInt(a.value,10).toFixed(2)+" + Tax",s.col3='<h5>Your Price:</h5><div class="product-price">'+u+"</div>",s.col4='<span class="product-value-message">'+a.value_message+"</span>",s.wrapperClose="</label>"}else{var f="";"custom-bundle"==a.group||null==a.group?(
// layout for custom bundles and individual products
"custom-bundle"==a.group&&(l=!0),s.col1='<img src="'+a.image+'">',s.col2='<h3 class="product-name" priceItemID='+a.priceItemID+">"+a.name+'</h3><p class="product-description-text">'+a.description+"</p>"):(l=!0,s.groupName=a.name,s.groupImage='<img src="'+a.image+'">',s.groupDescription=a.description,s.col1="",null!=a.title_display_option&&(s.col2="<h4>"+a.title_display_option+"</h4>")),s.col3='<h5>Price:</h5><div class="product-price">$'+parseFloat(a.value).toFixed(2)+' + Tax<br><span class="price-each">/ EACH</span></div>',s.col4='<label for="'+c+'"><h5>Quantity:</h5></label>',s.col4+='<div class="select-wrapper box-qty"><select key="'+a.key+'" name="'+c+'-quantity" id="'+c+'-quantity">',s.sequence=a.sequence;var v="",m=1;99<a.maximum_quantity&&(m=5);for(var b=0;b<=a.maximum_quantity;b+=m)v="",b==a.quantity&&(v=" SELECTED"),s.col4+='<option value="'+b+'"'+v+">"+b+"</option>";s.col4+="</select></div>"}
// add to groups or to markup
if(l)
// skip the custom bundle group
"custom-bundle"!=a.group&&(
// initialize object;
null==o[a.group]&&(o[a.group]={}),
// initialize array;
null==o[a.group].rows?o[a.group].rows=Array(s):o[a.group].rows.push(s));else{var y={sequence:a.sequence,markup:ff_functions.formatItem(s)};t.push(y)}}
// format groups
for(var h in o){var g=99,w="row group group-"+h,k="",y;if(null!=o[h].rows){r||(k+='<div class="col0">'+o[h].rows[0].groupImage+"</div>",k+='<div class="col1"><h3>'+o[h].rows[0].groupName+"</h3>",k+='<div class="product-description-text">'+o[h].rows[0].groupDescription+'</div></div><div class="option-reveal"><a class="show-options btn btn-info" data-target="group-'+h+'">View options</a></div>',g=o[h].rows[0].sequence);for(var p=0;p<o[h].rows.length;++p)
// add the row for each group
k+=ff_functions.formatItem(o[h].rows[p])}(y={sequence:g}).markup="bundles"==h?'<h3 class="step2-label">Choose Your Bundle:</h3><div class="'+w+'">'+k+"</div>":'<div class="'+w+'">'+k+"</div>",t.push(y)}
// sort the items
t.sort(function(e,r){return parseInt(e.sequence)<parseInt(r.sequence)?-1:1});
// build a string and return
var $="";for(var p in t)$+=t[p].markup;return $}}function e(){
// set the affiliate name based on the shortcode setting passed to javascript
pageData&&pageData.affiliate&&$(".affiliateName").html(pageData.affiliate),
// this call is necessary to make bundles an equal height
$(document).resize()}function r(){}
// validate that a bundle is selected
function t(){if(null==window.frogboxCart)return!1;if(window.frogboxCart.bundles)for(var e=0;e<window.frogboxCart.bundles.length;++e)if(window.frogboxCart.bundles[e].quantity)return!0;return!1}var o="step-2-content",n='<p>Your special <span class="affiliateName"></span> price will be based on the number of Frogboxes you require and the amount of time you need the boxes.<p>Prices start at two weeks, but keep them for as long as you need (choose in Step 3).</p><div id="bundles-wrapper"><div id="bundles-list"><input type="hidden" name="bundle" id="bundle"><div id="bundles-products-wrapper"><h3>Based on the size of your move, we’d suggest:</h3></div><span class="step-2-footnote">* Charges may apply for non-local delivery</span><hr><div class="row"><div class="col-sm-5 col-sm-offset-7"><button id="step-2-next" class="step-next next btn btn-default">Next</button></div></div></div>';return{formatItems:function(e,r){return i(e,r)},load:function(){return e()},processBundles:function(){return!0},render:function(){var e="";return e+=n,e+="</div>"},validate:function(){},validated:function(){return!!t()||($("#bundles-products-wrapper").append('<div class="error" id="bundleError">Please choose a bundle</div>'),!1)}}}(),Step3=function(){
// formats a list of items
function i(e,r){if(null!=e){for(var i=Array(),t={},o=0;o<e.length;++o){var n=e[o],a=ff_functions.sanitizeItemName(n.name),c={},s=!1,l=a,d="Minimalist",u="Regular",p="Collector";if(-1!=l.indexOf(d))var f=d;else if(-1!=l.indexOf(u))var f=u;else if(-1!=l.indexOf(p))var f=p;
// set up the item so we can format it generically
if(null!=n.group&&-1<n.group.indexOf("bundle")&&"custom-bundle"!=n.group){
// the layout for regular bundles
s=!0;var v="";if(ff_functions.getQuantity(n.key)&&(v="CHECKED"),c.wrapper='<label for="'+a+'"><h4>'+f+" </h4>",c.col1='<input type="radio" name="bundle-choice" value="'+n.priceItemID+'" key="'+n.key+'" id="'+a+'"'+v+">",null!=n.description_details){c.col2="";for(var m=0;m<n.description_details.length;++m){var b=!1,y=!1;n.description_details[m].title&&void 0!==n.description_details[m].title&&(c.col2+='<div class="product-details"><h4>'+n.description_details[m].title+"</h4>",b=!0),n.description_details[m].dimensions&&void 0!==n.description_details[m].dimensions&&(c.col2+="<p>"+n.description_details[m].dimensions,y=!0),n.description_details[m].title&&void 0!==n.description_details[m].description&&(c.col2+="<br>"+n.description_details[m].description),y&&(c.col2+="</p>"),b&&(c.col2+="</div>")}}c.col3='<h5>Price:</h5><div class="product-price">$'+parseInt(n.value,10).toFixed(2)+" + Tax</div>",c.col4='<span class="product-value-message">'+n.value_message+"</span>",c.wrapperClose="</label>"}else{var h="";"custom-bundle"==n.group||null==n.group?(
// layout for custom bundles and individual products
"custom-bundle"==n.group&&(s=!0),c.col1='<img src="'+n.image+'">',c.col2='<h3 class="product-name" priceItemID='+n.priceItemID+">"+n.name+'</h3><p class="product-description-text">'+n.description+"</p>"):(s=!0,c.groupName=n.name,c.groupImage='<img src="'+n.image+'">',c.groupDescription=n.description,c.col1="",null!=n.title_display_option&&(c.col2="<h4>"+n.title_display_option+"</h4>")),c.col3='<h5>Price:</h5><div class="product-price">$'+parseFloat(n.value).toFixed(2)+' + Tax<br><span class="price-each">/ EACH</span></div>',c.col4='<label for="'+a+'"><h5>Quantity:</h5></label>',c.col4+='<div class="select-wrapper box-qty"><select key="'+n.key+'" name="'+a+'-quantity" id="'+a+'-quantity">',c.sequence=n.sequence;var g="",w=1;99<n.maximum_quantity&&(w=5);for(var k=0;k<=n.maximum_quantity;k+=w)g="",k==n.quantity&&(g=" SELECTED"),c.col4+='<option value="'+k+'"'+g+">"+k+"</option>";c.col4+="</select></div>"}
// add to groups or to markup
if(s)
// initialize object;
null==t[n.group]&&(t[n.group]={}),
// initialize array;
null==t[n.group].rows?t[n.group].rows=Array(c):t[n.group].rows.push(c);else{var $={sequence:n.sequence,markup:ff_functions.formatItem(c)};i.push($)}}
// format groups
var x=jQuery("input:radio[name='bundle']:checked").val()+"-bundle";for(var C in t){var P=99,S="row group group-"+C;r&&x!=C&&(S+=" group-hidden");var D="";if(null!=t[C].rows){r||(D+='<div class="col0">'+t[C].rows[0].groupImage+"</div>",D+='<div class="col1"><h3>'+t[C].rows[0].groupName+"</h3>",D+='<div class="product-description-text">'+t[C].rows[0].groupDescription+'</div></div><div class="option-reveal"><a class="show-options btn btn-info" data-target="group-'+C+'">View options</a></div>',P=t[C].rows[0].sequence);for(var m=0;m<t[C].rows.length;++m)
// add the row for each group
D+=ff_functions.formatItem(t[C].rows[m])}var $={sequence:P,markup:'<div class="'+S+'"><h3 class="step2-label">Number of boxes:</h3>'+D+"</div>"};i.push($)}
// sort the items
i.sort(function(e,r){return parseInt(e.sequence)<parseInt(r.sequence)?-1:1});
// build a string and return
var _="";for(var m in i)_+=i[m].markup;return _}}function e(){}function r(){}var t="step-3-content",o='<p>Optionally you can add packing supplies and other products that will be useful during your move.</p><div id="packing-supplies-wrapper"></div><hr><div class="row"> <div class="col-sm-5"> <div id="step-3-previous" class="step-previous previous btn btn-previous">Previous</div></div><div class="col-sm-5 col-sm-offset-2"><button id="step-3-next" class="step-next next btn btn-default">Next</button></div> </div> ';return{formatItems:function(e,r){return i(e,r)},load:function(){},render:function(){var e='<div id="'+t+'" class="step-content">';return e+=o,e+="</div>"},validate:function(){}}}(),Step4=function(){function e(){
// initialize datepickers
ff_functions.initializeDatepicker("pickup"),ff_functions.initializeDatepicker("delivery"),
// changing postcodes changes them in step 1 also
$(".postcode").change(function(){var e=$(this).val(),r;"deliveryPostcode"==$(this).attr("id")?$("#delivery-suburb").val(e).change():$("#pickup-suburb").val(e).change()}),
// when a user enters a date, check that it is valid and download possible times
$(".booking-date").on("changeDate",function(){var e=$(this).val(),r=""+e.replace(/-/g,""),i=$(this).attr("id"),t=i.substring(0,i.length-5);
// clear errors
$("#"+i+"-error").remove();var o=window.availability[t];if(o&&o[r]){var n='<option value="">(select Time)</option>',a=o[r];for(var c in a)if(a.hasOwnProperty(c)){var s=a[c].routeID+"-"+a[c].startTime;n+='<option time="'+a[c].startTime+'" value="'+s+'">'+a[c].label+"</option>"}ff_functions.setTimeOptions(i,n)}else ff_functions.getSingleAvailability(i,e);"delivery-date"==i&!$("#pickup-date").val()&&(ff_functions.initializeDatepicker("pickup"),$("#pickup-date").trigger("changeDate")),
// a date changed, so check if we need to update the keepFor value and also the pricing
ff_functions.updateKeepFor()}),
// when the user checks the 'pickup same as delivery' box, copy the
// address and hide the fields. When the user unchecks it, unhide the
// fields
$("#pickupSame").change(function(){this.checked?($("#pickupStreet").val($("#deliveryStreet").val()),$("#pickupCity").val($("#deliveryCity").val()),$("#pickupProvince").val($("#deliveryProvince").val()),$("#pickupPostcode").val($("#deliveryPostcode").val()),$("#pickupDescription").val($("#deliveryDescription").val()),$("#pickupCountry").val($("#deliveryCountry").val()),$("#pickup-address-wrapper").addClass("group-hidden")):$("#pickup-address-wrapper").removeClass("group-hidden")})}var r="step-4-content",i='<div id="step-4-your-price">    <div class="row">    <div class="col-sm-6"><h4>Your Price and Pick-up Dates</h4>    <p>We understand that moves don\'t happen overnight, you can select a date that gives you enough time to get fully unpacked. The longer you keep the Frogboxes the cheaper their cost per week.</p>    </div>    <div id="step-4-current-order" class="col-sm-6"><h4>For your current order:</h4>    <ul id="current-order-price-by-duration"></ul>        </div>        </div>        </div>        <div class="row"><div id="delivery-date-wrapper" class="col-sm-6"><h4>Delivery Date and Time</h4><label for="delivery-date">Delivery Date:</label><input type="text" id="delivery-date" name="delivery-date" class="booking-date" required readonly><label for="delivery-time">Delivery Time:</label><div class="select-wrapper"><select id="delivery-time" class="booking-time" name="delivery-time" required></select>    </div></div><div id="pickup-date-wrapper" class="col-sm-6"><h4>Pick-up Date and Time</h4><label for="pickup-date">Pick-up Date:</label><input type="text" id="pickup-date" name="pickup-date" class="booking-date" required readonly><label for="pickup-time">Pick-up Time:</label><div class="select-wrapper"><select id="pickup-time" class="booking-time" name="pickup-time" required></select>    </div><div id="keep-for-wrapper">Keep for: <span id="keep-for-display">1 week</span><input type="hidden" id="keep-for" name="keep-for" value="7"></div></div>    </div>    <div class="row"><div id="delivery-address" class="col-sm-6">        <h4>Delivery Address</h4>    <label for="deliveryStreet">Address:</label><input type="text" id="deliveryStreet" name="deliveryStreet" required  placeholder="Apartment or suite # - Street address"><label for="deliveryCity">City:</label><input type="text" id="deliveryCity" name="deliveryCity" required ><label for="deliveryProvince" id="delivery-province-label">Province:</label> <div class="select-wrapper">    <select id="deliveryProvince" name="deliveryProvince" class="province" required></select> </div>  <label for="deliveryPostcode" id="delivery-postcode-label">Postal Code:</label>  <input type="text" id="deliveryPostcode" class="postcode" name="deliveryPostcode" required>  <input type="hidden" id="deliveryCountry" name="deliveryCountry" >  <label for="deliveryDescription">Description: <span class="field-optional">(optional)</span></label>      <textarea id="deliveryDescription" name="deliveryDescription"  placeholder="Do you have stairs?"></textarea>  <input type="checkbox" name="pickupSame" id="pickupSame" >  <label for="pickupSame">Same as delivery location?</label></div><div id="pick-up-address" class="col-sm-6">   <h4>Pick-up Address</h4> <div id="pickup-address-wrapper">     <label for="pickupStreet">Address:</label> <input type="text" id="pickupStreet" name="pickupStreet" required placeholder="Apartment or suite # - Street address"> <label for="pickupCity">City:</label> <input type="text" id="pickupCity" name="pickupCity" required >  <label for="pickupProvince">State/Province:</label>  <div class="select-wrapper">      <select id="pickupProvince" name="pickupProvince" class="province" required></select>  </div>  <label for="pickupPostcode">Zip/Postal Code:</label>  <input type="text" id="pickupPostcode" class="postcode" name="pickupPostcode" required >  <input type="hidden" id="pickupCountry" name="pickupCountry" >  <label for="pickupDescription">Description: <span class="field-optional">(optional)</span></label>      <textarea id="pickupDescription" name="pickupDescription" placeholder="Do you have stairs?"></textarea>   </div>    </div>    </div>    <hr>    <div class="row">  <div class="col-sm-5">        <div id="step-4-previous" class="step-previous previous btn btn-previous">Previous</div> </div> <div class="col-sm-5 col-sm-offset-2"> <button id="step-4-next" class="step-next next btn btn-default">Next</button>   </div>  </div> ';return{load:function(){return e()},render:function(){var e='<div id="'+r+'" class="step-content">';return e+=i,e+="</div>"},validate:function(){return!0}}}(),Step5=function(){function e(){
// set the form action if it is specified
pageData&&pageData.confirmation&&$("#frogbox-form").prop("action",pageData.confirmation),
// when a user enters a promo code, check for validity against the API
$("#promo-code").change(function(){if(null==window.frogboxCart&&(window.frogboxCart={}),$("#promo-code-error").remove(),window.frogboxCart.promo=null,$("#promo-valid").remove(),0<$("#promo-code").val().length){
// add a throbber
var e=ff_functions.addThrobber("promo-code"),r=ff_functions.getDeliverySuburb();if(r){var i="/ff/promo/"+r+"/"+$("#promo-code").val();$.ajax({dataType:"json",url:i,success:function(e,r,i){if(null!=e.error&&e.displayError){var t='<div class="error" id="promo-code-error">'+e.error+"</div>";$(t).appendTo('label[for="promo-code"]')}else null!=e&&($('<span id="promo-valid">'+e.promoCode+" is good.</span>").insertAfter("#promo-code"),window.frogboxCart.promo=e,ff_functions.generateSummary())},complete:function(){
// remove throbber
$(e).remove(),ff_functions.generateSummary()},error:function(e,r,i){console.log("error",e,r,i)}})}}ff_functions.generateSummary()}),
// hide/show delivery and pickup contact areas
$("input[name=delivery-contact]").change(function(){"different delivery contact"==$("input[name=delivery-contact]:checked").val()?$("#delivery-different-contact-wrapper").removeClass("group-hidden"):$("#delivery-different-contact-wrapper").addClass("group-hidden")}),$("input[name=pickup-contact]").change(function(){"different pickup contact"==$("input[name=pickup-contact]:checked").val()?$("#pickup-different-contact-wrapper").removeClass("group-hidden"):$("#pickup-different-contact-wrapper").addClass("group-hidden")})}function r(){}var i="step-5-content",t='<div id="contact-details" class="row"><h4>Contact Details</h4><div class="col-sm-6"><label for="contact-first">First Name:</label><input type="text" id="contact-first" name="contact-first" required></div><div class="col-sm-6"><label for="contact-last">Last Name:</label><input type="text" id="contact-last" name="contact-last" required></div><div class="col-sm-6"><label for="contact-primary-phone">Primary Phone:</label><input type="text" id="contact-primary-phone" name="contact-primary-phone" required></div><div class="col-sm-6"><label for="contact-primary-ext">ext: <span class="field-optional">(optional)</span></label><input type="text" id="contact-primary-ext" name="contact-primary-ext"></div><div class="col-sm-6"><label for="contact-alternate-phone">Alternate Phone: <span class="field-optional">(optional)</span></label><input type="text" id="contact-alternate-phone" name="contact-alternate-phone"></div><div class="col-sm-6"><label for="contact-primary-ext">ext: <span class="field-optional">(optional)</span></label><input type="text" id="contact-alternate-ext" name="contact-alternate-ext"></div><div class="col-sm-6"><label for="contact-email">Email:</label><input type="email" id="contact-email" name="contact-email" required></div></div><div class="row"><div id="delivery-contact-wrapper" class="step-5-contact col-sm-6"><h4>Delivery Contact</h4><input type="radio" name="delivery-contact" value="same as main contact" id="delivery-contact-main"><label for="delivery-contact-main">Same as Main Contact</label><input type="radio" name="delivery-contact" value="different delivery contact" id="delivery-contact-different"><label for="delivery-contact-different">Different delivery contact</label><div id="delivery-different-contact-wrapper" class="group-hidden"><label for="delivery-contact-first">First Name:</label><input type="text" name="delivery-contact-first" id="delivery-contact-first" ><label for="delivery-contact-last">Last Name:</label><input type="text" name="delivery-contact-last" id="delivery-contact-last" ></div></div><div id="pickup-contact-wrapper" class="step-5-contact col-sm-6"><h4>Pick-up Contact</h4><input type="radio" name="pickup-contact" value="same as main contact" id="pickup-contact-main"><label for="pickup-contact-main">Same as Main Contact</label><input type="radio" name="pickup-contact" value="different pickup contact" id="pickup-contact-different"><label for="pickup-contact-different">Different pick-up contact</label><div id="pickup-different-contact-wrapper" class="group-hidden"><label for="pickup-contact-first">First Name:</label><input type="text" name="pickup-contact-first" id="pickup-contact-first"><label for="pickup-contact-last">Last Name:</label><input type="text" name="pickup-contact-last" id="pickup-contact-last"></div></div></div><input type="hidden" id="promo-code" name="promo-code"><div id="payment-wrapper">   <h4>Payment</h4>        <p>We will collect payment at the same time we deliver the Frogboxes.</p> </div>   <div id="confirm-wrapper">       <h4>Confirm and Place Order</h4>   <p>Please <span class="ordering-emphasis">review your order</span> and contact details to confirm that everything is entered correctly. You can click on any step to edit the information on that step.</p>   <p>When you are ready to place your order, click <span class="ordering-emphasis">Place Order</span> below.</p>   </div>  <div> <button type="button" data-toggle="modal" data-target="#terms-conditions">Show Terms of Service</button> <label for="terms">I agree with the terms of service: </label>  <input type="checkbox" value="1" name="terms" id="terms" required>  </div>  <hr> <div class="row">      <div class="col-sm-5">      <div id="step-5-previous" class="step-previous previous btn btn-previous">Previous</div>      </div>      <div class="col-sm-5 col-sm-offset-2"> <button id="step-5-next" class="step-next next btn btn-default" name="_ff_submit" value="submit">Place Order</button></div></div>';return{load:function(){return e()},render:function(){var e='<div id="'+i+'" class="step-content">';return e+=t,e+="</div>"},validate:function(){}}}(),Validator={validate:function(){
/**
             *  Validation - move this to individual modules if we can
             */
$.validator.addMethod("greaterThan",function(e,r,i){return/Invalid|NaN/.test(new Date(e))?isNaN(e)&&isNaN($(i).val())||Number(e)>Number($(i).val()):new Date(e)>new Date($(i).val())},"Please enter a date after {0}."),
// validate that the date is after today
$.validator.addMethod("afterToday",function(e){return/Invalid|NaN/.test(new Date(e))?isNaN(e)||Number(e)>Number($(params).val()):new Date(e)>new Date},"Please enter a date after today. "),
// validate that a postal code matches a province or state
$.validator.addMethod("matchProvince",function(e,r){var i="d"==jQuery(r).attr("id").substr(0,1)?"delivery":"pickup",t=jQuery("#"+i+"Country").val(),o;return"Canada"==t?o=ff_functions.lookupProvince(e):"United States"==t&&(o=ff_functions.lookupState(e)),jQuery("#"+i+"Province").val()===o},"Please enter a valid zip/postal code for this state/province"),
// validate that a postal code matches Ontario
$.validator.addMethod("provinceMatchesOntario",function(e,r){var i="d"==jQuery(r).attr("id").substr(0,1)?"delivery":"pickup",t;return"Ontario"==jQuery("#"+i+"Province").val()},"Please enter an address in Ontario."),
// validate that a postal code field doesn't have a related error
$.validator.addMethod("noRelatedError",function(e,r){var i=jQuery(r).attr("id");return!jQuery('label[for="'+i+'"] .error').length}),
// initialize validation
$("#frogbox-form").validate({errorPlacement:function(e,r){e.appendTo('label[for="'+r.attr("id")+'"]')},rules:{
// province must match postal code
deliveryPostcode:{required:!0,matchProvince:"#deliveryProvince",noRelatedError:!0},pickupPostcode:{required:!0,matchProvince:"#pickupProvince",noRelatedError:!0},
// phone
"contact-primary-phone":{required:!0,phoneUS:!0},"contact-alternate-phone":{phoneUS:!0},
// delivery contact fields if 'different delivery contact' is checked
"delivery-contact-first":{required:"#delivery-contact-different:checked"},"delivery-contact-last":{required:"#delivery-contact-different:checked"},
// pickup contact fields if 'different pickup contact' is checked
"pickup-contact-first":{required:"#pickup-contact-different:checked"},"pickup-contact-last":{required:"#pickup-contact-different:checked"},
// date validation
"delivery-date":{afterToday:!0},"pickup-date":{greaterThan:"#delivery-date"},
// terms and conditions
terms:{required:!0}},messages:{"contact-alternate-phone":{phoneUS:"Enter a valid phone number. "},"contact-primary-phone":{phoneUS:"Enter a valid phone number. "},terms:{required:"Check the checkbox to agree to our Terms of Service. "}},ignore:[],submitHandler:function(e){window.orderFormSubmitting=!0,$("#step-5-next").hide();var r=ff_functions.addThrobber("step-5-next");$(r).append("<div>Thank you! Please wait while we process your order.</div>");
// append cart to order
var i=$("<input>").attr("type","hidden").attr("name","frogboxCart").val(JSON.stringify(window.frogboxCart));
// append page data to order
if($("#frogbox-form").append($(i)),pageData){var t=$("<input>").attr("type","hidden").attr("name","pageTitle").val(pageData.title),o=$("<input>").attr("type","hidden").attr("name","pageUrl").val(pageData.url);$("#frogbox-form").append($(t)).append($(o))}e.submit()}})}},Accordion=function(){function a(e){var r="#step-"+e+"-heading";
// recurse to the next step if we can't find the step
if(!$(r).length&&e<5)return a(e+1);setTimeout(function(){var e=$(r).offset();e&&$("body, html").animate({scrollTop:e.top-80})},310)}return{accordion:function(){
/**
             *  Initialize accordion.
             */
$("#accordion").accordion({heightStyle:"content",collapsible:!0,autoHeight:!1,active:0,animate:!1,activate:function(e,r){
// if we are going into step 5 and the promo code field is already filled out, validate it
var i=$("#accordion").accordion("option","active");4==i&&$("#promo-code").val().length&&$("#promo-code").change(),1<i&&(window.availability||ff_functions.initializeAvailability())},beforeActivate:function(e,r){
// return true if we are going backwards
if(null!==r.newHeader&&null!==r.oldHeader&&void 0!==r.newHeader.attr("id")&&void 0!==r.oldHeader.attr("id")&&r.newHeader.attr("id").substring(5,6)<r.oldHeader.attr("id").substring(5,6))return!0;
// return false if there is a throbber active in step 1
var i=$("#accordion").accordion("option","active");if($("#step-1 .throbber-loader").length&&0==i)return!1;var t=!0,o=$("#frogbox-form").validate(),n;return $(".step-content:eq( "+$("#accordion").accordion("option","active")+" )").find("input, select").each(function(e,r){try{var i;o.element("#"+$(r).attr("id"))||(t=!1)}catch(e){"bundle-choice"!=$(r).attr("name")&&console.log(r,e)}}),!(!Step2.validated()||!t)||(a($("#accordion").accordion("option","active")),!1)}}),$("#accordion button.step-next, #accordion div.step-previous").not("#step-5-next").click(function(e){e.preventDefault();var r=$(this).is(".next")?1:-1,i=$("#accordion").accordion("option","active")+r;$("#accordion").accordion("option","active",i),a(i),ff_functions.generateSummary()})},scrollToStep:function(e){a(e)}}}();// @codekit-prepend "ff_functions.js"
// @codekit-prepend "components/mover-order/step1.js"
// @codekit-prepend "components/mover-order/step2.js"
// @codekit-prepend "components/step3.js"
// @codekit-prepend "components/mover-order/step4.js"
// @codekit-prepend "components/mover-order/step5.js"
// @codekit-prepend "components/validate.js"
// @codekit-prepend "components/accordion.js"
// define these variables in the global scope
frogboxCart=null,disabledDates={delivery:[],pickup:[]},pendingRequests={},$(document).ready(function(){var e=new Array(Step1,Step2,Step3,Step4,Step5),r;
/**
     * Load the code for our steps and render it.
     *
     * Markup should be rendered before any event listeners are created in .load().
     *
     * On this form, the element for step 1 is combined with the element for step 2.
     */$("#step-2").append(Step1.render()).append(Step2.render()),$("#step-3").append(Step3.render()),$("#step-4").append(Step4.render()),$("#step-5").append(Step5.render()),Validator.validate();
/**
     * Load the listeners for each step.
     */
for(var i=0;i<e.length;++i)e[i].load();
/**
     * Set valid franchises
     */
/**
     * Add the line about the extra boxes if we are supposed to
     */
(window.validFranchises&&Step1.setValidFranchises(window.validFranchises)
/**
     * Set placeholder text
     */,window.zipPlaceholder&&$(".city").attr("placeholder",window.zipPlaceholder)
/**
     * Set label text
     * Your <span class="hoverText" title="Toronto - Mississauga - Brampton - Markham - '
     * 'Vaughan - Richmond Hill - Milton - Newmarket">Greater Toronto Area</span> Postal Code
     */,window.moverOptions.zipLabelText&&$(".suburb-label").html(window.moverOptions.zipLabelText)
/**
     * Set PricelistCode element for pricelist validation on the back end
     */,window.moverOptions.PricelistCode&&$("#PricelistCode").val(window.moverOptions.PricelistCode)
/**
     * Set tracking code in promocode field if it exists
     */,window.moverOptions.trackingCode&&$("#promo-code").val(window.moverOptions.trackingCode),1==window.moverOptions.extraBoxes)&&$('<span class="step-2-footnote" style="margin-left: 2em;">* Extra Boxes: You only pay if you use them. Charged $4.00 per box @pickup.</span>').insertAfter(".step-2-footnote")
/**
     * Look for a submission object. If it exists, set form fields based on submission.
     */;if(window.Submission){for(var t in
// set the page title
document.title=pageData.title,$(".page-header h1").html(pageData.title),frogboxCart=Submission.frogboxCart,delete Submission.frogboxCart,Submission)Submission.hasOwnProperty(t)&&"terms"!=t&&"quantity"!=t.substr(t.length-8)&&$("*[name="+t+"]").not('*[type="radio"]').val(Submission[t]);
// initialize availability
ff_functions.initializeAvailability(),
// load pricelists by calling .change() method on .city elements
$(".city").change(),
// load dates by triggering changeDate event on .booking-date elements
$(".booking-date").trigger("changeDate"),
// validate promo if it exists by calling .change() method
$("#promo-code").val().length&&$("#promo-code").change(),
// choose the bundle in step 2 and call the change event
$("#bundle-"+Submission.bundle).prop("checked",!0).change()}
/**
     * Display errors, if any.
     */if(window.Errors)
// display the errors
for(var o=0;o<window.Errors.length;o++){var n='<p class="error">'+window.Errors[o].errMsg+"</p>";$("#frogbox-form").prepend(n),$("#"+window.Errors[o].element).length&&$(n).insertBefore("#"+window.Errors[o].element)}
/**
     * Set up validation
     */for(var i=0;i<e.length;++i)e[i].validate();
/**
     * Accordion runs after validation
     */
/**
     * Open the accordion to where the first error is, if any.
     */
if(Accordion.accordion(),window.Errors){var a;if(Errors[0]){var c=Errors[0].element;"step-1"===c&&(c="step-2");var s=$("#"+c).closest(".step-content").attr("id");if(s[5]){var l=s[5]-1;2<l&&(// select new dates in step 3
l=2)}}else l=1;$("#accordion").accordion("option","active",l),Accordion.scrollToStep(l)}});