var Accordion = (function() {

    function scrollToStep(step) {
        var self = "#step-" + step + "-heading";

        // recurse to the next step if we can't find the step
        if (!$(self).length && step < 5) {
            return scrollToStep(step +1);
        }
        setTimeout(function () {
            var theOffset = $(self).offset();
            if (theOffset) {
                $('body, html').animate({scrollTop: theOffset.top - 80});
            }
        }, 310);
    }

    return {
        accordion: function () {
            /**
             *  Initialize accordion.
             */
            $("#accordion").accordion({
                heightStyle: "content",
                collapsible: true,
                autoHeight: false,
                active: 0,
                animate: false,
                activate: function(event, ui) {
                    // if we are going into step 5 and the promo code field is already filled out, validate it
                    var newStep = $('#accordion').accordion('option', 'active');
                    if (newStep == 4) {

                        var paymentWrapEl = $("#payment-wrapper");
                        var offlinePaymentEl = $(".offline-payment");
                        paymentWrapEl.hide();
                        offlinePaymentEl.show();
                        //stripe payment setup
                        // stripe payment is only for Vancouver and Seattle for now
                        var deliverCity = $("#delivery-city").val();
                        if(window.stripeLocationIds.includes(deliverCity) && !window.isCorporate){
                            paymentWrapEl.show();
                            offlinePaymentEl.hide();
                            stripe = Stripe(window.stripeKeys[deliverCity]);
                            //stripe = Stripe('pk_test_2FhPOaQaT17kAawQdaZDUQlo00I1OjdvOz');
                            elements = stripe.elements();

                            // Custom styling can be passed to options when creating an Element.
                            var style = {
                                base: {
                                    // Add your base input styles here. For example:
                                    fontSize: '16px',
                                    color: '#32325d',
                                },
                            };

                            // Create an instance of the card Element.
                            var card = elements.create('card', {style: style, hidePostalCode: true});

                            // Add an instance of the card Element into the `card-element` <div>.
                            card.mount('#card-element');

                            //bind focus event to element
                            card.on('focus', function (event) {
                                $("#card-errors").html("");
                            });
                        }


                        if ($('#promo-code').val().length) {
                            $('#promo-code').change();
                        }
                    }
                    if (newStep > 1) {
                        if (!window.availability) {
                            ff_functions.initializeAvailability();
                        }
                    }
                },
                beforeActivate: function (event, ui) {
                    // return true if we are going backwards
                    if (ui.newHeader !== null && ui.oldHeader !== null) {
                        if (ui.newHeader.attr('id') !== undefined && ui.oldHeader.attr('id') !== undefined) {
                            if ((ui.newHeader.attr('id').substring(5, 6)) < (ui.oldHeader.attr('id').substring(5, 6))) {
                                return true;
                            }
                        }
                    }

                    // return false if there is a throbber active in step 1
                    var currentStep = $('#accordion').accordion('option', 'active');
                    if ($('#step-1 .throbber-loader').length && currentStep == 0) {
                        return false;
                    }

                    var validStep = true;
                    var validator = $('#frogbox-form').validate();

                    $('.step-content:eq( ' + $('#accordion').accordion('option', 'active') + ' )').find('input, select').each(function (index, elem) {

                        try {
                            var test = validator.element('#' + $(elem).attr('id'));
                            if (!test) {
                                validStep = false;
                            }
                        }
                        catch (exception) {
                            if ($(elem).attr('name') != 'bundle-choice') {
                                console.log(elem, exception);
                            }
                        }
                    });

                    if (Step2.validated() && validStep) {
                        return true;
                    }
                    else {
                        var step = $('#accordion').accordion('option', 'active');
                        scrollToStep(step);
                        return false;
                    }
                }
            });

            $('#accordion button.step-next, #accordion div.step-previous').not('#step-5-next').click(function (e) {
                e.preventDefault();
                var delta = ($(this).is('.next') ? 1 : -1);
                var delta_result = $('#accordion').accordion('option', 'active') + delta;

                $('#accordion').accordion('option', 'active', ( delta_result  ));
                scrollToStep(delta_result);



                //this will add the 25 bundle to the frogbox cart by default when  step 2 is opened
                if(delta_result === 1){
                    //check if no bundle is added to the frogbox cart, if so then add the 25 bundle
                    let bundleAdded = false;
                    window.frogboxCart.bundles.map(bundle => {
                        if(bundle.quantity > 0){
                            bundleAdded = true;
                        }
                    });

                    if(bundleAdded === false){
                        ff_functions.setQuantity(969, 1);
                    }
                }





                ff_functions.generateSummary();
            });
            
        },
        scrollToStep: function(step) {
            scrollToStep(step);
        }
    }

})();