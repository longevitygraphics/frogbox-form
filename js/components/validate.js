var Validator = (function() {




    return {
        validate: function() {
            /**
             *  Validation - move this to individual modules if we can
             */
            $.validator.addMethod('greaterThan', function (value, element, params) {
                if (!/Invalid|NaN/.test(new Date(value))) {
                    return new Date(value) > new Date($(params).val());
                }
                return isNaN(value) && isNaN($(params).val()) || (Number(value) > Number($(params).val()));
            }, 'Please enter a date after {0}.');

            // validate that the date is after today
            $.validator.addMethod('afterToday', function (value) {
                if (!/Invalid|NaN/.test(new Date(value))) {
                    return (new Date(value) > new Date());
                }
                return isNaN(value) || (Number(value) > Number($(params).val()));
            }, 'Please enter a date after today. ');

            // validate that a postal code matches a province or state
            $.validator.addMethod('matchProvince', function (value, element) {
                var prefix = jQuery(element).attr('id').substr(0, 1) == 'd' ? 'delivery' : 'pickup';
                var country = jQuery('#' + prefix + 'Country').val();
                var expected;
                if (country == 'Canada') {
                    expected = ff_functions.lookupProvince(value);
                }
                else if (country == 'United States') {
                    expected = ff_functions.lookupState(value);
                }
                return (jQuery('#' + prefix + 'Province').val() === expected);
            }, 'Please enter a valid zip/postal code for this state/province');

            // validate that a postal code matches Ontario
            $.validator.addMethod('provinceMatchesOntario', function (value, element) {
                var prefix = jQuery(element).attr('id').substr(0, 1) == 'd' ? 'delivery' : 'pickup';
                var province = jQuery('#' + prefix + 'Province').val();
                return (province == 'Ontario');
            }, 'Please enter an address in Ontario.');

            // validate that a postal code field doesn't have a related error
            $.validator.addMethod('noRelatedError', function (value, element) {
                var id = jQuery(element).attr('id');
                if (jQuery('label[for="' + id + '"] .error').length) {
                    return false;
                }
                return true;
            });

            // initialize validation
            $('#frogbox-form').validate({
                errorPlacement: function (error, element) {
                    error.appendTo('label[for="' + element.attr('id') + '"]');
                },
                rules: {
                    // province must match postal code
                    'deliveryPostcode': {
                        required: true,
                        matchProvince: '#deliveryProvince',
                        noRelatedError: true
                    },
                    'pickupPostcode': {
                        required: true,
                        matchProvince: '#pickupProvince',
                        noRelatedError: true
                    },
                    // phone
                    'contact-primary-phone': {
                        required: true,
                        phoneUS: true
                    },
                    'contact-alternate-phone': {
                        phoneUS: true
                    },
                    // delivery contact fields if 'different delivery contact' is checked
                    'delivery-contact-first': {
                        required: '#delivery-contact-different:checked'
                    },
                    'delivery-contact-last': {
                        required: '#delivery-contact-different:checked'
                    },
                    // pickup contact fields if 'different pickup contact' is checked
                    'pickup-contact-first': {
                        required: '#pickup-contact-different:checked'
                    },
                    'pickup-contact-last': {
                        required: '#pickup-contact-different:checked'
                    },
                    // date validation
                    'delivery-date': {afterToday: true},
                    'pickup-date': {greaterThan: '#delivery-date'},
                    // terms and conditions
                    terms: {required: true}
                },
                messages: {
                    'contact-alternate-phone': {
                        phoneUS: 'Enter a valid phone number. '
                    },
                    'contact-primary-phone': {
                        phoneUS: 'Enter a valid phone number. '
                    },
                    terms: {
                        required: 'Check the checkbox to agree to our Terms of Service. '
                    }
                },
                ignore: [],
                submitHandler: function (form) {
                    $("#card-errors").html('');
                    window.orderFormSubmitting = true;

                    var throb = ff_functions.addThrobber('step-5-next');
                    $('#step-5-next').hide();
                    $(throb).show();
                    $(throb).append('<div>Thank you! Please wait while we process your order.</div>');

                    // append cart to order
                    var cart = $('<input>').attr('type', 'hidden').attr('name', 'frogboxCart').val(JSON.stringify(window.frogboxCart));
                    $('#frogbox-form').append($(cart));

                    // append page data to order
                    if (pageData) {
                        var pageTitle = $('<input>').attr('type', 'hidden').attr('name', 'pageTitle').val(pageData.title);
                        var pageUrl = $('<input>').attr('type', 'hidden').attr('name', 'pageUrl').val(pageData.url);
                        $('#frogbox-form').append($(pageTitle)).append($(pageUrl));
                    }


                    //stripe payment is only for Vancouver and Seattle for now
                    var deliverCity = $("#delivery-city").val();
                    debugger
                    //var deliverCity = "2";
                    if(window.stripeLocationIds.includes(deliverCity) && !window.isCorporate){

                        //authorize stripe payment
                        var other_data = getBillingDetails(form);
                        var totalAmount = window.frogboxCart.grandTotal;
                        var card = window.elements.getElement('card');


                        window.stripe.createToken(card, other_data)
                          .then(function (result) {
                              debugger
                              var token = null;
                              if (result.error) {
                                  // Inform the customer that there was an error.
                                  var errorElement = document.getElementById('card-errors');
                                  errorElement.textContent = result.error.message;
                                  $('#step-5-next').show();
                                  $(throb).hide();
                              } else {
                                   token = result.token;

                                  if (token != null) {
                                      var deliveryLocationId = $('#delivery-city').val();
                                      var url = '/ff/payment/' + token.id + '/' + totalAmount + '/' + deliveryLocationId;
                                      //fire ajax to create charge
                                      $.ajax({
                                          dataType: 'json',
                                          url: url,
                                          async: true,
                                          complete: function () {
                                              //$(throb).hide();
                                          },
                                          success: function (data, textStatus, jqXHR) {
                                              console.log(data);
                                              if (data.chargeId) {
                                                  var hiddenInput = document.createElement('input');
                                                  hiddenInput.setAttribute('type', 'hidden');
                                                  hiddenInput.setAttribute('name', 'chargeId');
                                                  hiddenInput.setAttribute('value', data.chargeId);
                                                  form.appendChild(hiddenInput);
                                                  form.submit();
                                              } else {
                                                  $("#card-errors").html("The payment failed. Please make sure the card credentials are correct and try again.").show();
                                                  $('body').animate({
                                                      scrollTop: $("#card-element").offset().top
                                                  }, 400);
                                                  $('#step-5-next').show();
                                                  $(throb).hide();
                                              }
                                          },
                                          error: function (jqXHR, textStatus, errorThrown) {
                                              //console.log('error', jqXHR, textStatus, errorThrown);
                                              $("#card-errors").html("The payment failed. Please make sure the card credentials are correct and try again.").show();
                                              $('body').animate({
                                                  scrollTop: $("#card-element").offset().top
                                              }, 400);
                                              $('#step-5-next').show();
                                              $(throb).hide();
                                          }
                                      });


                                  }
                              }

                          })
                          .catch(error => {
                              console.error('onRejected function called: ' + error);
                          });


                    }else{
                        form.submit();
                    }

                }
            });

            //get tip amount
            function getTipAmount(total){
                var tipPercentage = parseFloat($("input[name=tip]:checked").val());
                var tipAmount = null;
                if(tipPercentage) {
                    tipAmount = tipPercentage * total / 100;
                }
                return tipAmount;
            }

            //get billing details for Stripe createToken Request
            function getBillingDetails(){
                var sameAsDelivery = $("input[name=billing-details]").val() === 'same';
                var sameDeliveryContact = $("input[name=delivery-contact]").val() === 'same as main contact';
                var name, lastName, address, city, province, country, postalCode = '';
                if(sameAsDelivery) {
                    if(sameDeliveryContact){
                        name = $("#contact-first").val();
                         lastName = $("#contact-last").val();
                        if(lastName){
                            name += ' ' + lastName;
                        }
                    }else{
                        name = $("#delivery-contact-first").val();
                         lastName = $("#delivery-contact-last").val();
                        if(lastName){
                            name += ' ' + lastName;
                        }
                    }
                    address = $("#deliveryStreet").val();
                    city = $("#deliveryCity").val();
                    province = $("#deliveryProvince").val();
                    postalCode = $("#deliveryPostcode").val();
                    country = $("#deliveryCountry").val();

                }else{
                    name = $("#billingName").val();
                    address = $("#billingAddress").val();
                    city = $("#billingCity").val();
                    province = $("#billingProvince").val();
                    postalCode = $("#billingPostcode").val();
                    country = $("#billingCountry").val();

                }

                return {
                    name: name,
                    address_line1: address,
                    address_city: city,
                    address_state: province,
                    address_zip: postalCode,
                    address_country: country
                };
            }

        }
    }

})();
