    var Step2 = (function() {

    var id="step-2-content";
    var markup =
        '<p>Your price will be based on the number of Frogboxes you require and the amount of time you need the boxes. ' +
        'Prices start at one week, but keep the boxes for as long as you need (choose in Step 4).</p>'

        +'<div id="bundles-products-wrapper"><h3>Based on the size of your move, we\’d suggest:</h3></div>'

        +'<span id="step-2-footnote">* Charges may apply for non-local delivery</span>'

        +'<hr>'

        +'<div class="row">'
            +'<div class="col-sm-5">'
                +'<div id="step-2-previous" class="step-previous previous btn btn-previous">Previous</div>'
            +'</div>'
            +'<div class="col-sm-5 col-sm-offset-2">'
                +'<button id="step-2-next" class="step-next next btn btn-default">Next</button>'
            +'</div>'
        +'</div>';

    // setBundleSize removes any quantities from the model on non-selected
    // bundles, effectively deselecting items in other bundles
    function setBundleSize(bundle) {
        if (window.frogboxCart == null) {
            return;
        }
        if (window.frogboxCart['bundles'] != undefined && window.frogboxCart['bundles'].length > 0) {
            for (var pi = 0; pi < window.frogboxCart['bundles'].length; ++pi) {
                if (window.frogboxCart['bundles'][pi].group != bundle) {
                    window.frogboxCart['bundles'][pi].quantity = 0;
                }
            }
        }
    }

    // formats a list of items
    function formatItems(items, bundle) {
        if (items == undefined) {
            return;
        }
        var index;
        var itemsMarkup = Array();
        var groups = {};
        for (var index = 0; index < items.length; ++index) {
            var item = items[index];
            var name = ff_functions.sanitizeItemName(item.name);
            var markup = {};
            var inGroup = false;

            if (item.group == 'custom-bundle' || !item.group) {
                var itemClass = '';
                // layout for custom bundles and individual products
                if (item.group == 'custom-bundle') {
                    inGroup = true;
                }
                markup.col1 = '<img src="' + item.image + '">';
                markup.col2 = '<h3 class="product-name" priceItemID=' + item.priceItemID + '>' + item.name + '</h3>' + '<p class="product-description-text">' + item.description + '</p>';
                markup.col3 = '<h5>Price:</h5><div class="product-price">$' + parseFloat(item.value).toFixed(2) + ' + Tax<br><span class="price-each">/ EACH</span></div>';
                markup.col4 = '<label for="' + name + '"><h5>Quantity:</h5></label>';
                markup.col4 += '<div class="select-wrapper box-qty"><select key="' + item.key + '" name="' + name + '-quantity" id="'+ name + '-quantity">';
                markup.sequence = item.sequence;

                var selected = '';
                var qIncrement = 5;
                if (item.maximum_quantity > 99) {
                    qIncrement = 5;
                }
                for (var q = 0; q <= item.maximum_quantity; q = q + qIncrement) {
                    selected = '';
                    if (q == item.quantity) {
                        selected = ' SELECTED';
                    }
                    markup.col4 += '<option value="' + q + '"' + selected + '>' + q + '</option>';
                }
                markup.col4 += '</select></div>';

                // add to groups or to markup
                if (inGroup) {
                    // initialize object;
                    if (groups[item.group] == null) {
                        groups[item.group] = {};
                    }
                    // initialize array;
                    if (groups[item.group].rows == null) {
                        groups[item.group].rows = Array(markup);
                    }
                    else {
                        groups[item.group].rows.push(markup);
                    }
                }
                else {
                    var itemObj = { sequence: item.sequence, markup: ff_functions.formatItem(markup)};
                    itemsMarkup.push(itemObj);
                }
            }

        }

        // format groups
        for (var group in groups) {
            var itemSequence = 99;
            var classes = 'row group group-' + group;
            var groupMarkup = '';
            if (groups[group].rows != null) {
                if (!bundle) {
                    groupMarkup += '<div class="col0">' + groups[group].rows[0].groupImage + '</div>';
                    groupMarkup += '<div class="col1"><h3>' + groups[group].rows[0].groupName + '</h3>';
                    groupMarkup += '<div class="product-description-text">' + groups[group].rows[0].groupDescription + '</div></div><div class="option-reveal"><a class="show-options btn btn-info" data-target="group-'+ group+'">View options</a></div>';
                    itemSequence = groups[group].rows[0].sequence;
                }

                for (var i = 0; i < groups[group].rows.length; ++i) {
                    // add the row for each group
                    groupMarkup += ff_functions.formatItem(groups[group].rows[i]);
                }
            }
            var itemObj = { sequence: itemSequence, markup: '<div class="' + classes + '"><h3 class="step2-label">Number of boxes:</h3>' + groupMarkup + '</div>'};
            itemsMarkup.push(itemObj);
        }

        // sort the items
        itemsMarkup.sort(function(a, b) {
            if (parseInt(a.sequence) < parseInt(b.sequence)) {
                return -1;
            }
            else {
                return 1;
            }
        });

        // build a string and return
        var stringMarkup = '';
        for (var i in itemsMarkup) {
            stringMarkup += itemsMarkup[i].markup;
        }
        return stringMarkup;
    }


    function load() {
        // when the user selects a bundle type, show that bundle type and hide the others
        $('#bundles-wrapper input').change(function() {
            var checked = 'group-' + $("input:radio[name='bundle']:checked").val()
                + '-bundle';
            $('#bundles-products-wrapper .group').addClass('group-hidden');
            $('#bundles-products-wrapper .' + checked).removeClass('group-hidden');
            $("#bundles-wrapper input:radio[name='bundle']").closest('.inner-border').removeClass('active');
            $("#bundles-wrapper input:radio[name='bundle']:checked").closest('.inner-border').addClass('active');

            setBundleSize(checked.substring(6));
            ff_functions.updateItems();

            // scroll to bundles
            if ($(window).width() < 768) {
                $('html, body').animate({
                    scrollTop: $('#bundles-products-wrapper').offset().top - 40
                }, 500);
            }
        });

    }

    function validate() {
        return;
    }

    return {
        formatItems: function(items, bundle) {
          return formatItems(items, bundle);
        },
        load: function() {
            return load();
        },
        processBundles: function() {
            return true;
        },
        render: function() {
            var content = '<div id="' + id + '" class="step-content">';
            content += markup;
            content += '</div>';
            return content;
        },
        validate: function() {
            return validate();
        },
        validated: function() {
            return true;
        }
    };

})();
