var Step1 = (function() {

    var id = "step-1-content";

    var franchises =
        '<option value="" zip="">(select City)</option>'
        + '<option value="16" zip="98101">Seattle</option>'
        + '  <option value="15" zip="M4X">Toronto</option>'
        + '       <option value="2" zip="V6G">Vancouver</option>'
        + '       <option value="-" zip="">-</option>'
        + '        <option value="17" zip="V3X">Abbotsford-Langley</option>'
        + '       <option value="29" zip="83701">Boise</option>'
        + '      <option value="30" zip="T2B">Calgary</option>'
        + '       <option value="22" zip="L1B">Durham Region</option>'
        + '   <option value="27" zip="T5A">Edmonton</option>'
        + '  <option value="21" zip="L0R">Hamilton-Oakville</option>'
        + '       <option value="23" zip="V1P">Kelowna - Penticton</option>'
        + '       <option value="21" zip="N2A">Kitchener-Waterloo</option>'
        + '       <option value="32" zip="T1H">Lethbridge</option>'
        + '       <option value="20" zip="N6A">London</option>'
        + '       <option value="15" zip="L4T">Mississauga</option>'
        + '       <option value="24" zip="K2C">Ottawa</option>'
        + '        <option value="16" zip="98101">Seattle</option>'
        + '    <option value="17" zip="V3X">Surrey-Delta</option>'
        + '       <option value="15" zip="M4X">Toronto</option>'
        + '       <option value="2" zip="V6G">Vancouver</option>'
        + '       <option value="26" zip="V0S">Victoria</option>'
        + '        <option value="25" zip="R1A">Winnipeg</option>'
        + '        <option value="other">Don\'t see a location close to you?</option>';

    var markup =
        '<p>Choose the closest location to where you\'d like us to deliver the Frogboxes and the closest location where you\'d like us to pick them up when you are done.</p>'

    +'  <div class="row">'
        +'      <div class="location col-sm-6">'
        +'      <h4>Delivery Location:</h4>'
    +'   <div id="delivery-city-wrapper">'
        +'       <label for="delivery-city">Closest Frogbox Location:</label>'
    +'   <div class="select-wrapper">'
        +'      <select id="delivery-city" class="city" name="delivery-city" required>'
    + franchises
    +'  </select>'
    +'   </div>'
    +'   </div>'
    +'    </div>'

    +'   <div class="location col-sm-6">'
        +'      <h4>Pick-up Location:</h4>'
    +'   <div id="pickup-city-wrapper">'
        +'       <label for="pickup-city">Closest Frogbox Location:</label>'
    +'   <div class="select-wrapper">'
        +'       <select id="pickup-city" class="city" name="pickup-city" required>'
    + franchises
    +'   </select>'
    +'  </div>'
    +'  </div>'
    +'  </div>'
    +'  </div> <!-- .row -->'
    +'  <hr>'
    +' <div class="row">'
        +'<div class="col-sm-5 col-sm-offset-7">'
        +'<button id="step-1-next" class="step-next next btn btn-default">Next</button>'
        +'</div>'
        +'</div>';


    // determine if an element belongs to the 'delivery' or 'pickup' group based on its ID.
    function getWhich(element) {
        return ($(element).attr('id').slice(0, 8) == 'delivery') ? 'delivery' : 'pickup';
    }

    // sets defaults for location state/province, zip/postcode and country based on franchise
    function setLocationDefaults(which, franchise) {
        var country = whichCountry(franchise);

        // set the country
        $('#' + which + 'Country').val(country);

        // set defaults for state/province and zipcode/postcode based on country, without losing original value
        var currentValue = $('#' + which + 'Province').val();
        if (country == 'Canada') {
            $('#' + which + 'Province').html(canProvinces()).val(currentValue);
            $('#' + which + '-province-label').html('Province:');
            $('#' + which + '-postcode-label').html('Postal Code:');
        }
        else {
            $('#' + which + 'Province').html(usStates()).val(currentValue);
            $('#' + which + '-province-label').html('State:');
            $('#' + which + '-postcode-label').html('Zip Code:');
        }
    }

    // determine if a franchise is in the US or Canada
    function whichCountry(franchise) {
        return (franchise == 16 || franchise == 18 || franchise == 29) ? 'United States' : 'Canada';
    }

    // select list of Canadian provinces - matches what Vonigo needs to validate against
    function canProvinces() {
        var provinces = [
            '', 'Alberta', 'British Columbia', 'Manitoba', 'New Brunswick',
            'Newfoundland and Labrador', 'Nova Scotia', 'Ontario',
            'Prince Edward Island', 'Quebec', 'Saskatchewan',
            'Northwest Territories', 'Nunavut', 'Yukon'];
        return buildOptions(provinces);
    }

    // select list of US states - matches what Vonigo needs to validate against
    function usStates() {
        var states = [
            "", "Idaho", "Minnesota", "Washington", "",
            "Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado",
            "Connecticut", "Delaware", "District Of Columbia", "Florida", "Georgia",
            "Hawaii", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky",
            "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan",
            "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada",
            "New Hampshire", "New Jersey", "New Mexico", "New York",
            "North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon",
            "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota",
            "Tennessee", "Texas", "Utah", "Vermont", "Virginia",
            "West Virginia", "Wisconsin", "Wyoming" ];
        return buildOptions(states);
    }

    // build a state/provinces options list
    function buildOptions(options) {
        var markup = '';
        for (var i = 0; i < options.length; ++i) {
            markup += '<option value="' + options[i] + '">' + options[i] + '</option>';
        }
        return markup;
    }


    function load() {
        // when city changes, update products and addresses
        $('.city').change(function() {
            // remove error messages
            var errorSelector = '#' + $(this).attr('id') + '-error';
            $(errorSelector).remove();

            // manage labels and defaults on address forms based on country of chosen franchise
            setLocationDefaults(getWhich(this), $(this).val());

            // update products
            $(this).closest('.location').find('.other-message').remove();
            // check that we didn't pick '-'
            if ($(this).val() == '-') {
                $(this).val('');
                return;
            }
            else if ($(this).val() === 'other') {
                $(this).parent().append("<div class='other-message'>Currently we don't support other areas but please <a href='/contact'>Contact Us</a> if you have any questions.</div>");
                $(this).val('');
                return;
            }

            // if both cities are set to different values, get the IF pricelist and cache it
            var dC = ff_functions.getDeliveryFranchise();
            var pC = ff_functions.getPickupFranchise();
            if (dC != false && pC != false && dC != pC) {
                var errorParent = $(this).attr('id');
                var errorID = errorParent + '-error';
                var allInterfranchiseUrl = '/ff/pricelist/IF' + dC + '/7';
                var realUrl = '/ff/pricelist/IF' + dC + '/' + $('#keep-for').val();
                var throbberID = ff_functions.addThrobber($(this).attr('id'));
                ff_functions.updatePricelist(allInterfranchiseUrl, throbberID, errorID, errorParent, 'IF', false,
                    function() {
                        var throbberID = ff_functions.addThrobber($(this).attr('id'));
                        ff_functions.updatePricelist(realUrl, throbberID, errorID, errorParent, 'IF', true);
                    }
                );
                // disallow 'same as delivery checkbox'
                $('#pickupSame').attr('disabled', true).hide();
                $('label[for="pickupSame"]').hide();
            }
            else if (dC == pC) {
                // nullify the record of the interfranchise pricelist
                ff_functions.unSetPriceList('interfranchise');
                // allow 'same as delivery' checkbox
                $('#pickupSame').attr('disabled', false).show();
                $('label[for="pickupSame"]').show();
            }

            // initialize variable - should we change the delivery list?
            var delivery = false;
            errorParent = $(this).attr('id');
            errorID = errorParent + '-error';
            if (errorParent == 'delivery-city') {
                delivery = true;
            }
            var theSuburb = delivery ? getSuburb('delivery') : getSuburb('pickup');

            // if the interfranchise pricelist is null, go get new pricelist
            if (ff_functions.getPriceList('interfranchise') == undefined || ff_functions.getPriceList('interfranchise') == null) {
                // when the city changes, update the pricelist
                if (theSuburb) {
                    var getFranchise = delivery ? dC : pC;
                    var keepFor = $('#keep-for').val();
                    var updateAll = false;
                    if (keepFor == 7) {
                        updateAll = true;
                    }
                    var allPricelists = '/ff/pricelist/' + theSuburb + '/7';
                    var url = '/ff/pricelist/' + theSuburb + '/' + keepFor;
                    throbberID = ff_functions.addThrobber($(this).attr('id'));
                    var updateFinal = false;
                    if (delivery === true) {
                        updateFinal = true;
                    }
                    // download all pricelists, and update if keepfor is at 7
                    ff_functions.updatePricelist(allPricelists, throbberID, errorID, errorParent, delivery, updateAll,
                        function () {
                            throbberID = ff_functions.addThrobber($(this).attr('id'));
                            ff_functions.updatePricelist(url, throbberID, errorID, errorParent, delivery, updateFinal);
                        }
                    );
                }
            }

            // set the state or province
            var prefix = delivery ? 'delivery' : 'pickup';
            if ($('#' + prefix + 'Country').val() == 'Canada') {
                var province = ff_functions.lookupProvince(theSuburb);
                if (province != undefined) {
                    $('#' + prefix + 'Province').val(province);
                }
            }
            else if ($('#' + prefix + 'Country').val() == 'United States') {
                var state = ff_functions.lookupState(getFranchise);

                if (state != undefined) {
                    $('#' + prefix + 'Province').val(state);
                }
            }

            // update calendar and grey out dates for that franchise
            var country = whichCountry(getFranchise);
            var validSuburb;
            if (country == 'Canada') {
                validSuburb = ff_functions.lookupProvince(theSuburb);
            }
            else if (country == 'United States') {
                validSuburb = ff_functions.lookupState(theSuburb);
            }
            if (validSuburb) {
                var dURL = '/ff/dates/' + theSuburb;
                var whichID = $(this).attr('id');
                $.ajax({
                    dataType: 'json',
                    url: dURL,
                    success: function (data, textStatus, jqXHR) {
                        if (data.error != null && data.displayError) {
                            var errorElement = '<div class="error" id="' + whichID + '-error">' + data.error + '</div>';
                            $(errorElement).appendTo('label[for="' + whichID + '"]');
                        }
                        else {
                            var which = whichID.substring(0, whichID.length - 7);
                            disabledDates[which] = data;
                            ff_functions.initializeDatepicker(which);
                        }
                    },
                    error: function ( jqXHR ,  textStatus,  errorThrown) {
                        console.log('error', jqXHR, textStatus, errorThrown);
                    }
                });
            }
        });

    }

    function getSuburb(which) {
        if (jQuery('#' + which + 'Postcode').val() != '' && jQuery('#' + which + 'Postcode').val() != null) {
            return ff_functions.shortenPostCode(jQuery('#' + which + 'Postcode').val());
        }
        else if (jQuery('#' + which + '-city option:selected').attr('zip') != '' && jQuery('#' + which + '-city option:selected').attr('zip') != null) {
            return jQuery('#' + which + '-city option:selected').attr('zip').toUpperCase();
        }
        else {
            return false;
        }
    }

    function validate() {
        return true;
    }

    return {
        getPriceListCode: function(which) {
            return getSuburb(which);
        },
        getSuburb: function(which) {
          return getSuburb(which);
        },
        load: function() {
          return load();
        },
        render: function() {
            var content = '<div id="' + id + '" class="step-content">';
            content += markup;
            content += '</div>';
            return content;
        },
        updateItemsDone: function() {},
        validate: function() {
            return validate();
        }
    };

})();
