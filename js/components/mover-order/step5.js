var Step5 = (function() {

    var id="step-5-content";

    var markup =
        '<div id="contact-details" class="row">'
        + '<h4>Contact Details</h4>'
        + '<div class="col-sm-6">'
        + '<label for="contact-first">First Name:</label>'
        + '<input type="text" id="contact-first" name="contact-first" required>'
        + '</div>'
        + '<div class="col-sm-6">'
        + '<label for="contact-last">Last Name:</label>'
        + '<input type="text" id="contact-last" name="contact-last" required>'
        + '</div>'
        + '<div class="col-sm-6">'
        + '<label for="contact-primary-phone">Primary Phone:</label>'
        + '<input type="text" id="contact-primary-phone" name="contact-primary-phone" required>'
        + '</div>'
        + '<div class="col-sm-6">'
        + '<label for="contact-primary-ext">ext: <span class="field-optional">(optional)</span></label>'
        + '<input type="text" id="contact-primary-ext" name="contact-primary-ext">'
        + '</div>'
        + '<div class="col-sm-6">'
        + '<label for="contact-alternate-phone">Alternate Phone: <span class="field-optional">(optional)</span></label>'
        + '<input type="text" id="contact-alternate-phone" name="contact-alternate-phone">'
        + '</div>'
        + '<div class="col-sm-6">'
        + '<label for="contact-primary-ext">ext: <span class="field-optional">(optional)</span></label>'
        + '<input type="text" id="contact-alternate-ext" name="contact-alternate-ext">'
        + '</div>'
        + '<div class="col-sm-6">'
        + '<label for="contact-email">Email:</label>'
        + '<input type="email" id="contact-email" name="contact-email" required>'
        + '</div>'
        + '</div>'

        + '<div class="row">'
        + '<div id="delivery-contact-wrapper" class="step-5-contact col-sm-6">'
        + '<h4>Delivery Contact</h4>'
        + '<input type="radio" name="delivery-contact" value="same as main contact" id="delivery-contact-main">'
        + '<label for="delivery-contact-main">Same as Main Contact</label>'
        + '<input type="radio" name="delivery-contact" value="different delivery contact" id="delivery-contact-different">'
        + '<label for="delivery-contact-different">Different delivery contact</label>'
        + '<div id="delivery-different-contact-wrapper" class="group-hidden">'
        + '<label for="delivery-contact-first">First Name:</label>'
        + '<input type="text" name="delivery-contact-first" id="delivery-contact-first" >'
        + '<label for="delivery-contact-last">Last Name:</label>'
        + '<input type="text" name="delivery-contact-last" id="delivery-contact-last" >'
        + '</div>'
        + '</div>'
        + '<div id="pickup-contact-wrapper" class="step-5-contact col-sm-6">'
        + '<h4>Pick-up Contact</h4>'
        + '<input type="radio" name="pickup-contact" value="same as main contact" id="pickup-contact-main">'
        + '<label for="pickup-contact-main">Same as Main Contact</label>'
        + '<input type="radio" name="pickup-contact" value="different pickup contact" id="pickup-contact-different">'
        + '<label for="pickup-contact-different">Different pick-up contact</label>'
        + '<div id="pickup-different-contact-wrapper" class="group-hidden">'
        + '<label for="pickup-contact-first">First Name:</label>'
        + '<input type="text" name="pickup-contact-first" id="pickup-contact-first">'
        + '<label for="pickup-contact-last">Last Name:</label>'
        + '<input type="text" name="pickup-contact-last" id="pickup-contact-last">'
        + '</div>'
        + '</div>'
        + '</div>'
        + '<input type="hidden" id="promo-code" name="promo-code">'
        + '<div id="payment-wrapper">'
        + '   <h4>Payment</h4>'
        + '        <p>We will collect payment at the same time we deliver the Frogboxes.</p>'
    + ' </div>'
    + '   <div id="confirm-wrapper">'
    + '       <h4>Confirm and Place Order</h4>'
    + '   <p>Please <span class="ordering-emphasis">review your order</span> and contact details to confirm that everything is entered correctly. You can click on any step to edit the information on that step.</p>'
    + '   <p>When you are ready to place your order, click <span class="ordering-emphasis">Place Order</span> below.</p>'
    + '   </div>'
    + '  <div>'
    + ' <button type="button" data-toggle="modal" data-target="#terms-conditions">Show Terms of Service</button>'
    + ' <label for="terms">I agree with the terms of service: </label>'
    + '  <input type="checkbox" value="1" name="terms" id="terms" required>'
    + '  </div>'
    + '  <hr>'
    + ' <div class="row">'
    + '      <div class="col-sm-5">'
    + '      <div id="step-5-previous" class="step-previous previous btn btn-previous">Previous</div>'
    + '      </div>'
    + '      <div class="col-sm-5 col-sm-offset-2">'
        + ' <button id="step-5-next" class="step-next next btn btn-default" name="_ff_submit" value="submit">Place Order</button>'
    + '</div>'
    + '</div>';


    function load() {
        // set the form action if it is specified
        if (pageData && pageData.confirmation) {
            $('#frogbox-form').prop('action', pageData.confirmation);
        }

        // when a user enters a promo code, check for validity against the API
        $('#promo-code').change(function() {
            if (window.frogboxCart == null) {
                window.frogboxCart = {};
            }
            $('#promo-code-error').remove();
            window.frogboxCart.promo = null;
            $('#promo-valid').remove();
            if ($('#promo-code').val().length > 0) {

                // add a throbber
                var throbberID = ff_functions.addThrobber('promo-code');
                var dS = ff_functions.getDeliverySuburb();
                if (dS) {
                    var promoURL = '/ff/promo/' + dS + '/' + $('#promo-code').val();
                    $.ajax({
                        dataType: 'json',
                        url: promoURL,
                        success: function (data, textStatus, jqXHR) {
                            if (data.error != null && data.displayError) {
                                var errorElement = '<div class="error" id="promo-code-error">'
                                    + data.error + '</div>';
                                $(errorElement).appendTo('label[for="promo-code"]');
                            }
                            else {
                                if (data != undefined) {
                                    $('<span id="promo-valid">' + data.promoCode + ' is good.</span>').insertAfter('#promo-code')
                                    window.frogboxCart.promo = data;
                                    ff_functions.generateSummary();
                                }
                            }
                        },
                        complete: function () {
                            // remove throbber
                            $(throbberID).remove();
                            ff_functions.generateSummary();
                        },
                        error: function ( jqXHR ,  textStatus,  errorThrown) {
                            console.log('error', jqXHR, textStatus, errorThrown);
                        }
                    });
                }
            }
            ff_functions.generateSummary();
        });

        // hide/show delivery and pickup contact areas
        $('input[name=delivery-contact]').change(function() {
            if ($('input[name=delivery-contact]:checked').val() == 'different delivery contact') {
                $('#delivery-different-contact-wrapper').removeClass('group-hidden');
            }
            else {
                $('#delivery-different-contact-wrapper').addClass('group-hidden');
            }
        });
        $('input[name=pickup-contact]').change(function() {
            if ($('input[name=pickup-contact]:checked').val() == 'different pickup contact') {
                $('#pickup-different-contact-wrapper').removeClass('group-hidden');
            }
            else {
                $('#pickup-different-contact-wrapper').addClass('group-hidden');
            }
        });


    }

    function validate() {
        return;
    }

    return {
        load: function() {
            return load();
        },
        render: function() {
            var content = '<div id="' + id + '" class="step-content">';
            content += markup;
            content += '</div>';
            return content;
        },
        validate: function() {
            return validate();
        }
    };

})();
