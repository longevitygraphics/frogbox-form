var Step4 = (function() {

    var id="step-4-content";

    var markup = '<div id="step-4-your-price">'
        +'    <div class="row">'
        +'    <div class="col-sm-6">'
        +'<h4>Your Price and Pick-up Dates</h4>'
        +'    <p>We understand that moves don\'t happen overnight, you can select a date that gives you enough time to get fully unpacked. The longer you keep the Frogboxes the cheaper their cost per week.</p>'
        +'    </div>'

        +'    <div id="step-4-current-order" class="col-sm-6">'
        +'<h4>For your current order:</h4>'
        +'    <ul id="current-order-price-by-duration"></ul>'
        +'        </div>'

        +'        </div>'
        +'        </div>'

        +'        <div class="row">'
        +'<div id="delivery-date-wrapper" class="col-sm-6">'
        +'<h4>Delivery Date and Time</h4>'
        +'<label for="delivery-date">Delivery Date:</label>'
        +'<input type="text" id="delivery-date" name="delivery-date" class="booking-date" required readonly>'
        +'<label for="delivery-time">Delivery Time:</label>'
        +'<div class="select-wrapper">'
        +'<select id="delivery-time" class="booking-time" name="delivery-time" required></select>'
        +'    </div>'
        +'</div>'
        +'<div id="pickup-date-wrapper" class="col-sm-6">'
        +'<h4>Pick-up Date and Time</h4>'
        +'<label for="pickup-date">Pick-up Date:</label>'
        +'<input type="text" id="pickup-date" name="pickup-date" class="booking-date" required readonly>'
        +'<label for="pickup-time">Pick-up Time:</label>'
        +'<div class="select-wrapper">'
        +'<select id="pickup-time" class="booking-time" name="pickup-time" required></select>'
        +'    </div>'
        +'<div id="keep-for-wrapper">Keep for: '
        +'<span id="keep-for-display">1 week</span>'
        +'<input type="hidden" id="keep-for" name="keep-for" value="7">'
        +'</div>'
        +'</div>'
        +'    </div>'

        +'    <div class="row">'
        +'<div id="delivery-address" class="col-sm-6">'
        +'        <h4>Delivery Address</h4>'

        +'    <label for="deliveryStreet">Address:</label>'
        +'<input type="text" id="deliveryStreet" name="deliveryStreet" required  placeholder="Apartment or suite # - Street address">'
        +'<label for="deliveryCity">City:</label>'
        +'<input type="text" id="deliveryCity" name="deliveryCity" required >'
        +'<label for="deliveryProvince" id="delivery-province-label">Province:</label>'
        +' <div class="select-wrapper">'
        +'    <select id="deliveryProvince" name="deliveryProvince" class="province" required></select>'
        +' </div>'
        +'  <label for="deliveryPostcode" id="delivery-postcode-label">Postal Code:</label>'
        +'  <input type="text" id="deliveryPostcode" class="postcode" name="deliveryPostcode" required>'
        +'  <input type="hidden" id="deliveryCountry" name="deliveryCountry" >'
        +'  <label for="deliveryDescription">Description: <span class="field-optional">(optional)</span></label>'
        +'      <textarea id="deliveryDescription" name="deliveryDescription"  placeholder="Do you have stairs?"></textarea>'
        +'  <input type="checkbox" name="pickupSame" id="pickupSame" >'

        +'  <label for="pickupSame">Same as delivery location?</label>'
        +'</div>'

        +'<div id="pick-up-address" class="col-sm-6">'
        +'   <h4>Pick-up Address</h4>'

        +' <div id="pickup-address-wrapper">'
        +'     <label for="pickupStreet">Address:</label>'
        +' <input type="text" id="pickupStreet" name="pickupStreet" required placeholder="Apartment or suite # - Street address">'
        +' <label for="pickupCity">City:</label>'
    +' <input type="text" id="pickupCity" name="pickupCity" required >'
        +'  <label for="pickupProvince">State/Province:</label>'
        +'  <div class="select-wrapper">'
        +'      <select id="pickupProvince" name="pickupProvince" class="province" required></select>'
        +'  </div>'
        +'  <label for="pickupPostcode">Zip/Postal Code:</label>'
        +'  <input type="text" id="pickupPostcode" class="postcode" name="pickupPostcode" required >'
        +'  <input type="hidden" id="pickupCountry" name="pickupCountry" >'
        +'  <label for="pickupDescription">Description: <span class="field-optional">(optional)</span></label>'
        +'      <textarea id="pickupDescription" name="pickupDescription" placeholder="Do you have stairs?"></textarea>'
        +'   </div>'
        +'    </div>'
        +'    </div>'

        +'    <hr>'
        +'    <div class="row">'
        +'  <div class="col-sm-5">'
        +'        <div id="step-4-previous" class="step-previous previous btn btn-previous">Previous</div>'
        +' </div>'
        +' <div class="col-sm-5 col-sm-offset-2">'
        +' <button id="step-4-next" class="step-next next btn btn-default">Next</button>'
        +'   </div>'
        +'  </div> ';

    function load() {
        // initialize datepickers
        ff_functions.initializeDatepicker('pickup');
        ff_functions.initializeDatepicker('delivery');

        // changing postcodes changes them in step 1 also
        $('.postcode').change(function() {
            var zip = $(this).val();
            var whichID = $(this).attr('id');
            if (whichID == 'deliveryPostcode') {
                $('#delivery-suburb').val(zip).change();
            }
            else {
                $('#pickup-suburb').val(zip).change();
            }
        });

        // when a user enters a date, check that it is valid and download possible times
        $('.booking-date').on('changeDate', function() {
            var theDate = $(this).val();
            var shortDate = "" + theDate.replace(/-/g, "");

            var whichID = $(this).attr('id');
            var whichType = whichID.substring(0, whichID.length -5);

            // clear errors
            $('#' + whichID + '-error').remove();

            var allOptions = window.availability[whichType];

            if (allOptions && allOptions[shortDate]) {
                var markup = '<option value="">(select Time)</option>';
                var options = allOptions[shortDate];
                for (var option in options) {
                    if (options.hasOwnProperty(option)) {
                        var routeTime = options[option].routeID + '-' + options[option].startTime;
                        markup += '<option time="' + options[option].startTime + '" value="'+ routeTime +'">' + options[option].label + '</option>';
                    }
                }
                ff_functions.setTimeOptions(whichID, markup);
            }
            else {
                ff_functions.getSingleAvailability(whichID, theDate);
            }

            if (whichID == 'delivery-date' &! $('#pickup-date').val()) {
                ff_functions.initializeDatepicker('pickup');
                $('#pickup-date').trigger('changeDate');
            }

            // a date changed, so check if we need to update the keepFor value and also the pricing
            ff_functions.updateKeepFor();
        });

        // when the user checks the 'pickup same as delivery' box, copy the
        // address and hide the fields. When the user unchecks it, unhide the
        // fields
        $('#pickupSame').change(function() {
            if (this.checked) {
                $('#pickupStreet').val($('#deliveryStreet').val());
                $('#pickupCity').val($('#deliveryCity').val());
                $('#pickupProvince').val($('#deliveryProvince').val());
                $('#pickupPostcode').val($('#deliveryPostcode').val());
                $('#pickupDescription').val($('#deliveryDescription').val());
                $('#pickupCountry').val($('#deliveryCountry').val());
                $('#pickup-address-wrapper').addClass('group-hidden');
            }
            else {
                $('#pickup-address-wrapper').removeClass('group-hidden');
            }
        });

    }

    return {
        load: function() {
            return load();
        },
        render: function() {
            var content = '<div id="' + id + '" class="step-content">';
            content += markup;
            content += '</div>';
            return content;
        },
        validate: function() {
            return true;
        }
    };

})();
