var Step1 = (function() {

    var id = "step-1-content";

    var markup =
        '<p>Enter the postal code where you\'d like us to deliver the Frogboxes and the postal code where you\'d like '
        +'us to pick them up when you are done with your move.</p>'

        +'<div class="row">'

        +'<div class="location col-sm-6">'
        +'<h4>Delivery Location:</h4>'
        +'<div id="delivery-suburb-wrapper">'
        +'<label for="delivery-suburb" class="suburb-label"></label>'


        +'<div class="input-wrapper">'
        +'<input id="delivery-suburb" class="city" name="delivery-suburb" type="textfield">'
        +'</div></div></div>'

        +'<div class="location col-sm-6">'
        +'<h4>Pick-up Location:</h4>'
        +'<div id="pickup-suburb-wrapper">'
        +'<label for="pickup-suburb" class="suburb-label"></label>'

        +'<div class="input-wrapper">'
        +'<input id="pickup-suburb" class="city" name="pickup-suburb" type="textfield">'
        +'</div></div></div>'

        +'<input id="delivery-city" type="hidden" value="" name="delivery-city">'
        +'<input id="pickup-city" type="hidden" value="" name="pickup-city">'
        +'<input id="includeMovePack" type="hidden" value="1" name="includeMovePack">'
        +'<input id="isTag" type="hidden" value="274" name="isTag">'
        +'<input id="PricelistCode" type="hidden" value="" name="PricelistCode">'

        +'</div>'; // row

    var validFranchises = Array();

    // determine if an element belongs to the 'delivery' or 'pickup' group based on its ID.
    function getWhich(element) {
        return ($(element).attr('id').slice(0, 8) == 'delivery') ? 'delivery' : 'pickup';
    }

    // sets defaults for location state/province, zip/postcode and country based on franchise
    function setLocationDefaults(which, franchise) {
        var country = whichCountry(franchise);

        // set the country
        $('#' + which + 'Country').val(country);

        // set defaults for state/province and zipcode/postcode based on country, without losing original value
        var currentValue = $('#' + which + '-suburb').val();
        if (country == 'Canada') {
            $('#' + which + 'Province').html(canProvinces()).val(ff_functions.lookupProvince(currentValue));
            $('#' + which + '-province-label').html('Province:');
            $('#' + which + '-postcode-label').html('Postal Code:');
        }
        else {
            $('#' + which + 'Province').html(usStates()).val(ff_functions.lookupState(currentValue));
            $('#' + which + '-province-label').html('State:');
            $('#' + which + '-postcode-label').html('Zip Code:');
        }
    }

    // determine if a franchise is in the US or Canada
    function whichCountry(franchise) {
        return (franchise == 16 || franchise == 18 || franchise == 29) ? 'United States' : 'Canada';
    }

    // select list of Canadian provinces - matches what Vonigo needs to validate against
    function canProvinces() {
        var provinces = [
            '', 'Alberta', 'British Columbia', 'Manitoba', 'New Brunswick',
            'Newfoundland and Labrador', 'Nova Scotia', 'Ontario',
            'Prince Edward Island', 'Quebec', 'Saskatchewan',
            'Northwest Territories', 'Nunavut', 'Yukon'];
        return buildOptions(provinces);
    }

    // select list of US states - matches what Vonigo needs to validate against
    function usStates() {
        var states = [
            "", "Idaho", "Minnesota", "Washington", "",
            "Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado",
            "Connecticut", "Delaware", "District Of Columbia", "Florida", "Georgia",
            "Hawaii", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky",
            "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan",
            "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada",
            "New Hampshire", "New Jersey", "New Mexico", "New York",
            "North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon",
            "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota",
            "Tennessee", "Texas", "Utah", "Vermont", "Virginia",
            "West Virginia", "Wisconsin", "Wyoming" ];
        return buildOptions(states);
    }

    // build a state/provinces options list
    function buildOptions(options) {
        var markup = '';
        for (var i = 0; i < options.length; ++i) {
            markup += '<option value="' + options[i] + '">' + options[i] + '</option>';
        }
        return markup;
    }

    function validateZip(zip, throbberID, successCallback, failureCallback) {
        var url = '/ff/zip/' + zip;

        // if the request is pending, don't request it a second time
        if (ff_functions.isPending(url)) {
            window.setTimeout(function() {
                validateZip(zip, throbberID, successCallback, failureCallback);
            }, 1000);
            return;
        }

        ff_functions.setPending(url);
        $.ajax({
            dataType: 'json',
            url: url,
            success: function(data, textStatus, jqXHR) {
                if (validFranchise(data) && successCallback) {
                    successCallback(data);
                }
                else if (failureCallback) {
                    failureCallback(data);
                }
            },
            error: function() {
                console.log('error', jqXHR, textStatus, errorThrown);
            },
            complete: function() {
                ff_functions.clearPending(url);
                $(throbberID).remove();
            }
        });
    }

    function validFranchise(franchise) {
        return (validFranchises.indexOf(franchise) > -1);
    }

    function getKeepFor() {
        return $('#keep-for').val();
    }

    function setDeliveryFranchise(franchise) {
        $('#delivery-city').val(franchise);
        setLocationDefaults('delivery', franchise);
        setPriceListCodeByFranchise(franchise);

        // this is the part where we update the pricelist
        var updateAll = false;
        if (getKeepFor() === 7) {
            updateAll = true;
        }
        var errorParent = 'delivery-city';
        var errorID = errorParent + '-error';
        var throbberID = 'delivery-city-throbber';
        var url = '/ff/pricelist/' + getPriceListCode('delivery') + '/' + getKeepFor();
        ff_functions.updatePricelist(url, throbberID, errorID, errorParent, true, updateAll,
            function() {
                ff_functions.processPricelist(ff_functions.getPriceList(url), true);
            }
        );

        // this is the part where we get the delivery day options
        ff_functions.loadDisabledDates('delivery', franchise, getSuburb('delivery'));

    }

    function setPickupFranchise(franchise) {

        $('#pickup-city').val(franchise);
        setLocationDefaults('pickup', franchise);

        // update the pickup pricelist
        var url = '/ff/pricelist/' + getPriceListCode('pickup') + '/' + getKeepFor();
        ff_functions.updatePricelist(url, 'fakeThrob', 'fakeError', 'fakeErr', false, true);

        // reiinitialize the datepicker
        ff_functions.loadDisabledDates('pickup', franchise, getSuburb('pickup'));
    }

    function load() {
        loadInitialBundles();

        // validate the zip code
        $('.city').change(function() {
            var errorParent = $(this).attr('id');
            var errorID = errorParent + '-error';
            $('#' + errorID).remove();

            var delivery = false;
            if (errorParent === 'delivery-suburb') {
                delivery = true;
            }

            var fullZip = $(this).val();

            var zip = ff_functions.shortenPostCode(fullZip);
            var throbberID = ff_functions.addThrobber($(this).attr('id'));
            validateZip(zip, throbberID,
                function(franchise) { // success callback
                    $('#' + errorID).remove();
                    if (delivery) {
                        $('#deliveryPostcode').val(fullZip).removeClass('error');
                        setDeliveryFranchise(franchise);
                    }
                    else {
                        $('#pickupPostcode').val(fullZip).removeClass('error');
                        setPickupFranchise(franchise);
                    }
                },
                function() { // failure callback
                  var errorElement = '<div class="error" id="' + errorID + '">'
                    + 'These bundles aren\'t available in your area. You can still <a href="order">place a regular order!</a>'
                    + '</div>';
                  if ($('label[for="' + errorParent + '"]').length) {
                    $(errorElement).appendTo('label[for="' + errorParent + '"]');
                  }
                  else {
                    $(errorElement).appendTo('#' + errorParent);
                  }
                  // set the active step to step 1 (at index 0)
                  $('#accordion').accordion('option', 'active', ( 0 ));
                  Accordion.scrollToStep(0);
            });
        });
    }

    /**
     * Loads initial bundles - the 7, 14, 21 and 28 day pricelist for mover
     */
    function loadInitialBundles() {
        // the pricelist should be added from wp_localize_script
        if (Pricelist) {

            var pricelists = JSON.parse(Pricelist);

            var url = '/ff/pricelist/' + window.moverOptions.PricelistCode + '/7';

            // if there was an error, try loading it over ajax
            if (pricelists.error) {
                ff_functions.updatePricelist(url, 'fakeThrob', 'fakeError', 'fakeErr', true, true,
                    function() { // callback - set the pickup pricelist
                        ff_functions.updatePricelist(url, 'fakeThrob', 'fakeError', 'fakeErr', false, true);
                    }
                );
            }
            // if there wasn't an error, process the pricelist
            else {
                // expect an array of pricelists
                for (var i = 0; i < pricelists.length; ++i) {
                    var newUrl = url.substring(0, url.lastIndexOf('/')+1);
                    url = newUrl + (i+1) * 7;
                    ff_functions.setPriceList(url, pricelists[i]);

                    // only upate if it is the first list we are processing
                    if (i == 0) {
                        ff_functions.processPricelist(pricelists[i], true);
                        ff_functions.processPricelist(pricelists[i], false);
                    }
                }
            }
        }
        // in any case, bind the bundle selection process to the bundles
        bindBundleChoice();
    }


    function bindBundleChoice() {
        // when the user selects a bundle type, show that bundle type and hide the others
        $('.bundle-choice').change(function() {
            $('#bundleError').remove();
            var checked = $("input:radio[name='bundle-choice']:checked").val();
            setBundleSize(checked);
            ff_functions.updateItems(); // resets markup
            bindBundleChoice();
            $(document).resize(); // this call is necessary to make bundles an equal height
        });
    }

    // setBundleSize removes any quantities from the model on non-selected
    // bundles, effectively deselecting items in other bundles
    function setBundleSize(bundle) {
        $('#bundle').val(bundle);
        if (window.frogboxCart == null) {
            return;
        }
        if (window.frogboxCart['bundles'] && window.frogboxCart['bundles'].length > 0) {
            for (var pi = 0; pi < window.frogboxCart['bundles'].length; ++pi) {
                if (window.frogboxCart['bundles'][pi].priceItemID != bundle) {
                    window.frogboxCart['bundles'][pi].quantity = 0;
                }
                else {
                    window.frogboxCart['bundles'][pi].quantity = 1;
                }
            }
        }
    }

    function getSuburb(which) {
        if (jQuery('#' + which + 'Postcode').val()) {
            return ff_functions.shortenPostCode(jQuery('#' + which + 'Postcode').val());
        }
        else if (jQuery('#' + which + '-suburb').val()) {
            return ff_functions.shortenPostCode(jQuery('#' + which + '-suburb').val());
        }
        else {
            return false;
        }
    }

    function getPriceListCode(which) {
        return window.moverOptions.PricelistCode;
    }

    function setPriceListCodeByFranchise(franchise) {
        var parts = window.moverOptions.PricelistCode.split('-');
        var code = parts[0] + '-' + parts[1] + '-' + franchise;
        setPriceListCode(code);
    }

    function setPriceListCode(code) {
        window.moverOptions.PricelistCode = code;
        $('#PricelistCode').val(code);
    }

    function validate() {
        var rules = {
            required: true,
            noRelatedError: true,
            messages: {
                required: 'Please enter a postal code.',
                noRelatedError: ''
            }
        };
        $('#delivery-suburb').rules('add', rules);
        $('#pickup-suburb').rules('add', rules);
        $('#deliveryPostcode').rules('add', rules);
        $('#pickupPostcode').rules('add', rules);
    }

    function setValidFranchises(franchises) {
        validFranchises = franchises.map(Number);
    }

    function updateItemsDone() {
        bindBundleChoice();
    }

    return {
        getPriceListCode: function(which) {
            return getPriceListCode(which);
        },
        getSuburb: function(which) {
            return getSuburb(which);
        },
        getValidFranchises: function() {
            return validFranchises;
        },
        load: function() {
          return load();
        },
        render: function() {
            var content = '<div id="' + id + '" class="step-content">';
            content += markup;
            return content;
        },
        setValidFranchises: function(franchises) {
            return setValidFranchises(franchises);
        },
        updateItemsDone: function() {
            return updateItemsDone();
        },
        validate: function() {
            return validate();
        },
        validFranchise: function(f) {
            return validFranchise(f);
        },
        validateZip: function(zip, throbberID, successCallback, failureCallback) {
            return validateZip(zip, throbberID, successCallback, failureCallback);
        }
    };

})();
