var Step2 = (function() {

    var id="step-2-content";
    var markup =
          '<p>Your special <span class="affiliateName"></span> price will be based on the number of Frogboxes you ' +
          'require and the amount of time you need the boxes.' +
          '<p>Prices start at two weeks, but keep them for as long as you need (choose in Step 3).</p>'

          + '<div id="bundles-wrapper">'
          + '<div id="bundles-list"><input type="hidden" name="bundle" id="bundle">'
          + '<div id="bundles-products-wrapper"><h3>Based on the size of your move, we\’d suggest:</h3></div>'
          + '<span class="step-2-footnote">* Charges may apply for non-local delivery</span>'

          + '<hr>'

          + '<div class="row">'
          + '<div class="col-sm-5 col-sm-offset-7">'
          + '<button id="step-2-next" class="step-next next btn btn-default">Next</button>'
          + '</div></div></div>';

    // formats a list of items
    function formatItems(items, bundle) {
        if (items == undefined) {
            return;
        }
        var index;
        var itemsMarkup = Array();
        var groups = {};
        if (bundle) {
            var newItems = items.slice(0,4);
            items = newItems;
        }
        for (var index = 0; index < items.length; ++index) {
            var item = items[index];
            var name = ff_functions.sanitizeItemName(item.name);
            var markup = {};
            var inGroup = false;

            // set up the item so we can format it generically
            if (item.group != null && item.group.indexOf('bundle') > -1 && item.group != 'custom-bundle') {
                // the layout for regular bundles
                item.group='bundles';
                inGroup = true;
                var bundleChecked = '';
                if (ff_functions.getQuantity(item.key)) {
                    bundleChecked = 'CHECKED';
                }
                markup.wrapper = '<label for="' + name + '"><h4>' + item.name + ' </h4>';
                markup.col1 = '<input type="radio" name="bundle-choice" value="' + item.priceItemID + '"'
                    + ' key="' + item.key + '" ' + 'id="' + name + '"' + bundleChecked + ' class="bundle-choice">';
                if (item.description_details != null) {
                    markup.col2 = '';
                    for (var i = 0; i < item.description_details.length; ++i) {
                        markup.col2 += '<div class="product-details"><h4>' + item.description_details[i].title + '</h4>';
                        markup.col2 += '<p>' + item.description_details[i].dimensions;
                        if (item.description_details[i].description) {
                            markup.col2 += '<br>' + item.description_details[i].description;
                        }
                        markup.col2 += '</p></div>';
                    }
                }
                var price;
                if (item.value == 0) {
                    price = 'Free';
                }
                else {
                    price = '$' + parseInt(item.value, 10).toFixed(2) + ' + Tax';
                }
                markup.col3 = '<h5>Your Price:</h5><div class="product-price">' + price + '</div>';
                markup.col4 = '<span class="product-value-message">' + item.value_message + '</span>';
                markup.wrapperClose = '</label>';
            }
            else {
                var itemClass = '';
                if (item.group == 'custom-bundle' || item.group == null) {
                    // layout for custom bundles and individual products
                    if (item.group == 'custom-bundle') {
                        inGroup = true;
                    }
                    markup.col1 = '<img src="' + item.image + '">';
                    markup.col2 = '<h3 class="product-name" priceItemID=' + item.priceItemID + '>' + item.name + '</h3>' + '<p class="product-description-text">' + item.description + '</p>';
                }
                else {
                    inGroup = true;
                    markup.groupName = item.name;
                    markup.groupImage = '<img src="' + item.image + '">';
                    markup.groupDescription = item.description;
                    markup.col1 = '';
                    if (item.title_display_option != null) {
                        markup.col2 = '<h4>' + item.title_display_option + '</h4>';
                    }
                }
                markup.col3 = '<h5>Price:</h5><div class="product-price">$' + parseFloat(item.value).toFixed(2) + ' + Tax<br><span class="price-each">/ EACH</span></div>';
                markup.col4 = '<label for="' + name + '"><h5>Quantity:</h5></label>';
                markup.col4 += '<div class="select-wrapper box-qty"><select key="' + item.key + '" name="' + name + '-quantity" id="'+ name + '-quantity">';
                markup.sequence = item.sequence;

                var selected = '';
                var qIncrement = 1;
                if (item.maximum_quantity > 99) {
                    qIncrement = 5;
                }
                for (var q = 0; q <= item.maximum_quantity; q = q + qIncrement) {
                    selected = '';
                    if (q == item.quantity) {
                        selected = ' SELECTED';
                    }
                    markup.col4 += '<option value="' + q + '"' + selected + '>' + q + '</option>';
                }
                markup.col4 += '</select></div>';
            }

            // add to groups or to markup
            if (inGroup) {
                // skip the custom bundle group
                if (item.group != 'custom-bundle') {
                    // initialize object;
                    if (groups[item.group] == null) {
                        groups[item.group] = {};
                    }
                    // initialize array;
                    if (groups[item.group].rows == null) {
                        groups[item.group].rows = Array(markup);
                    }
                    else {
                        groups[item.group].rows.push(markup);
                    }
                }
            }
            else {
                var itemObj = { sequence: item.sequence, markup: ff_functions.formatItem(markup)};
                itemsMarkup.push(itemObj);
            }
        }

        // format groups
        for (var group in groups) {
            var itemSequence = 99;
            var classes = 'row group group-' + group;

            var groupMarkup = '';
            if (groups[group].rows != null) {
                if (!bundle) {
                    groupMarkup += '<div class="col0">' + groups[group].rows[0].groupImage + '</div>';
                    groupMarkup += '<div class="col1"><h3>' + groups[group].rows[0].groupName + '</h3>';
                    groupMarkup += '<div class="product-description-text">' + groups[group].rows[0].groupDescription + '</div></div><div class="option-reveal"><a class="show-options btn btn-info" data-target="group-'+ group+'">View options</a></div>';
                    itemSequence = groups[group].rows[0].sequence;
                }

                for (var i = 0; i < groups[group].rows.length; ++i) {
                    // add the row for each group
                    groupMarkup += ff_functions.formatItem(groups[group].rows[i]);
                }
            }
            var itemObj = { "sequence": itemSequence };
            if (group == 'bundles') {
                itemObj.markup = '<h3 class="step2-label">Choose Your Bundle:</h3><div class="' + classes + '">' + groupMarkup + '</div>';
            }
            else {
                itemObj.markup = '<div class="' + classes + '">' + groupMarkup + '</div>';
            }
            itemsMarkup.push(itemObj);
        }

        // sort the items
        itemsMarkup.sort(function(a, b) {
            if (parseInt(a.sequence) < parseInt(b.sequence)) {
                return -1;
            }
            else {
                return 1;
            }
        });

        // build a string and return
        var stringMarkup = '';
        for (var i in itemsMarkup) {
            stringMarkup += itemsMarkup[i].markup;
        }
        return stringMarkup;
    }

    function load() {
        // set the affiliate name based on the shortcode setting passed to javascript
        if (pageData && pageData.affiliate) {
            $('.affiliateName').html(pageData.affiliate);
        }

        // this call is necessary to make bundles an equal height
        $(document).resize();
    }

    function validate() {
        return;
    }

    // validate that a bundle is selected
    function bundleSelected() {
        if (window.frogboxCart == null) {
            return false;
        }
        if (window.frogboxCart.bundles) {
            for (var i = 0; i < window.frogboxCart.bundles.length; ++i) {
                if (window.frogboxCart.bundles[i].quantity) {
                    return true;
                }
            }
        }
        return false;
    }

    return {
        formatItems: function(items, bundle) {
            return formatItems(items, bundle);
        },
        load: function() {
            return load();
        },
        processBundles: function() {
            return true;
        },
        render: function() {
            var content = '';
            content += markup;
            content += '</div>';
            return content;
        },
        validate: function() {
            return validate();
        },
        validated: function() {
            if (bundleSelected()) {
                return true;
            }
            else {
                $('#bundles-products-wrapper').append('<div class="error" id="bundleError">Please choose a bundle</div>');
                return false;
            }
        }
    };

})();