var Step2 = (function() {

    var id = "step-2-content";
    var markup =
        '<h2 role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample" class="fb-calculator collapsed"><span><i class="fa fa-calculator" aria-hidden="true"></i></span>Frogbox Calculator</h2>'
        + '  <div class="collapse the-calculator" id="collapseExample">'
        + '     <div class="row row-custom-1">'
        + '         <div class="col-sm-6">'
        + '           <div class="the-adults">'
        + '               <label for="adults-moving" style="padding:0 10px 10px 0;font-size:18px;color: #18a8c7;">Number of adults moving?</label>'
        + '               <div class="select-wrapper box-qty" style="display: inline-block;">'
        + '                   <select key="#" name="adults-moving" id="adults-moving" aria-invalid="false" class="valid calc-input">'
        + '                       <option value="1">1</option>'
        + '                       <option value="2">2</option>'
        + '                       <option value="3">3</option>'
        + '                       <option value="4">4</option>'
        + '                       <option value="5">5</option>'
        + '                       <option value="6">6+</option>'
        + '                   </select>'
        + '               </div>'
        + '           </div>'
        + '           <div class="the-kids">'
        + '               <label for="kids-moving" style="padding:0 10px 10px 0;font-size:18px;color: #18a8c7;">Number of kids moving?</label>'
        + '               <div class="select-wrapper box-qty" style="display: inline-block;">'
        + '                  <select key="#" name="kids-moving" id="kids-moving" aria-invalid="false" class="valid calc-input">'
        + '                       <option value="0">0</option>'
        + '                       <option value="1">1</option>'
        + '                       <option value="2">2</option>'
        + '                       <option value="3">3</option>'
        + '                       <option value="4">4</option>'
        + '                       <option value="5">5</option>'
        + '                       <option value="6">6+</option>'
        + '                   </select>'
        + '               </div>'
        + '           </div>'
        + '           <div class="the-home">'
        + '               <label for="home-size" style="padding:0 10px 10px 0;font-size:18px;color: #18a8c7;">Number of bedrooms?</label>'
        + '               <div class="select-wrapper box-qty" style="display: inline-block;">'
        + '                   <select key="#" name="home-size" id="home-size" aria-invalid="false" class="valid calc-input">'
        + '                       <option value="15">0</option>'
        + '                       <option value="25">1</option>'
        + '                       <option value="35">2</option>'
        + '                       <option value="50">3</option>'
        + '                       <option value="60">4</option>'
        + '                       <option value="85">5+</option>'
        + '                   </select>'
        + '               </div>'
        + '           </div>'
        + '         </div>'
        + '           <div class="col-sm-6">'
        + '               <div class="the-storage">'
        + '                   <div style="padding:0 10px 10px 0;font-size:18px;color: #18a8c7;">Do you have a garage or storage unit?</div>'
        + '                   <ul>'
        + '                       <li>'
        + '                           <label class="radio" for="garage-yes">Yes</label><input type="radio" name="garage" id="garage-yes" value="10" class="calc-input">'
        + '                       </li>'
        + '                       <li>'
        + '                           <label class="radio" for="garage-no">No</label><input type="radio" name="garage" id="garage-no" value="0" class="calc-input">'
        + '                       </li>'
        + '                   </ul>'
        + '               </div>'
        + '               <div class="the-collector">'
        + '                   <div style="padding:0 10px 10px 0;font-size:18px;color: #18a8c7;">On a scale of Minimalist to Collector where do you fall?</div>'
        + '                   <ul>'
        + '                       <li>'
        + '                           <input type="radio" name="stuff" id="stuff-min" value="-5" class="calc-input"><label class="radio" for="stuff-min" data-toggle="tooltip" data-placement="bottom" title="Minimalist is my mantra">Minimalist</label>'
        + '                       </li>'
        + '                       <li>'
        + '                           <input type="radio" name="stuff" id="stuff-reg" value="0" class="calc-input"><label class="radio" for="stuff-reg" data-toggle="tooltip" data-placement="bottom" title="I have a regular amount of stuff">Regular</label>'
        + '                       </li>'
        + '                       <li>'
        + '                           <input type="radio" name="stuff" id="stuff-col" value="10" class="calc-input"><label class="radio" for="stuff-col" data-toggle="tooltip" data-placement="bottom" title="I am a collector">Collector</label>'
        + '                       </li>'
        + '                   </ul>'
        + '               </div>'
        + '           </div>'
        // +'           <h4> # of boxes: <span id="boxCount"></span></h4>'
        + '     </div>'
        + ' </div>'
        + '  <div id="bundles-products-wrapper"><h2>Bundles:</h2></div>'
        + '<span id="step-2-footnote">* Charges may apply for non-local delivery</span>'
        + '<hr>'
        + '  <div class="row">'
        + '     <div class="col-sm-5">'
        + '     <div id="step-2-previous" class="step-previous previous btn btn-previous">Previous</div>'
        + '     </div>'
        + '    <div class="col-sm-5 col-sm-offset-2">'
        + '   <button id="step-2-next" class="step-next next btn btn-default">Next</button>'
        + '   </div>'
        + '   </div>'
        + '   </div><input name="bundle" type="hidden" value="calculator">';
    var matchingKey;
    var currentSelection = { // set some defaults for initial slide to display in frogbox breakdown
        sqft: 35,
        adults: 1,
        kids: 0,
        garage: 0,
        type: -5
    };

    function clearBundle() {
        if (window.frogboxCart == null) {
            return;
        }
        if (window.frogboxCart['bundles'] != undefined && window.frogboxCart['bundles'].length > 0) {
            for (var pi = 0; pi < window.frogboxCart['bundles'].length; ++pi) {
                window.frogboxCart['bundles'][pi].quantity = 0;
            }
        }
    }

    function totalBoxes(item) {
        if (item.bundle_items) {
            var boxes = parseInt(item.bundle_items.Frogboxes);
            if (item.bundle_items["Small Frogboxes"] != null) {
                boxes += parseInt(item.bundle_items["Small Frogboxes"]);
            }
        }
        else {
            return item.quantity;
        }
        return boxes;
    }

    function closest(arr, closestTo) {
        // find the largest bundle 
        countArray = [];
        for (var i = 0; i < arr.length; i++) {
            if (arr[i].bundle_items) {
                countArray.push(totalBoxes(arr[i]));
            } else {
                arr.splice(i, 1); // discard custom bundle objects
            }
        }
        var closest = Math.max.apply(null, countArray);

        // find the closest bundle to the calculated boxes needed, rounding up
        for (var i = 0; i < arr.length; i++) {
            if (closestTo > closest) {
                var lastIndex = arr.length - 1;
                return closest = arr[lastIndex].key;
            }
            ;
            // if boxes needed exceeds biggest bundle, just return biggest bundle 
            if (arr[i].bundle_items) {
                var frogboxCount = totalBoxes(arr[i]);
                if (frogboxCount >= closestTo) {
                    closest = arr[i].key;
                    return closest;
                }
            }
        }
    }

    // formats a list of items
    function formatItems(items, bundle) {
        if (items == undefined) {
            return;
        }
        var itemsMarkup = Array();
        var slideNavItemsMarkup = Array();
        var groups = {};

        for (var index = 0; index < items.length; ++index) {
            var item = items[index];
            var name = ff_functions.sanitizeItemName(item.name);
            var markup = {};
            var inGroup = false;

            var nameLabelStr = name;
            var nameLabelMini = "Minimalist";
            var nameLabelReg = "Regular";
            var nameLabelCollect = "Collector";

            if (nameLabelStr.indexOf(nameLabelMini) != -1) {
                var nameLabel = nameLabelMini;
            }
            else if (nameLabelStr.indexOf(nameLabelReg) != -1) {
                var nameLabel = nameLabelReg;
            }
            else if (nameLabelStr.indexOf(nameLabelCollect) != -1) {
                var nameLabel = nameLabelCollect;
            }

            // set up the item so we can format it generically
            if (item.group != null && item.group.indexOf('bundle') > -1 && item.group != 'custom-bundle') {

                // format slide nav bar
                var navMarkup = '<div class="number-box" key="' + item.key + '">' + totalBoxes(item) + '</div>';
                var navItemObj = {sequence: item.sequence, markup: navMarkup};
                slideNavItemsMarkup.push(navItemObj);

                // the layout for regular bundles
                inGroup = true;
                var bundleChecked = '';
                if (ff_functions.getQuantity(item.key)) {
                    bundleChecked = 'CHECKED';
                }

                if (item.description_details != null) {

                    // format slide items
                    var descriptionMarkup = '';
                    for (var d in item.description_details) {
                        if (item.description_details[d].title !== '') {
                            descriptionMarkup += item.description_details[d].title + '<br>';
                        }
                    }

                    var slideItemMarkup = formatSlideItemMarkup(item, bundleChecked, descriptionMarkup);

                    var slideItemObj = {sequence: item.sequence, markup: slideItemMarkup};
                    itemsMarkup.push(slideItemObj);
                }
            }
            else {
                var itemClass = '';
                markup.col1 = '';
                if (item.group == 'custom-bundle' || item.group == null) {
                    if (item.group == 'custom-bundle') {
                        inGroup = true;
                    }
                    markup.image = '<img src="' + item.imageLarge + '">';
                    markup.col2 = '<h3 class="product-name" priceItemID=' + item.priceItemID + '>' + item.name + '</h3>' + '<p class="product-description-text">' + item.description + '</p>';
                    registerCustomBundleOption(item);
                }
                else {
                    inGroup = true;
                    markup.groupName = item.name;
                    markup.groupImage = '<img src="' + item.imageLarge + '">';
                    markup.groupDescription = item.description;
                    if (item.title_display_option != null) {
                        markup.col2 = '<h4>' + item.title_display_option + '</h4>';
                    }
                }
                markup.col3 = '<h5>Price:</h5><div class="product-price">$' + parseFloat(item.value).toFixed(2) + ' + Tax<br><span class="price-each">/ EACH</span></div>';
                markup.col4 = '<label for="' + name + '"><h5>Quantity:</h5></label>';

                markup.col4 += '<div class="select-wrapper box-qty"><select class="custom-bundle-boxes" key="' + item.key + '" name="' + name + '-quantity" id="' + getItemId(item) + '">';
                markup.sequence = item.sequence;

                // 0 boxes
                var selected = '';
                if (item.quantity == 0) {
                    selected = ' SELECTED';
                }
                markup.col4 += '<option value="0"' + selected + '>' + 0 + '</option>';

                // 100 - 200 boxes
                var selected = '';
                for (var q = 100; q <= 200; q = q + 5) {
                    selected = '';
                    if (q == item.quantity) {
                        selected = ' SELECTED';
                    }
                    markup.col4 += '<option value="' + q + '"' + selected + '>' + q + '</option>';
                }
                markup.col4 += '</select></div>';
            }

            if (item.group == 'custom-bundle') {

                if (groups[item.group] == null) {
                    groups[item.group] = {};
                }
                // initialize array;
                if (groups[item.group].rows == null) {
                    groups[item.group].rows = Array(markup);
                }
                else {
                    groups[item.group].rows.push(markup);
                }
            }
        }

        // format groups
        var activeBundle = jQuery("input:radio[name='bundle']:checked").val() + '-bundle';
        for (var group in groups) {
            var itemSequence = 99;
            var classes = 'the-breakdown row group group-' + group;
            if (bundle && activeBundle != group) {
                classes += ' group-hidden';
            }
            var groupMarkup = '';
            if (groups[group].rows != null) {

                for (var i = 0; i < groups[group].rows.length; ++i) {
                    // add the row for each group
                    groupMarkup += ff_functions.formatItem(groups[group].rows[i], 'col-sm-4');
                }

                // add the image from the first item in the group
                groupMarkup += '<div class="col-sm-4 custom-bundle-image">' + groups[group].rows[0].image + '</div>';

            }

            var customButton = '<label class="bundle-choice-label" key="custom-bundle">';
            customButton += '<input type="radio" id="custom-bundle" name="bundle-choice" value="custom-bundle" key="custom-bundle">';
            var customButtonClose = '</label>';

            var itemObj = {
                sequence: itemSequence,
                markup: '<div class="' + classes + '" key="custom-bundle">' + customButton + '<h3 class="step2-label">Customize your order:</h3><div class="row">' + groupMarkup + '</div>' + customButtonClose + '</div>'
            };
            itemsMarkup.push(itemObj);

            // format slide nav bar
            var navMarkup = '<div class="number-box" key="custom-bundle">100+</div>';
            var navItemObj = {sequence: item.sequence, markup: navMarkup};
            slideNavItemsMarkup.push(navItemObj);
        }

        // sort the items
        itemsMarkup.sort(function (a, b) {
            if (parseInt(a.sequence) < parseInt(b.sequence)) {
                return -1;
            }
            else {
                return 1;
            }
        });

        // build a string and return
        var slideNavMarkup =
            '<div class="row row-standard">'
            + ' <div class="col-sm-12">'
            + '     <div class="slider-package">';
        for (var i in slideNavItemsMarkup) {
            slideNavMarkup += slideNavItemsMarkup[i].markup;
        }
        slideNavMarkup +=
            ' </div></div></div>';

        var stringMarkup = '';
        stringMarkup += slideNavMarkup;
        stringMarkup +=
            '<div class="row row-standard">'
            + '   <div class="col-sm-12 slider-breakdown">';
        for (var i in itemsMarkup) {
            stringMarkup += itemsMarkup[i].markup;
        }
        stringMarkup += '</div>';
        return stringMarkup;
    }


    /**
     * Create a an ID for a product to access the quantity in the DOM.
     *
     * @param item
     * @returns {string}
     */
    function getItemId(item) {
        return item.name.replace(/ /g, '-') + '-quantity';
    }

    function formatSlideItemMarkup(item, bundleChecked, descriptionMarkup) {
        return slideItemMarkup =
            '   <div class="the-breakdown" key="' + item.key + '"><label class="bundle-choice-label" key="' + item.key + '">'
            + '     <input type="radio" id="' + item.key + '" name="bundle-choice" value="' + item.priceItemID + '"'
            + '     key="' + item.key + '" ' + 'id="' + name + '"' + bundleChecked + '>'
            + '     <div class="row">'
            + '         <div class="col-sm-8"><img src="' + item.imageLarge + '" alt>'
            + '         </div>'
            + '         <div class="col-sm-4">'
            + '             <div class="the-selections">' + descriptionMarkup
            + '             </div>'
            + '             <div class="the-price"><h2 class="price">Price: <br>$' + parseFloat(item.value).toFixed(2) + ' + Tax</h2>'
            + '             </div>'
            + '             <div class="the-dimensions">Frogboxes:<br>24\" X 20\" X 12\" 70 litres<br>(2.4 Cubic Feet)<br><span class="label-thom">SM</span>Small Frogboxes:<br>21\" x 15\" x 9\" 37 litres<br>(1.3 Cubic Feet)'
            + '             </div>'
            + '         </div>'
            + '     </div>'
            + ' </label></div>';
        return slideItemMarkup;
    }

    /* Stores info about custom bundle options so data about this selection will persist */
    function registerCustomBundleOption(item) {
        if (undefined === window.customBundleOptions) {
            clearCustomBundleOptions();
        }
        var itemId = getItemId(item);
        window.customBundleOptions[itemId] = item.quantity;
    }

    function clearCustomBundleOptions() {
        window.customBundleOptions = {};
    }

    function setCustomBundleQuantity(id, quantity) {
        if (undefined === window.customBundleOptions) {
            clearCustomBundleOptions();
            return false;
        }
        if (undefined === window.customBundleOptions[id]) {
            return false;
        }
        window.customBundleOptions[id] = quantity;

    }

    function loadCustomBundleQuanities() {
        if (undefined === window.customBundleOptions) {
            clearCustomBundleOptions();
            return false;
        }
        for (var key in window.customBundleOptions) {
            if (window.customBundleOptions.hasOwnProperty(key)) {
                for (var i = 0; i < frogboxCart.bundles.length; ++i) {
                    var id = getItemId(frogboxCart.bundles[i]);
                    if (id === key) {
                        var boxCount = window.customBundleOptions[key];
                        if (frogboxCart.bundles[i].quantity != boxCount) {
                            // set the quantity in the cart
                            // frogboxCart.bundles[i].quantity = boxCount; // might not need this line - check after resolving infinite loop
                            // also update select to match
                            $('#' + id).val(boxCount).change();
                        }
                    }
                }
            }
        }
    }

    /**
     * Don't call ff_functions.updateItems() because that would be an infinite loop.
     */
    function clearCustomBundlesFromCart() {
        if (undefined === window.customBundleOptions) {
            clearCustomBundleOptions();
            return false;
        }
        for (var key in window.customBundleOptions) {
            if (window.customBundleOptions.hasOwnProperty(key)) {
                for (var i = 0; i < frogboxCart.bundles.length; ++i) {
                    var id = getItemId(frogboxCart.bundles[i]);
                    if (id === key) {
                        if (frogboxCart.bundles[i].quantity != 0) {
                            // frogboxCart.bundles[i].quantity = 0; // might not need this line - check after resolving infinite loop
                            $('.custom-bundle-boxes').val(0).change();
                        }
                    }
                }
            }
        }
    }

    /**
     * Don't call ff_functions.updateItems() because that would be an infinite loop.
     */
    function bindCustomBundleBoxes() {
        // update the custom bundle model when the selects change
        $('.custom-bundle-boxes').on('change', function (e) {
            var id = $(this).attr('id');
            var value = $(this).val();
            // if a human changed the select value, update the model
            if (e.originalEvent !== undefined) {
                setCustomBundleQuantity(id, value);
            }
            ff_functions.setQuantity(jQuery(this).attr('key'), jQuery(this).val());
            ff_functions.generateSummary();
        });

        // when a bundle is selected, clear out the quantities for individual box items
        $('input[name="bundle-choice"]').change(function () {
            clearCustomBundlesFromCart();
        });

        // when the custom bundle slide is selected, remove other bundles and load in the correct quantities
        $('#custom-bundle').change(function () {
            clearBundle();
            loadCustomBundleQuanities();
        });
    }


    function updateBoxes() {

        var storage;
        var size;
        var collectorType;

        var adults = $('#adults-moving').val() * 7;
        var children = $('#kids-moving').val() * 5;
        var sqft = $('#home-size').val() || 25;
        var garage = $('input[name="garage"]:checked').val() || 0;
        var type = $('input[name="stuff"]:checked').val() || -5;

        var boxes = getVal(adults) + getVal(children) + getVal(sqft) + getVal(garage) + getVal(type);
        $('#boxCount').text(boxes);
        matchingKey = closest(window.frogboxCart.bundles, boxes);

        // update currentSelection object so we can use these values in the slider
        currentSelection.adults = adults / 7;
        currentSelection.kids = children / 5;
        currentSelection.sqft = getVal(sqft);
        currentSelection.garage = getVal(garage);
        currentSelection.type = getVal(type);

        updateSelections();
        $('.the-selection-details').html(size + ' Square Feet <br>' + currentSelection.kids + ' Children <br>'
            + currentSelection.adults + ' Adults <br>Garage/Storage: ' + storage + '<br>' + collectorType);
        activeSlide(matchingKey);
    }

    // update values in currentSelection and re-render frogbox breakdown upon input change
    function updateSelections() {

        storage = currentSelection.garage === 10 ? 'Yes' : 'No';

        if (currentSelection.sqft === 25) {
            size = "500-1000";
        } else if (currentSelection.sqft === 35) {
            size = "1000-1500";
        } else if (currentSelection.sqft === 50) {
            size = "2000-3000";
        } else if (currentSelection.sqft === 60) {
            size = "4000-5000";
        } else if (currentSelection.sqft === 85) {
            size = "6000+";
        }

    }

    // update active slides based on calculator input values
    function activeSlide(key) {
        var newInitialSlide;
        $('.slick-slide').not('.slick-initialized').each(function (index) {
            var thisKey = $(this).attr('key');
            if (thisKey === key && !($(this).hasClass('slick-cloned'))) {
                newInitialSlide = $(this).attr('data-slick-index');
                $('.slider-package').slick('slickGoTo', newInitialSlide, false);
                $('.slider-breakdown').slick('slickGoTo', newInitialSlide, false);
            }
        });
    }

    function getVal(val) {
        if (val === undefined) {
            return 0;
        }
        else {
            return parseInt(val);
        }
    }

    function initializeActiveSlide() {
        if (undefined === window.activeSlide) {
            window.activeSlide = null;
        }
    }

    function setActiveSlide(value) {
        window.activeSlide = value;
    }

    function getActiveSlide() {
        initializeActiveSlide();
        return window.activeSlide;
    }

    // automatically select the checkbox for the active slide
    function checkActiveSlide() {
        var currentKey = getActiveSlide();
        if (currentKey === undefined || currentKey === null) {
            currentKey = getSelectedBundleKey();
        }
        console.log('currentKey');
        console.log(currentKey);
        var currentInput = $('#' + currentKey)[0];
        $(currentInput).prop('checked', true).change();
    }


    function load() {
        // wait to initialize the slider until we have the bundles loaded
        var checkExist = setInterval(function() {
            if ($('.slider-package').length && $('.slider-breakdown').length) {
                window.setTimeout(function() {

                    // update the amount of boxes needed based on calculator input
                    $('.calc-input').change(function(e) {
                        updateBoxes();
                    });

                    bindSlider();

                    clearInterval(checkExist);
                }, 500);
            }
        }, 100); // check every 100ms
    }

    function getSelectedBundleKey() {
        if (frogboxCart === undefined || frogboxCart.bundles === undefined) {
            return undefined;
        }

        for (var i = 0; i < frogboxCart.bundles.length; ++i) {
            if (frogboxCart.bundles[i].quantity >= 0) {
                return frogboxCart.bundles[i].key;
            }
        }
        return undefined;
    }

    function bindSlider() {

        $('#accordion').on('accordionactivate', function(event, ui) {
            // slick slider settings
            $('.slider-package').not('.slick-initialized').slick({
                dots: false,
                arrows: true,
                infinite: true,
                speed: 300,
                focusOnSelect: true,
                centerMode: true,
                slidesToShow: 7,
                slidesToScroll: 1,
                asNavFor: '.slider-breakdown',
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 7,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 4,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 4,
                            slidesToScroll: 1
                        }
                    }
                ]
            });

            $('.slider-breakdown').not('.slick-initialized').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                asNavFor: '.slider-package'
            });

            // select active bundle on init
            $('.slider-breakdown').on('init', function(){
                checkActiveSlide();
            });

            // select active bundle when slide changes
            $('.slider-breakdown').on('afterChange', function(){
                var currentKey = $('.slick-active.the-breakdown').attr('key');
                if (undefined !== currentKey) {
                    setActiveSlide(currentKey);
                }
                checkActiveSlide();
            });
        });

    }

    function getIndexOfBundleByKey(key) {
        var slides = $('.the-breakdown');
        for (var i = 0; i <  slides.length; ++i) {
            if (slides[i].getAttribute('key') === key) {
                return i;
            }
        }
        return undefined;
    }

    function processBundles() {
        bindSlider();
        checkActiveSlide();
        var index = getIndexOfBundleByKey(getSelectedBundleKey());
        bindCustomBundleBoxes();
    }

    function validate() {
        return;
    }

    return {
        formatItems: function(items, bundle) {
          return formatItems(items, bundle);
        },
        load: function() {
            return load();
        },
        processBundles: function() {
            processBundles();
        },
        render: function() {
            var content = '<div id="' + id + '" class="step-content">';
            content += markup;
            content += '</div>';
            return content;
        },
        validate: function() {
            return validate();
        },
        validated: function() {
            return true;
        }
    };

})();
