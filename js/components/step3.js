var Step3 = (function() {


    var id="step-3-content";
    var markup='<p>Optionally you can add packing supplies and other products that will be useful during your move.</p>'
        +'<div id="packing-supplies-wrapper">'
        +'</div>'
        +'<hr>'
        +'<div class="row">'
        +' <div class="col-sm-5">'
        +' <div id="step-3-previous" class="step-previous previous btn btn-previous">Previous</div>'
        +'</div>'
        +'<div class="col-sm-5 col-sm-offset-2">'
        +'<button id="step-3-next" class="step-next next btn btn-default">Next</button>'
        +'</div> '
        +'</div> ';

    // formats a list of items
    function formatItems(items, bundle) {
        if (items == undefined) {
            return;
        }
        var itemsMarkup = Array();
        var groups = {};
        for (var index = 0; index < items.length; ++index) {
            var item = items[index];
            var name = ff_functions.sanitizeItemName(item.name);
            var markup = {};
            var inGroup = false;

            //SF: finding value in string.
            var nameLabelStr = name;
            var nameLabelMini = "Minimalist";
            var nameLabelReg = "Regular";
            var nameLabelCollect = "Collector";

            if(nameLabelStr.indexOf(nameLabelMini) != -1){
                var nameLabel = nameLabelMini;
            }
            else if(nameLabelStr.indexOf(nameLabelReg) != -1){
                var nameLabel = nameLabelReg;
            }
            else if(nameLabelStr.indexOf(nameLabelCollect) != -1){
                var nameLabel = nameLabelCollect;
            }

            // set up the item so we can format it generically
            if (item.group != null && item.group.indexOf('bundle') > -1 && item.group != 'custom-bundle') {
                // the layout for regular bundles
                inGroup = true;
                var bundleChecked = '';
                if (ff_functions.getQuantity(item.key)) {
                    bundleChecked = 'CHECKED';
                }
                markup.wrapper = '<label for="' + name + '"><h4>' + nameLabel + ' </h4>';
                markup.col1 = '<input type="radio" name="bundle-choice" value="' + item.priceItemID + '"'
                    + ' key="' + item.key + '" ' + 'id="' + name + '"' + bundleChecked + '>';
                if (item.description_details != null) {
                    markup.col2 = '';
                    for (var i = 0; i < item.description_details.length; ++i) {
                        var closeDiv = false;
                        var closeP = false;
                        if (item.description_details[i].title && item.description_details[i].title !== undefined) {
                            markup.col2 += '<div class="product-details"><h4>' + item.description_details[i].title + '</h4>';
                            closeDiv = true;
                        }
                        if (item.description_details[i].dimensions && item.description_details[i].dimensions !== undefined) {
                            markup.col2 += '<p>' + item.description_details[i].dimensions;
                            closeP = true;
                        }
                        if (item.description_details[i].title && item.description_details[i].description !== undefined) {
                            markup.col2 += '<br>' + item.description_details[i].description;
                        }
                        if (closeP) {
                            markup.col2 += '</p>';
                        }
                        if (closeDiv) {
                            markup.col2 += '</div>';
                        }
                    }
                }
                markup.col3 = '<h5>Price:</h5><div class="product-price">$' + parseInt(item.value, 10).toFixed(2) + ' + Tax</div>';
                markup.col4 = '<span class="product-value-message">' + item.value_message + '</span>';
                markup.wrapperClose = '</label>';
            }
            else {
                var itemClass = '';
                if (item.group == 'custom-bundle' || item.group == null) {
                    // layout for custom bundles and individual products
                    if (item.group == 'custom-bundle') {
                        inGroup = true;
                    }
                    markup.col1 = '<img src="' + item.image + '">';
                    markup.col2 = '<h3 class="product-name" priceItemID=' + item.priceItemID + '>' + item.name + '</h3>' + '<p class="product-description-text">' + item.description + '</p>';
                }
                else {
                    inGroup = true;
                    markup.groupName = item.name;
                    markup.groupImage = '<img src="' + item.image + '">';
                    markup.groupDescription = item.description;
                    markup.col1 = '';
                    if (item.title_display_option != null) {
                        markup.col2 = '<h4>' + item.title_display_option + '</h4>';
                    }
                }
                markup.col3 = '<h5>Price:</h5><div class="product-price">$' + parseFloat(item.value).toFixed(2) + ' + Tax<br><span class="price-each">/ EACH</span></div>';
                markup.col4 = '<label for="' + name + '"><h5>Quantity:</h5></label>';
                markup.col4 += '<div class="select-wrapper box-qty"><select key="' + item.key + '" name="' + name + '-quantity" id="'+ name + '-quantity">';
                markup.sequence = item.sequence;

                var selected = '';
                var qIncrement = 1;
                if (item.maximum_quantity > 99) {
                    qIncrement = 5;
                }
                for (var q = 0; q <= item.maximum_quantity; q = q + qIncrement) {
                    selected = '';
                    if (q == item.quantity) {
                        selected = ' SELECTED';
                    }
                    markup.col4 += '<option value="' + q + '"' + selected + '>' + q + '</option>';
                }
                markup.col4 += '</select></div>';
            }

            // add to groups or to markup
            if (inGroup) {
                // initialize object;
                if (groups[item.group] == null) {
                    groups[item.group] = {};
                }
                // initialize array;
                if (groups[item.group].rows == null) {
                    groups[item.group].rows = Array(markup);
                }
                else {
                    groups[item.group].rows.push(markup);
                }
            }
            else {
                var itemObj = { sequence: item.sequence, markup: ff_functions.formatItem(markup)};
                itemsMarkup.push(itemObj);
            }
        }

        // format groups
        var activeBundle = jQuery("input:radio[name='bundle']:checked").val() + '-bundle';
        for (var group in groups) {
            var itemSequence = 99;
            var classes = 'row group group-' + group;
            if (bundle && activeBundle != group) {
                classes += ' group-hidden';
            }
            var groupMarkup = '';
            if (groups[group].rows != null) {
                if (!bundle) {
                    groupMarkup += '<div class="col0">' + groups[group].rows[0].groupImage + '</div>';
                    groupMarkup += '<div class="col1"><h3>' + groups[group].rows[0].groupName + '</h3>';
                    groupMarkup += '<div class="product-description-text">' + groups[group].rows[0].groupDescription + '</div></div><div class="option-reveal"><a class="show-options btn btn-info" data-target="group-'+ group+'">View options</a></div>';
                    itemSequence = groups[group].rows[0].sequence;
                }

                for (var i = 0; i < groups[group].rows.length; ++i) {
                    // add the row for each group
                    groupMarkup += ff_functions.formatItem(groups[group].rows[i]);
                }
            }
            var itemObj = { sequence: itemSequence, markup: '<div class="' + classes + '"><h3 class="step2-label">Number of boxes:</h3>' + groupMarkup + '</div>'};
            itemsMarkup.push(itemObj);
        }

        // sort the items
        itemsMarkup.sort(function(a, b) {
            if (parseInt(a.sequence) < parseInt(b.sequence)) {
                return -1;
            }
            else {
                return 1;
            }
        });

        // build a string and return
        var stringMarkup = '';
        for (var i in itemsMarkup) {
            stringMarkup += itemsMarkup[i].markup;
        }
        return stringMarkup;
    }



    function load() {
        return;
    }

    function validate() {
        return;
    }

    return {
        formatItems: function(items, bundle) {
          return formatItems(items, bundle);
        },
        load: function() {
            return load();
        },
        render: function() {
            var content = '<div id="' + id + '" class="step-content">';
            content += markup;
            content += '</div>';
            return content;
        },
        validate: function() {
            return validate();
        }
    };

})();
