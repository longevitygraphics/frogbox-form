// if we detected a franchise
if (geoFranchise != null && geoFranchise.franchise != 0) {
    // see if geoFranchise.postCode is an option, set it if it is
    if ($("#delivery-city option[zip='" + geoFranchise.postCode + "']").length > 0) {
        $("#delivery-city option[zip='" + geoFranchise.postCode + "']").attr('selected', 'selected').change();
    }
    else {
        // if not, add it and select it
        $('#delivery-city').append($("<option></option").attr("value", geoFranchise.franchise).attr("zip", geoFranchise.postCode).text(geoFranchise.place).attr('selected', 'selected')).change();
    }
}

