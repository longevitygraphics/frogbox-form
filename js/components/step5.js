var Step5 = (function() {

    var id="step-5-content";

    var markup =
        '<div id="contact-details" class="row">'
        + '<h4>Contact Details</h4>'
        + '<div class="col-sm-6">'
        + '<label for="contact-first">First Name:</label>'
        + '<input type="text" id="contact-first" name="contact-first" required>'
        + '</div>'
        + '<div class="col-sm-6">'
        + '<label for="contact-last">Last Name:</label>'
        + '<input type="text" id="contact-last" name="contact-last" required>'
        + '</div>'
        + '<div class="col-sm-6">'
        + '<label for="contact-primary-phone">Primary Phone:</label>'
        + '<input type="text" id="contact-primary-phone" name="contact-primary-phone" required>'
        + '</div>'
        + '<div class="col-sm-6">'
        + '<label for="contact-primary-ext">ext: <span class="field-optional">(optional)</span></label>'
        + '<input type="text" id="contact-primary-ext" name="contact-primary-ext">'
        + '</div>'
        + '<div class="col-sm-6">'
        + '<label for="contact-alternate-phone">Alternate Phone: <span class="field-optional">(optional)</span></label>'
        + '<input type="text" id="contact-alternate-phone" name="contact-alternate-phone">'
        + '</div>'
        + '<div class="col-sm-6">'
        + '<label for="contact-primary-ext">ext: <span class="field-optional">(optional)</span></label>'
        + '<input type="text" id="contact-alternate-ext" name="contact-alternate-ext">'
        + '</div>'
        + '<div class="col-sm-6">'
        + '<label for="contact-email">Email:</label>'
        + '<input type="email" id="contact-email" name="contact-email" required>'
        + '</div>'
        + '</div>'

        + '<div class="row">'
        + '<div id="delivery-contact-wrapper" class="step-5-contact col-sm-6">'
        + '<h4>Delivery Contact</h4>'
        + '<input type="radio" checked name="delivery-contact" value="same as main contact" id="delivery-contact-main">'
        + '<label for="delivery-contact-main">Same as Main Contact</label>'
        + '<input type="radio" name="delivery-contact" value="different delivery contact" id="delivery-contact-different">'
        + '<label for="delivery-contact-different">Different delivery contact</label>'
        + '<div id="delivery-different-contact-wrapper" class="group-hidden">'
        + '<label for="delivery-contact-first">First Name:</label>'
        + '<input type="text" name="delivery-contact-first" id="delivery-contact-first" >'
        + '<label for="delivery-contact-last">Last Name:</label>'
        + '<input type="text" name="delivery-contact-last" id="delivery-contact-last" >'
        + '</div>'
        + '</div>'
        + '<div id="pickup-contact-wrapper" class="step-5-contact col-sm-6">'
        + '<h4>Pick-up Contact</h4>'
        + '<input type="radio" checked name="pickup-contact" value="same as main contact" id="pickup-contact-main">'
        + '<label for="pickup-contact-main">Same as Main Contact</label>'
        + '<input type="radio" name="pickup-contact" value="different pickup contact" id="pickup-contact-different">'
        + '<label for="pickup-contact-different">Different pick-up contact</label>'
        + '<div id="pickup-different-contact-wrapper" class="group-hidden">'
        + '<label for="pickup-contact-first">First Name:</label>'
        + '<input type="text" name="pickup-contact-first" id="pickup-contact-first">'
        + '<label for="pickup-contact-last">Last Name:</label>'
        + '<input type="text" name="pickup-contact-last" id="pickup-contact-last">'
        + '</div>'
        + '</div>'
        + '</div>'
        + '<div class="row">'
        + '<div class="col-md-6">'
        + '<div id="promo-code-wrapper">'
        + '<h4>Promo Code</h4>'
        + '<label for="promo-code">Promotional Code #</label>'
        + '<input type="text" id="promo-code" name="promo-code">'
        + '</div>'
        + '</div>'
        + '</div>'
        + '<div id="payment-wrapper">'
        + '<h3 class="pl-15">Payment</h3>'
        + '<div class="row">'
            + '<div class="col-md-6">'
                + '<h4 id="billing-card-label">Card details</h4>'
                + '<!-- A Stripe Element will be inserted here. --><div id="card-element"></div>'
                + '<!-- Used to display Element errors. --><div class="error" id="card-errors" role="alert"></div>'
            + '</div>'
        + '</div>'
        + '<div class="row">'
        + '<div id="delivery-contact-wrapper" class="step-5-contact col-sm-6">'
        + '<h4>Billing Details</h4>'
        + '<input type="radio" checked name="billing-details" value="same" id="same-billing-details">'
        + '<label for="same-billing-details">Same as Delivery details</label>'
        + '<input type="radio" name="billing-details" value="different" id="different-billing-details">'
        + '<label for="different-billing-details">Different Billing details</label>'
        + '</div>'
        + '</div>'

        + '<div id="stripe-payment-wrap">'
            + '<div class="billing-form" style="display: none">'
                + '<div class="row">'
                    + '<div class="col-md-6">'
                        + '<label for="billingName">Name on card:</label>'
                        + '<input type="input" id="billingName" name="billingName">'
                    + '</div>'
                    + '<div class="col-md-6">'
                        + '<label for="billingAddress">Address:</label>'
                        + '<input type="input" id="billingAddress" name="billingAddress">'
                    + '</div>'
                + '</div>'
                + '<div class="row">'
                    + '<div class="col-md-6">'
                        + '<label for="billingCity">City:</label>'
                        + '<input type="input" id="billingCity" name="billingCity">'
                    + '</div>'
                    + '<div class="col-md-6">'
                        + '<label for="billingCountry" id="billing-country-label">Country:</label>'
                        + '<div class="select-wrapper">'
                            + '<select id="billingCountry" name="billingCountry">'
                                +'<option value="Canada">Canada</option>'
                                +'<option value="United States">United States</option>'
                            + '</select>'
                        + '</div>'
                    + '</div>'
                + '<div class="row">'
                    + '<div class="col-md-6">'
                        + '<label for="billingProvince" id="billing-province-label">Province/State</label>'
                        + '<div class="select-wrapper">'
                            + '<select class="province" id="billingProvince" name="billingProvince"></select>'
                        + '</div>'
                    + '</div>'
                    + '<div class="col-md-6">'
                        + '<label for="billingPostcode" id="billing-postcode-label">Zip/Postal code</label>'
                        + '<input type="input" id="billingPostcode" class="postcode" name="billingPostcode">'
                    + '</div>'
                + '</div>'
            + '</div>'
        + '</div>'
        + '<div class="tip-row">'
            + '<h4 class="pl-15">Tip</h4>'
            + '<div class="row">'
                + '<div class="col-sm-6 col-md-3">'
                    + '<input type="radio" name="tip" value="0" id="no-tip" checked>'
                    + '<label for="no-tip">No tip</label>'
                    + '<input type="radio" name="tip" value="2.5" id="tip-2.5">'
                    + '<label for="tip-2.5">2.5%</label>'
                + '</div>'
                + '<div class="col-sm-6 col-md-3">'
                    + '<input type="radio" name="tip" value="5" id="tip-5">'
                    + '<label for="tip-5">5%</label>'
                    + '<input type="radio" name="tip" value="10" id="tip-10">'
                    + '<label for="tip-10">10%</label>'
                + '</div>'
            + '</div>'
        + '</div>'
    + '</div>'

    + '<p class="offline-payment">We will collect payment at the same time we deliver the Frogboxes.</p>'
    + ' </div>'
    + '   <div id="mover-recommendations-wrapper">'
        + '       <h4>Mover Recommendations</h4>'
    + '   <p>Looking for a mover you can trust? Check this box and we\'ll send you contact info for credible movers in your area'
    + '   <input type="checkbox" value="1" name="mover-recommendations">'
    + '   </p>'
    + '   </div>'
    + '   <div id="confirm-wrapper">'
    + '       <h4>Confirm and Place Order</h4>'
    + '   <p>Please <span class="ordering-emphasis">review your order</span> and contact details to confirm that everything is entered correctly. You can click on any step to edit the information on that step.</p>'
    + '   <p>When you are ready to place your order, click <span class="ordering-emphasis">Place Order</span> below.</p>'
    + '   </div>'
    + '  <div>'
    + ' <button type="button" data-toggle="modal" data-target="#terms-conditions">Show Terms of Service</button>'
    + ' <label for="terms">I agree with the terms of service: </label>'
    + '  <input type="checkbox" value="1" name="terms" id="terms" required>'
    + '  </div>'
    + '  <hr>'
    + ' <div class="row">'
    + '      <div class="col-sm-5">'
    + '      <div id="step-5-previous" class="step-previous previous btn btn-previous">Previous</div>'
    + '      </div>'
    + '      <div class="col-sm-5 col-sm-offset-2">'
    + '<input type="hidden" name="_ff_submit" value="submit">'
        + ' <button id="step-5-next" class="step-next next btn btn-default" value="submit">Place Order</button>'
    + '</div>'
    + '</div>';


    function load() {


        // set the form action if it is specified
        if (pageData && pageData.confirmation) {
            $('#frogbox-form').prop('action', pageData.confirmation);
        }

        // when a user enters a promo code, check for validity against the API
        $('#promo-code').change(function() {
            if (window.frogboxCart == null) {
                window.frogboxCart = {};
            }
            $('#promo-code-error').remove();
            window.frogboxCart.promo = null;
            $('#promo-valid').remove();
            if ($('#promo-code').val().length > 0) {

                //promo discount is allowed only for orders above $110
                if(window.frogboxCart.chargeTotal < 110){
                    var errorElement = '<div class="error" id="promo-code-error"> Sorry, We can\'t apply a promo code to orders under $110, Feel free to add more items to your cart</div>';
                    $(errorElement).appendTo('label[for="promo-code"]');
                    return false;
                }


                // add a throbber
                var throbberID = ff_functions.addThrobber('promo-code');
                var dS = ff_functions.getDeliverySuburb();
                if (dS) {
                    var promoURL = '/ff/promo/' + dS + '/' + $('#promo-code').val();
                    $.ajax({
                        dataType: 'json',
                        url: promoURL,
                        success: function (data, textStatus, jqXHR) {
                            if (data.error !== undefined && data.displayError) {
                                var errorElement = '<div class="error" id="promo-code-error">'
                                    + data.error + '</div>';
                                $(errorElement).appendTo('label[for="promo-code"]');
                            }
                            else {
                                if (data !== undefined) {
                                    $('<span id="promo-valid">' + data.promoCode + ' is good.</span>').insertAfter('#promo-code')
                                    window.frogboxCart.promo = data;
                                    ff_functions.generateSummary();
                                }
                            }
                        },
                        complete: function () {
                            // remove throbber
                            $(throbberID).remove();
                            ff_functions.generateSummary();
                        },
                        error: function ( jqXHR ,  textStatus,  errorThrown) {
                            console.log('error', jqXHR, textStatus, errorThrown);
                        }
                    });
                }
            }
            ff_functions.generateSummary();
        });

        //hide/show billing details form
        $("input[name=billing-details]").change(function () {
            var billingForm = $(".billing-form");
            if($(this).val() === 'same'){
                billingForm.hide();
            }else{
                billingForm.show();
            }
        });

        // hide/show delivery and pickup contact areas
        $('input[name=delivery-contact]').change(function() {
            if ($('input[name=delivery-contact]:checked').val() == 'different delivery contact') {
                $('#delivery-different-contact-wrapper').removeClass('group-hidden');
            }
            else {
                $('#delivery-different-contact-wrapper').addClass('group-hidden');
            }
        });
        $('input[name=pickup-contact]').change(function() {
            if ($('input[name=pickup-contact]:checked').val() == 'different pickup contact') {
                $('#pickup-different-contact-wrapper').removeClass('group-hidden');
            }
            else {
                $('#pickup-different-contact-wrapper').addClass('group-hidden');
            }
        });


        //on tip change
        $("input[name=tip]").change(function(){
            window.frogboxCart.tipPercentage = parseFloat($(this).val());
            ff_functions.generateSummary();
        });



    }

    function setupPaymentMarkup() {
        var paymentWrapper = $("#payment-wrapper");

    }


    function validate() {
        return;
    }

    return {
        load: function() {
            return load();
        },
        render: function() {
            var content = '<div id="' + id + '" class="step-content">';
            content += markup;
            content += '</div>';
            return content;
        },
        validate: function() {
            return validate();
        }
    };

})();
