/* closure for functions so we can access them from other scripts */

var ff_functions = (function() {

    var pickupFranchiseHasChanged = false;
    var deliveryFranchiseHasChanged = false;

    var priceLists = {'delivery': null, 'pickup': null };

    // formats the keep for info to be pretty and consistent
    function formatKeepFor(keepFor) {
        if (!keepFor) {
            keepFor = parseInt(jQuery('#keep-for').val(), 10);
        }
        var weeks = parseInt(keepFor / 7, 10);
        var weekStatement = '';
        if (weeks > 0) {
            weekStatement = weeks + ' week';
        }
        if (weeks > 1) {
            weekStatement += 's';
        }

        var days = Math.round(keepFor % 7);
        var dayStatement = '';
        if (days > 0) {
            dayStatement = days + ' day';
        }
        if (days > 1) {
            dayStatement += 's';
        }
        var separator = '';
        if (weeks > 0 && days > 0) {
            separator = ', ';
        }
        var output = weekStatement + separator + dayStatement;
        return output;
    }

    // gets a property for an item for a pricelist
    function getItemProperty(property, key, priceList) {
        for (var p = 0; p < 2; ++p) {
            var prop = new Array('bundles', 'supplies')[p];
            if (priceList[prop] && Array.isArray(priceList[prop])) {
                for (var po = 0; po < priceList[prop].length; ++po) {
                    if (key === priceList[prop][po].key) {
                        return priceList[prop][po][property];
                    }
                }
            }
        }
        return 0;
    }

    // gets a quantity from the data model for a specific item
    function getQuantity(key) {
        return getItemProperty('quantity', key, window.frogboxCart);
    }

    // gets the value of an item in a given pricelist
    function getValue(key, priceList) {
        return getItemProperty('value', key, priceList);
    }

    // calculates a total for a pricelist based on what is in the frogboxCart
    function calculateTotal(priceList) {
        var theCart = window.frogboxCart;
        if (!priceList) {
            return 0;
        }
        var subtotal = 0;
        for (var prop in {'bundles': '', 'supplies': ''}) {
            if (theCart[prop] !== undefined && theCart[prop].length && theCart[prop].length > 0) {
                for (var pi = 0; pi < theCart[prop].length; ++pi) {
                    if (theCart[prop][pi].quantity > 0) {
                        subtotal += theCart[prop][pi].quantity * getValue(theCart[prop][pi].key, priceList);
                    }
                }
            }
        }
        var tax = 0;
        if (priceList.taxPercent != null) {
            tax = subtotal * priceList.taxPercent / 100;
        }
        var total = subtotal + tax;
        return Number(total).toFixed(2);
    }

    function formatSummary(name, count) {
        var markup = '<div class="product"><span class="product-name">' + name +
            '</span><span class="product-quantity">' + count + '</span></div>';
        return markup;
    }

    function setPending(url) {
        window.pendingRequests[url] = 1;
    }

    function clearPending(url) {
        window.pendingRequests[url] = 0;
    }

    function isPending(url) {
        return window.pendingRequests[url] === 1;
    }

    function getServiceCharge(zip) {
        var url = '/ff/serviceCharge/' + zip;

        // if the request is pending, don't request it a second time
        if (isPending(url)) {
            return;
        }

        if (window.serviceCharges === undefined) {
            window.serviceCharges = {};
        }
        else if (window.serviceCharges[zip] !== undefined) {
            return window.serviceCharges[zip];
        }

        setPending(url);
        $.ajax({
            dataType: 'json',
            url: url,
            async: true,
            complete: function() {
                clearPending(url);
                generateSummary();
            },
            success: function(data, textStatus, jqXHR) {
                if (data > 0) {
                    window.serviceCharges[zip] = data;
                }
                else {
                    window.serviceCharges[zip] = 0;
                }
            },
            error: function ( jqXHR ,  textStatus,  errorThrown) {
                console.log('error', jqXHR, textStatus, errorThrown);
            }
        });
    }

    // calculate delivery charege
    function calculateDeliveryCharge() {
        var zip = ff_functions.getDeliverySuburb();
        return calculateServiceCharge(zip);
    }

    // calculate pickup charge
    function calculatePickupCharge() {
        var zip = ff_functions.getPickupSuburb();
        return calculateServiceCharge(zip);
    }

    // calculate service charge
    function calculateServiceCharge(zip) {

        if (window.serviceCharges === undefined) {
            window.serviceCharges = {};
        }

        if (window.serviceCharges[zip] === undefined) {
            window.setTimeout(function() {
                getServiceCharge(zip);
            }, Math.floor(Math.random() * 1000) + 1  );
            return 0;
        }

        var charge = window.serviceCharges[zip];
        return +charge;
    }


    // update delivery, pickup and taxes
    function updateAdditionalCharges() {
        if (window.frogboxCart == null) {
            window.frogboxCart = {};
        }

        // read delivery charge and taxes from delivery priceslist
        if (priceLists.delivery != null && priceLists.delivery != undefined) {
            window.frogboxCart.taxPercent = priceLists.delivery.taxPercent;
        }
    }

    // process a pricelist - called from $.ajax:success or statically
    function processPricelist(priceList, delivery) {
        if (delivery === 'IF') {
            setPriceList('interfranchise', priceList);
        }
        else if (delivery) {
            setPriceList('delivery', priceList);
        }
        else if (delivery === false) {
            setPriceList('pickup', priceList);
        }

        if (getPriceList('interfranchise') != null) {
            updateModel(getPriceList('interfranchise'), delivery);
        }
        else if (getPriceList('delivery') != null && getPriceList('pickup') != null)  {
            if (useDeliveryPriceList()) {
                updateModel(getPriceList('delivery'), delivery);
            }
            else {
                updateModel(getPriceList('pickup'), delivery);
            }
        }
        updateItems();
    }

    // returns true if an order is interfranchise
    function orderIsInterfranchise() {
        var dc = $('#delivery-city').val();
        var pc = $('#pickup-city').val();
        return (dc !== pc);
    }

    // updateModel takes a new model object and overwrites the old one
    function updateModel(data, delivery) {
        // if frogboxCart is empty, set its value to the incoming data
        if (window.frogboxCart == null) {
            window.frogboxCart = data;
            window.frogboxCart.groupsInit = 0;
            return;
        }

        // otherwise, go through each item and update the quantity
        for (var p = 0; p < 2; ++p) {
            var prop = new Array('bundles', 'supplies')[p];
            for (var pi = 0; pi < data[prop].length; ++pi) {
                if (data[prop][pi].name) {
                    data[prop][pi].quantity = getQuantity(data[prop][pi].key);
                }
            }
        }
        window.frogboxCart = data;

        generateSummary();
    }

    // extract the move pack details from the dimension fields
    function extractMovePack(details) {
        for (var i = 0; i < details.length; ++i) {
            if (details[i].title === 'Move Pack') {
                var items = details[i].dimensions.split("<br>");
                return details[i].dimensions.split("<br>");
            }
        }
    }

    function getPromoDiscount(subtotal) {
        $('#promoThresholdNotMet').remove();

        // check for valid promo code
        if (window.frogboxCart.promo != null) {

            // check to see that we meet the threshold
            if (!isNaN(window.frogboxCart.promo.promoAmountIfMoreThen)) {
                if (subtotal < window.frogboxCart.promo.promoAmountIfMoreThen) {
                    var error = ' You must purchase at least $' + window.frogboxCart.promo.promoAmountIfMoreThen + ' of goods and services to use this promo code. ';
                    var element = '<span class="error" id="promoThresholdNotMet">' + error + '</span>';

                    if ($('#promo-valid').length) {
                        $(element).insertAfter('#promo-valid');
                    }
                    else {
                        $(element).insertAfter('#promo-code');
                    }
                    return 0;
                }
            }

            if (window.frogboxCart.promo.promoMeasure == 'percent') {
                return Math.ceil(subtotal * window.frogboxCart.promo.promoDiscount) / 100;
            }
            else {
                return window.frogboxCart.promo.promoDiscount;
            }
        }

        return 0;
    }

    // generate a summary of items in the frogbox cart
    function generateSummary() {

        updateAdditionalCharges();
        // if there's a keep-for error, copy it to the sidebar and return
        if (jQuery('#keep-for-error').length) {
            var keepForError = jQuery('#keep-for-error').clone();
            jQuery('#your-price-price').html(keepForError);
            jQuery('#your-order').addClass('group-hidden');
            return;
        }

        if (window.frogboxCart == null) {
            return;
        }

        // override frogboxCart.minimumCharge with window.minimumCharge
        window.frogboxCart.minimumCharge = window.minimumCharge.minimumCharge;

        var subtotal = 0;
        var summary = '';
        var tax = 0;
        var tip = 0;
        var total = 0;
        jQuery('#your-order').removeClass('group-hidden');

        for (var prop in {'bundles': '', 'supplies': ''}) {
            if (window.frogboxCart[prop] != undefined && window.frogboxCart[prop].length > 0) {
                for (var pi = 0; pi < window.frogboxCart[prop].length; ++pi) {
                    if (window.frogboxCart[prop][pi].quantity > 0) {
                        subtotal += window.frogboxCart[prop][pi].quantity * window.frogboxCart[prop][pi].value;
                        if (window.frogboxCart[prop][pi].bundle_items != null) {
                            for (var bundleItem in window.frogboxCart[prop][pi].bundle_items) {
                                if (bundleItem.length > 0 && bundleItem != 'Pack') {
                                    summary += formatSummary(bundleItem, window.frogboxCart[prop][pi].bundle_items[bundleItem]);
                                }
                                else if (bundleItem == 'Pack') {
                                    if (window.frogboxCart[prop][pi].description_details) {
                                        var mpItems = extractMovePack(window.frogboxCart[prop][pi].description_details);
                                        for (var mp = 0; mp < mpItems.length; ++mp) {
                                            var mpd = mpItems[mp].trim().split(/ (.+)/, 2);
                                            summary += formatSummary(mpd[1], mpd[0]);
                                        }
                                    }
                                }
                            }
                        }
                        else {
                            summary += formatSummary(window.frogboxCart[prop][pi].name, window.frogboxCart[prop][pi].quantity);
                        }
                    }
                }
            }
        }

        // if there is a subtotal, calculate delivery and pickup charges
        if (subtotal) {

            var deliveryCharge = calculateDeliveryCharge();
            var pickupCharge = calculatePickupCharge();

            var chargeTotal = subtotal + deliveryCharge + pickupCharge;

            // make sure we clear delivery charge message before creating it
            jQuery('#minimum-charge-wrapper').remove();

            if (chargeTotal < window.frogboxCart.minimumCharge) {
                var minimumCharge = window.frogboxCart.minimumCharge - chargeTotal;
                
                // add required checkbox to step 5 if this a local list that usually doesn't have a delivery charge
                var minimumElement = "<div id='minimum-charge-wrapper'><input type='checkbox' name='minimum-charge' id='minimum-charge' required>"
                    + "<label for='minimum-charge'>I understand that I will be charged a delivery fee to meet the $"
                    + window.minimumCharge.minimumCharge + " minimum charge.</label>"
                    + "<a href='#step-3-header'>Add more products instead</a>"
                    + "</div>";
                jQuery(minimumElement).insertBefore('#payment-wrapper');
                jQuery('#minimum-charge-wrapper a').click(function() {
                    $('#accordion').accordion('option', 'active', 2);
                    Accordion.scrollToStep(3);
                });
                if (minimumCharge > deliveryCharge) {
                    deliveryCharge = deliveryCharge + minimumCharge;
                }
            }

            if (deliveryCharge > 0) {
                subtotal += deliveryCharge;
                summary += formatSummary('Delivery Charge', '$' + deliveryCharge.toFixed(2));
            }

            frogboxCart.deliveryCharge = deliveryCharge;

            if (pickupCharge > 0) {
                subtotal += pickupCharge;
                summary += formatSummary('Pickup Charge', '$' + pickupCharge.toFixed(2));
            }

            frogboxCart.pickupCharge = pickupCharge;
            window.frogboxCart.chargeTotal = subtotal;
        }

        var discount = getPromoDiscount(subtotal);

        var taxable = subtotal - discount;

        tax = Math.ceil(taxable * window.frogboxCart.taxPercent) / 100;
        if (!tax) {
            tax = 0;
        }
        total = tax + taxable;

        if(window.frogboxCart.tipPercentage){
            tip = window.frogboxCart.tipPercentage * taxable / 100;
            total = total + tip;
        }

        window.frogboxCart.grandTotal = total.toFixed(2);
        var pricing = '<div class="order-subtotal"><span class="pricing-description">Subtotal:</span><span class="pricing-price">$'
            + subtotal.toFixed(2) + '</span></div>';

        discount = parseInt(discount);
        if (discount > 0) {
            pricing += '<div class="order-discount"><span class="pricing-description promo">' + window.frogboxCart.promo.promoCode + '</span><span class="pricing-price">-$'
                + discount.toFixed(2) + '</span></div>';
        }
        pricing += '<div class="order-tax"><span class="pricing-description">Tax:</span><span class="pricing-price">$'
            + tax.toFixed(2) + '</span></div>';

        if(tip > 0){
            pricing += '<div class="order-tax"><span class="pricing-description">Tip:</span><span class="pricing-price">$'
              + tip.toFixed(2) + '</span></div>';
        }

        pricing += '<div class="order-total"><span class="pricing-description">Total:</span><span class="pricing-price">$'
            + total.toFixed(2) + '</span></div>';
        jQuery('#your-price-price').html(pricing);
        jQuery('#your-price-products').html(summary);
        jQuery('#mobile-order-price').html('$' + total.toFixed(2));

        jQuery('#your-order-extra').html('');

        // append (change) if your-order is set
        if (jQuery('#your-order').length > 0) {
            jQuery('#your-price-products').append('<div class="change-link">(<a href="#" id="change-order">change</a>)</div>');
            jQuery('#change-order').click(function() {
                jQuery('#accordion').accordion({ active: 1 }); // accordion sections start numbering at 0; switch to #2
            });
        }

        // append delivery & pickup locations and (change)
        var dS = ff_functions.getDeliverySuburb();
        var pS = ff_functions.getPickupSuburb();
        if (dS && pS) {
            jQuery('#your-order-extra').append('<h4>Delivery:</h4><div class="sidebar-detail">' + jQuery('#delivery-city option:selected').text() + '</div><h4>Pick-up:</h4><div class="sidebar-detail">' + jQuery('#pickup-city option:selected').text()  + '</div><div class="change-link">(<a href="#" id="change-locations">change</a>)</div>');
            jQuery('#change-locations').click(function() {
                jQuery('#accordion').accordion({ active: 0 }); // accordion sections start numbering at 0; switch to #1
            });
        }

        if (jQuery('#delivery-date').val() && jQuery('#pickup-date').val()) {
            jQuery('#your-order-extra').append('<h4>Keep for:</h4><div class="sidebar-detail">' + formatKeepFor() + '</div>');
        }

        var deliveryAddress = formatAddress('delivery');
        var pickupAddress = formatAddress('pickup');
        if (deliveryAddress && pickupAddress) {
            jQuery('#your-order-extra').append('<h4>Delivery Address:</h4><div class="sidebar-detail">' + deliveryAddress + '</div><h4>Pick-up Address:</h4><div class="sidebar-details">' + pickupAddress + '</div><div class="change-link">(<a href="#" id="change-address">change</a>)</div>');
            jQuery('#change-address').click(function() {
                jQuery('#accordion').accordion({ active: 3 }); // accordion sections start numbering at 0; switch to #4
            });
        }
        generateYourCurrentOrder();
    }

    // lookup province based on first letter of post code
    function lookupProvince(code) {
        var provinceLookup = {'A':'Newfoundland and Labrador', 'B':'Nova Scotia',
            'C':'Prince Edward Island', 'E':'New Brunswick', 'G':'Quebec',
            'H':'Quebec', 'J':'Quebec', 'K':'Ontario', 'L':'Ontario', 'M':'Ontario',
            'N':'Ontario', 'P':'Ontario', 'R':'Manitoba', 'S':'Saskatchewan',
            'T':'Alberta', 'V':'British Columbia', 'X':'Northwest Territories|Nunavut',
            'Y':'Yukon Territory'};
        if (code) {
            return provinceLookup[code.substring(0, 1).toUpperCase()];
        }
    }

    // lookup state based on first two numbers in zip code
    function lookupState(code) {
        lookup = code.substring(0, 5);
        var stateLookup = {'83':'Idaho', '55':'Minnesota', '56':'Minnesota',
            '98':'Washington', '99':'Washington'};
        var state = stateLookup[lookup.substring(0,2)];
        if (state === 'Washington' && (+lookup > 99500)) {
            state = undefined; // Alaska
        }

        // it is an error if the code is less than 5  letters
        if (code.length === undefined || code.length < 5) {
            state = undefined;
        }
        return state;
    }

    // update a pricelist
    // url - ajax url to get pricelist from
    // throbberID - ID of throbber to remove after ajax is complete
    // errorID - div to write errors in
    // errorParent - the 'for' attribute of the label element to place the error in
    // delivery - boolean, is this the pricelist for delivery or not
    // update - boolean - update the model
    function updatePricelist(url, throbberID, errorID, errorParent, delivery, update, callback) {
        $('#' + errorID).remove();
        if (getPriceList(url) != null) {
            if (update) {
                processPricelist(getPriceList(url), delivery);
            }
            if (callback) {
                callback();
            }
            window.setTimeout(function() {
                // don't remove the throbber immediately - give items time to render
                $(throbberID).remove();
            }, 1000);
            return true;
        }


        // if the request is pending, don't request it a second time
        if (isPending(url)) {
            window.setTimeout(function() {
                updatePricelist(url, throbberID, errorID, errorParent, delivery, update, callback);
            }, 1000);
            return;
        }

        setPending(url);
        $.ajax({
            dataType: 'json',
            url: url,
            async: true,
            success: function(data, textStatus, jqXHR) {
                if (!data || data.error != null && data.displayError) {
                    $('#' + errorID).remove();
                    var errorElement = '<div class="error" id="' + errorID + '">'
                        + data.error + '</div>';
                    if ($('label[for="' + errorParent + '"]').length) {
                        $(errorElement).appendTo('label[for="' + errorParent + '"]');
                    }
                    else {
                        $(errorElement).appendTo('#' + errorParent);
                    }
                    generateSummary();
                }
                else {
                    var newUrl = '';
                    // expect an array of pricelists
                    for (var i = 0; i < data.length; ++i) {
                        // if we are getting 4 priceslists, keep track of them all
                        if (data.length == 4) {
                            newUrl = url.substring(0, url.lastIndexOf('/')+1);
                            url = newUrl + (i+1) * 7;
                            // don't try to update frogboxcart if it is not the 7-day list
                        }
                        if (i > 0) {
                            update = false;
                        }
                        setPriceList(url, data[i]);
                        if (update) {
                            processPricelist(data[i], delivery);
                        }
                    }
                }
            },
            complete: function() {
                clearPending(url);
                if (callback) {
                    callback();
                }
                window.setTimeout(function() {
                    // don't remove the throbber immediately - give items time to render
                    $(throbberID).remove();
                }, 1000);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log('json parse error', jqXHR, textStatus, errorThrown);
            }
        });
    }

    function get4WeekPricingSummary() {
        if (window.frogboxCart.zip == null) {
            return [];
        }

        if (orderIsInterfranchise()) {
            zip = window.frogboxCart.zip;
        }
        else {
            zip = ff_functions.getDeliverySuburb();
        }

        var base = '/ff/pricelist/' + zip + '/';
        var pricing = [];
        for (var days = 7; days <= 28; days=days +7) {
            priceList = base + days;
            var subtotal = calculateTotal(priceLists[priceList]);
            var deliveryCharge = calculateDeliveryCharge();
            var pickupCharge = calculatePickupCharge();
            var total = +subtotal + +deliveryCharge + +pickupCharge;
            pricing.push({'price': total.toFixed(2), 'weeks':formatKeepFor(days)})
        }
        return pricing;
    }

    // updates the 'your current order' section
    function generateYourCurrentOrder() {
        var pricing = get4WeekPricingSummary();
        var markup = '';
        for (var i = 0; i < pricing.length; i++) {
            markup += '<li>$' + pricing[i].price + ' for ' + pricing[i].weeks + '</li>';
        }
        jQuery('#current-order-price-by-duration').html(markup);
    }

    // returns a string with an address formatted as HTML
    function formatAddress(which) {
        var street = jQuery('#' + which + 'Street').val();
        var city = jQuery('#' + which + 'City').val();
        var province = jQuery('#' + which + 'Province').val();
        var postcode = jQuery('#' + which + 'Postcode').val();
        var description = jQuery('#' + which + 'Description').val();
        var address = '';
        if (street) {
            address += street;
        }
        else {
            return '';
        }
        if (city) {
            address += '<br>' + city;
        }
        else {
            return '';
        }
        if (province ) {
            address += ', ' + province;
        }
        if (postcode) {
            address += '<br>' + postcode;
        }
        else {
            return '';
        }
        if (description) {
            address += '<br>' + description;
        }
        return address;
    }

    // format a single item
    function formatItem(markup, itemClass) {
        if (!itemClass) {
            itemClass = 'product';
        }
        // format the columns
        var itemMarkup = '<div class="' + itemClass + '">';
        if (markup.wrapper != null) {
            itemMarkup += markup.wrapper;
        }
        itemMarkup += '<div class="inner-border">';
        itemMarkup += '<div class="col1">';
        if (markup.col1 != null) {
            itemMarkup += markup.col1;
        }
        itemMarkup += '</div><div class="col2">';
        if (markup.col2 != null) {
            itemMarkup += markup.col2;
        }
        itemMarkup += '</div><div class="col3">';
        if (markup.col3 != null) {
            itemMarkup += markup.col3;
        }
        itemMarkup += '</div><div class="col4">';
        if (markup.col4 != null) {
            itemMarkup += markup.col4;
        }
        itemMarkup += '</div></div>'; // close col4 and inner-border
        if (markup.wrapperClose != null) {
            itemMarkup += markup.wrapperClose;
        }
        itemMarkup += '</div>'; // close product
        return itemMarkup;
    }

    // updates the data model with a new quantity for a specific item
    function setQuantity(key, quantity) {
        for (var p = 0; p < 2; ++p) {
            var prop = Array('bundles', 'supplies')[p];
            if (Array.isArray(window.frogboxCart[prop])) {
                for (var pi = 0; pi < window.frogboxCart[prop].length; ++pi) {
                    if (key == window.frogboxCart[prop][pi].key) {
                        window.frogboxCart[prop][pi].quantity = quantity;
                        return true;
                    }
                }
            }
        }
        return false;
    }

    function updateItems() {
        if (window.frogboxCart == null || jQuery.isEmptyObject(window.frogboxCart)) {
            return;
        }

        jQuery('#bundles-products-wrapper').html(Step2.formatItems(window.frogboxCart.bundles, true));
        Step2.processBundles();
        jQuery('#packing-supplies-wrapper').html(Step3.formatItems(window.frogboxCart.supplies, false));
        // adding function for hide and showing group items
        jQuery('#packing-supplies-wrapper .group .product .inner-border').hide();

        jQuery('.show-options').click(function(){
            if (jQuery(this).data('target').length) {
                var target = "." + jQuery(this).data("target") + " .product .inner-border";
                //jQuery('#packing-supplies-wrapper .group .product').not(target).hide();
                if (jQuery(target).length) {
                    jQuery(target).toggle();
                }
            }
        });

        // bind new selects with change event
        jQuery('.product select').change(function() {
            setQuantity(jQuery(this).attr('key'), jQuery(this).val());
            generateSummary();
        });

        jQuery("input:radio[name='bundle-choice']").change(function() {
            jQuery("input:radio[name='bundle-choice']").closest('.inner-border').removeClass('active');
            jQuery("input:radio[name='bundle-choice']").each(function() {
                setQuantity(jQuery(this).attr('key'), 0);
            });
            if (jQuery(this).is(':checked')) {
                jQuery(this).closest('.inner-border').addClass('active');
                setQuantity(jQuery(this).attr('key'), 1);
                generateSummary();
            }
        });

        Step1.updateItemsDone();
    }

    function shortenPostCode(zip) {
        // if the first five digits are numeric, use that
        var firstFive = zip.trim().substring(0,5);
        if (!isNaN(firstFive)) {
            return firstFive;
        }
        // use the first three if we have at least three
        if (zip.trim().length >= 3) {
            return zip.trim().substring(0, 3).toUpperCase();
        }
        // otherwise, return false
        return false;
    }

    // validates a suburb/zip and retrieves the franchise for a zip from Vonigo
    function validateSuburbZip(which, zip) {
        zip = shortenPostCode(zip);
        var url = '/ff/zip/' + zip;

        // if the request is pending, don't request it a second time
        if (isPending(url)) {
            window.setTimeout(function() {
                validateSuburbZip(which, zip);
            }, 1000);
            return;
        }

        setPending(url);
        $.ajax({
            dataType: 'json',
            url: url,
            complete: clearPending(url),
            success: function(data, textStatus, jqXHR) {
                var franchiseID = data;
                setSuburb(which, zip, franchiseID, otherZipExists(which));
            },
            error: function() {
                setSuburb(which, zip, 0);
                console.log('json parse error', jqXHR, textStatus, errorThrown);
            }
        });
    }

    // changes mm/dd/yy to yyyymmdd
    function formatDate(date) {
        return date.substring(0,4) + date.substring(5,7) + date.substring(8);
    }

    function setTimeOptions(whichID, options, clear) {
        var whichType = whichID.substring(0, whichID.length -5);

        // check if we've got a time selected
        var chosenTime = $('#' + whichType + '-time option:selected').attr('time');

        if (window.chosenTime === undefined) {
            window.chosenTime = { 'pickup-time': '', 'delivery-time': '' };
        }
        if (chosenTime) {
            window.chosenTime[whichID] = chosenTime;
        }
        else if (window.chosenTime[whichID] !== undefined) {
            chosenTime = window.chosenTime[whichID];
        }

        if (undefined === window.formSubmitting) {
            window.formSubmitting = {
                'pickup-time': false, 'delivery-time': false,
                'delivery-date': false, 'pickup-date': false
            };
        }

        // put the new data in
        $('#' + whichType + '-time').html(options);
        // if we had a time selected, try to select it again
        if (chosenTime) {
            if ($('#' + whichType + '-time option[time="' + chosenTime + '"').length > 0) {
                $('#' + whichType + '-time option[time="' + chosenTime + '"').attr('selected', 'selected').change();
            }
            else if (!window.formSubmitting[whichID]) {
                var message = "Your " + whichType + " time is no longer available. Please choose a new one."
                ff_functions.makeAlert(message, false);
            }
        }

        // empty set handling
        if (!clear && $('#' + whichType + '-time option').length <= 1) {
            var errorElement = '<div class="error" id="' + whichID +'-error">'
                + 'No times are available for this date. Please choose another date.' + '</div>';
            $(errorElement).appendTo('label[for="' + whichID + '"]');
        }
    }

    function clearTimeOptions(whichID) {
        setTimeOptions(whichID, [], true);
    }

    // checks that that a zip code is filled out
    function otherZipExists(which) {
        var other = (which == 'pickup') ? 'deliveryPostcode' : 'pickupPostcode';
        return ($('#' + other).val().length) ? true : false;
    }

    // callback for ajax requests
    function setSuburb(which, zip, newCity, update) {
        // re-validate promo codes if the delivery suburb changed
        if (which == 'delivery') {
            $('#promo-code').change();
        }

        // if valid, maybe make an update
        var errorID = which + 'Postcode';
        if (newCity > 0) {
            // remove any errors
            $('label[for="' + errorID + '"] .error').remove();

            var oldCity = getFranchise(which);

            // update the service times if the city has changed
            if (newCity != oldCity) {
                // note that the franchise has changed
                ff_functions[which + 'FranchiseHasChanged'] = true;

                // update availability
                var dateVal = $('#' + which + '-date').val();
                var date = dateVal.replace(/-/g, '');
                ff_functions.initializeAvailability();
                ff_functions.getAvailability(which, zip, function() {
                    var whichID = which + '-date';
                    if (window.availability[which] && window.availability[which][date]) {
                        setTimeOptions(whichID, window.availability[which][date]);
                    }
                    else {
                        // reload time options if it isn't in the availability array
                        getSingleAvailability(whichID, date);
                    }
                });
            }

            // always update the option with newcity to use the new zip, select it but don't trigger
            $("#" + which + "-city option[value='" + newCity + "']").first().attr('zip', zip).attr('selected', 'selected');
        }
        // if invalid, show an error message
        else {
            var error = "<div class='error'>Currently we don't service this address but please <a href='/contact'>Contact Us</a> if you have any questions.</div>";
            $(error).appendTo('label[for="' + errorID + '"]');
        }

        // update the pricelist if both postcodes are set
        if (update) {
            // trigger .change() on the franchise to update pricelists
            $("#" + which + "-city option[value='" + newCity + "']").first().attr('zip', zip).attr('selected', 'selected').change();

            // trigger the change on the other franchise; use a delay to try to prevent a franchise conflict
            var otherType = (which == 'pickup') ? 'delivery' : 'pickup';
            var otherCity = getFranchise(otherType);
            var otherZip = Step1.getSuburb(otherType);
            var timeout = window.setTimeout(function() {
                $("#" + otherType + "-city option[value='" + otherCity + "']").attr('zip', otherZip).attr('selected', 'selected').change();
            }, 1500);

            // show a message if at least one of the franchises has changed
            if (ff_functions.deliveryFranchiseHasChanged || ff_functions.pickupFranchiseHasChanged) {
                var message = "<p>Oh no! Looks like your postal/zips don't match the location we thought you were in. No problem! Now you will need to re-enter some info because your location has changed.</p>";
                makeAlert(message, true);
                $('#bundles-products-wrapper .row').remove();
                $('#bundles-products-wrapper').append('<div class="throbber-loader" id="step2-throbber">Loading...</div>');
                ff_functions.deliveryFranchiseHasChanged = false;
                ff_functions.pickupFranchiseHasChanged = false;
            }
        }
    }

    function makeAlert(message, jump) {
        if ($('.alert').length > 0) {
            var newMessage = '<div class="alert-body">' + message + '</div>';
            $(newMessage).insertAfter('.alert-body');
        }
        else {
            var alertBox = '<div class="alert-overlay"><div class="alert"><div class="alert-body">' + message + '</div><div class="alert-ok">Ok</div></div></div>';
            $('body').append(alertBox);
            $('.alert-ok').click(function() {
                closeAlert();
                if ($('.bundle-choice').length < 1 && jump) {
                    jQuery('#accordion').accordion({active: 1}); // accordion sections start numbering at 0; switch to #2
                    // scroll to bundles
                    $('html, body').animate({
                        scrollTop: $('#bundles-products-wrapper').offset().top - 40
                    }, 500);
                }
            });
        }
        $('.alert').focus();
    }

    function closeAlert() {
        $('.alert-overlay').remove();
    }

    function getFranchise(which) {
        if (jQuery('#' + which + '-city').val()) {
            return jQuery('#' + which + '-city').val();
        }
        else {
            return false;
        }
    }

    function setFranchise(which, newFranchise) {
        if (newFranchise != null && newFranchise > 0) {
            jQuery('#' + which + '-city').val(newFranchise);
        }
    }

    // add a throbber to a div after a form element
    // @param string elementID - the element id of the element to add the throbber after
    // - do not prefix with '#'.
    function addThrobber(elementID) {
        var throbberID = elementID + '-throbber';
        if (!($('#' + throbberID).length)) {
            var throbberHTML = '<div class="throbber-loader" id="' + throbberID+ '">Loading...</div>';
            $(throbberHTML).insertAfter('#' + elementID);
        }
        return '#' + throbberID;
    }

    // gets availability for times that haven't already loaded
    function getSingleAvailability(whichID, theDate) {
        if (undefined === window.formSubmitting) {
            window.formSubmitting = {
                'pickup-time': false, 'delivery-time': false,
                'delivery-date': false, 'pickup-date': false
            };
        }

        if (ff_functions.validDate(theDate)) {
            var whichType = whichID.substring(0, whichID.length -5);
            $('#' + whichID + '-error').remove();
            var throbberID = ff_functions.addThrobber(whichType + '-time');
            window.formSubmitting[whichID] = true;
            clearTimeOptions(whichID);

            var getSuburb = (whichType == 'delivery') ? ff_functions.getDeliverySuburb() : ff_functions.getPickupSuburb();
            var timesURL = '/ff/times/' + getSuburb + '/' + formatDate(theDate);

            // if the request is pending, don't request it a second time
            if (isPending(timesURL)) {
                window.setTimeout(function() {
                    getSingleAvailability(whichID, theDate);
                }, 1000);
                return;
            }

            setPending(timesURL);
            $.ajax({
                dataType: 'json',
                url: timesURL,
                success: function(data, textStatus, jqXHR) {
                    if (data !== undefined && data.error != null && data.displayError) {
                        var errorElement = '<div class="error" id="' + whichID +'-error">'
                            + data.error + '</div>';
                        $(errorElement).appendTo('label[for="' + whichID + '"]');
                    }
                    else if (data.options !== undefined && data.options.length > 0){
                        window.formSubmitting[whichID] = false;
                        setTimeOptions(whichID, data.options);
                    }
                },
                complete: function(jqXHR, textStatus) {
                    clearPending(timesURL);
                    window.formSubmitting[whichID] = false;
                    $(throbberID).remove();
                },
                error: function ( jqXHR ,  textStatus,  errorThrown) {
                    console.log('error', jqXHR, textStatus, errorThrown);
                }
            });
        }
    }

    function getAvailability(which, zip, successCallback) {
        if (zip) {
            url = '/ff/availability/' + zip;

            // if the request is pending, do not request it a second time
            if (isPending(url)) {
                window.setTimeout(function() {
                    getAvailability(which, zip, successCallback);
                }, 1000);
                return;
            }

            var settings = {delivery: $('#delivery-time').attr('time'), pickup: $('#pickup-time').attr('time')};
            setPending(url);
            $.ajax({
                dataType: 'json',
                url: url,
                complete: clearPending(url),
                async: true,
                success: function (data, textStatus, jqXHR) {
                    var options = data.options;
                    if (which === 'both') {
                        window.availability.delivery = options;
                        window.availability.pickup = options;
                    }
                    else {
                        window.availability[which] = options;
                    }
                    if (successCallback) {
                        successCallback();
                    }
                },
                error: function ( jqXHR ,  textStatus,  errorThrown) {
                    console.log('error', jqXHR, textStatus, errorThrown);
                }
            });
        }
    }

    // initialize availability - franchises should have been chosen at this point
    function initializeAvailability() {
        if (!window.availability) {
            window.availability = {'delivery': {}, 'pickup': {}}
        }
        deliverySuburb = ff_functions.getDeliverySuburb();
        pickupSuburb = ff_functions.getPickupSuburb();

        if (deliverySuburb === pickupSuburb) {
            getAvailability('both', ff_functions.getDeliverySuburb());
        }
        else {
            getAvailability('delivery', ff_functions.getDeliverySuburb());
            getAvailability('pickup', ff_functions.getPickupSuburb());
        }
    }

    function whichCountry(franchise) {
        return (franchise == 16 || franchise == 18 || franchise == 29) ? 'United States' : 'Canada';
    }

    // determine if an element belongs to the 'delivery' or 'pickup' group based on its ID.
    function getWhich(element) {
        return ($(element).attr('id').slice(0, 8) == 'delivery') ? 'delivery' : 'pickup';
    }

    function loadDisabledDates(which, franchise, suburb) {
        // if the request is pending, don't request it a second time
        if (isPending(dURL)) {
            return;
        }

        // update calendar and grey out dates for that franchise
        var country = whichCountry(franchise);
        var validSuburb;
        if (country === 'Canada') {
            validSuburb = lookupProvince(suburb);
        }
        else if (country === 'United States') {
            validSuburb = lookupState(suburb);
        }
        if (validSuburb) {
            var dURL = '/ff/dates/' + suburb;
            var whichID = which + '-city';

            setPending(dURL);
            $.ajax({
                dataType: 'json',
                url: dURL,
                complete: clearPending(dURL),
                success: function (data, textStatus, jqXHR) {
                    if (data.error !== null && data.displayError) {
                        var errorElement = '<div class="error" id="' + whichID + '-error">' + data.error + '</div>';
                        $(errorElement).appendTo('label[for="' + whichID + '"]');
                    }
                    else if (!data.error) {
                        disabledDates[which] = data;
                        ff_functions.initializeDatepicker(which);
                    }
                },
                error: function ( jqXHR ,  textStatus,  errorThrown) {
                    console.log('error', jqXHR, textStatus, errorThrown);
                }
            });
        }
    }

    function renderDatePickerKey() {
        var pricing = get4WeekPricingSummary();
        return '<div class="datepickerKeyWrapper">'
            + '<span class="1-week-pricing datePickerKey">' + (pricing[0].price ? pricing[0].price : '1 wk') + '</span>'
            + '<span class="2-week-pricing datePickerKey">' + (pricing[1].price ? pricing[1].price : '2 wks') + '</span>'
            + '<span class="3-week-pricing datePickerKey">' + (pricing[2].price ? pricing[2].price : '3 wks') + '</span>'
            + '<span class="4-week-pricing datePickerKey">' + (pricing[3].price ? pricing[3].price : '4 wks') + '</span></div>';
    }

    // intialize or re-initialize a datepicker
    function initializeDatepicker(which) {
        var whichDate = new Date();
        var whichElement = '#' + which + '-date';
        $(whichElement).datepicker('remove');
        whichDate.setDate(whichDate.getDate() + 1);
        if (which === 'pickup') {
            var deliveryDate = $('#delivery-date').val();
            if (validDate(deliveryDate) &! validDate($('#pickup-date').val())) {
                whichDate = new Date(deliveryDate);
                whichDate.setDate(whichDate.getDate() + 8);
            }
            else {
                whichDate.setDate(whichDate.getDate() + 6)
            }
        }
        var dayClass;
        $(whichElement).datepicker({
            format: "yyyy-mm-dd",
            startDate: "+1d",
            todayBtn: false,
            autoclose: true,
            todayHighlight: false,
            defaultViewDate: { year: whichDate.getYear(), month: whichDate.getMonth(), day: whichDate.getDate() },
            beforeShowDay: function(date) {
                var daysFromDelivery = null;
                if (which === 'pickup') {
                    daysFromDelivery = dateDifference(jQuery('#delivery-date').val(), date);
                }
                var dateString = jQuery.datepicker.formatDate('yy-mm-dd', date);
                if (daysFromDelivery) {
                    if (daysFromDelivery > 0 && daysFromDelivery <= 9) {
                        dayClass = '1-week-pricing';
                    }
                    else if (daysFromDelivery <= 16) {
                        dayClass = '2-week-pricing';
                    }
                    else if (daysFromDelivery <= 23) {
                        dayClass = '3-week-pricing';
                    }
                    else if (daysFromDelivery <= 30) {
                        dayClass = '4-week-pricing';
                    }
                    else {
                        dayClass = '';
                    }
                }
                else {
                    dayClass = '';
                }
                if (disabledDates[which].indexOf(dateString) === -1) {
                    return {enabled: true, classes: dayClass}
                }
                else {
                    return {enabled: false, classes: dayClass}
                }

            }
        });
    }

    // calculate the difference between two dates
    function dateDifference(d1, d2) {
        dateOne = new Date(d1);
        dateTwo = new Date(d2);
        if (dateTwo < dateOne) {
            return false;
        }
        return Math.round((dateTwo - dateOne) / 86400000); // # of milliseconds in a day
    }

    // compare the pricelist to the one in the cart and use the one with the
    // higher priceListLevelValue property
    function useDeliveryPriceList() {
        if (getPriceList('delivery').priceListLevelValue >= getPriceList('pickup').priceListLevelValue) {
            return true;
        }
        return false;
    }

    function getPriceList(key) {
        return ff_functions.allPriceLists()[key];
    }

    function setPriceList(key, priceList) {
        if (priceList && !priceList.error) {
            priceLists[key] = priceList;
        }
    }

    // validate a date based on format
    function validDate(d) {
        var dr = /\d{4}\-\d{2}\-\d{2}/;
        if (d.match(dr)) {
            return true;
        }
        return false;
    }

    function updateKeepFor() {
        if (ff_functions.validDate($('#delivery-date').val()) && ff_functions.validDate($('#pickup-date').val())) {
            var currentKeepFor = $('#keep-for').val();
            var newKeepFor = ff_functions.dateDifference($('#delivery-date').val(), $('#pickup-date').val());
            if (newKeepFor != currentKeepFor && newKeepFor > 0) {
                // update DOM and user info
                $('#keep-for').val(newKeepFor);
                $('#keep-for-display').text(ff_functions.formatKeepFor());
                // if the number of weeks has changed, update pricing
                if (keepForWeeks(currentKeepFor) != keepForWeeks(newKeepFor)) {
                    updateCartPricing();
                }
            }
        }
    }

    function keepForWeeks(val) {
        var mod = val % 7;
        var div = parseInt(val / 7);
        if (mod > 2) {
            div = div +1;
        }
        return div;
    }

    function updateCartPricing() {
        // update pricelists
        var dS = Step1.getPriceListCode('delivery');
        if (dS) {
            var plUrl = '/ff/pricelist/' + dS + '/' + $('#keep-for').val();
            throbberID = ff_functions.addThrobber('keep-for');
            var errorID = 'keep-for-error';
            var errorParent = 'keep-for-wrapper';
        }
        var pS = Step1.getPriceListCode('pickup');
        if (pS) {
            ff_functions.updatePricelist(plUrl, throbberID, errorID, errorParent, true, true);
            plUrl = '/ff/pricelist/' + pS + '/' + $('#keep-for').val();
            throbberID = ff_functions.addThrobber('keep-for');
            errorID = 'keep-for-error';
            ff_functions.updatePricelist(plUrl, throbberID, errorID, errorParent, false, true);
        }

        // if interfranchise, update that one
        if (ff_functions.orderIsInterfranchise()) {
            var dF = ff_functions.getDeliveryFranchise();
            if (dF) {
                var ifUrl = '/ff/pricelist/IF' + dF + '/' + $('#keep-for').val();
                throbberID = ff_functions.addThrobber('keep-for');
                ff_functions.updatePricelist(ifUrl, throbberID, errorID, errorParent, 'IF', true);
            }
        }
    }

    function sanitizeItemName(name) {
        return name.replace(/[^A-Za-z0-9 ]/g, '').replace(/ /g, '-');
    }

    // updates price listings
    return {
        addThrobber: function(elementID) {
            return addThrobber(elementID);
        },
        allPriceLists: function() {
            return priceLists;
        },
        clearPending: function(url) {
            clearPending(url);
        },
        dateDifference: function(d1, d2) {
            return dateDifference(d1, d2);
        },
        formatKeepFor: function () {
            return formatKeepFor();
        },
        formatItem: function(markup, itemClass) {
            return formatItem(markup, itemClass);
        },
        generateSummary: function() {
            generateSummary();
        },
        getAvailability: function(which, zip, successCallback) {
            return getAvailability(which, zip, successCallback);
        },
        getDeliveryFranchise: function() {
            return getFranchise('delivery');
        },
        getDeliverySuburb: function() {
            return Step1.getSuburb('delivery');
        },
        getPickupFranchise: function() {
            return getFranchise('pickup');
        },
        getPickupSuburb: function() {
            return Step1.getSuburb('pickup');
        },
        getPriceList: function(key) {
            return getPriceList[key];
        },
        getQuantity: function(key){
            return getQuantity(key);
        },
        getSingleAvailability: function(whichID, theDate) {
            return getSingleAvailability(whichID, theDate)
        },
        getWhich: function(element) {
            return getWhich(element);
        },
        initializeAvailability: function() {
            return initializeAvailability();
        },
        initializeDatepicker: function(which) {
            return initializeDatepicker(which);
        },
        isPending: function(url) {
            return isPending(url);
        },
        loadDisabledDates: function(which, franchise, suburb) {
            return loadDisabledDates(which, franchise, suburb);
        },
        lookupProvince: function(province) {
            return lookupProvince(province);
        },
        lookupState: function(state) {
            return lookupState(state);
        },
        makeAlert: function(message, jump) {
            return makeAlert(message, jump);
        },
        orderIsInterfranchise: function() {
            return orderIsInterfranchise();
        },
        processPricelist: function(priceList, delivery) {
            processPricelist(priceList, delivery);
        },
        sanitizeItemName: function(name) {
            return sanitizeItemName(name);
        },
        setDeliverySuburb: function(newSuburb) {
            return validateSuburbZip('delivery', newSuburb);
        },
        setPending: function(url) {
            setPending(url);
        },
        setPickupSuburb: function(newSuburb) {
            return validateSuburbZip('pickup', newSuburb);
        },
        setPriceList: function(key, priceList) {
            return setPriceList(key, priceList);
        },
        setQuantity: function(key, quantity) {
            return setQuantity(key, quantity);
        },
        setTimeOptions: function(whichID, options) {
            return setTimeOptions(whichID, options);
        },
        shortenPostCode: function(zip) {
            return shortenPostCode(zip);
        },
        updateKeepFor: function() {
            return updateKeepFor();
        },
        updatePricelist: function(url, throbberID, errorID, errorParent, delivery, update, callback) {
            return updatePricelist(url, throbberID, errorID, errorParent, delivery, update, callback);
        },
        unSetPriceList: function(key) {
            priceLists[key] = null;
        },
        updateItems: function () {
            updateItems();
        },
        validDate: function(date) {
            return validDate(date);
        },
        whichCountry: function(franchise) {
            return whichCountry(franchise);
        }
    };

})();
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = ff_functions;
}
else {
    window.ff_functions = ff_functions;
}