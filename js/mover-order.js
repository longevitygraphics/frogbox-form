// @codekit-prepend "ff_functions.js"
// @codekit-prepend "components/mover-order/step1.js"
// @codekit-prepend "components/mover-order/step2.js"
// @codekit-prepend "components/step3.js"
// @codekit-prepend "components/mover-order/step4.js"
// @codekit-prepend "components/mover-order/step5.js"
// @codekit-prepend "components/validate.js"
// @codekit-prepend "components/accordion.js"

// define these variables in the global scope
frogboxCart = null;
disabledDates = {'delivery': [], 'pickup': []};
pendingRequests = {};

$(document).ready(function () {

    var steps = new Array(Step1, Step2, Step3, Step4, Step5);

    /**
     * Load the code for our steps and render it.
     *
     * Markup should be rendered before any event listeners are created in .load().
     *
     * On this form, the element for step 1 is combined with the element for step 2.
     */
    $('#step-2').append(Step1.render()).append(Step2.render());
    $('#step-3').append(Step3.render());
    $('#step-4').append(Step4.render());
    $('#step-5').append(Step5.render());

    Validator.validate();

    /**
     * Load the listeners for each step.
     */
    for (var i = 0; i < steps.length; ++i) {
        steps[i].load();
    }

    /**
     * Set valid franchises
     */
    if (window.validFranchises) {
        Step1.setValidFranchises(window.validFranchises);
    }

    /**
     * Set placeholder text
     */
    if (window.zipPlaceholder) {
        $('.city').attr('placeholder', window.zipPlaceholder);
    }

    /**
     * Set label text
     * Your <span class="hoverText" title="Toronto - Mississauga - Brampton - Markham - '
     * 'Vaughan - Richmond Hill - Milton - Newmarket">Greater Toronto Area</span> Postal Code
     */
    if (window.moverOptions.zipLabelText) {
        $('.suburb-label').html(window.moverOptions.zipLabelText);
    }

    /**
     * Set PricelistCode element for pricelist validation on the back end
     */
    if (window.moverOptions.PricelistCode) {
        $('#PricelistCode').val(window.moverOptions.PricelistCode);
    }

    /**
     * Set tracking code in promocode field if it exists
     */
    if (window.moverOptions.trackingCode) {
        $('#promo-code').val(window.moverOptions.trackingCode);
    }

    /**
     * Add the line about the extra boxes if we are supposed to
     */
    if (window.moverOptions.extraBoxes == 1) {
        var footnote = '<span class="step-2-footnote" style="margin-left: 2em;">* Extra Boxes: You only pay if you use them. Charged $4.00 per box @pickup.</span>';
        $(footnote).insertAfter('.step-2-footnote');
    }

    /**
     * Look for a submission object. If it exists, set form fields based on submission.
     */
    if (window.Submission) {
        // set the page title
        document.title = pageData.title;
        $('.page-header h1').html(pageData.title);

        frogboxCart = Submission.frogboxCart;
        delete Submission.frogboxCart;
        for (var property in Submission) {
            if (Submission.hasOwnProperty(property)) {
                if (property != 'terms' && property.substr(property.length -8) != 'quantity') {
                    $("*[name=" + property + "]").not('*[type="radio"]').val(Submission[property]);
                }
            }
        }

        // initialize availability
        ff_functions.initializeAvailability();

        // load pricelists by calling .change() method on .city elements
        $('.city').change();

        // load dates by triggering changeDate event on .booking-date elements
        $('.booking-date').trigger('changeDate');

        // validate promo if it exists by calling .change() method
        if ($('#promo-code').val().length) {
            $('#promo-code').change();
        }

        // choose the bundle in step 2 and call the change event
        $('#bundle-' + Submission.bundle).prop('checked', true).change();
    }

    /**
     * Display errors, if any.
     */
    if (window.Errors) {
        // display the errors
        for (var n = 0; n < window.Errors.length; n++) {
            var newElement = '<p class="error">' + window.Errors[n].errMsg + '</p>';
            $('#frogbox-form').prepend(newElement);
            if ($('#' + window.Errors[n].element).length) {
                $(newElement).insertBefore('#' + window.Errors[n].element);
            }
        }
    }

    /**
     * Set up validation
     */
    for (var i = 0; i < steps.length; ++i) {
        steps[i].validate();
    }

    /**
     * Accordion runs after validation
     */
    Accordion.accordion();

    /**
     * Open the accordion to where the first error is, if any.
     */
    if (window.Errors) {
        var first = Errors[0];
        if (first) {
            var stepID = Errors[0].element;
            if (stepID === 'step-1') {
                stepID = 'step-2';
            }
            var stepElement = $('#' + stepID).closest('.step-content').attr('id');
            if (stepElement[5]) {
                var openStep = stepElement[5] - 1;
                if (openStep > 2) { // select new dates in step 3
                    openStep = 2;
                }
            }
        }
        else {
            openStep = 1;
        }
        $('#accordion').accordion('option', 'active', ( openStep ));
        Accordion.scrollToStep(openStep);
    }

});