/* closure for functions so we can access them from other scripts */
var ff_functions=function(){
// formats the keep for info to be pretty and consistent
function C(e){e||(e=parseInt(jQuery("#keep-for").val(),10));var i=parseInt(e/7,10),t="";0<i&&(t=i+" week"),1<i&&(t+="s");var r=Math.round(e%7),o="";0<r&&(o=r+" day"),1<r&&(o+="s");var n="",a;return 0<i&&0<r&&(n=", "),t+n+o}
// gets a property for an item for a pricelist
function t(e,i,t){for(var r=0;r<2;++r){var o=new Array("bundles","supplies")[r];if(t[o]&&Array.isArray(t[o]))for(var n=0;n<t[o].length;++n)if(i===t[o][n].key)return t[o][n][e]}return 0}
// gets a quantity from the data model for a specific item
function n(e){return t("quantity",e,window.frogboxCart)}
// gets the value of an item in a given pricelist
function c(e,i){return t("value",e,i)}
// calculates a total for a pricelist based on what is in the frogboxCart
function s(e){var i=window.frogboxCart;if(!e)return 0;var t=0;for(var r in{bundles:"",supplies:""})if(void 0!==i[r]&&i[r].length&&0<i[r].length)for(var o=0;o<i[r].length;++o)0<i[r][o].quantity&&(t+=i[r][o].quantity*c(i[r][o].key,e));var n=0,a;return null!=e.taxPercent&&(n=t*e.taxPercent/100),Number(t+n).toFixed(2)}function P(e,i){var t;return'<div class="product"><span class="product-name">'+e+'</span><span class="product-quantity">'+i+"</span></div>"}function u(e){window.pendingRequests[e]=1}function p(e){window.pendingRequests[e]=0}function f(e){return 1===window.pendingRequests[e]}function r(r){var e="/ff/serviceCharge/"+r;
// if the request is pending, don't request it a second time
if(!f(e)){if(void 0===window.serviceCharges)window.serviceCharges={};else if(void 0!==window.serviceCharges[r])return window.serviceCharges[r];u(e),$.ajax({dataType:"json",url:e,async:!0,complete:function(){p(e),m()},success:function(e,i,t){window.serviceCharges[r]=0<e?e:0},error:function(e,i,t){console.log("error",e,i,t)}})}}
// calculate delivery charege
function S(){var e;return i(ff_functions.getDeliverySuburb())}
// calculate pickup charge
function D(){var e;return i(ff_functions.getPickupSuburb())}
// calculate service charge
function i(e){return void 0===window.serviceCharges&&(window.serviceCharges={}),void 0===window.serviceCharges[e]?(window.setTimeout(function(){r(e)},Math.floor(1e3*Math.random())+1),0):+window.serviceCharges[e];var i}
// update delivery, pickup and taxes
function _(){null==window.frogboxCart&&(window.frogboxCart={}),
// read delivery charge and taxes from delivery priceslist
null!=ce.delivery&&null!=ce.delivery&&(window.frogboxCart.taxPercent=ce.delivery.taxPercent)}
// process a pricelist - called from $.ajax:success or statically
function v(e,i){"IF"===i?Z("interfranchise",e):i?Z("delivery",e):!1===i&&Z("pickup",e),null!=J("interfranchise")?o(J("interfranchise"),i):null!=J("delivery")&&null!=J("pickup")&&(G()?o(J("delivery"),i):o(J("pickup"),i)),w()}
// returns true if an order is interfranchise
function l(){var e,i;return $("#delivery-city").val()!==$("#pickup-city").val()}
// updateModel takes a new model object and overwrites the old one
function o(e,i){
// if frogboxCart is empty, set its value to the incoming data
if(null==window.frogboxCart)return window.frogboxCart=e,void(window.frogboxCart.groupsInit=0);
// otherwise, go through each item and update the quantity
for(var t=0;t<2;++t)for(var r=new Array("bundles","supplies")[t],o=0;o<e[r].length;++o)e[r][o].name&&(e[r][o].quantity=n(e[r][o].key));window.frogboxCart=e,m()}
// extract the move pack details from the dimension fields
function j(e){for(var i=0;i<e.length;++i)if("Move Pack"===e[i].title){var t=e[i].dimensions.split("<br>");return e[i].dimensions.split("<br>")}}function T(e){
// check for valid promo code
if($("#promoThresholdNotMet").remove(),null==window.frogboxCart.promo)return 0;
// check to see that we meet the threshold
if(!isNaN(window.frogboxCart.promo.promoAmountIfMoreThen)&&e<window.frogboxCart.promo.promoAmountIfMoreThen){var i,t='<span class="error" id="promoThresholdNotMet">'+(" You must purchase at least $"+window.frogboxCart.promo.promoAmountIfMoreThen+" of goods and services to use this promo code. ")+"</span>";return $("#promo-valid").length?$(t).insertAfter("#promo-valid"):$(t).insertAfter("#promo-code"),0}return"percent"==window.frogboxCart.promo.promoMeasure?Math.ceil(e*window.frogboxCart.promo.promoDiscount)/100:window.frogboxCart.promo.promoDiscount}
// generate a summary of items in the frogbox cart
function m(){
// if there's a keep-for error, copy it to the sidebar and return
if(_(),jQuery("#keep-for-error").length){var e=jQuery("#keep-for-error").clone();return jQuery("#your-price-price").html(e),void jQuery("#your-order").addClass("group-hidden")}if(null!=window.frogboxCart){
// override frogboxCart.minimumCharge with window.minimumCharge
window.frogboxCart.minimumCharge=window.minimumCharge.minimumCharge;var i=0,t="",r=0,o=0;for(var n in jQuery("#your-order").removeClass("group-hidden"),{bundles:"",supplies:""})if(null!=window.frogboxCart[n]&&0<window.frogboxCart[n].length)for(var a=0;a<window.frogboxCart[n].length;++a)if(0<window.frogboxCart[n][a].quantity)if(i+=window.frogboxCart[n][a].quantity*window.frogboxCart[n][a].value,null!=window.frogboxCart[n][a].bundle_items){for(var c in window.frogboxCart[n][a].bundle_items)if(0<c.length&&"Pack"!=c)t+=P(c,window.frogboxCart[n][a].bundle_items[c]);else if("Pack"==c&&window.frogboxCart[n][a].description_details)for(var s=j(window.frogboxCart[n][a].description_details),l=0;l<s.length;++l){var d=s[l].trim().split(/ (.+)/,2);t+=P(d[1],d[0])}}else t+=P(window.frogboxCart[n][a].name,window.frogboxCart[n][a].quantity);
// if there is a subtotal, calculate delivery and pickup charges
if(i){var u=S(),p=D(),f=i+u+p;if(
// make sure we clear delivery charge message before creating it
jQuery("#minimum-charge-wrapper").remove(),f<window.frogboxCart.minimumCharge){var v=window.frogboxCart.minimumCharge-f,m="<div id='minimum-charge-wrapper'><input type='checkbox' name='minimum-charge' id='minimum-charge' required><label for='minimum-charge'>I understand that I will be charged a delivery fee to meet the $"+window.minimumCharge.minimumCharge+" minimum charge.</label><a href='#step-3-header'>Add more products instead</a></div>";
// add required checkbox to step 5 if this a local list that usually doesn't have a delivery charge
jQuery(m).insertBefore("#payment-wrapper"),jQuery("#minimum-charge-wrapper a").click(function(){$("#accordion").accordion("option","active",2),Accordion.scrollToStep(3)}),u<v&&(u+=v)}0<u&&(i+=u,t+=P("Delivery Charge","$"+u.toFixed(2))),frogboxCart.deliveryCharge=u,0<p&&(i+=p,t+=P("Pickup Charge","$"+p.toFixed(2))),frogboxCart.pickupCharge=p}var h=T(i),y=i-h;(r=Math.ceil(y*window.frogboxCart.taxPercent)/100)||(r=0),o=r+y;var b='<div class="order-subtotal"><span class="pricing-description">Subtotal:</span><span class="pricing-price">$'+i.toFixed(2)+"</span></div>";0<(h=parseInt(h))&&(b+='<div class="order-discount"><span class="pricing-description promo">'+window.frogboxCart.promo.promoCode+'</span><span class="pricing-price">-$'+h.toFixed(2)+"</span></div>"),b+='<div class="order-tax"><span class="pricing-description">Tax:</span><span class="pricing-price">$'+r.toFixed(2)+"</span></div>",b+='<div class="order-total"><span class="pricing-description">Total:</span><span class="pricing-price">$'+o.toFixed(2)+"</span></div>",jQuery("#your-price-price").html(b),jQuery("#your-price-products").html(t),jQuery("#mobile-order-price").html("$"+o.toFixed(2)),jQuery("#your-order-extra").html(""),
// append (change) if your-order is set
0<jQuery("#your-order").length&&(jQuery("#your-price-products").append('<div class="change-link">(<a href="#" id="change-order">change</a>)</div>'),jQuery("#change-order").click(function(){jQuery("#accordion").accordion({active:1});// accordion sections start numbering at 0; switch to #2
}));
// append delivery & pickup locations and (change)
var g=ff_functions.getDeliverySuburb(),w=ff_functions.getPickupSuburb();g&&w&&(jQuery("#your-order-extra").append('<h4>Delivery:</h4><div class="sidebar-detail">'+jQuery("#delivery-city option:selected").text()+'</div><h4>Pick-up:</h4><div class="sidebar-detail">'+jQuery("#pickup-city option:selected").text()+'</div><div class="change-link">(<a href="#" id="change-locations">change</a>)</div>'),jQuery("#change-locations").click(function(){jQuery("#accordion").accordion({active:0});// accordion sections start numbering at 0; switch to #1
})),jQuery("#delivery-date").val()&&jQuery("#pickup-date").val()&&jQuery("#your-order-extra").append('<h4>Keep for:</h4><div class="sidebar-detail">'+C()+"</div>");var k=q("delivery"),x=q("pickup");k&&x&&(jQuery("#your-order-extra").append('<h4>Delivery Address:</h4><div class="sidebar-detail">'+k+'</div><h4>Pick-up Address:</h4><div class="sidebar-details">'+x+'</div><div class="change-link">(<a href="#" id="change-address">change</a>)</div>'),jQuery("#change-address").click(function(){jQuery("#accordion").accordion({active:3});// accordion sections start numbering at 0; switch to #4
})),Q()}}
// lookup province based on first letter of post code
function d(e){var i;if(e)return{A:"Newfoundland and Labrador",B:"Nova Scotia",C:"Prince Edward Island",E:"New Brunswick",G:"Quebec",H:"Quebec",J:"Quebec",K:"Ontario",L:"Ontario",M:"Ontario",N:"Ontario",P:"Ontario",R:"Manitoba",S:"Saskatchewan",T:"Alberta",V:"British Columbia",X:"Northwest Territories|Nunavut",Y:"Yukon Territory"}[e.substring(0,1).toUpperCase()]}
// lookup state based on first two numbers in zip code
function h(e){lookup=e.substring(0,5);var i,t={83:"Idaho",55:"Minnesota",56:"Minnesota",98:"Washington",99:"Washington"}[lookup.substring(0,2)];return"Washington"===t&&99500<+lookup&&(t=void 0),
// it is an error if the code is less than 5  letters
(void 0===e.length||e.length<5)&&(t=void 0),t}
// update a pricelist
// url - ajax url to get pricelist from
// throbberID - ID of throbber to remove after ajax is complete
// errorID - div to write errors in
// errorParent - the 'for' attribute of the label element to place the error in
// delivery - boolean, is this the pricelist for delivery or not
// update - boolean - update the model
function y(a,e,c,s,l,d,i){if($("#"+c).remove(),null!=J(a))return d&&v(J(a),l),i&&i(),window.setTimeout(function(){
// don't remove the throbber immediately - give items time to render
$(e).remove()},1e3),!0;
// if the request is pending, don't request it a second time
f(a)?window.setTimeout(function(){y(a,e,c,s,l,d,i)},1e3):(u(a),$.ajax({dataType:"json",url:a,async:!0,success:function(e,i,t){if(!e||null!=e.error&&e.displayError){$("#"+c).remove();var r='<div class="error" id="'+c+'">'+e.error+"</div>";$('label[for="'+s+'"]').length?$(r).appendTo('label[for="'+s+'"]'):$(r).appendTo("#"+s),m()}else
// expect an array of pricelists
for(var o="",n=0;n<e.length;++n)
// if we are getting 4 priceslists, keep track of them all
4==e.length&&(a=(o=a.substring(0,a.lastIndexOf("/")+1))+7*(n+1)),0<n&&(d=!1),Z(a,e[n]),d&&v(e[n],l)},complete:function(){p(a),i&&i(),window.setTimeout(function(){
// don't remove the throbber immediately - give items time to render
$(e).remove()},1e3)},error:function(e,i,t){console.log("json parse error",e,i,t)}}))}function a(){if(null==window.frogboxCart.zip)return[];l()?zip=window.frogboxCart.zip:zip=ff_functions.getDeliverySuburb();for(var e="/ff/pricelist/"+zip+"/",i=[],t=7;t<=28;t+=7){priceList=e+t;var r,o,n,a=+s(ce[priceList])+ +S()+ +D();i.push({price:a.toFixed(2),weeks:C(t)})}return i}
// updates the 'your current order' section
function Q(){for(var e=a(),i="",t=0;t<e.length;t++)i+="<li>$"+e[t].price+" for "+e[t].weeks+"</li>";jQuery("#current-order-price-by-duration").html(i)}
// returns a string with an address formatted as HTML
function q(e){var i=jQuery("#"+e+"Street").val(),t=jQuery("#"+e+"City").val(),r=jQuery("#"+e+"Province").val(),o=jQuery("#"+e+"Postcode").val(),n=jQuery("#"+e+"Description").val(),a="";return i?(a+=i,t?(a+="<br>"+t,r&&(a+=", "+r),o?(a+="<br>"+o,n&&(a+="<br>"+n),a):""):""):""}
// format a single item
function b(e,i){i||(i="product");
// format the columns
var t='<div class="'+i+'">';// close product
return null!=e.wrapper&&(t+=e.wrapper),t+='<div class="inner-border">',t+='<div class="col1">',null!=e.col1&&(t+=e.col1),t+='</div><div class="col2">',null!=e.col2&&(t+=e.col2),t+='</div><div class="col3">',null!=e.col3&&(t+=e.col3),t+='</div><div class="col4">',null!=e.col4&&(t+=e.col4),t+="</div></div>",// close col4 and inner-border
null!=e.wrapperClose&&(t+=e.wrapperClose),t+="</div>"}
// updates the data model with a new quantity for a specific item
function g(e,i){for(var t=0;t<2;++t){var r=Array("bundles","supplies")[t];if(Array.isArray(window.frogboxCart[r]))for(var o=0;o<window.frogboxCart[r].length;++o)if(e==window.frogboxCart[r][o].key)return window.frogboxCart[r][o].quantity=i,!0}return!1}function w(){null==window.frogboxCart||jQuery.isEmptyObject(window.frogboxCart)||(jQuery("#bundles-products-wrapper").html(Step2.formatItems(window.frogboxCart.bundles,!0)),Step2.processBundles(),jQuery("#packing-supplies-wrapper").html(Step3.formatItems(window.frogboxCart.supplies,!1)),
// adding function for hide and showing group items
jQuery("#packing-supplies-wrapper .group .product .inner-border").hide(),jQuery(".show-options").click(function(){if(jQuery(this).data("target").length){var e="."+jQuery(this).data("target")+" .product .inner-border";
//jQuery('#packing-supplies-wrapper .group .product').not(target).hide();
jQuery(e).length&&jQuery(e).toggle()}}),
// bind new selects with change event
jQuery(".product select").change(function(){g(jQuery(this).attr("key"),jQuery(this).val()),m()}),jQuery("input:radio[name='bundle-choice']").change(function(){jQuery("input:radio[name='bundle-choice']").closest(".inner-border").removeClass("active"),jQuery("input:radio[name='bundle-choice']").each(function(){g(jQuery(this).attr("key"),0)}),jQuery(this).is(":checked")&&(jQuery(this).closest(".inner-border").addClass("active"),g(jQuery(this).attr("key"),1),m())}),Step1.updateItemsDone())}function k(e){
// if the first five digits are numeric, use that
var i=e.trim().substring(0,5);return isNaN(i)?
// use the first three if we have at least three
3<=e.trim().length&&e.trim().substring(0,3).toUpperCase():i}
// validates a suburb/zip and retrieves the franchise for a zip from Vonigo
function x(o,n){var e="/ff/zip/"+(n=k(n));
// if the request is pending, don't request it a second time
f(e)?window.setTimeout(function(){x(o,n)},1e3):(u(e),$.ajax({dataType:"json",url:e,complete:p(e),success:function(e,i,t){var r;F(o,n,e,z(o))},error:function(){F(o,n,0),console.log("json parse error",jqXHR,textStatus,errorThrown)}}))}
// changes mm/dd/yy to yyyymmdd
function I(e){return e.substring(0,4)+e.substring(5,7)+e.substring(8)}function N(e,i,t){var r=e.substring(0,e.length-5),o=$("#"+r+"-time option:selected").attr("time"),n;
// check if we've got a time selected
// if we had a time selected, try to select it again
if(void 0===window.chosenTime&&(window.chosenTime={"pickup-time":"","delivery-time":""}),o?window.chosenTime[e]=o:void 0!==window.chosenTime[e]&&(o=window.chosenTime[e]),void 0===window.formSubmitting&&(window.formSubmitting={"pickup-time":!1,"delivery-time":!1,"delivery-date":!1,"pickup-date":!1}),
// put the new data in
$("#"+r+"-time").html(i),o)if(0<$("#"+r+'-time option[time="'+o+'"').length)$("#"+r+'-time option[time="'+o+'"').attr("selected","selected").change();else if(!window.formSubmitting[e]){var a="Your "+r+" time is no longer available. Please choose a new one.";ff_functions.makeAlert(a,!1)}
// empty set handling
!t&&$("#"+r+"-time option").length<=1&&$('<div class="error" id="'+e+'-error">No times are available for this date. Please choose another date.</div>').appendTo('label[for="'+e+'"]')}function A(e){N(e,[],!0)}
// checks that that a zip code is filled out
function z(e){var i;return!!$("#"+("pickup"==e?"deliveryPostcode":"pickupPostcode")).val().length}
// callback for ajax requests
function F(i,e,t,r){
// re-validate promo codes if the delivery suburb changed
"delivery"==i&&$("#promo-code").change();
// if valid, maybe make an update
var o=i+"Postcode";if(0<t){var n;
// update the service times if the city has changed
if(
// remove any errors
$('label[for="'+o+'"] .error').remove(),t!=O(i)){
// note that the franchise has changed
ff_functions[i+"FranchiseHasChanged"]=!0;
// update availability
var a,c=$("#"+i+"-date").val().replace(/-/g,"");ff_functions.initializeAvailability(),ff_functions.getAvailability(i,e,function(){var e=i+"-date";window.availability[i]&&window.availability[i][c]?N(e,window.availability[i][c]):
// reload time options if it isn't in the availability array
H(e,c)})}
// always update the option with newcity to use the new zip, select it but don't trigger
$("#"+i+"-city option[value='"+t+"']").first().attr("zip",e).attr("selected","selected")}
// if invalid, show an error message
else{var s;$("<div class='error'>Currently we don't service this address but please <a href='/contact'>Contact Us</a> if you have any questions.</div>").appendTo('label[for="'+o+'"]')}
// update the pricelist if both postcodes are set
if(r){
// trigger .change() on the franchise to update pricelists
$("#"+i+"-city option[value='"+t+"']").first().attr("zip",e).attr("selected","selected").change();
// trigger the change on the other franchise; use a delay to try to prevent a franchise conflict
var l="pickup"==i?"delivery":"pickup",d=O(l),u=Step1.getSuburb(l),p=window.setTimeout(function(){$("#"+l+"-city option[value='"+d+"']").attr("zip",u).attr("selected","selected").change()},1500),f;
// show a message if at least one of the franchises has changed
if(ff_functions.deliveryFranchiseHasChanged||ff_functions.pickupFranchiseHasChanged)M("<p>Oh no! Looks like your postal/zips don't match the location we thought you were in. No problem! Now you will need to re-enter some info because your location has changed.</p>",!0),$("#bundles-products-wrapper .row").remove(),$("#bundles-products-wrapper").append('<div class="throbber-loader" id="step2-throbber">Loading...</div>'),ff_functions.deliveryFranchiseHasChanged=!1,ff_functions.pickupFranchiseHasChanged=!1}}function M(e,i){if(0<$(".alert").length){var t;$('<div class="alert-body">'+e+"</div>").insertAfter(".alert-body")}else{var r='<div class="alert-overlay"><div class="alert"><div class="alert-body">'+e+'</div><div class="alert-ok">Ok</div></div></div>';$("body").append(r),$(".alert-ok").click(function(){L(),$(".bundle-choice").length<1&&i&&(jQuery("#accordion").accordion({active:1}),// accordion sections start numbering at 0; switch to #2
// scroll to bundles
$("html, body").animate({scrollTop:$("#bundles-products-wrapper").offset().top-40},500))})}$(".alert").focus()}function L(){$(".alert-overlay").remove()}function O(e){return!!jQuery("#"+e+"-city").val()&&jQuery("#"+e+"-city").val()}function e(e,i){null!=i&&0<i&&jQuery("#"+e+"-city").val(i)}
// add a throbber to a div after a form element
// @param string elementID - the element id of the element to add the throbber after
// - do not prefix with '#'.
function E(e){var i=e+"-throbber",t;$("#"+i).length||$('<div class="throbber-loader" id="'+i+'">Loading...</div>').insertAfter("#"+e);return"#"+i}
// gets availability for times that haven't already loaded
function H(o,e){if(void 0===window.formSubmitting&&(window.formSubmitting={"pickup-time":!1,"delivery-time":!1,"delivery-date":!1,"pickup-date":!1}),ff_functions.validDate(e)){var i=o.substring(0,o.length-5);$("#"+o+"-error").remove();var t=ff_functions.addThrobber(i+"-time");window.formSubmitting[o]=!0,A(o);var r,n="/ff/times/"+("delivery"==i?ff_functions.getDeliverySuburb():ff_functions.getPickupSuburb())+"/"+I(e);
// if the request is pending, don't request it a second time
if(f(n))return void window.setTimeout(function(){H(o,e)},1e3);u(n),$.ajax({dataType:"json",url:n,success:function(e,i,t){if(void 0!==e&&null!=e.error&&e.displayError){var r='<div class="error" id="'+o+'-error">'+e.error+"</div>";$(r).appendTo('label[for="'+o+'"]')}else void 0!==e.options&&0<e.options.length&&(window.formSubmitting[o]=!1,N(o,e.options))},complete:function(e,i){p(n),window.formSubmitting[o]=!1,$(t).remove()},error:function(e,i,t){console.log("error",e,i,t)}})}}function V(o,e,n){if(e){
// if the request is pending, do not request it a second time
if(url="/ff/availability/"+e,f(url))return void window.setTimeout(function(){V(o,e,n)},1e3);var i={delivery:$("#delivery-time").attr("time"),pickup:$("#pickup-time").attr("time")};u(url),$.ajax({dataType:"json",url:url,complete:p(url),async:!0,success:function(e,i,t){var r=e.options;"both"===o?(window.availability.delivery=r,window.availability.pickup=r):window.availability[o]=r,n&&n()},error:function(e,i,t){console.log("error",e,i,t)}})}}
// initialize availability - franchises should have been chosen at this point
function K(){window.availability||(window.availability={delivery:{},pickup:{}}),deliverySuburb=ff_functions.getDeliverySuburb(),pickupSuburb=ff_functions.getPickupSuburb(),deliverySuburb===pickupSuburb?V("both",ff_functions.getDeliverySuburb()):(V("delivery",ff_functions.getDeliverySuburb()),V("pickup",ff_functions.getPickupSuburb()))}function U(e){return 16==e||18==e||29==e?"United States":"Canada"}
// determine if an element belongs to the 'delivery' or 'pickup' group based on its ID.
function R(e){return"delivery"==$(e).attr("id").slice(0,8)?"delivery":"pickup"}function B(o,e,i){
// if the request is pending, don't request it a second time
if(!f(n)){
// update calendar and grey out dates for that franchise
var t=U(e),r;if("Canada"===t?r=d(i):"United States"===t&&(r=h(i)),r){var n="/ff/dates/"+i,a=o+"-city";u(n),$.ajax({dataType:"json",url:n,complete:p(n),success:function(e,i,t){if(null!==e.error&&e.displayError){var r='<div class="error" id="'+a+'-error">'+e.error+"</div>";$(r).appendTo('label[for="'+a+'"]')}else e.error||(disabledDates[o]=e,ff_functions.initializeDatepicker(o))},error:function(e,i,t){console.log("error",e,i,t)}})}}}function W(){var e=a();return'<div class="datepickerKeyWrapper"><span class="1-week-pricing datePickerKey">'+(e[0].price?e[0].price:"1 wk")+'</span><span class="2-week-pricing datePickerKey">'+(e[1].price?e[1].price:"2 wks")+'</span><span class="3-week-pricing datePickerKey">'+(e[2].price?e[2].price:"3 wks")+'</span><span class="4-week-pricing datePickerKey">'+(e[3].price?e[3].price:"4 wks")+"</span></div>"}
// intialize or re-initialize a datepicker
function Y(r){var e=new Date,i="#"+r+"-date",o;if($(i).datepicker("remove"),e.setDate(e.getDate()+1),"pickup"===r){var t=$("#delivery-date").val();ee(t)&!ee($("#pickup-date").val())?(e=new Date(t)).setDate(e.getDate()+8):e.setDate(e.getDate()+6)}$(i).datepicker({format:"yyyy-mm-dd",startDate:"+1d",todayBtn:!1,autoclose:!0,todayHighlight:!1,defaultViewDate:{year:e.getYear(),month:e.getMonth(),day:e.getDate()},beforeShowDay:function(e){var i=null;"pickup"===r&&(i=X(jQuery("#delivery-date").val(),e));var t=jQuery.datepicker.formatDate("yy-mm-dd",e);return o=i?0<i&&i<=9?"1-week-pricing":i<=16?"2-week-pricing":i<=23?"3-week-pricing":i<=30?"4-week-pricing":"":"",-1===disabledDates[r].indexOf(t)?{enabled:!0,classes:o}:{enabled:!1,classes:o}}})}
// calculate the difference between two dates
function X(e,i){return dateOne=new Date(e),dateTwo=new Date(i),!(dateTwo<dateOne)&&Math.round((dateTwo-dateOne)/864e5)}
// compare the pricelist to the one in the cart and use the one with the
// higher priceListLevelValue property
function G(){return J("delivery").priceListLevelValue>=J("pickup").priceListLevelValue}function J(e){return ff_functions.allPriceLists()[e]}function Z(e,i){i&&!i.error&&(ce[e]=i)}
// validate a date based on format
function ee(e){var i=/\d{4}\-\d{2}\-\d{2}/;return!!e.match(i)}function ie(){if(ff_functions.validDate($("#delivery-date").val())&&ff_functions.validDate($("#pickup-date").val())){var e=$("#keep-for").val(),i=ff_functions.dateDifference($("#delivery-date").val(),$("#pickup-date").val());i!=e&&0<i&&(
// update DOM and user info
$("#keep-for").val(i),$("#keep-for-display").text(ff_functions.formatKeepFor()),
// if the number of weeks has changed, update pricing
te(e)!=te(i)&&re())}}function te(e){var i=e%7,t=parseInt(e/7);return 2<i&&(t+=1),t}function re(){
// update pricelists
var e=Step1.getPriceListCode("delivery");if(e){var i="/ff/pricelist/"+e+"/"+$("#keep-for").val();throbberID=ff_functions.addThrobber("keep-for");var t="keep-for-error",r="keep-for-wrapper"}var o=Step1.getPriceListCode("pickup");
// if interfranchise, update that one
if(o&&(ff_functions.updatePricelist(i,throbberID,t,r,!0,!0),i="/ff/pricelist/"+o+"/"+$("#keep-for").val(),throbberID=ff_functions.addThrobber("keep-for"),t="keep-for-error",ff_functions.updatePricelist(i,throbberID,t,r,!1,!0)),ff_functions.orderIsInterfranchise()){var n=ff_functions.getDeliveryFranchise();if(n){var a="/ff/pricelist/IF"+n+"/"+$("#keep-for").val();throbberID=ff_functions.addThrobber("keep-for"),ff_functions.updatePricelist(a,throbberID,t,r,"IF",!0)}}}function oe(e){return e.replace(/[^A-Za-z0-9 ]/g,"").replace(/ /g,"-")}
// updates price listings
var ne=!1,ae=!1,ce={delivery:null,pickup:null};return{addThrobber:function(e){return E(e)},allPriceLists:function(){return ce},clearPending:function(e){p(e)},dateDifference:function(e,i){return X(e,i)},formatKeepFor:function(){return C()},formatItem:function(e,i){return b(e,i)},generateSummary:function(){m()},getAvailability:function(e,i,t){return V(e,i,t)},getDeliveryFranchise:function(){return O("delivery")},getDeliverySuburb:function(){return Step1.getSuburb("delivery")},getPickupFranchise:function(){return O("pickup")},getPickupSuburb:function(){return Step1.getSuburb("pickup")},getPriceList:function(e){return J[e]},getQuantity:function(e){return n(e)},getSingleAvailability:function(e,i){return H(e,i)},getWhich:function(e){return R(e)},initializeAvailability:function(){return K()},initializeDatepicker:function(e){return Y(e)},isPending:function(e){return f(e)},loadDisabledDates:function(e,i,t){return B(e,i,t)},lookupProvince:function(e){return d(e)},lookupState:function(e){return h(e)},makeAlert:function(e,i){return M(e,i)},orderIsInterfranchise:function(){return l()},processPricelist:function(e,i){v(e,i)},sanitizeItemName:function(e){return oe(e)},setDeliverySuburb:function(e){return x("delivery",e)},setPending:function(e){u(e)},setPickupSuburb:function(e){return x("pickup",e)},setPriceList:function(e,i){return Z(e,i)},setQuantity:function(e,i){return g(e,i)},setTimeOptions:function(e,i){return N(e,i)},shortenPostCode:function(e){return k(e)},updateKeepFor:function(){return ie()},updatePricelist:function(e,i,t,r,o,n,a){return y(e,i,t,r,o,n,a)},unSetPriceList:function(e){ce[e]=null},updateItems:function(){w()},validDate:function(e){return ee(e)},whichCountry:function(e){return U(e)}}}();"undefined"!=typeof module&&void 0!==module.exports?module.exports=ff_functions:window.ff_functions=ff_functions;var Step1=function(){
// determine if an element belongs to the 'delivery' or 'pickup' group based on its ID.
function C(e){return"delivery"==$(e).attr("id").slice(0,8)?"delivery":"pickup"}
// sets defaults for location state/province, zip/postcode and country based on franchise
function P(e,i){var t=S(i);
// set the country
$("#"+e+"Country").val(t);
// set defaults for state/province and zipcode/postcode based on country, without losing original value
var r=$("#"+e+"Province").val();"Canada"==t?($("#"+e+"Province").html(o()).val(r),$("#"+e+"-province-label").html("Province:"),$("#"+e+"-postcode-label").html("Postal Code:")):($("#"+e+"Province").html(n()).val(r),$("#"+e+"-province-label").html("State:"),$("#"+e+"-postcode-label").html("Zip Code:"))}
// determine if a franchise is in the US or Canada
function S(e){return 16==e||18==e||29==e?"United States":"Canada"}
// select list of Canadian provinces - matches what Vonigo needs to validate against
function o(){var e;return i(["","Alberta","British Columbia","Manitoba","New Brunswick","Newfoundland and Labrador","Nova Scotia","Ontario","Prince Edward Island","Quebec","Saskatchewan","Northwest Territories","Nunavut","Yukon"])}
// select list of US states - matches what Vonigo needs to validate against
function n(){var e;return i(["","Idaho","Minnesota","Washington","","Alabama","Alaska","Arizona","Arkansas","California","Colorado","Connecticut","Delaware","District Of Columbia","Florida","Georgia","Hawaii","Illinois","Indiana","Iowa","Kansas","Kentucky","Louisiana","Maine","Maryland","Massachusetts","Michigan","Mississippi","Missouri","Montana","Nebraska","Nevada","New Hampshire","New Jersey","New Mexico","New York","North Carolina","North Dakota","Ohio","Oklahoma","Oregon","Pennsylvania","Rhode Island","South Carolina","South Dakota","Tennessee","Texas","Utah","Vermont","Virginia","West Virginia","Wisconsin","Wyoming"])}
// build a state/provinces options list
function i(e){for(var i="",t=0;t<e.length;++t)i+='<option value="'+e[t]+'">'+e[t]+"</option>";return i}function e(){
// when city changes, update products and addresses
$(".city").change(function(){
// remove error messages
var e="#"+$(this).attr("id")+"-error";
// check that we didn't pick '-'
if($(e).remove(),
// manage labels and defaults on address forms based on country of chosen franchise
P(C(this),$(this).val()),
// update products
$(this).closest(".location").find(".other-message").remove(),"-"!=$(this).val()){if("other"===$(this).val())return $(this).parent().append("<div class='other-message'>Currently we don't support other areas but please <a href='/contact'>Contact Us</a> if you have any questions.</div>"),void $(this).val("");
// if both cities are set to different values, get the IF pricelist and cache it
var i=ff_functions.getDeliveryFranchise(),t=ff_functions.getPickupFranchise();if(0!=i&&0!=t&&i!=t){var r=$(this).attr("id"),o=r+"-error",n="/ff/pricelist/IF"+i+"/7",a="/ff/pricelist/IF"+i+"/"+$("#keep-for").val(),c=ff_functions.addThrobber($(this).attr("id"));ff_functions.updatePricelist(n,c,o,r,"IF",!1,function(){var e=ff_functions.addThrobber($(this).attr("id"));ff_functions.updatePricelist(a,e,o,r,"IF",!0)}),
// disallow 'same as delivery checkbox'
$("#pickupSame").attr("disabled",!0).hide(),$('label[for="pickupSame"]').hide()}else i==t&&(
// nullify the record of the interfranchise pricelist
ff_functions.unSetPriceList("interfranchise"),
// allow 'same as delivery' checkbox
$("#pickupSame").attr("disabled",!1).show(),$('label[for="pickupSame"]').show());
// initialize variable - should we change the delivery list?
var s=!1;r=$(this).attr("id"),o=r+"-error","delivery-city"==r&&(s=!0);var l=D(s?"delivery":"pickup");
// if the interfranchise pricelist is null, go get new pricelist
if((null==ff_functions.getPriceList("interfranchise")||null==ff_functions.getPriceList("interfranchise"))&&l){var d=s?i:t,u=$("#keep-for").val(),p=!1;7==u&&(p=!0);var f="/ff/pricelist/"+l+"/7",v="/ff/pricelist/"+l+"/"+u;c=ff_functions.addThrobber($(this).attr("id"));var m=!1;!0===s&&(m=!0),
// download all pricelists, and update if keepfor is at 7
ff_functions.updatePricelist(f,c,o,r,s,p,function(){c=ff_functions.addThrobber($(this).attr("id")),ff_functions.updatePricelist(v,c,o,r,s,m)})}
// set the state or province
var h=s?"delivery":"pickup";if("Canada"==$("#"+h+"Country").val()){var y=ff_functions.lookupProvince(l);null!=y&&$("#"+h+"Province").val(y)}else if("United States"==$("#"+h+"Country").val()){var b=ff_functions.lookupState(d);null!=b&&$("#"+h+"Province").val(b)}
// update calendar and grey out dates for that franchise
var g=S(d),w;if("Canada"==g?w=ff_functions.lookupProvince(l):"United States"==g&&(w=ff_functions.lookupState(l)),w){var k="/ff/dates/"+l,x=$(this).attr("id");$.ajax({dataType:"json",url:k,success:function(e,i,t){if(null!=e.error&&e.displayError){var r='<div class="error" id="'+x+'-error">'+e.error+"</div>";$(r).appendTo('label[for="'+x+'"]')}else{var o=x.substring(0,x.length-7);disabledDates[o]=e,ff_functions.initializeDatepicker(o)}},error:function(e,i,t){console.log("error",e,i,t)}})}}else $(this).val("")})}function D(e){return""!=jQuery("#"+e+"Postcode").val()&&null!=jQuery("#"+e+"Postcode").val()?ff_functions.shortenPostCode(jQuery("#"+e+"Postcode").val()):""!=jQuery("#"+e+"-city option:selected").attr("zip")&&null!=jQuery("#"+e+"-city option:selected").attr("zip")&&jQuery("#"+e+"-city option:selected").attr("zip").toUpperCase()}function t(){return!0}var r="step-1-content",a='<option value="" zip="">(select City)</option><option value="16" zip="98101">Seattle</option>  <option value="15" zip="M4X">Toronto</option>       <option value="2" zip="V6G">Vancouver</option>       <option value="-" zip="">-</option>        <option value="17" zip="V3X">Abbotsford-Langley</option>       <option value="29" zip="83701">Boise</option>      <option value="30" zip="T2B">Calgary</option>       <option value="22" zip="L1B">Durham Region</option>   <option value="27" zip="T5A">Edmonton</option>  <option value="21" zip="L0R">Hamilton-Oakville</option>       <option value="23" zip="V1P">Kelowna - Penticton</option>       <option value="21" zip="N2A">Kitchener-Waterloo</option>       <option value="32" zip="T1H">Lethbridge</option>       <option value="20" zip="N6A">London</option>       <option value="15" zip="L4T">Mississauga</option>       <option value="24" zip="K2C">Ottawa</option>        <option value="16" zip="98101">Seattle</option>    <option value="17" zip="V3X">Surrey-Delta</option>       <option value="15" zip="M4X">Toronto</option>       <option value="2" zip="V6G">Vancouver</option>       <option value="26" zip="V0S">Victoria</option>        <option value="25" zip="R1A">Winnipeg</option>        <option value="other">Don\'t see a location close to you?</option>',c='<p>Choose the closest location to where you\'d like us to deliver the Frogboxes and the closest location where you\'d like us to pick them up when you are done.</p>  <div class="row">      <div class="location col-sm-6">      <h4>Delivery Location:</h4>   <div id="delivery-city-wrapper">       <label for="delivery-city">Closest Frogbox Location:</label>   <div class="select-wrapper">      <select id="delivery-city" class="city" name="delivery-city" required>'+a+'  </select>   </div>   </div>    </div>   <div class="location col-sm-6">      <h4>Pick-up Location:</h4>   <div id="pickup-city-wrapper">       <label for="pickup-city">Closest Frogbox Location:</label>   <div class="select-wrapper">       <select id="pickup-city" class="city" name="pickup-city" required>'+a+'   </select>  </div>  </div>  </div>  </div> \x3c!-- .row --\x3e  <hr> <div class="row"><div class="col-sm-5 col-sm-offset-7"><button id="step-1-next" class="step-next next btn btn-default">Next</button></div></div>';return{getPriceListCode:function(e){return D(e)},getSuburb:function(e){return D(e)},load:function(){return e()},render:function(){var e='<div id="'+r+'" class="step-content">';return e+=c,e+="</div>"},updateItemsDone:function(){},validate:function(){return!0}}}(),Step2=function(){
// setBundleSize removes any quantities from the model on non-selected
// bundles, effectively deselecting items in other bundles
function i(e){if(null!=window.frogboxCart&&null!=window.frogboxCart.bundles&&0<window.frogboxCart.bundles.length)for(var i=0;i<window.frogboxCart.bundles.length;++i)window.frogboxCart.bundles[i].group!=e&&(window.frogboxCart.bundles[i].quantity=0)}
// formats a list of items
function t(e,i){if(null!=e){for(var t,r=Array(),o={},t=0;t<e.length;++t){var n=e[t],a=ff_functions.sanitizeItemName(n.name),c={},s=!1;if("custom-bundle"==n.group||!n.group){var l="";
// layout for custom bundles and individual products
"custom-bundle"==n.group&&(s=!0),c.col1='<img src="'+n.image+'">',c.col2='<h3 class="product-name" priceItemID='+n.priceItemID+">"+n.name+'</h3><p class="product-description-text">'+n.description+"</p>",c.col3='<h5>Price:</h5><div class="product-price">$'+parseFloat(n.value).toFixed(2)+' + Tax<br><span class="price-each">/ EACH</span></div>',c.col4='<label for="'+a+'"><h5>Quantity:</h5></label>',c.col4+='<div class="select-wrapper box-qty"><select key="'+n.key+'" name="'+a+'-quantity" id="'+a+'-quantity">',c.sequence=n.sequence;var d="",u=5;99<n.maximum_quantity&&(u=5);for(var p=0;p<=n.maximum_quantity;p+=u)d="",p==n.quantity&&(d=" SELECTED"),c.col4+='<option value="'+p+'"'+d+">"+p+"</option>";
// add to groups or to markup
if(c.col4+="</select></div>",s)
// initialize object;
null==o[n.group]&&(o[n.group]={}),
// initialize array;
null==o[n.group].rows?o[n.group].rows=Array(c):o[n.group].rows.push(c);else{var f={sequence:n.sequence,markup:ff_functions.formatItem(c)};r.push(f)}}}
// format groups
for(var v in o){var m=99,h="row group group-"+v,y="";if(null!=o[v].rows){i||(y+='<div class="col0">'+o[v].rows[0].groupImage+"</div>",y+='<div class="col1"><h3>'+o[v].rows[0].groupName+"</h3>",y+='<div class="product-description-text">'+o[v].rows[0].groupDescription+'</div></div><div class="option-reveal"><a class="show-options btn btn-info" data-target="group-'+v+'">View options</a></div>',m=o[v].rows[0].sequence);for(var b=0;b<o[v].rows.length;++b)
// add the row for each group
y+=ff_functions.formatItem(o[v].rows[b])}var f={sequence:m,markup:'<div class="'+h+'"><h3 class="step2-label">Number of boxes:</h3>'+y+"</div>"};r.push(f)}
// sort the items
r.sort(function(e,i){return parseInt(e.sequence)<parseInt(i.sequence)?-1:1});
// build a string and return
var g="";for(var b in r)g+=r[b].markup;return g}}function e(){
// when the user selects a bundle type, show that bundle type and hide the others
$("#bundles-wrapper input").change(function(){var e="group-"+$("input:radio[name='bundle']:checked").val()+"-bundle";$("#bundles-products-wrapper .group").addClass("group-hidden"),$("#bundles-products-wrapper ."+e).removeClass("group-hidden"),$("#bundles-wrapper input:radio[name='bundle']").closest(".inner-border").removeClass("active"),$("#bundles-wrapper input:radio[name='bundle']:checked").closest(".inner-border").addClass("active"),i(e.substring(6)),ff_functions.updateItems(),
// scroll to bundles
$(window).width()<768&&$("html, body").animate({scrollTop:$("#bundles-products-wrapper").offset().top-40},500)})}function r(){}var o="step-2-content",n='<p>Your price will be based on the number of Frogboxes you require and the amount of time you need the boxes. Prices start at one week, but keep the boxes for as long as you need (choose in Step 4).</p><div id="bundles-products-wrapper"><h3>Based on the size of your move, we’d suggest:</h3></div><span id="step-2-footnote">* Charges may apply for non-local delivery</span><hr><div class="row"><div class="col-sm-5"><div id="step-2-previous" class="step-previous previous btn btn-previous">Previous</div></div><div class="col-sm-5 col-sm-offset-2"><button id="step-2-next" class="step-next next btn btn-default">Next</button></div></div>';return{formatItems:function(e,i){return t(e,i)},load:function(){return e()},processBundles:function(){return!0},render:function(){var e='<div id="'+o+'" class="step-content">';return e+=n,e+="</div>"},validate:function(){},validated:function(){return!0}}}(),Step3=function(){
// formats a list of items
function t(e,i){if(null!=e){for(var t=Array(),r={},o=0;o<e.length;++o){var n=e[o],a=ff_functions.sanitizeItemName(n.name),c={},s=!1,l=a,d="Minimalist",u="Regular",p="Collector";if(-1!=l.indexOf(d))var f=d;else if(-1!=l.indexOf(u))var f=u;else if(-1!=l.indexOf(p))var f=p;
// set up the item so we can format it generically
if(null!=n.group&&-1<n.group.indexOf("bundle")&&"custom-bundle"!=n.group){
// the layout for regular bundles
s=!0;var v="";if(ff_functions.getQuantity(n.key)&&(v="CHECKED"),c.wrapper='<label for="'+a+'"><h4>'+f+" </h4>",c.col1='<input type="radio" name="bundle-choice" value="'+n.priceItemID+'" key="'+n.key+'" id="'+a+'"'+v+">",null!=n.description_details){c.col2="";for(var m=0;m<n.description_details.length;++m){var h=!1,y=!1;n.description_details[m].title&&void 0!==n.description_details[m].title&&(c.col2+='<div class="product-details"><h4>'+n.description_details[m].title+"</h4>",h=!0),n.description_details[m].dimensions&&void 0!==n.description_details[m].dimensions&&(c.col2+="<p>"+n.description_details[m].dimensions,y=!0),n.description_details[m].title&&void 0!==n.description_details[m].description&&(c.col2+="<br>"+n.description_details[m].description),y&&(c.col2+="</p>"),h&&(c.col2+="</div>")}}c.col3='<h5>Price:</h5><div class="product-price">$'+parseInt(n.value,10).toFixed(2)+" + Tax</div>",c.col4='<span class="product-value-message">'+n.value_message+"</span>",c.wrapperClose="</label>"}else{var b="";"custom-bundle"==n.group||null==n.group?(
// layout for custom bundles and individual products
"custom-bundle"==n.group&&(s=!0),c.col1='<img src="'+n.image+'">',c.col2='<h3 class="product-name" priceItemID='+n.priceItemID+">"+n.name+'</h3><p class="product-description-text">'+n.description+"</p>"):(s=!0,c.groupName=n.name,c.groupImage='<img src="'+n.image+'">',c.groupDescription=n.description,c.col1="",null!=n.title_display_option&&(c.col2="<h4>"+n.title_display_option+"</h4>")),c.col3='<h5>Price:</h5><div class="product-price">$'+parseFloat(n.value).toFixed(2)+' + Tax<br><span class="price-each">/ EACH</span></div>',c.col4='<label for="'+a+'"><h5>Quantity:</h5></label>',c.col4+='<div class="select-wrapper box-qty"><select key="'+n.key+'" name="'+a+'-quantity" id="'+a+'-quantity">',c.sequence=n.sequence;var g="",w=1;99<n.maximum_quantity&&(w=5);for(var k=0;k<=n.maximum_quantity;k+=w)g="",k==n.quantity&&(g=" SELECTED"),c.col4+='<option value="'+k+'"'+g+">"+k+"</option>";c.col4+="</select></div>"}
// add to groups or to markup
if(s)
// initialize object;
null==r[n.group]&&(r[n.group]={}),
// initialize array;
null==r[n.group].rows?r[n.group].rows=Array(c):r[n.group].rows.push(c);else{var $={sequence:n.sequence,markup:ff_functions.formatItem(c)};t.push($)}}
// format groups
var x=jQuery("input:radio[name='bundle']:checked").val()+"-bundle";for(var C in r){var P=99,S="row group group-"+C;i&&x!=C&&(S+=" group-hidden");var D="";if(null!=r[C].rows){i||(D+='<div class="col0">'+r[C].rows[0].groupImage+"</div>",D+='<div class="col1"><h3>'+r[C].rows[0].groupName+"</h3>",D+='<div class="product-description-text">'+r[C].rows[0].groupDescription+'</div></div><div class="option-reveal"><a class="show-options btn btn-info" data-target="group-'+C+'">View options</a></div>',P=r[C].rows[0].sequence);for(var m=0;m<r[C].rows.length;++m)
// add the row for each group
D+=ff_functions.formatItem(r[C].rows[m])}var $={sequence:P,markup:'<div class="'+S+'"><h3 class="step2-label">Number of boxes:</h3>'+D+"</div>"};t.push($)}
// sort the items
t.sort(function(e,i){return parseInt(e.sequence)<parseInt(i.sequence)?-1:1});
// build a string and return
var _="";for(var m in t)_+=t[m].markup;return _}}function e(){}function i(){}var r="step-3-content",o='<p>Optionally you can add packing supplies and other products that will be useful during your move.</p><div id="packing-supplies-wrapper"></div><hr><div class="row"> <div class="col-sm-5"> <div id="step-3-previous" class="step-previous previous btn btn-previous">Previous</div></div><div class="col-sm-5 col-sm-offset-2"><button id="step-3-next" class="step-next next btn btn-default">Next</button></div> </div> ';return{formatItems:function(e,i){return t(e,i)},load:function(){},render:function(){var e='<div id="'+r+'" class="step-content">';return e+=o,e+="</div>"},validate:function(){}}}(),Step4=function(){function e(){
// initialize datepickers
ff_functions.initializeDatepicker("pickup"),ff_functions.initializeDatepicker("delivery"),
// when a user enters a date, check that it is valid and download possible times
$(".booking-date").on("changeDate",function(){var e=$(this).val(),i=""+e.replace(/-/g,""),t=$(this).attr("id"),r=t.substring(0,t.length-5);
// clear errors
$("#"+t+"-error").remove();var o=window.availability[r];if(o&&o[i]){var n='<option value="">(select Time)</option>',a=o[i];for(var c in a)if(a.hasOwnProperty(c)){var s=a[c].routeID+"-"+a[c].startTime;n+='<option time="'+a[c].startTime+'" value="'+s+'">'+a[c].label+"</option>"}ff_functions.setTimeOptions(t,n)}else ff_functions.getSingleAvailability(t,e);"delivery-date"==t&!$("#pickup-date").val()&&(ff_functions.initializeDatepicker("pickup"),$("#pickup-date").trigger("changeDate")),
// a date changed, so check if we need to update the keepFor value and also the pricing
ff_functions.updateKeepFor()}),
/* Changes to postcode fields may change priceslists */
$(".postcode").change(function(){var e;"deliveryPostcode"==$(this).attr("id")?ff_functions.setDeliverySuburb($(this).val()):ff_functions.setPickupSuburb($(this).val())}),
// when the user checks the 'pickup same as delivery' box, copy the
// address and hide the fields. When the user unchecks it, unhide the
// fields
$("#pickupSame").change(function(){this.checked?($("#pickupStreet").val($("#deliveryStreet").val()),$("#pickupCity").val($("#deliveryCity").val()),$("#pickupProvince").val($("#deliveryProvince").val()),$("#pickupPostcode").val($("#deliveryPostcode").val()),$("#pickupDescription").val($("#deliveryDescription").val()),$("#pickupCountry").val($("#deliveryCountry").val()),$("#pickup-address-wrapper").addClass("group-hidden"),$("#pickupPostcode").change()):$("#pickup-address-wrapper").removeClass("group-hidden")})}var i="step-4-content",t='<div id="step-4-your-price">    <div class="row">    <div class="col-sm-6"><h4>Your Price and Pick-up Dates</h4>    <p>We understand that moves don\'t happen overnight, you can select a date that gives you enough time to get fully unpacked. The longer you keep the Frogboxes the cheaper their cost per week.</p>    </div>    <div id="step-4-current-order" class="col-sm-6"><h4>For your current order:</h4>    <ul id="current-order-price-by-duration"></ul>        </div>        </div>        </div>        <div class="row"><div id="delivery-date-wrapper" class="col-sm-6"><h4>Delivery Date and Time</h4><label for="delivery-date">Delivery Date:</label><input type="text" id="delivery-date" name="delivery-date" class="booking-date" required readonly><label for="delivery-time">Delivery Time:</label><div class="select-wrapper"><select id="delivery-time" class="booking-time" name="delivery-time" required></select>    </div></div><div id="pickup-date-wrapper" class="col-sm-6"><h4>Pick-up Date and Time</h4><label for="pickup-date">Pick-up Date:</label><input type="text" id="pickup-date" name="pickup-date" class="booking-date" required readonly><label for="pickup-time">Pick-up Time:</label><div class="select-wrapper"><select id="pickup-time" class="booking-time" name="pickup-time" required></select>    </div><div id="keep-for-wrapper">Keep for: <span id="keep-for-display">1 week</span><input type="hidden" id="keep-for" name="keep-for" value="7"></div></div>    </div>    <div class="row"><div id="delivery-address" class="col-sm-6">        <h4>Delivery Address</h4>    <label for="deliveryStreet">Address:</label><input type="text" id="deliveryStreet" name="deliveryStreet" required placeholder="Apartment or suite # - Street address"><label for="deliveryCity">City:</label><input type="text" id="deliveryCity" name="deliveryCity" required ><label for="deliveryProvince" id="delivery-province-label">Province:</label> <div class="select-wrapper">    <select id="deliveryProvince" name="deliveryProvince" class="province" required></select> </div>  <label for="deliveryPostcode" id="delivery-postcode-label">Postal Code:</label>  <input type="text" id="deliveryPostcode" class="postcode" name="deliveryPostcode" required>  <input type="hidden" id="deliveryCountry" name="deliveryCountry" >  <label for="deliveryDescription">Delivery Notes: <span class="field-optional">(optional)</span></label>      <textarea id="deliveryDescription" name="deliveryDescription" placeholder="ie. 2 flights of stairs, no elevator, loading bay in back alley, etc."></textarea>  <input type="checkbox" name="pickupSame" id="pickupSame" >  <label for="pickupSame">Same as delivery location?</label></div><div id="pick-up-address" class="col-sm-6">   <h4>Pick-up Address</h4> <div id="pickup-address-wrapper">     <label for="pickupStreet">Address:</label> <input type="text" id="pickupStreet" name="pickupStreet" required placeholder="Apartment or suite # - Street address"> <label for="pickupCity">City:</label> <input type="text" id="pickupCity" name="pickupCity" required >  <label for="pickupProvince">State/Province:</label>  <div class="select-wrapper">      <select id="pickupProvince" name="pickupProvince" class="province" required></select>  </div>  <label for="pickupPostcode">Zip/Postal Code:</label>  <input type="text" id="pickupPostcode" class="postcode" name="pickupPostcode" required >  <input type="hidden" id="pickupCountry" name="pickupCountry" >  <label for="pickupDescription">Pickup Notes: <span class="field-optional">(optional)</span></label>      <textarea id="pickupDescription" name="pickupDescription" placeholder="ie. 2 flights of stairs, no elevator, loading bay in back alley, etc."></textarea>   </div>    </div>    </div>    <hr>    <div class="row">  <div class="col-sm-5">        <div id="step-4-previous" class="step-previous previous btn btn-previous">Previous</div> </div> <div class="col-sm-5 col-sm-offset-2"> <button id="step-4-next" class="step-next next btn btn-default">Next</button>   </div>  </div> ';return{load:function(){return e()},render:function(){var e='<div id="'+i+'" class="step-content">';return e+=t,e+="</div>"},validate:function(){}}}(),Step5=function(){function e(){
// hide/show delivery and pickup contact areas
$("input[name=delivery-contact]").change(function(){"different delivery contact"==$("input[name=delivery-contact]:checked").val()?$("#delivery-different-contact-wrapper").removeClass("group-hidden"):$("#delivery-different-contact-wrapper").addClass("group-hidden")}),$("input[name=pickup-contact]").change(function(){"different pickup contact"==$("input[name=pickup-contact]:checked").val()?$("#pickup-different-contact-wrapper").removeClass("group-hidden"):$("#pickup-different-contact-wrapper").addClass("group-hidden")})}function i(){}var t="step-5-content",r='<div id="contact-details" class="row"><h4>Contact Details</h4><div class="col-sm-6"><label for="contact-first">First Name:</label><input type="text" id="contact-first" name="contact-first" required></div><div class="col-sm-6"><label for="contact-last">Last Name:</label><input type="text" id="contact-last" name="contact-last" required></div><div class="col-sm-6"><label for="contact-primary-phone">Primary Phone:</label><input type="text" id="contact-primary-phone" name="contact-primary-phone" required></div><div class="col-sm-6"><label for="contact-primary-ext">ext: <span class="field-optional">(optional)</span></label><input type="text" id="contact-primary-ext" name="contact-primary-ext"></div><div class="col-sm-6"><label for="contact-alternate-phone">Alternate Phone: <span class="field-optional">(optional)</span></label><input type="text" id="contact-alternate-phone" name="contact-alternate-phone"></div><div class="col-sm-6"><label for="contact-primary-ext">ext: <span class="field-optional">(optional)</span></label><input type="text" id="contact-alternate-ext" name="contact-alternate-ext"></div><div class="col-sm-6"><label for="contact-email">Email:</label><input type="email" id="contact-email" name="contact-email" required></div></div><div class="row"><div id="delivery-contact-wrapper" class="step-5-contact col-sm-6"><h4>Delivery Contact</h4><input type="radio" name="delivery-contact" value="same as main contact" id="delivery-contact-main"><label for="delivery-contact-main">Same as Main Contact</label><input type="radio" name="delivery-contact" value="different delivery contact" id="delivery-contact-different"><label for="delivery-contact-different">Different delivery contact</label><div id="delivery-different-contact-wrapper" class="group-hidden"><label for="delivery-contact-first">First Name:</label><input type="text" name="delivery-contact-first" id="delivery-contact-first" ><label for="delivery-contact-last">Last Name:</label><input type="text" name="delivery-contact-last" id="delivery-contact-last" ></div></div><div id="pickup-contact-wrapper" class="step-5-contact col-sm-6"><h4>Pick-up Contact</h4><input type="radio" name="pickup-contact" value="same as main contact" id="pickup-contact-main"><label for="pickup-contact-main">Same as Main Contact</label><input type="radio" name="pickup-contact" value="different pickup contact" id="pickup-contact-different"><label for="pickup-contact-different">Different pick-up contact</label><div id="pickup-different-contact-wrapper" class="group-hidden"><label for="pickup-contact-first">First Name:</label><input type="text" name="pickup-contact-first" id="pickup-contact-first"><label for="pickup-contact-last">Last Name:</label><input type="text" name="pickup-contact-last" id="pickup-contact-last"></div></div></div><div id="payment-wrapper">   <h4>Payment</h4>        <p>We will collect payment at the same time we deliver the Frogboxes.</p> </div>   <div id="mover-recommendations-wrapper">       <h4>Mover Recommendations</h4>   <p>Looking for a mover you can trust? Check this box and we\'ll send you contact info for credible movers in your area   <input type="checkbox" value="1" name="mover-recommendations">   </p>   </div>   <div id="confirm-wrapper">       <h4>Confirm and Place Order</h4>   <p>Please <span class="ordering-emphasis">review your order</span> and contact details to confirm that everything is entered correctly. You can click on any step to edit the information on that step.</p>   <p>When you are ready to place your order, click <span class="ordering-emphasis">Place Order</span> below.</p>   </div>  <div> <button type="button" data-toggle="modal" data-target="#terms-conditions">Show Terms of Service</button> <label for="terms">I agree with the terms of service: </label>  <input type="checkbox" value="1" name="terms" id="terms" required>  </div>  <hr> <div class="row">      <div class="col-sm-5">      <div id="step-5-previous" class="step-previous previous btn btn-previous">Previous</div>      </div>      <div class="col-sm-5 col-sm-offset-2"> <button id="step-5-next" class="step-next next btn btn-default" name="_ff_submit" value="submit">Place Order</button></div></div>';return{load:function(){return e()},render:function(){var e='<div id="'+t+'" class="step-content">';return e+=r,e+="</div>"},validate:function(){}}}(),Validator={validate:function(){
/**
             *  Validation - move this to individual modules if we can
             */
$.validator.addMethod("greaterThan",function(e,i,t){return/Invalid|NaN/.test(new Date(e))?isNaN(e)&&isNaN($(t).val())||Number(e)>Number($(t).val()):new Date(e)>new Date($(t).val())},"Please enter a date after {0}."),
// validate that the date is after today
$.validator.addMethod("afterToday",function(e){return/Invalid|NaN/.test(new Date(e))?isNaN(e)||Number(e)>Number($(params).val()):new Date(e)>new Date},"Please enter a date after today. "),
// validate that a postal code matches a province or state
$.validator.addMethod("matchProvince",function(e,i){var t="d"==jQuery(i).attr("id").substr(0,1)?"delivery":"pickup",r=jQuery("#"+t+"Country").val(),o;return"Canada"==r?o=ff_functions.lookupProvince(e):"United States"==r&&(o=ff_functions.lookupState(e)),jQuery("#"+t+"Province").val()===o},"Please enter a valid zip/postal code for this state/province"),
// validate that a postal code matches Ontario
$.validator.addMethod("provinceMatchesOntario",function(e,i){var t="d"==jQuery(i).attr("id").substr(0,1)?"delivery":"pickup",r;return"Ontario"==jQuery("#"+t+"Province").val()},"Please enter an address in Ontario."),
// validate that a postal code field doesn't have a related error
$.validator.addMethod("noRelatedError",function(e,i){var t=jQuery(i).attr("id");return!jQuery('label[for="'+t+'"] .error').length}),
// initialize validation
$("#frogbox-form").validate({errorPlacement:function(e,i){e.appendTo('label[for="'+i.attr("id")+'"]')},rules:{
// province must match postal code
deliveryPostcode:{required:!0,matchProvince:"#deliveryProvince",noRelatedError:!0},pickupPostcode:{required:!0,matchProvince:"#pickupProvince",noRelatedError:!0},
// phone
"contact-primary-phone":{required:!0,phoneUS:!0},"contact-alternate-phone":{phoneUS:!0},
// delivery contact fields if 'different delivery contact' is checked
"delivery-contact-first":{required:"#delivery-contact-different:checked"},"delivery-contact-last":{required:"#delivery-contact-different:checked"},
// pickup contact fields if 'different pickup contact' is checked
"pickup-contact-first":{required:"#pickup-contact-different:checked"},"pickup-contact-last":{required:"#pickup-contact-different:checked"},
// date validation
"delivery-date":{afterToday:!0},"pickup-date":{greaterThan:"#delivery-date"},
// terms and conditions
terms:{required:!0}},messages:{"contact-alternate-phone":{phoneUS:"Enter a valid phone number. "},"contact-primary-phone":{phoneUS:"Enter a valid phone number. "},terms:{required:"Check the checkbox to agree to our Terms of Service. "}},ignore:[],submitHandler:function(e){window.orderFormSubmitting=!0,$("#step-5-next").hide();var i=ff_functions.addThrobber("step-5-next");$(i).append("<div>Thank you! Please wait while we process your order.</div>");
// append cart to order
var t=$("<input>").attr("type","hidden").attr("name","frogboxCart").val(JSON.stringify(window.frogboxCart));
// append page data to order
if($("#frogbox-form").append($(t)),pageData){var r=$("<input>").attr("type","hidden").attr("name","pageTitle").val(pageData.title),o=$("<input>").attr("type","hidden").attr("name","pageUrl").val(pageData.url);$("#frogbox-form").append($(r)).append($(o))}e.submit()}})}},Accordion=function(){function a(e){var i="#step-"+e+"-heading";
// recurse to the next step if we can't find the step
if(!$(i).length&&e<5)return a(e+1);setTimeout(function(){var e=$(i).offset();e&&$("body, html").animate({scrollTop:e.top-80})},310)}return{accordion:function(){
/**
             *  Initialize accordion.
             */
$("#accordion").accordion({heightStyle:"content",collapsible:!0,autoHeight:!1,active:0,animate:!1,activate:function(e,i){
// if we are going into step 5 and the promo code field is already filled out, validate it
var t=$("#accordion").accordion("option","active");4==t&&$("#promo-code").val().length&&$("#promo-code").change(),1<t&&(window.availability||ff_functions.initializeAvailability())},beforeActivate:function(e,i){
// return true if we are going backwards
if(null!==i.newHeader&&null!==i.oldHeader&&void 0!==i.newHeader.attr("id")&&void 0!==i.oldHeader.attr("id")&&i.newHeader.attr("id").substring(5,6)<i.oldHeader.attr("id").substring(5,6))return!0;
// return false if there is a throbber active in step 1
var t=$("#accordion").accordion("option","active");if($("#step-1 .throbber-loader").length&&0==t)return!1;var r=!0,o=$("#frogbox-form").validate(),n;return $(".step-content:eq( "+$("#accordion").accordion("option","active")+" )").find("input, select").each(function(e,i){try{var t;o.element("#"+$(i).attr("id"))||(r=!1)}catch(e){"bundle-choice"!=$(i).attr("name")&&console.log(i,e)}}),!(!Step2.validated()||!r)||(a($("#accordion").accordion("option","active")),!1)}}),$("#accordion button.step-next, #accordion div.step-previous").not("#step-5-next").click(function(e){e.preventDefault();var i=$(this).is(".next")?1:-1,t=$("#accordion").accordion("option","active")+i;$("#accordion").accordion("option","active",t),a(t),ff_functions.generateSummary()})},scrollToStep:function(e){a(e)}}}();// @codekit-prepend "ff_functions.js"
// @codekit-prepend "components/commercial-order/step1.js"
// @codekit-prepend "components/commercial-order/step2.js"
// @codekit-prepend "components/step3.js"
// @codekit-prepend "components/step4.js"
// @codekit-prepend "components/commercial-order/step5.js"
// @codekit-prepend "components/validate.js"
// @codekit-prepend "components/accordion.js"
// define these variables in the global scope
frogboxCart=null,disabledDates={delivery:[],pickup:[]},pendingRequests={},$(document).ready(function(){
/**
     * Load the code for our steps and render it.
     *
     * Markup should be rendered before any event listeners are created in .load().
     */
for(var e=new Array(Step1,Step2,Step3,Step4,Step5),i=0;i<e.length;++i)$("#step-"+(i+1)).append(e[i].render()),e[i].load();Validator.validate();
/**
     * Load the listeners for each step.
     */
for(var i=0;i<e.length;++i)e[i].load();
/**
     * Look for a submission object. If it exists, set form fields based on submission.
     */if(window.Submission){for(var t in
// set the page title if there was an error
document.title=pageData.title,$(".page-header h1").html(pageData.title),frogboxCart=Submission.frogboxCart,delete Submission.frogboxCart,Submission)Submission.hasOwnProperty(t)&&"terms"!=t&&"quantity"!=t.substr(t.length-8)&&$("*[name="+t+"]").not('*[type="radio"]').val(Submission[t]);
// initialize availability
ff_functions.initializeAvailability(),
// load pricelists by calling .change() method on .city elements
$(".city").change(),
// load dates by triggering changeDate event on .booking-date elements
$(".booking-date").trigger("changeDate"),
// validate promo if it exists by calling .change() method
$("#promo-code").val().length&&$("#promo-code").change(),
// choose the bundle in step 2 and call the change event
$("#bundle-"+Submission.bundle).prop("checked",!0).change()}
/**
     * Display errors, if any.
     */if(window.Errors)
// display the errors
for(var r=0;r<window.Errors.length;r++){var o='<p class="error">'+window.Errors[r].errMsg+"</p>";$("#frogbox-form").prepend(o),$("#"+window.Errors[r].element).length&&$(o).insertBefore("#"+window.Errors[r].element)}
/**
     * Set up validation
     */for(var i=0;i<e.length;++i)e[i].validate();
/**
     * Accordion runs after validation
     */
/**
     * Open the accordion to where the first error is, if any.
     */
if(Accordion.accordion(),window.Errors){var n=Errors[0],a=$("#"+Errors[0].element).closest(".step-content").attr("id")[5]-1;3<a&&(// select new dates in step 4
a=3),$("#accordion").accordion("option","active",a),Accordion.scrollToStep(a)}});